#!/usr/bin/env python3

import os
os.environ["MKL_THREADING_LAYER"] = "GNU"
import argparse
import fbu
from fbu import Regularization
import ROOT
import math
import random
import numpy as np
from array import array
import scipy.stats as stats
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed, TMath, TF1, TLine, kBird, TGaxis
import pandas as pn
import time

parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")
parser.add_argument('--rfile_data','-rfile_data', type=str, default="input.root")
parser.add_argument('--rfile_particle','-rfile_particle', type=str, default="input.root")
parser.add_argument('--rfile_matrix','-rfile_matrix', type=str, default="input.root")
parser.add_argument('--rfile_background','-rfile_background', type=str, default="input.root")
parser.add_argument('--h_data','-h_data', type=str, default="h_data")
parser.add_argument('--h_particle','-h_particle', type=str, default="h_particle")
parser.add_argument('--h_matrix','-h_matrix', type=str, default="h_matrix")
parser.add_argument('--h_background','-h_background', type=str, default="h_background")

parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{t,had} [GeV]")
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)
parser.add_argument('--SplitFromBinLow', '-SplitFromBinLow', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinLow', '-ParameterSplitFromBinLow', type=float, default=1.5)
parser.add_argument('--SplitFromBinHigh', '-SplitFromBinHigh', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinHigh', '-ParameterSplitFromBinHigh', type=float, default=1.5)
parser.add_argument('--maxiterations', '-maxiterations', type=int, default=30)
parser.add_argument('--batch', '-batch', type=int, default=0)
parser.add_argument('--sampling', '-sampling', type=int, default=1000)
parser.add_argument('--plotlike', '-plotlike', type=int, default=0)
parser.add_argument('--nrebinpost', '-nrebinpost', type=int, default=1)
parser.add_argument('--counts', '-counts', type=int, default=10000)


args = parser.parse_args()

if (args.batch == 1):
    ROOT.gROOT.SetBatch(1)

def PrintCan(can, outputname):
    outputname = outputname.split('/')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.png')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.pdf')

def MakeListFromHisto(hist):
    vals = []
    for i in range(1, hist.GetXaxis().GetNbins()+1):
        val = hist.GetBinContent(i)
        vals.append(val)
    return vals

def NormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetXaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetYaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetYaxis().GetNbins()+1):
                migra.SetBinContent(i,j,migra.GetBinContent(i,j) / sum)
    return migra

def CheckNormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(j,i)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetXaxis().GetNbins()+1):
                migra.SetBinContent(j,j,migra.GetBinContent(j,i) / sum)
    return migra

def MakeListResponse(h2):
    vals = []
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        column = []
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            column.append(val)
        vals.append(column)
    return vals

def TransposeMatrix(h_response_unf):
    h_responce_transpose = h_response_unf.Clone(h_response_unf.GetName()+"clone")
    h_responce_transpose.Reset()
    for i in range(1,h_response_unf.GetXaxis().GetNbins()+1):
        for j in range(1,h_response_unf.GetXaxis().GetNbins()+1):
            h_responce_transpose.SetBinContent(i,j,h_response_unf.GetBinContent(j,i))
            h_responce_transpose.SetBinError(i,j,h_response_unf.GetBinError(j,i))
    h_responce_transpose.GetXaxis().SetTitle(h_response_unf.GetYaxis().GetTitle())
    h_responce_transpose.GetYaxis().SetTitle(h_response_unf.GetXaxis().GetTitle())
    return h_responce_transpose
    
def SaveHistograms(outputname = "output"):
    outputname = outputname.split('/')
    outfile = TFile('./outputs/'+outputname[len(outputname)-1]+".root", 'recreate')
    outfile.cd()
    for j in range(len(histograms)):
        histograms[j].Write()
    print("End of the unfolding.")
    outfile.Write()
    outfile.Close() 

def Plot2D(ListOf2DPosteriors):
    c = TCanvas("Posteriors2D"+outputname,"Posteriors2D"+outputname,0,0,1600, 1600)
    c.Divide(math.ceil(pow(len(ListOf2DPosteriors),0.5)),math.ceil(pow(len(ListOf2DPosteriors),0.5)))
    for i in range(len(ListOf2DPosteriors)):
        c.cd(i+1)
        ListOf2DPosteriors[i].GetZaxis().SetRangeUser(0,1.0)
        ListOf2DPosteriors[i].Draw("colz")
    gApplication.Run()

def PlotPosteriors(ListOfPosteriors, unfolded, h_ptcl_or, ListOfDensities, LogLike, outputname = "", fit = 0):
    RepeatIteration = False
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.5),int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.0))

    index = 1
    ArrayOfAxis = []

    for i in range(len(ListOfDensities)):
        c.cd(i+1)
        ListOfPosteriors[i].SetLineColor(2)
        if fit ==1:
            ListOfPosteriors[i].Fit("gaus")    
        ListOfPosteriors[i].Draw("hist")
        padT = TPad("padT"+str(i),"padT"+str(i),0,0,1,1)
        padT.SetFillStyle(4000)
        xmin = ListOfDensities[i].GetXaxis().GetXmin() 
        ymin = ListOfDensities[i].GetMinimum()
        xmax = ListOfDensities[i].GetXaxis().GetXmax()
        ymax = 900
        dx = (xmax-xmin)/0.8
        dy = (ymax-ymin)/0.8
        print("Now: ", xmin,xmax,ymin,ymax)
        padT.Range(xmin-0.1*dx,ymin-0.1*dy,xmax+0.1*dx,ymax+0.1*dy)
        padT.Draw()
        padT.cd()
        ListOfDensities[i].SetLineColor(6)
        ListOfDensities[i].SetLineStyle(2)
        ListOfDensities[i].SetMarkerColor(6)
        print("*******************",ListOfDensities[i].GetXaxis().GetNbins())
        ListOfDensities[i].Draw("][sames e0 hist")
        axis = TGaxis(xmax,ymin,xmax,ymax,ymin,ymax,50510,"+L")
        axis.SetName("right axis "+str(i))
        axis.SetLabelColor(6)
        ArrayOfAxis.append(axis)
        ArrayOfAxis[i].Draw()
        padT.Update()
        c.cd(i+1)
        ListOfPosteriors[i].Draw("hist same")
        index = index + 1 

    c.cd(index)
    LogLike.Draw()
    index = index + 1        

    c.cd(1)
    #gStyle.SetOptStat(0)
    gPad.Modified()

    c.cd(index)
    pad1 = TPad("pad1","pad1",0,0.40,1,1)
    pad1.SetTopMargin(0.15)
    pad1.SetBottomMargin(0.01)
    pad1.SetFillStyle(0)
    pad1.SetTicks(1,1)
    pad1.SetBorderMode(0)
    pad1.Draw()
    c.cd(index)
    pad2 = TPad("pad2","pad2",0,0.01,1,0.422)
    pad2.SetFillStyle(0)
    pad2.SetTopMargin(0.043)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks(1,1)
    pad2.Draw()
    pad2.Modified()
    c.cd(index)
    pad1.cd()
    #gStyle.SetOptStat(0)
    h_ptcl_or.SetTitle("")
    h_ptcl_or.SetLineColor(2)
    h_ptcl_or.GetYaxis().SetTitleOffset(1.5)
    h_ptcl_or.GetYaxis().SetTitle("Events")
    legend = TLegend(0.55,0.5,0.85,0.8)
    legend.SetFillStyle(0)
    legend.AddEntry(h_ptcl_or,"Particle")
    legend.AddEntry(unfolded,"FBU Baron imp.","p")
    legend.SetBorderSize(0)
    unfolded.SetLineColor(1)
    unfolded.SetMarkerColor(1)
    unfolded.SetMarkerStyle(22)
    unfolded.Draw("same p x0")
    h_ptcl_or.Draw("hist same")
    legend.Draw("same")
    pad1.RedrawAxis()
    pad2.cd()
    h_ptcl_or_clone = h_ptcl_or.Clone(h_ptcl_or.GetName()+"_clone") 
    unfolded_clone = unfolded.Clone(unfolded.GetName()+"_clone")
    h_ptcl_or_clone.Divide(h_ptcl_or) 
    unfolded_clone.Divide(h_ptcl_or) 
    
    h_ptcl_or_clone.SetMaximum(1.3)
    h_ptcl_or_clone.SetMinimum(0.7)
    
    h_ptcl_or_clone.GetXaxis().SetTitleOffset(2.5)
    h_ptcl_or_clone.GetXaxis().SetTitle(args.title)
    
    h_ptcl_or_clone.GetYaxis().SetTitle("Ratio      ")
    h_ptcl_or_clone.Draw("hist")
    unfolded_clone.Draw("same p x0")
    pad2.RedrawAxis()
    histograms.append(c) 
    PrintCan(c,c.GetName()+"_posteriors")
    return RepeatIteration

def PlotRatio(h_reco_unfolded, h_ptcl_or, Name = ""):
    
    gStyle.SetPadLeftMargin(0.12)
    gStyle.SetPadRightMargin(0.12)
    c = TCanvas("canvas_ratio"+Name,"canvas_ratio"+Name,0,0,800, 800)
    c.cd()
    pad1 = TPad("pad1"+Name,"pad1"+Name,0,0.40,1,1)
    pad1.SetTopMargin(0.15)
    pad1.SetBottomMargin(0.01)
    pad1.SetFillStyle(0)
    pad1.SetTicks(1,1)
    pad1.SetBorderMode(0)
    pad1.Draw()
    c.cd()
    pad2 = TPad("pad2"+Name,"pad2"+Name,0,0.01,1,0.422)
    pad2.SetFillStyle(0)
    pad2.SetTopMargin(0.043)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks(1,1)
    pad2.Draw()
    pad2.Modified()
    c.cd()
    pad1.cd()
    gStyle.SetOptStat(0)
    h_ptcl_or.SetTitle("")
    h_ptcl_or.SetLineColor(2)
    h_ptcl_or.GetXaxis().SetTitleSize(34)
    h_ptcl_or.GetXaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleSize(27)
    h_ptcl_or.GetYaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleOffset(1.5)
    h_ptcl_or.GetYaxis().SetTitle("Events")
    h_ptcl_or.GetYaxis().SetLabelFont(43)
    h_ptcl_or.GetYaxis().SetLabelSize(25)
    legend = TLegend(0.55,0.5,0.85,0.8)
    legend.SetFillStyle(0)
    legend.AddEntry(h_ptcl_or,"Particle")
    legend.AddEntry(h_reco_unfolded,"FBU Baron imp.","p")
    legend.SetBorderSize(0)
    h_reco_unfolded.SetLineColor(1)
    h_reco_unfolded.SetMarkerColor(1)
    h_reco_unfolded.SetMarkerStyle(22)
    h_reco_unfolded.Draw("same p x0")
    h_ptcl_or.Draw("hist same")
    legend.Draw("same")
    pad1.RedrawAxis()
    pad2.cd()
    h_ptcl_or_clone = h_ptcl_or.Clone(h_ptcl_or.GetName()+"_clone"+Name) 
    h_reco_unfolded_clone = h_reco_unfolded.Clone(h_reco_unfolded.GetName()+"_clone"+Name)
    h_ptcl_or_clone.Divide(h_ptcl_or) 
    h_reco_unfolded_clone.Divide(h_ptcl_or) 
    
    h_ptcl_or_clone.GetXaxis().SetTitleSize(27)
    h_ptcl_or_clone.GetXaxis().SetTitleFont(43)
    h_ptcl_or_clone.GetYaxis().SetTitleSize(27)
    h_ptcl_or_clone.GetYaxis().SetTitleFont(43)
    
    h_ptcl_or_clone.GetXaxis().SetLabelFont(43)
    h_ptcl_or_clone.GetXaxis().SetLabelSize(25)
    h_ptcl_or_clone.GetYaxis().SetLabelFont(43)
    h_ptcl_or_clone.GetYaxis().SetLabelSize(25)
    
    h_ptcl_or_clone.SetMaximum(1.3)
    h_ptcl_or_clone.SetMinimum(0.7)
    
    h_ptcl_or_clone.GetXaxis().SetTitleOffset(2.5)
    h_ptcl_or_clone.GetXaxis().SetTitle(args.title)
    
    h_ptcl_or_clone.GetYaxis().SetTitle("Ratio      ")
    h_ptcl_or_clone.Draw("hist")
    h_reco_unfolded_clone.Draw("same p x0")
    pad2.RedrawAxis()
    c.Update()
    histograms.append(c) # here is the crash probably
    PrintCan(c, "ratio"+Name)

#def CalculateLikelihood(T_random,M,D):
#    #T_random = np.array(D).dot(M)                                       
#    R = np.array(T_random).dot(M)                                       # CREATING RECO
#    D_int64 = D.astype(np.int64)
#    R_int64 = R.astype(np.int64)
#    LikeTest = stats.poisson.pmf(D_int64, R_int64)
#    return np.prod(LikeTest)
def CreateRandomGaus(Dim):
    r = []
    for i in range(Dim):
        r.append(np.random.normal())
    return r

def Leapfrog(Theta, r, eps):
    Grad = []
    for j in range(len(Theta)):
        Grad.append(DerivationI(Theta, M, D, i=j)) # step of derivation is one
    rW = r + (eps/2)*

def Hamilton(Theta, eps, L, Like, Bins):    
    for m in range(1,Bins+1):
        r0 = CreateRandomGaus(Bins)
        ThetaM = Theta
        ThetaW = Theta
        rW = r0
        for i in range(1,L+1):
            ThetaW, rW = Leapfrog(ThetaW, rW, eps)
        
        print(m)
def DerivationI(T_random, M,D, i = 0, step = 1):
    T_random_plus = T_random.copy()
    T_random_plus[i] = T_random_plus[i] + step
    T_random_minus = T_random.copy()
    T_random_minus[i] = T_random_minus[i] - step
    #print("-----------------PLUS", T_random_plus,i,step)
    lhplus = CalculateLikelihood(T_random_plus, M, D)
    #print("----------------MINUS", T_random_minus,i,step)
    lhminus = CalculateLikelihood(T_random_minus, M, D)
    if (lhplus != False ) and (lhminus != False ):
        derivation = (lhplus - lhminus)/(2*step)
        #print ("plus, minus, derivation ", lhplus, lhminus, derivation) 
        return derivation
    else:
        return float('nan')

def CalculateLikelihood(T_random,M,D):
    R = np.array(T_random).dot(M)                                       # CREATING RECO
    lh = np.log(stats.norm(R, np.sqrt(R)).pdf(D))
    #print("LH: ", lh)
    myBool = True
    for i in range(len(lh)):
        if ((lh[i] == float('nan')) or (lh[i] == float('-inf')) or (lh[i] == float('inf'))):
            #print("Returning False")
            myBool = False
    if (myBool == True):
        return lh.sum()
    else:
        return False

def FindNewPseudo(Temp_T_random, M, D, ListOfColumns, lower, upper):
    Temp_T_random_orig = Temp_T_random
    Temp_T_random_No_like = Temp_T_random
    #print("Test: ", Temp_T_random_No_like)
    ListOfSuperColumns = ListOfColumns.copy()
    ListOfSuperColumns.append('int')
    FindBestLike = pn.DataFrame(columns=ListOfSuperColumns)
    NewDictOfPseudos = {}
    CheckList = []
    for j in range(len(Temp_T_random)):
        Check = False
        Steps = np.arange(Temp_T_random_orig[j]-(upper[j]-lower[j])*0.2,Temp_T_random_orig[j]+(upper[j]-lower[j])*0.2,(upper[j]-lower[j])/args.sampling)
        for PercentStep in Steps:
            Temp_T_random_No_like[j] = PercentStep
            NewLikelihood = CalculateLikelihood(Temp_T_random_No_like, M, D)
            for l in range(len(D)):
                    NewDictOfPseudos['True_bin_'+str(l)] = Temp_T_random_No_like[l]
            NewDictOfPseudos['int'] = [j]
            NewDictOfPseudos['LogLikelihood'] = [NewLikelihood]
            TempPseudoExp = pn.DataFrame.from_dict(NewDictOfPseudos) 
            if (NewLikelihood != False):
                FindBestLike = pn.concat([FindBestLike, TempPseudoExp], ignore_index=True, sort = True)
                Check = True
            Temp_T_random_No_like = Temp_T_random_orig
        CheckList.append(Check)

    return FindBestLike, CheckList

def FindNewPseudo2(Temp_T_random, M, D, ListOfColumns, lower, upper):
    Temp_T_random_orig = Temp_T_random
    Temp_T_random_No_like = Temp_T_random
    #print("Test: ", Temp_T_random_No_like)
    ListOfSuperColumns = ListOfColumns.copy()
    ListOfSuperColumns.append('int')
    FindBestLike = pn.DataFrame(columns=ListOfSuperColumns)
    NewDictOfPseudos = {}
    CheckList = []
    for j in range(len(Temp_T_random)):
        mystep = 1
        #Derivation = DerivationI(Temp_T_random_No_like, M, D, j, step=mystep)
        BreakIter = 1
        while True:
            Derivation = DerivationI(Temp_T_random_No_like, M, D, j, step=mystep)
            if (Derivation > 0.0):
                Temp_T_random_No_like[j] = Temp_T_random_No_like[j] + mystep
            if (Derivation < 0.0):
                Temp_T_random_No_like[j] = Temp_T_random_No_like[j] - mystep
            if ((abs(Derivation) < 0.001) or (BreakIter > 1000) or math.isnan(Derivation)==True):
                break
            print("BreakIter, derivation", BreakIter, Derivation)   
            BreakIter = BreakIter + 1
        Check = False
        Steps = np.arange(Temp_T_random_orig[j]-(upper[j]-lower[j])*0.2,Temp_T_random_orig[j]+(upper[j]-lower[j])*0.2,(upper[j]-lower[j])/args.sampling)
        for PercentStep in Steps:
            Temp_T_random_No_like[j] = PercentStep
            NewLikelihood = CalculateLikelihood(Temp_T_random_No_like, M, D)
            for l in range(len(D)):
                    NewDictOfPseudos['True_bin_'+str(l)] = Temp_T_random_No_like[l]
            NewDictOfPseudos['int'] = [j]
            NewDictOfPseudos['LogLikelihood'] = [NewLikelihood]
            TempPseudoExp = pn.DataFrame.from_dict(NewDictOfPseudos) 
            if (NewLikelihood != False):
                FindBestLike = pn.concat([FindBestLike, TempPseudoExp], ignore_index=True, sort = True)
                Check = True
            Temp_T_random_No_like = Temp_T_random_orig
        CheckList.append(Check)

    return FindBestLike, CheckList

def MyBaronRooUnfold(h_reco_get,h_ptcl_get,h_response_unf,h_reco_get_bkg,matrix_name=args.h_matrix, h_reco_getG0_name=args.h_data, h_ptcl_getG0_name = args.h_particle,h_reco_get_bkg_name = args.h_background,outputname=args.h_data+"_unfolded",nrebin = args.nrebin, sampling = 1000, maxiteration = 30):

    h_response_unf.ClearUnderflowAndOverflow()
    h_response_unf.GetXaxis().SetRange(1, h_response_unf.GetXaxis().GetNbins() )
    h_response_unf.GetYaxis().SetRange(1, h_response_unf.GetYaxis().GetNbins() )
    h_response_unf.SetName("Migration_Matrix_simulation")

    ########### ACCEPTANCY
    h_acc = h_response_unf.ProjectionX("reco_recoandparticleX") # Reco M
    h_acc.Divide(h_reco_get)
    ########### AKCEPTANCE saved in h_acc #############
    ########### EFFICIENCY
    h_eff = h_response_unf.ProjectionY("reco_recoandparticleY") # Ptcl M
    h_eff.Divide(h_ptcl_get)

    h_reco_get_input_clone=h_reco_get_input.Clone("")
    #h_reco_get_input_clone.Add(h_reco_get_bkg,-1)
    h_reco_get_input_clone.Multiply(h_acc)
   
    h_ptcl_or = rfile_particle.Get(h_ptcl_getG0_name)
    h_ptcl_or.SetMaximum(h_ptcl_or.GetMaximum()*1.5)
    
    h_response_unf_fbu = TransposeMatrix(h_response_unf)
    h_response_unf_fbu_norm = NormalizeResponse(h_response_unf_fbu)
    h_response_unf_fbu_norm.SetName("Migration_Matrix_simulation_transpose")
    histograms.append(h_response_unf_fbu_norm)
    M = np.array(MakeListResponse(h_response_unf_fbu_norm))
    D = np.array(MakeListFromHisto(h_reco_get_input_clone)) 
    B = np.array(MakeListFromHisto(h_reco_get_bkg))

    lower=[]
    upper=[]
    OldLower = []
    OldUpper = []

    h_det_div_ptcl=h_reco_get_input_clone.Clone("")
    h_det_div_ptcl.Divide(h_ptcl_or)
    h_det_div_ptcl.Divide(h_eff)
    h_det_div_ptcl.SetName("det_div_ptcl")
    
    ListOfColumns = []

    for l in range(len(D)):
        lower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-args.par)*h_det_div_ptcl.GetBinContent(l+1))
        upper.append(h_reco_get_input_clone.GetBinContent(l+1)*args.par*h_det_div_ptcl.GetBinContent(l+1)) 
        OldLower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-args.par)*h_det_div_ptcl.GetBinContent(l+1))
        OldUpper.append(h_reco_get_input_clone.GetBinContent(l+1)*args.par*h_det_div_ptcl.GetBinContent(l+1)) 
        ListOfColumns.append('True_bin_'+str(l))

    ListOfColumns.append('LogLikelihood')
    ListOfSuperColumns = ListOfColumns.copy()
    ListOfSuperColumns.append('int')
    print(ListOfSuperColumns)
    
    IterNumber = 1
    counts = 0

    SuperPseudoExp = pn.DataFrame(columns=ListOfSuperColumns)
    while counts < args.counts:
        PseudoExp = pn.DataFrame(columns=ListOfSuperColumns)
        print("Runnig iteration number: ",IterNumber)
        #if IterNumber == 1:
        ListOfPosteriors = []
        for s in range(len(lower)):
            print("Space check: ",lower[s], upper[s])
        Binning = []
        ListOfDensities = []
        for k in range(len(D)):
            help_array = np.arange(lower[k],upper[k],(upper[k]-lower[k])/100000) # here improvement
            Binning.append(help_array)
            x = array("d",Binning[k])
            posteriors = TH1D("poster_"+str(k)+"_iter"+str(IterNumber),"poster_"+str(k)+"_iter"+str(IterNumber), len(Binning[k])-1,x)
            density = TH2D("density_"+str(k)+"_iter_"+str(IterNumber),"density_"+str(k)+"_iter_"+str(IterNumber), len(Binning[k])-1,x,1000,0,1000)
            ListOfPosteriors.append(posteriors)
            ListOfDensities.append(density)        

        for m in range(len(D)): # number of POSTERIORS 
            print("Processing Posterior ",m)
            FillingNumber = 0
            Filling = False
            while (Filling == False):
                T_random = []
                test2 = 0
                for l in range(len(D)):
                    if FillingNumber % 2 == 0:
                        RandomValue = (upper[l] - lower[l])*np.random.random_sample() + lower[l]
                    else:
                        ListOfColumns0 = ListOfColumns.copy()
                        if IterNumber == 1: 
                            ListOfColumns0.pop()
                        HelpList0 = ListOfColumns0.copy()
                        HelpList0.pop(l)
                        TestExp = PseudoExp.drop(columns = HelpList0)
                        TestExp = PseudoExp.groupby([ListOfColumns0[l],'int'], sort=False)['LogLikelihood'].max()
                        TestExp = TestExp.drop_duplicates()
                        TestExp = TestExp.reset_index()
                        if l in TestExp['int'].tolist():
                            test2 = test2 + 1
                            american = TestExp['int'] == l
                            TestExp = TestExp[american]
                            #print("if ----------")
                            #print(TestExp)
                            TestExp = TestExp.groupby([ListOfColumns0[l],'int'], sort=False)['LogLikelihood'].max()
                            TestExp = TestExp.drop_duplicates()
                            TestExp = TestExp.reset_index()
                            RandomValue = TestExp[ListOfColumns0[l]][0]
                        else:
                            RandomValue = TestExp[ListOfColumns0[l]][int(len(TestExp)*np.random.random_sample())]
                            #print("else ----------")
                            #print(TestExp)
                        #RandomValue = TestExp[ListOfColumns0[l]][0]
                        print("*********** ",RandomValue)
                    T_random.append(RandomValue)
                Loglike = CalculateLikelihood(T_random,M,D)
                if (Loglike != False):
                    FindBestLike, CheckList = FindNewPseudo2(T_random, M, D, ListOfColumns, lower, upper)
                    PseudoExp = pn.concat([PseudoExp, FindBestLike], ignore_index=True, sort = True)
                    PseudoExp = PseudoExp.drop_duplicates()
                    PseudoExp = PseudoExp.sort_values(by=['int'],ascending=False)
                    PseudoExp = PseudoExp.reset_index(drop=True)
                    #print(PseudoExp)
                    PseudoExp = PseudoExp.sort_values(by=['LogLikelihood'],ascending=False)
                    PseudoExp = PseudoExp.reset_index(drop=True)
                    FillingNumber = FillingNumber + 1
                    test = 0
                    for i in range(len(CheckList)):
                        if CheckList[i] == False:
                            test = test + 1
                    if test == 0:
                        Filling = True
                    if test2 == len(D):
                        Filling = True
            if Filling == True:
                print("OK: ", FindBestLike['LogLikelihood'].max())

        SuperPseudoExp = pn.concat([SuperPseudoExp, PseudoExp], ignore_index=True, sort = True)      
        SuperPseudoExp = SuperPseudoExp.sort_values(by=['int'],ascending=False)
        SuperPseudoExp = SuperPseudoExp.reset_index(drop=True)
        PseudoExp = PseudoExp.sort_values(by=['int'],ascending=False)
        PseudoExp = PseudoExp.reset_index(drop=True)
        #print(SuperPseudoExp)

        LogLike = TH1D("loglike_"+str(IterNumber),"loglike_"+str(IterNumber), 1000,0,1000)

        if IterNumber == 1:
            ListOfColumns.pop()
        for index in range(len(PseudoExp.LogLikelihood)):
            LogLike.Fill(np.log(PseudoExp.LogLikelihood[index])+800)
            i = 0

        ListOfPseudoExp = []
        for j in range(len(D)):
            HelpList = ListOfColumns.copy()
            HelpList.pop(j)
            ListOfPseudoExp.append(PseudoExp)
            ListOfPseudoExp[j] = ListOfPseudoExp[j][ListOfPseudoExp[j].int == j]
            ListOfPseudoExp[j] = ListOfPseudoExp[j].drop(columns = HelpList)
            ListOfPseudoExp[j] = ListOfPseudoExp[j].groupby([ListOfColumns[j],'int'], sort=False)['LogLikelihood'].max()
            ListOfPseudoExp[j] = ListOfPseudoExp[j].drop_duplicates()
            ListOfPseudoExp[j] = ListOfPseudoExp[j].reset_index()

        ListOfEvents = []

        for k in range(len(D)):
            for index, rows in ListOfPseudoExp[k].iterrows():
                ListOfPosteriors[k].Fill(rows[ListOfColumns[k]], rows["LogLikelihood"])
                #if (math.isnan(np.log(rows["LogLikelihood"])) == False): 
                ListOfDensities[k].Fill(rows[ListOfColumns[k]],rows["LogLikelihood"])
            ListOfEvents.append(len(ListOfPseudoExp[k]))

        counts = min(ListOfEvents) + counts
        print(ListOfEvents)
        print("Counts: ", counts)
        print("----------------------------------------------------------------------")

        unfolded = h_ptcl_or.Clone("result_petr_by_fit")
        unfolded2 = unfolded.Clone("result_petr_by_mean")
        unfolded.Reset()
        unfolded2.Reset()

        DensitiesX = []
        lower = []
        upper = []

        for k in range(len(ListOfPosteriors)):
            if ((ListOfPosteriors[k].Integral() !=  0.0)and(counts > args.counts)):
                ListOfPosteriors[k].Scale(1./ListOfPosteriors[k].Integral())
                print("Normalize")
            else:
                print("WARNING1: Integral is zero, I am not normalizing. You need to increase number of iterations.")
            unfolded2.SetBinContent(k+1,ListOfPosteriors[k].GetMean())
            unfolded2.SetBinError(k+1,ListOfPosteriors[k].GetRMS())
            histograms.append(ListOfPosteriors[k])
            lower.append(ListOfPosteriors[k].GetMean()-ListOfPosteriors[k].GetMean()*0.1)
            upper.append(ListOfPosteriors[k].GetMean()+ListOfPosteriors[k].GetMean()*0.1)
            histograms.append(ListOfDensities[k].ProfileX())
            DensitiesX.append(ListOfDensities[k].ProfileX())

        unfolded.Divide(h_eff)
        unfolded2.Divide(h_eff)
        histograms.append(h_ptcl_or)
        histograms.append(unfolded)
        histograms.append(unfolded2)   
        histograms.append(LogLike)
        Repeat = PlotPosteriors(ListOfPosteriors, unfolded2, h_ptcl_or,DensitiesX, LogLike, str(IterNumber)) 
        IterNumber = IterNumber + 1
    
    ListOfPseudoExp = []
    for j in range(len(D)):
        HelpList = ListOfColumns.copy()
        HelpList.pop(j)
        ListOfPseudoExp.append(SuperPseudoExp)
        ListOfPseudoExp[j] = ListOfPseudoExp[j][ListOfPseudoExp[j].int == j]
        ListOfPseudoExp[j] = ListOfPseudoExp[j].drop(columns = HelpList)
        ListOfPseudoExp[j] = ListOfPseudoExp[j].groupby([ListOfColumns[j],'int'], sort=False)['LogLikelihood'].max()
        ListOfPseudoExp[j] = ListOfPseudoExp[j].drop_duplicates()
        ListOfPseudoExp[j] = ListOfPseudoExp[j].reset_index()
    ListOfEvents = []
    ListOfPosteriors = []
    ListOfDensities = []
    for k in range(len(D)):
        posteriors = TH1D("poster_"+str(k)+"_iter"+str(IterNumber)+"_final","poster_"+str(k)+"_iter"+str(IterNumber)+"_final", 200,lower[k],upper[k])
        ListOfPosteriors.append(posteriors)
        density = TH2D("density_"+str(k)+"_iter_"+str(IterNumber)+"_final","density_"+str(k)+"_iter_"+str(IterNumber)+"_final", 200,lower[k],upper[k],1000,0,1000)
        ListOfDensities.append(density)
        for index, rows in ListOfPseudoExp[k].iterrows():
            ListOfPosteriors[k].Fill(rows[ListOfColumns[k]], rows["LogLikelihood"])
            ListOfDensities[k].Fill(rows[ListOfColumns[k]],rows["LogLikelihood"])
    
    DensitiesX = []
    unfolded2.Reset()

    for k in range(len(ListOfPosteriors)):
        if ListOfPosteriors[k].Integral() !=  0.0 :
            ListOfPosteriors[k].Scale(1./ListOfPosteriors[k].Integral())
            print("Normalize.")
        else:
            print("WARNING1: Integral is zero, I am not normalizing. You need to increase number of iterations.")
        unfolded2.SetBinContent(k+1,ListOfPosteriors[k].GetMean())
        unfolded2.SetBinError(k+1,ListOfPosteriors[k].GetRMS())
        histograms.append(ListOfPosteriors[k])
        histograms.append(ListOfDensities[k].ProfileX())
        DensitiesX.append(ListOfDensities[k].ProfileX())
    unfolded2.Divide(h_eff)
    histograms.append(unfolded2)   
    Repeat = PlotPosteriors(ListOfPosteriors, unfolded2, h_ptcl_or,DensitiesX, LogLike, str(IterNumber), fit = 1) 
    SaveHistograms("test") 
    return unfolded2, ListOfPosteriors
        
histograms = []


matrix_name=args.h_matrix
h_reco_getG0_name=args.h_data
h_ptcl_getG0_name = args.h_particle
h_reco_get_bkg_name = args.h_background
outputname=args.h_data+"_unfolded"
nrebin = args.nrebin

rfile_data = TFile(args.rfile_data, 'read')
rfile_particle = TFile(args.rfile_particle, 'read')
rfile_matrix = TFile(args.rfile_matrix, 'read')
rfile_background = TFile(args.rfile_background, 'read')
#GET DATA
h_reco_get = rfile_data.Get(h_reco_getG0_name)
h_reco_get.Rebin(nrebin)
#GET PARTICLE
h_ptcl_get = rfile_particle.Get(h_ptcl_getG0_name)
h_ptcl_get.Rebin(nrebin)
#GET MATRIX
h_response_unf = rfile_matrix.Get(matrix_name)
h_response_unf.Rebin2D(nrebin,nrebin)
h_reco_get_input = rfile_data.Get(h_reco_getG0_name)
h_reco_get_bkg = rfile_background.Get(h_reco_get_bkg_name)
h_reco_get_bkg.Rebin(nrebin)

#MyBaronRooUnfold(h_reco_get=h_reco_get,h_ptcl_get=h_ptcl_get,h_response_unf=h_response_unf,h_reco_get_bkg=h_reco_get_bkg, matrix_name=h_response_unf.GetName(), h_ptcl_getG0_name=h_ptcl_get.GetName(), h_reco_getG0_name=h_reco_get.GetName(), h_reco_get_bkg_name=h_reco_get_bkg.GetName(), sampling=args.sampling)
Hamilton(1, 1, 1, 1, 5)

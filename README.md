# Unfolding

1.) Install ROOT
2.) Install Fully Bayesian Unfolding package - FBU available here https://github.com/gerbaudo/fbu
3.) Install RooUnfold package - http://hepunx.rl.ac.uk/~adye/software/unfold/RooUnfold.html

1.) first just test the script writing:

python Unfolding.py

In input.root are saved 4 histograms = data, backgournd, particle level and migration matrix

2.) TO RUN THE SCRIPT you need 4 inputs data, backgournd, particle level and migration matrix, Migration matrix has detector level on X axis, and particle level on Y axis

example:

python Unfolding.py --rfile_data "/home/petr/GitLab/unfolding/input.root" --rfile_particle "/home/petr/GitLab/unfolding/input.root" --rfile_matrix "/home/petr/GitLab/unfolding/input.root" --rfile_background "/home/petr/GitLab/unfolding/input.root" --h_data "h_data" --h_particle "h_particle" --h_matrix "h_matrix" --h_background "h_background"


#!/usr/bin/env python3

import os
os.environ["MKL_THREADING_LAYER"] = "GNU"
import argparse
from matplotlib import pyplot as plt
import fbu
from fbu import Regularization
import ROOT
import math
import random
import numpy as np
from array import array
import scipy.stats as stats
#from ROOT import RooUnfoldResponse
#from ROOT import RooUnfold
#from ROOT import RooUnfoldBayes
#from ROOT import RooUnfoldSvd
#from ROOT import RooUnfoldTUnfold
#from ROOT import RooUnfoldIds
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed, TMath, TF1, TLine, kBird
import matplotlib.pyplot as plt
import pandas as pn

parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")
parser.add_argument('--N', '-N', type=int, default=100)
parser.add_argument('--rfile_data','-rfile_data', type=str, default="input.root")
parser.add_argument('--rfile_particle','-rfile_particle', type=str, default="input.root")
parser.add_argument('--rfile_matrix','-rfile_matrix', type=str, default="input.root")
parser.add_argument('--rfile_background','-rfile_background', type=str, default="input.root")
parser.add_argument('--h_data','-h_data', type=str, default="h_data")
parser.add_argument('--h_particle','-h_particle', type=str, default="h_particle")
parser.add_argument('--h_matrix','-h_matrix', type=str, default="h_matrix")
parser.add_argument('--h_background','-h_background', type=str, default="h_background")

parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{t,had} [GeV]")
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)
parser.add_argument('--SplitFromBinLow', '-SplitFromBinLow', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinLow', '-ParameterSplitFromBinLow', type=float, default=1.5)
parser.add_argument('--SplitFromBinHigh', '-SplitFromBinHigh', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinHigh', '-ParameterSplitFromBinHigh', type=float, default=1.5)
parser.add_argument('--maxiterations', '-maxiterations', type=int, default=30)
parser.add_argument('--batch', '-batch', type=int, default=0)
parser.add_argument('--sampling', '-sampling', type=int, default=500)
parser.add_argument('--plotlike', '-plotlike', type=int, default=0)
parser.add_argument('--nrebinpost', '-nrebinpost', type=int, default=2)
parser.add_argument('--likerange', '-likerange', type=float, default=0.02)


args = parser.parse_args()

if (args.batch == 1):
    ROOT.gROOT.SetBatch(1)

class TrueValue:
    def __init__(self, TrueValue = [], Likelihood = 0.0):

        self.TrueValue = TrueValue
        self.Likelihood = Likelihood

def DivideBinWidth(h):
    for j in range(1, h.GetNbinsX()+1):
        h.SetBinContent(j,((h.GetBinContent(j))/(h.GetBinWidth(j))))
        h.SetBinError(j,((h.GetBinError(j))/(h.GetBinWidth(j))))

def PrintCan(can, outputname):
    outputname = outputname.split('/')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.png')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.pdf')

def MakeListFromHisto(hist):
    vals = []
    for i in range(1, hist.GetXaxis().GetNbins()+1):
        val = hist.GetBinContent(i)
        vals.append(val)
    return vals

def NormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetXaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetYaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetYaxis().GetNbins()+1):
                migra.SetBinContent(i,j,migra.GetBinContent(i,j) / sum)
    return migra

def CheckNormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(j,i)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetXaxis().GetNbins()+1):
                migra.SetBinContent(j,j,migra.GetBinContent(j,i) / sum)
    return migra

def MakeListResponse(h2):
    vals = []
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        column = []
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            column.append(val)
        vals.append(column)
    return vals

def MakeUnfoldedHisto(reco4bins, h1s, tag = '_unfolded'):
    hname = reco4bins.GetName()+tag
    hist = reco4bins.Clone(hname)
    hist.Reset()
    histMean = reco4bins.Clone(hname+"Mean")
    histMean.Reset()
    i = -1
    for h1 in h1s:
        i = i+1
        h1.Rebin(8)
        h1.Fit("gaus")
        fit = h1.GetFunction("gaus") 
        chi2 = fit.GetChisquare()
        p1 = fit.GetParameter(1)
        p0 = fit.GetParameter(0)
        p2 = fit.GetParameter(2)
        hist.SetBinContent(i+1, p1)
        hist.SetBinError(i+1, p2)
        histMean.SetBinContent(i+1, h1.GetMean())
        histMean.SetBinError(i+1, h1.GetRMS())
    return hist, histMean

def MakeTH1Ds(trace, tag = 'trace', nbins = 600):
    h1s = []
    gmax = -999
    gmin = 1e19
    for line in trace:
        xmin = min(line)
        xmax = max(line)
        gmin = min(xmin, gmin)
        gmax = max(xmax, gmax)
    i = -1
    gmax = -999
    gmin = 1e19
    
    for line in trace:
        gxmin = min(line)
        gxmax = max(line)
        i = i+1
        hname = tag + '_{:}'.format(i)
        h1 = TH1D(hname, hname, nbins, gmin, gmax)
        print("-----------------------")
        k=0
        for val in line:
            h1.Fill(val)
        h1s.append(h1)
        print(i)
        print("Appending traces")
        print(h1)
        histograms.append(h1)
    return h1s

def TransposeMatrix(h_response_unf):
    h_responce_transpose = h_response_unf.Clone(h_response_unf.GetName()+"clone")
    h_responce_transpose.Reset()
    for i in range(1,h_response_unf.GetXaxis().GetNbins()+1):
        for j in range(1,h_response_unf.GetXaxis().GetNbins()+1):
            h_responce_transpose.SetBinContent(i,j,h_response_unf.GetBinContent(j,i))
            h_responce_transpose.SetBinError(i,j,h_response_unf.GetBinError(j,i))
    h_responce_transpose.GetXaxis().SetTitle(h_response_unf.GetYaxis().GetTitle())
    h_responce_transpose.GetYaxis().SetTitle(h_response_unf.GetXaxis().GetTitle())
    return h_responce_transpose
    
def SaveHistograms(outputname = "output"):
    outputname = outputname.split('/')
    outfile = TFile('./outputs/'+outputname[len(outputname)-1]+".root", 'recreate')
    outfile.cd()
    for j in range(len(histograms)):
        histograms[j].Write()
    print("End of the unfolding.")
    outfile.Write()
    outfile.Close() 

def Plot2D(ListOf2DPosteriors):
    c = TCanvas("Posteriors2D"+outputname,"Posteriors2D"+outputname,0,0,1600, 1600)
    c.Divide(math.ceil(pow(len(ListOf2DPosteriors),0.5)),math.ceil(pow(len(ListOf2DPosteriors),0.5)))
    for i in range(len(ListOf2DPosteriors)):
        c.cd(i+1)
        ListOf2DPosteriors[i].GetZaxis().SetRangeUser(0,1.0)
        ListOf2DPosteriors[i].Draw("colz")
    gApplication.Run()

def PlotPosteriors(ListOfPosteriors, unfolded, h_ptcl_or, correlation, ListOfDensities, LogLike, outputname = ""):
    RepeatIteration = False
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.55555),int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.0))
    legends = []
    ptcl_lines = []
    index = 1
    for i in range(len(ListOfPosteriors)):
        ListOfPosteriors[i].SetMaximum(ListOfPosteriors[i].GetMaximum()*2.0)
        ListOfPosteriors[i].SetMinimum(0.0)
        fit = ListOfPosteriors[i].GetFunction("FitFunction"+str(i)) 
        #fit = ListOfPosteriors[i].GetFunction("gaus") 
        chi2 = fit.GetChisquare()
        p1 = fit.GetParameter(1)
        p2 = fit.GetParameter(2)
        FitIntegral = fit.Integral(p1-10*p2,p1+10*p2) # fit integral , get mean plus minus 10 sigma
        PriorIntegral = ListOfPosteriors[i].Integral("width")
        if FitIntegral != 0 :
            Percentage = round((100*PriorIntegral/FitIntegral),0)
        c.cd(i+1)
        gPad.SetFrameFillColor(10)
        if (Percentage < 90) or (Percentage > 110) or (chi2/len(ListOfPosteriors) > 20):
            #if (Percentage < 90):
            gPad.SetFillColor(kRed-4)
            RepeatIteration = True
        else:
            gPad.SetFillColor(8)
        leg = TLegend(0.1,0.5,0.85,0.9)
        leg.SetFillStyle(0)
        leg.AddEntry(None,"MeanHist = "+str(round(ListOfPosteriors[i].GetMean(),0))+", RMShist = "+str(round(ListOfPosteriors[i].GetRMS(),0))+", MeanFit = "+str(round(p1,0))+", #sigma_{fit} = "+str(round(p2,0)),"")
        leg.AddEntry(ListOfPosteriors[i],"Posterior ","l")
        leg.AddEntry(fit,"Fit ","l")
        leg.AddEntry(None,"#chi^{2}/NDF = "+str(round(chi2/len(ListOfPosteriors),2)),"")
        leg.AddEntry(None,"Integral Prior/Fit = "+str(round(Percentage,0))+" %","")
        leg.SetBorderSize(0)
        legends.append(leg)
        ListOfPosteriors[i].SetTitle("Posterior in bin "+str(i+1))
        gStyle.SetOptStat(0)
        ListOfPosteriors[i].Draw("hist")
        fit.Draw("same")
        t2 = TLine (h_ptcl_or.GetBinContent(i+1), ListOfPosteriors[i].GetMinimum(), h_ptcl_or.GetBinContent(i+1), ListOfPosteriors[i].GetMaximum())
        t2.SetLineColor(2)
        ptcl_lines.append(t2)
        ptcl_lines[i].Draw("same")
        legends[i].Draw("same")
        index=index+1

    for i in range(len(ListOfDensities)):
        c.cd(index)
        minimum = ListOfDensities[i].GetXaxis().GetBinLowEdge( ListOfDensities[i].FindFirstBinAbove() )
        maximum = ListOfDensities[i].GetXaxis().GetBinUpEdge( ListOfDensities[i].FindLastBinAbove() )
        FitFunction = TF1("FitFunction"+str(i)+"bfit","[2]*x*x-[1]*x+[0]" , minimum,maximum)
        #FitFunction.SetParameter(0,ListOfPosteriors[k].GetMean())
        #FitFunction.SetParameter(1,ListOfPosteriors[k].GetRMS())
        FitFunction.SetParameter(2,-1.0)
        #help_ProfileX.Fit("FitFunction"+str(k))
        ListOfDensities[i].Fit("FitFunction"+str(i)+"bfit")
        fit2 = ListOfDensities[i].GetFunction("FitFunction"+str(i)+"bfit") 
        ListOfDensities[i].Draw("hist")
        fit2.Draw("same")
        index = index + 1 

    c.cd(index)
    LogLike.Draw()
    index = index + 1        
        #c.Update()

    c.cd(1)
    gStyle.SetOptStat(0)
    gPad.Modified()
    #c.Update()

    c.cd(index)
    pad1 = TPad("pad1","pad1",0,0.40,1,1)
    pad1.SetTopMargin(0.15)
    pad1.SetBottomMargin(0.01)
    pad1.SetFillStyle(0)
    pad1.SetTicks(1,1)
    pad1.SetBorderMode(0)
    pad1.Draw()
    c.cd(index)
    pad2 = TPad("pad2","pad2",0,0.01,1,0.422)
    pad2.SetFillStyle(0)
    pad2.SetTopMargin(0.043)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks(1,1)
    pad2.Draw()
    pad2.Modified()
    c.cd(index)
    pad1.cd()
    gStyle.SetOptStat(0)
    h_ptcl_or.SetTitle("")
    h_ptcl_or.SetLineColor(2)
    #h_ptcl_or.GetYaxis().SetRangeUser(0,h_ptcl_or.GetMaximum()*1.5)
    #h_ptcl_or.GetXaxis().SetTitleSize(34)
    #h_ptcl_or.GetXaxis().SetTitleFont(43)
    #h_ptcl_or.GetYaxis().SetTitleSize(27)
    #h_ptcl_or.GetYaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleOffset(1.5)
    h_ptcl_or.GetYaxis().SetTitle("Events")
    #h_ptcl_or.GetYaxis().SetLabelFont(43)
    #h_ptcl_or.GetYaxis().SetLabelSize(25)
    legend = TLegend(0.55,0.5,0.85,0.8)
    legend.SetFillStyle(0)
    legend.AddEntry(h_ptcl_or,"Particle")
    legend.AddEntry(unfolded,"FBU Baron imp.","p")
    legend.SetBorderSize(0)
    unfolded.SetLineColor(1)
    unfolded.SetMarkerColor(1)
    unfolded.SetMarkerStyle(22)
    unfolded.Draw("same p x0")
    h_ptcl_or.Draw("hist same")
    legend.Draw("same")
    pad1.RedrawAxis()
    pad2.cd()
    h_ptcl_or_clone = h_ptcl_or.Clone(h_ptcl_or.GetName()+"_clone") 
    unfolded_clone = unfolded.Clone(unfolded.GetName()+"_clone")
    h_ptcl_or_clone.Divide(h_ptcl_or) 
    unfolded_clone.Divide(h_ptcl_or) 
    
    #h_ptcl_or_clone.GetXaxis().SetTitleSize(27)
    #h_ptcl_or_clone.GetXaxis().SetTitleFont(43)
    #h_ptcl_or_clone.GetYaxis().SetTitleSize(27)
    #h_ptcl_or_clone.GetYaxis().SetTitleFont(43)
    #h_ptcl_or_clone.GetXaxis().SetLabelFont(43)
    #h_ptcl_or_clone.GetXaxis().SetLabelSize(25)
    #h_ptcl_or_clone.GetYaxis().SetLabelFont(43)
    #h_ptcl_or_clone.GetYaxis().SetLabelSize(25)
    
    h_ptcl_or_clone.SetMaximum(1.3)
    h_ptcl_or_clone.SetMinimum(0.7)
    
    h_ptcl_or_clone.GetXaxis().SetTitleOffset(2.5)
    h_ptcl_or_clone.GetXaxis().SetTitle(args.title)
    
    #h_ptcl_or_clone.GetYaxis().SetTitle("#frac{Unfolded}{Simulation}      ")
    h_ptcl_or_clone.GetYaxis().SetTitle("Ratio      ")
    h_ptcl_or_clone.Draw("hist")
    unfolded_clone.Draw("same p x0")
    pad2.RedrawAxis()
    c.cd(index+1)
    correlation.SetTitle("Bin correlations")
    correlation.SetMaximum(1.0)
    correlation.SetMinimum(-1.0)
    #gStyle.SetPalette(kBird)
    #correlation.SetContour(4)
    correlation.Draw("colz")
    c.Update()
    histograms.append(c) # here is the crash probably
    
    PrintCan(c,c.GetName()+"_posteriors")

    #PlotRatio(unfolded, h_ptcl_or, c.GetName())
    #gApplication.Run()
    return RepeatIteration

def PlotRatio(h_reco_unfolded, h_ptcl_or, Name = ""):
    
    gStyle.SetPadLeftMargin(0.12)
    gStyle.SetPadRightMargin(0.12)
    c = TCanvas("canvas_ratio"+Name,"canvas_ratio"+Name,0,0,800, 800)
    c.cd()
    pad1 = TPad("pad1"+Name,"pad1"+Name,0,0.40,1,1)
    pad1.SetTopMargin(0.15)
    pad1.SetBottomMargin(0.01)
    pad1.SetFillStyle(0)
    pad1.SetTicks(1,1)
    pad1.SetBorderMode(0)
    pad1.Draw()
    c.cd()
    pad2 = TPad("pad2"+Name,"pad2"+Name,0,0.01,1,0.422)
    pad2.SetFillStyle(0)
    pad2.SetTopMargin(0.043)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks(1,1)
    pad2.Draw()
    pad2.Modified()
    c.cd()
    pad1.cd()
    gStyle.SetOptStat(0)
    h_ptcl_or.SetTitle("")
    h_ptcl_or.SetLineColor(2)
    #h_ptcl_or.GetYaxis().SetRangeUser(0,h_ptcl_or.GetMaximum()*1.5)
    h_ptcl_or.GetXaxis().SetTitleSize(34)
    h_ptcl_or.GetXaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleSize(27)
    h_ptcl_or.GetYaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleOffset(1.5)
    h_ptcl_or.GetYaxis().SetTitle("Events")
    h_ptcl_or.GetYaxis().SetLabelFont(43)
    h_ptcl_or.GetYaxis().SetLabelSize(25)
    legend = TLegend(0.55,0.5,0.85,0.8)
    legend.SetFillStyle(0)
    legend.AddEntry(h_ptcl_or,"Particle")
    legend.AddEntry(h_reco_unfolded,"FBU Baron imp.","p")
    legend.SetBorderSize(0)
    h_reco_unfolded.SetLineColor(1)
    h_reco_unfolded.SetMarkerColor(1)
    h_reco_unfolded.SetMarkerStyle(22)
    h_reco_unfolded.Draw("same p x0")
    h_ptcl_or.Draw("hist same")
    legend.Draw("same")
    pad1.RedrawAxis()
    pad2.cd()
    h_ptcl_or_clone = h_ptcl_or.Clone(h_ptcl_or.GetName()+"_clone"+Name) 
    h_reco_unfolded_clone = h_reco_unfolded.Clone(h_reco_unfolded.GetName()+"_clone"+Name)
    h_ptcl_or_clone.Divide(h_ptcl_or) 
    h_reco_unfolded_clone.Divide(h_ptcl_or) 
    
    h_ptcl_or_clone.GetXaxis().SetTitleSize(27)
    h_ptcl_or_clone.GetXaxis().SetTitleFont(43)
    h_ptcl_or_clone.GetYaxis().SetTitleSize(27)
    h_ptcl_or_clone.GetYaxis().SetTitleFont(43)
    
    h_ptcl_or_clone.GetXaxis().SetLabelFont(43)
    h_ptcl_or_clone.GetXaxis().SetLabelSize(25)
    h_ptcl_or_clone.GetYaxis().SetLabelFont(43)
    h_ptcl_or_clone.GetYaxis().SetLabelSize(25)
    
    h_ptcl_or_clone.SetMaximum(1.3)
    h_ptcl_or_clone.SetMinimum(0.7)
    
    h_ptcl_or_clone.GetXaxis().SetTitleOffset(2.5)
    h_ptcl_or_clone.GetXaxis().SetTitle(args.title)
    
    #h_ptcl_or_clone.GetYaxis().SetTitle("#frac{Unfolded}{Simulation}      ")
    h_ptcl_or_clone.GetYaxis().SetTitle("Ratio      ")
    h_ptcl_or_clone.Draw("hist")
    h_reco_unfolded_clone.Draw("same p x0")
    pad2.RedrawAxis()
    c.Update()
    histograms.append(c) # here is the crash probably
    PrintCan(c, "ratio"+Name)

def FindMaximumIndex(ListOfTrueValues):
    MaxLikelihood = 2.2250738585072014e-308
    Index = 0
    for i in range(len(ListOfTrueValues)):
        if ListOfTrueValues[i].Likelihood > MaxLikelihood:
            MaxLikelihood = ListOfTrueValues[i].Likelihood
            Index = i
    return Index
            

def MyBaronRooUnfold(h_reco_get,h_ptcl_get,h_response_unf,h_reco_get_bkg,matrix_name=args.h_matrix, h_reco_getG0_name=args.h_data, h_ptcl_getG0_name = args.h_particle,h_reco_get_bkg_name = args.h_background,outputname=args.h_data+"_unfolded",nrebin = args.nrebin, N = 100, sampling = 1000, maxiteration = 30):

    h_response_unf.ClearUnderflowAndOverflow()
    h_response_unf.GetXaxis().SetRange(1, h_response_unf.GetXaxis().GetNbins() )
    h_response_unf.GetYaxis().SetRange(1, h_response_unf.GetYaxis().GetNbins() )
    h_response_unf.SetName("Migration_Matrix_simulation")

    ########### ACCEPTANCY
    h_acc = h_response_unf.ProjectionX("reco_recoandparticleX") # Reco M
    h_acc.Divide(h_reco_get)
    ########### AKCEPTANCE saved in h_acc #############
    ########### EFFICIENCY
    h_eff = h_response_unf.ProjectionY("reco_recoandparticleY") # Ptcl M
    h_eff.Divide(h_ptcl_get)

    h_reco_get_input_clone=h_reco_get_input.Clone("")
    #h_reco_get_input_clone.Add(h_reco_get_bkg,-1)
    h_reco_get_input_clone.Multiply(h_acc)
   
    h_ptcl_or = rfile_particle.Get(h_ptcl_getG0_name)
    h_ptcl_or.SetMaximum(h_ptcl_or.GetMaximum()*1.5)
    
    h_response_unf_fbu = TransposeMatrix(h_response_unf)
    h_response_unf_fbu_norm = NormalizeResponse(h_response_unf_fbu)
    h_response_unf_fbu_norm.SetName("Migration_Matrix_simulation_transpose")
    histograms.append(h_response_unf_fbu_norm)
    M = MakeListResponse(h_response_unf_fbu_norm)
    D = MakeListFromHisto(h_reco_get_input_clone) 
    B = MakeListFromHisto(h_reco_get_bkg)

    lower=[]
    upper=[]

    h_det_div_ptcl=h_reco_get_input_clone.Clone("")
    h_det_div_ptcl.Divide(h_ptcl_or)
    h_det_div_ptcl.Divide(h_eff)
    h_det_div_ptcl.SetName("det_div_ptcl")
    
    ListOfColumns = []

    for l in range(len(D)):
        lower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-args.par)*h_det_div_ptcl.GetBinContent(l+1))
        upper.append(h_reco_get_input_clone.GetBinContent(l+1)*args.par*h_det_div_ptcl.GetBinContent(l+1)) 
        ListOfColumns.append('True_bin_'+str(l))

    OldLower = lower
    OldUpper = upper

    ListOfColumns.append('LogLikelihood')
    
    D_np = np.array(D)

    Repeat = True
    IterNumber = 1
    while Repeat:
        PseudoExp = pn.DataFrame(columns=ListOfColumns)
        print("Runnig iteration number: ",IterNumber)
        ListOfPosteriors = []
        ListOfFinePosteriors = []
        for s in range(len(lower)):
            print("Space check: ",lower[s], upper[s])
        Binning = []
        ListOfDensities = []
        for k in range(len(D)):
            Empty = []
            Binning.append(Empty)
            help_array = np.arange(lower[k],upper[k],(upper[k]-lower[k])/sampling)
            for s in range(len(help_array)):
                Binning[k].append(help_array[s])
            x = array("d",Binning[k])
            posteriors = TH1D("poster_"+str(k)+"_iter"+str(IterNumber),"poster_"+str(k)+"_iter"+str(IterNumber), len(Binning[k])-1,x)
            fine_posteriors = TH1D("poster_fine"+str(k)+"_iter"+str(IterNumber),"poster_fine"+str(k)+"_iter"+str(IterNumber), len(Binning[k])-1,x)
            density = TH2D("density_"+str(k)+"_iter_"+str(IterNumber),"density_"+str(k)+"_iter_"+str(IterNumber), len(Binning[k])-1,x,1000,0,1000)
            ListOfPosteriors.append(posteriors)
            ListOfFinePosteriors.append(fine_posteriors)
            ListOfDensities.append(density)

        ListOf2DPosteriors = []
        ListOfTrueValues = []

        for j in range(len(D)):
            for l in range(len(D)):
                FirstAx = array("d",Binning[j])
                SecondAx = array("d",Binning[l])
                posteriors2D = TH2D("2D_poster_"+str(j)+"_"+str(l)+"_iter_"+str(IterNumber),"2D_poster_"+str(j)+"_"+str(l)+"_iter_"+str(IterNumber), len(Binning[j])-1,FirstAx,len(Binning[l])-1,SecondAx)
                ListOf2DPosteriors.append(posteriors2D) 

        help_corr = np.arange(0,len(D)+1)
        help_corr2 = array("d",help_corr)

        correlation = TH2D("correlation"+str(IterNumber),"correlation"+str(IterNumber), len(help_corr)-1,help_corr2,len(help_corr)-1,help_corr2)

        for m in range(len(D)): # number of POSTERIORS 
            print("Processing Posterior ",m)
            FillingTest = False
            for k in range(len(Binning[m])): # number of BINS in POSTERIOR = 100 = my SAMPLING
                for s in range(N):   # number of ITERATIONS = 10 000 
                    T_random = []
                    DictOfPseudos = {}
                    for l in range(len(D)):
                        RandomValue = (upper[l] - lower[l])*np.random.random_sample() + lower[l]
                        DictOfPseudos['True_bin_'+str(l)] = [RandomValue]
                        T_random.append(RandomValue)  # RANDOM TRUTH
                    T_random[m] = ListOfPosteriors[m].GetBinCenter(k+1)                              # FIXING true value in bin m
                    R_np = np.array(T_random).dot(np.array(M))                                       # CREATING RECO
                    D_np_int64 = D_np.astype(np.int64)
                    R_np_int64 = R_np.astype(np.int64)
                    LikeTest = stats.poisson.pmf(D_np_int64, R_np_int64)
                    LikelihoodProd = np.prod(LikeTest)
                    if (LikelihoodProd != 0.0)and(math.isnan(LikelihoodProd) == False)and(math.isinf(LikelihoodProd) == False) :
                        #for j in range(len(D)):
                        #    for l in range(len(D)):
                        #        for n in range(len(ListOf2DPosteriors)):
                        #            if (str(j)+"_"+str(l)) in ListOf2DPosteriors[n].GetName():
                        #                ListOf2DPosteriors[n].SetBinContent( ListOf2DPosteriors[n].GetXaxis().FindBin(T_random[j]),ListOf2DPosteriors[n].GetYaxis().FindBin(T_random[l]), np.prod(LikeTest) + ListOf2DPosteriors[n].GetBinContent(ListOf2DPosteriors[n].GetXaxis().FindBin(T_random[j]),ListOf2DPosteriors[n].GetYaxis().FindBin(T_random[l])))
                        DictOfPseudos['LogLikelihood'] = [LikelihoodProd]
                        #print("Dict:", DictOfPseudos)
                        TempPseudoExp = pn.DataFrame.from_dict(DictOfPseudos) 
                        PseudoExp = pn.concat([PseudoExp, TempPseudoExp], ignore_index=True)
                        ListOfPosteriors[m].SetBinContent( ListOfPosteriors[m].FindBin(T_random[m]), LikelihoodProd + ListOfPosteriors[m].GetBinContent(ListOfPosteriors[m].FindBin(T_random[m])))
                        MyTrue = TrueValue()
                        MyTrue.Likelihood = LikelihoodProd
                        MyTrue.TrueValue = T_random
                        ListOfTrueValues.append(MyTrue)
                        FillingTest = True
            if FillingTest == True:
                print("Posterior ",m," is OK.")

        #MaxTrueIndex = FindMaximumIndex(ListOfTrueValues)
        #print("Index: ", MaxTrueIndex, ListOfTrueValues[MaxTrueIndex].TrueValue, int(np.log(ListOfTrueValues[MaxTrueIndex].Likelihood)+800))
        New_T_random = PseudoExp.loc[PseudoExp['LogLikelihood'].idxmax()]
        New_T_random = New_T_random.tolist()
        New_T_random.pop()

        LogLike = TH1D("loglike_"+str(IterNumber),"loglike_"+str(IterNumber), 1000,0,1000)

        for j in range(len(ListOfTrueValues)):
            LogLike.Fill(int(np.log(ListOfTrueValues[j].Likelihood)+800))
            for i in range(len(ListOfDensities)):
                ListOfDensities[i].Fill(ListOfTrueValues[j].TrueValue[i],int(np.log(ListOfTrueValues[j].Likelihood)+800))
                #print("Filling densities:" ,j,i, ListOfTrueValues[j].TrueValue[i],int(np.log(ListOfTrueValues[j].Likelihood)+800))


        unfolded = h_ptcl_or.Clone("result_petr_by_fit")
        unfolded2 = unfolded.Clone("result_petr_by_mean")
        unfolded.Reset()
        unfolded2.Reset()

        DensitiesX = []

        lower = []
        upper = []
        for k in range(len(ListOfPosteriors)):
            ListOfPosteriors[k].Rebin(args.nrebinpost)
            ListOfFinePosteriors[k].Rebin(args.nrebinpost)
            if ListOfPosteriors[k].Integral() !=  0.0:
                ListOfPosteriors[k].Scale(1./ListOfPosteriors[k].Integral())
                print("Normalize")
            else:
                print("WARNING1: Integral is zero, I am not normalizing. You need to increase number of iterations.")
            for m in range(1,ListOfPosteriors[k].GetXaxis().GetNbins()+1):
                if (ListOfPosteriors[k].GetBinContent(m) > 1e-15)and(ListOfPosteriors[k].GetBinContent(m) < float("inf")):
                    ListOfFinePosteriors[k].SetBinContent(m,ListOfPosteriors[k].GetBinContent(m))
                    #print("Filing bin: ",m, " posterior ",k," value ",ListOfPosteriors[k].GetBinContent(m))
                    ListOfFinePosteriors[k].SetBinError(m,0.01*1.0/ListOfPosteriors[k].GetBinContent(m))
            FitFunction = TF1("FitFunction"+str(k),"gaus")
            FitFunction.SetParameter(1,ListOfFinePosteriors[k].GetMean())
            ListOfFinePosteriors[k].Fit("FitFunction"+str(k),"Q")
            f1 = ListOfFinePosteriors[k].GetFunction("FitFunction"+str(k))
            p1 = f1.GetParameter(1)
            p2 = f1.GetParameter(2)
            #if IterNumber > 3:
            #    lower.append(p1-4*p2)
            #    upper.append(p1+4*p2)
            #else: 
            minimum0 = ListOfFinePosteriors[k].GetXaxis().GetBinLowEdge( ListOfFinePosteriors[k].FindFirstBinAbove() )
            maximum0 = ListOfFinePosteriors[k].GetXaxis().GetBinUpEdge( ListOfFinePosteriors[k].FindLastBinAbove() )
            #    lower.append(minimum)
            #    upper.append(maximum)
            unfolded2.SetBinContent(k+1,ListOfPosteriors[k].GetMean())
            unfolded2.SetBinError(k+1,ListOfPosteriors[k].GetRMS())
            unfolded.SetBinContent(k+1,p1)
            unfolded.SetBinError(k+1,p2)
            histograms.append(ListOfFinePosteriors[k])
            histograms.append(ListOfPosteriors[k])
            ListOfDensities[k].Rebin2D(args.nrebinpost)
            help_ProfileX = ListOfDensities[k].ProfileX()
            minimum = help_ProfileX.GetXaxis().GetBinLowEdge( help_ProfileX.FindFirstBinAbove() )
            maximum = help_ProfileX.GetXaxis().GetBinUpEdge( help_ProfileX.FindLastBinAbove() )
            FitFunction = TF1("FitFunction"+str(k),"[2]*x*x-[1]*x+[0]" , minimum,maximum)
            #FitFunction.SetParameter(0,ListOfPosteriors[k].GetMean())
            #FitFunction.SetParameter(1,ListOfPosteriors[k].GetRMS())
            FitFunction.SetParameter(2,-1.0)
            help_ProfileX.Fit("FitFunction"+str(k))
            fit2 =help_ProfileX.GetFunction("FitFunction"+str(k))
            a = fit2.GetParameter(2)
            b = fit2.GetParameter(1)
            c = fit2.GetParameter(0)
            #if IterNumber == 1 :
                #for i in range(len(New_T_random)):
            lower.append(New_T_random[k]-New_T_random[k]*args.likerange)
            upper.append(New_T_random[k]+New_T_random[k]*args.likerange) 
            #else:
            #    if (a > 0)and(b>0) :
            #        #for i in range(len(New_T_random)):
            #        lower.append(New_T_random[k]-(OldUpper[k]-OldLower[k])*0.1)
            #        upper.append(New_T_random[k]+(OldUpper[k]-OldLower[k])*0.1) 
            #    else:
            #        print("Vertex: ", b/(2*a)-4*np.sqrt(-1.0/(2*a)),b/(2*a)-4*np.sqrt(-1.0/(2*a)))
            #        lower.append(abs(b/(2*a))-4*np.sqrt(abs(1.0/(2*a))))
            #        upper.append(abs(b/(2*a))+4*np.sqrt(abs(1.0/(2*a))))
            

            histograms.append(ListOfDensities[k].ProfileX())
            DensitiesX.append(ListOfDensities[k].ProfileX())

        #for k in range(len(ListOf2DPosteriors)):
        #    ListOf2DPosteriors[k].Rebin2D(args.nrebinpost)
        #    if ListOf2DPosteriors[k].Integral() != 0:
        #        ListOf2DPosteriors[k].Scale(1.0/ListOf2DPosteriors[k].Integral())
        #    histograms.append(ListOf2DPosteriors[k])
        
        #for j in range(len(D)):
        #    for l in range(len(D)):
        #        for n in range(len(ListOf2DPosteriors)):
        #            if (str(j)+"_"+str(l)) in ListOf2DPosteriors[n].GetName():
        #                correlation.SetBinContent(j+1,l+1,ListOf2DPosteriors[n].GetCorrelationFactor())
        histograms.append(correlation)
        histograms.append(LogLike)

        unfolded.Divide(h_eff)
        unfolded2.Divide(h_eff)
        histograms.append(h_ptcl_or)
        histograms.append(unfolded)
        histograms.append(unfolded2)   
        Repeat = PlotPosteriors(ListOfFinePosteriors, unfolded, h_ptcl_or, correlation,DensitiesX, LogLike, "test_"+str(IterNumber)) 
        IterNumber = IterNumber + 1
        SaveHistograms("test") 
        #Plot2D(ListOf2DPosteriors)
        if IterNumber > args.maxiterations:
            Repeat == False
        if Repeat == False:
            break
        
    return unfolded, ListOfFinePosteriors
        
histograms = []


matrix_name=args.h_matrix
h_reco_getG0_name=args.h_data
h_ptcl_getG0_name = args.h_particle
h_reco_get_bkg_name = args.h_background
outputname=args.h_data+"_unfolded"
nrebin = args.nrebin

rfile_data = TFile(args.rfile_data, 'read')
rfile_particle = TFile(args.rfile_particle, 'read')
rfile_matrix = TFile(args.rfile_matrix, 'read')
rfile_background = TFile(args.rfile_background, 'read')
#GET DATA
h_reco_get = rfile_data.Get(h_reco_getG0_name)
h_reco_get.Rebin(nrebin)
#GET PARTICLE
h_ptcl_get = rfile_particle.Get(h_ptcl_getG0_name)
h_ptcl_get.Rebin(nrebin)
#GET MATRIX
h_response_unf = rfile_matrix.Get(matrix_name)
h_response_unf.Rebin2D(nrebin,nrebin)
h_reco_get_input = rfile_data.Get(h_reco_getG0_name)
h_reco_get_bkg = rfile_background.Get(h_reco_get_bkg_name)
h_reco_get_bkg.Rebin(nrebin)

MyBaronRooUnfold(h_reco_get=h_reco_get,h_ptcl_get=h_ptcl_get,h_response_unf=h_response_unf,h_reco_get_bkg=h_reco_get_bkg, matrix_name=h_response_unf.GetName(), h_ptcl_getG0_name=h_ptcl_get.GetName(), h_reco_getG0_name=h_reco_get.GetName(), h_reco_get_bkg_name=h_reco_get_bkg.GetName(), N=args.N, sampling=args.sampling)

#!/usr/bin/env python
import os
os.environ["MKL_THREADING_LAYER"] = "GNU"
import argparse
#from matplotlib import pyplot as plt
#import matplotlib
import fbu
from fbu import Regularization
import ROOT
import math
import random
import numpy as np
from array import array
from ROOT import RooUnfoldResponse
from ROOT import RooUnfold
from ROOT import RooUnfoldBayes
from ROOT import RooUnfoldSvd
from ROOT import RooUnfoldTUnfold
from ROOT import RooUnfoldIds
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed, TMath, TF1, TLine
import pymc3 as pm
import theano

parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")
parser.add_argument('--N', '-N', type=int, default=100)
parser.add_argument('--rfile_data','-rfile_data', type=str, default="input_original.root")
parser.add_argument('--rfile_particle','-rfile_particle', type=str, default="input_original.root")
parser.add_argument('--rfile_matrix','-rfile_matrix', type=str, default="input_original.root")
parser.add_argument('--rfile_background','-rfile_background', type=str, default="input_original.root")
parser.add_argument('--rfile_compare','-rfile_compare', type=str, default="./outputs/PseudoTop_Reco_top_had_pt_unfolded.root")
parser.add_argument('--h_compare','-h_compare', type=str, default="result_fbu_Mean")
parser.add_argument('--h_data','-h_data', type=str, default="h_data")
parser.add_argument('--h_particle','-h_particle', type=str, default="h_particle")
parser.add_argument('--h_matrix','-h_matrix', type=str, default="h_matrix")
parser.add_argument('--h_background','-h_background', type=str, default="h_background")

parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{t,had} [GeV]")
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)
parser.add_argument('--SplitFromBinLow', '-SplitFromBinLow', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinLow', '-ParameterSplitFromBinLow', type=float, default=1.5)
parser.add_argument('--SplitFromBinHigh', '-SplitFromBinHigh', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinHigh', '-ParameterSplitFromBinHigh', type=float, default=1.5)
parser.add_argument('--maxiterations', '-maxiterations', type=int, default=30)
parser.add_argument('--batch', '-batch', type=int, default=0)
parser.add_argument('--sampling', '-sampling', type=int, default=100)

args = parser.parse_args()

if (args.batch == 1):
    ROOT.gROOT.SetBatch(1)

#const = 5e2*args.nrebin
const = 1.0

def DivideBinWidth(h):
    for j in range(1, h.GetNbinsX()+1):
        h.SetBinContent(j,((h.GetBinContent(j))/(h.GetBinWidth(j))))
        h.SetBinError(j,((h.GetBinError(j))/(h.GetBinWidth(j))))

def PrintCan(can, outputname):
    outputname = outputname.split('/')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.png')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.pdf')

def MakeListFromHisto(hist):
    vals = []
    for i in range(1, hist.GetXaxis().GetNbins()+1):
        val = hist.GetBinContent(i)
        vals.append(val/const)
    return vals

def NormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetXaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetYaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetYaxis().GetNbins()+1):
                migra.SetBinContent(i,j,migra.GetBinContent(i,j) / sum)
    return migra

def CheckNormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(j,i)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetXaxis().GetNbins()+1):
                migra.SetBinContent(j,j,migra.GetBinContent(j,i) / sum)
    return migra

def MakeListResponse(h2):
    vals = []
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        column = []
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            column.append(val)
        vals.append(column)
    return vals

def MakeUnfoldedHisto(reco4bins, h1s, tag = '_unfolded'):
    hname = reco4bins.GetName()+tag
    hist = reco4bins.Clone(hname)
    hist.Reset()
    histMean = reco4bins.Clone(hname+"Mean")
    histMean.Reset()
    i = -1
    for h1 in h1s:
        i = i+1
        h1.Rebin(8)
        h1.Fit("gaus")
        fit = h1.GetFunction("gaus") 
        chi2 = fit.GetChisquare()
        p1 = fit.GetParameter(1)
        p0 = fit.GetParameter(0)
        p2 = fit.GetParameter(2)
        hist.SetBinContent(i+1, p1)
        hist.SetBinError(i+1, p2)
        histMean.SetBinContent(i+1, h1.GetMean())
        histMean.SetBinError(i+1, h1.GetRMS())
    return hist, histMean

def MakeTH1Ds(trace, tag = 'trace', nbins = 600):
    h1s = []
    gmax = -999
    gmin = 1e19
    for line in trace:
        xmin = min(line)
        xmax = max(line)
        gmin = min(xmin, gmin)
        gmax = max(xmax, gmax)
    i = -1
    gmax = -999
    gmin = 1e19
    
    for line in trace:
        gxmin = min(line)
        gxmax = max(line)
        i = i+1
        hname = tag + '_{:}'.format(i)
        h1 = TH1D(hname, hname, nbins, gmin, gmax)
        print("-----------------------")
        k=0
        for val in line:
            h1.Fill(val)
        h1.Scale(1.0/h1.Integral())
        h1s.append(h1)
        print(i)
        print("Appending traces")
        print(h1)
        histograms.append(h1)
    return h1s

def TransposeMatrix(h_response_unf):
    h_responce_transpose = h_response_unf.Clone(h_response_unf.GetName()+"clone")
    h_responce_transpose.Reset()
    for i in range(1,h_response_unf.GetXaxis().GetNbins()+1):
        for j in range(1,h_response_unf.GetXaxis().GetNbins()+1):
            h_responce_transpose.SetBinContent(i,j,h_response_unf.GetBinContent(j,i))
            h_responce_transpose.SetBinError(i,j,h_response_unf.GetBinError(j,i))
    h_responce_transpose.GetXaxis().SetTitle(h_response_unf.GetYaxis().GetTitle())
    h_responce_transpose.GetYaxis().SetTitle(h_response_unf.GetXaxis().GetTitle())
    return h_responce_transpose
    
def SaveHistograms(outputname = "output"):
    outputname = outputname.split('/')
    outfile = TFile('./outputs/'+outputname[len(outputname)-1]+".root", 'recreate')
    outfile.cd()
    for j in range(len(histograms)):
        histograms[j].Write()
    print("End of the unfolding.")
    outfile.Write()
    outfile.Close() 

def PlotPosteriors(ListOfPosteriors, unfolded, h_compare,h_ptcl_or, outputname = ""):
    RepeatIteration = False
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(math.ceil(pow(len(ListOfPosteriors),0.5)),math.ceil(pow(len(ListOfPosteriors),0.5)))
    legends = []
    lines = []
    ptcl_lines = []
    for i in range(len(ListOfPosteriors)):
        ##ListOfPosteriors[i].SetMaximum(ListOfPosteriors[i].GetMaximum()*2.0)
        ##fit = ListOfPosteriors[i].GetFunction("gaus") 
        ##chi2 = fit.GetChisquare()
        ##p1 = fit.GetParameter(1)
        ##p2 = fit.GetParameter(2)
        ##FitIntegral = fit.Integral(p1-10*p2,p1+10*p2) # fit integral , get mean plus minus 10 sigma
        ##PriorIntegral = ListOfPosteriors[i].Integral("width")
        ##if FitIntegral != 0 :
        ##    Percentage = round((100*PriorIntegral/FitIntegral),0)
        ##else: 
        ##    Percentage = 0.0
        c.cd(i+1)
        ##gPad.SetFrameFillColor(10)
        ##if (Percentage < 90):                # 1.CONDITION of iteration, integral of histogram is at least 90% on fit integral
        ##    gPad.SetFillColor(kRed-4)
        ##    RepeatIteration = True
        ##else:
        ##    gPad.SetFillColor(8)
        ##leg = TLegend(0.1,0.5,0.85,0.9)
        ##leg.SetFillStyle(0)
        ##leg.AddEntry(None,"MeanHist = "+str(round(ListOfPosteriors[i].GetMean(),0))+", RMShist = "+str(round(ListOfPosteriors[i].GetRMS(),0))+", MeanFit = "+str(round(p1,0))+", #sigma_{fit} = "+str(round(p2,0)),"")
        ##leg.AddEntry(ListOfPosteriors[i],"Posterior ","l")
        ##leg.AddEntry(fit,"Fit ","l")
        ##leg.AddEntry(None,"#chi^{2}/NDF = "+str(round(chi2/len(ListOfPosteriors),2)),"")
        ##leg.AddEntry(None,"Integral Prior/Fit = "+str(round(Percentage,0))+" %","")
        ##leg.SetBorderSize(0)
        ##legends.append(leg)
        ##ListOfPosteriors[i].SetTitle("Posterior in bin "+str(i+1))
        ##gStyle.SetOptStat(0)
        ListOfPosteriors[i].Draw("colz")
        t = TLine (h_compare.GetBinContent(i+1), ListOfPosteriors[i].GetMinimum(), h_compare.GetBinContent(i+1), ListOfPosteriors[i].GetMaximum())
        t.SetLineColor(2)
        lines.append(t)
        lines[i].Draw("same")
        t2 = TLine (h_ptcl_or.GetBinContent(i+1), ListOfPosteriors[i].GetMinimum(), h_ptcl_or.GetBinContent(i+1), ListOfPosteriors[i].GetMaximum())
        t2.SetLineColor(3)
        ptcl_lines.append(t2)
        ptcl_lines[i].Draw("same")
        ##legends[i].Draw("same")
        ##if (round(chi2/len(ListOfPosteriors),2)) > 20.0: # 2.CONDITION of iteration, chi2/NDF < 20.0 
        ##    RepeatIteration = True
        c.Update()

    c.cd(1)
    gStyle.SetOptStat(0)
    gPad.Modified()
    c.Update()

    PrintCan(c,c.GetName()+"_posteriors")
    PlotRatio(unfolded,h_compare)
    #c2 = TCanvas("s"+outputname,"s"+outputname,0,0,1600, 1600)
    #c2.cd()
    #h_like.Draw("colz")
    gApplication.Run()

def PlotRatio(h_reco_unfolded,h_compare):
    
    gStyle.SetPadLeftMargin(0.12)
    gStyle.SetPadRightMargin(0.12)
    c = TCanvas("canvas_ratio","canvas_ratio",0,0,800, 800)
    c.cd()
    pad1 = TPad("pad1","pad1",0,0.40,1,1)
    pad1.SetTopMargin(0.15)
    pad1.SetBottomMargin(0.01)
    pad1.SetFillStyle(0)
    pad1.SetTicks(1,1)
    pad1.SetBorderMode(0)
    pad1.Draw()
    c.cd()
    pad2 = TPad("pad2","pad2",0,0.01,1,0.422)
    pad2.SetFillStyle(0)
    pad2.SetTopMargin(0.043)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks(1,1)
    pad2.Draw()
    pad2.Modified()
    c.cd()
    pad1.cd()
    gStyle.SetOptStat(0)
    h_compare.SetTitle("")
    h_compare.SetLineColor(2)
    #h_compare.GetYaxis().SetRangeUser(0,h_compare.GetMaximum()*1.5)
    h_compare.GetXaxis().SetTitleSize(34)
    h_compare.GetXaxis().SetTitleFont(43)
    h_compare.GetYaxis().SetTitleSize(27)
    h_compare.GetYaxis().SetTitleFont(43)
    h_compare.GetYaxis().SetTitleOffset(1.5)
    h_compare.GetYaxis().SetTitle("Events")
    h_compare.GetYaxis().SetLabelFont(43)
    h_compare.GetYaxis().SetLabelSize(25)
    legend = TLegend(0.55,0.5,0.85,0.8)
    legend.SetFillStyle(0)
    legend.AddEntry(h_compare,"FBU GerBaudo")
    legend.AddEntry(h_reco_unfolded,"FBU Baron","p")
    legend.SetBorderSize(0)
    h_reco_unfolded.SetLineColor(1)
    h_reco_unfolded.SetMarkerColor(1)
    h_reco_unfolded.SetMarkerStyle(22)
    h_compare.Draw("hist")
    h_reco_unfolded.Draw("same p x0")
    legend.Draw("same")
    pad1.RedrawAxis()
    pad2.cd()
    h_compare_clone = h_compare.Clone(h_compare.GetName()+"_clone")
    h_reco_unfolded_clone = h_reco_unfolded.Clone(h_reco_unfolded.GetName()+"_clone")
    h_compare_clone.Divide(h_compare)
    h_reco_unfolded_clone.Divide(h_compare)
    
    h_compare_clone.GetXaxis().SetTitleSize(27)
    h_compare_clone.GetXaxis().SetTitleFont(43)
    h_compare_clone.GetYaxis().SetTitleSize(27)
    h_compare_clone.GetYaxis().SetTitleFont(43)
    
    h_compare_clone.GetXaxis().SetLabelFont(43)
    h_compare_clone.GetXaxis().SetLabelSize(25)
    h_compare_clone.GetYaxis().SetLabelFont(43)
    h_compare_clone.GetYaxis().SetLabelSize(25)
    
    h_compare_clone.SetMaximum(1.3)
    h_compare_clone.SetMinimum(0.7)
    
    h_compare_clone.GetXaxis().SetTitleOffset(2.5)
    h_compare_clone.GetXaxis().SetTitle(args.title)
    
    #h_compare_clone.GetYaxis().SetTitle("#frac{Unfolded}{Simulation}      ")
    h_compare_clone.GetYaxis().SetTitle("Ratio      ")
    h_compare_clone.Draw("hist")
    h_reco_unfolded_clone.Draw("same p x0")
    pad2.RedrawAxis()
    c.Update()
    histograms.append(c) # here is the crash probably
    PrintCan(c, "ratio")

def MyRooUnfold(matrix_name=args.h_matrix, h_reco_getG0_name=args.h_data, h_ptcl_getG0_name = args.h_particle,h_reco_get_bkg_name = args.h_background,outputname=args.h_data+"_unfolded",nrebin = args.nrebin):

    rfile_data = TFile(args.rfile_data, 'read')
    rfile_particle = TFile(args.rfile_particle, 'read')
    rfile_matrix = TFile(args.rfile_matrix, 'read')
    rfile_background = TFile(args.rfile_background, 'read')

    rfile_comapre = TFile(args.rfile_compare, 'read')
    h_compare = rfile_comapre.Get(args.h_compare)

    #GET DATA
    h_reco_get = rfile_data.Get(h_reco_getG0_name)
    h_reco_get.Rebin(nrebin)
    #GET PARTICLE
    h_ptcl_get = rfile_particle.Get(h_ptcl_getG0_name)
    h_ptcl_get.Rebin(nrebin)
    #GET MATRIX
    h_response_unf = rfile_matrix.Get(matrix_name)
    h_response_unf.ClearUnderflowAndOverflow()
    h_response_unf.GetXaxis().SetRange(1, h_response_unf.GetXaxis().GetNbins() )
    h_response_unf.GetYaxis().SetRange(1, h_response_unf.GetYaxis().GetNbins() )
    h_response_unf.Rebin2D(nrebin,nrebin)
    h_response_unf.SetName("Migration_Matrix_simulation")

    ########### ACCEPTANCY
    h_acc = h_response_unf.ProjectionX("reco_recoandparticleX") # Reco M
    h_acc.Divide(h_reco_get)
    ########### AKCEPTANCE saved in h_acc #############
    ########### EFFICIENCY
    h_eff = h_response_unf.ProjectionY("reco_recoandparticleY") # Ptcl M
    h_eff.Divide(h_ptcl_get)
    
    h_reco_get_input = rfile_data.Get(h_reco_getG0_name)
    h_reco_get_bkg = rfile_background.Get(h_reco_get_bkg_name)
    h_reco_get_bkg.Rebin(nrebin)

    h_reco_get_input_clone=h_reco_get_input.Clone("")
    h_reco_get_input_clone.Add(h_reco_get_bkg,-1)
    h_reco_get_input_clone.Multiply(h_acc)
    
   
    h_reco_or = rfile_data.Get(h_reco_getG0_name)
    h_ptcl_or = rfile_particle.Get(h_ptcl_getG0_name)
    h_ptcl_or.SetMaximum(h_ptcl_or.GetMaximum()*1.5)
    
    h_response_unf_fbu = TransposeMatrix(h_response_unf)
    h_response_unf_fbu_norm = NormalizeResponse(h_response_unf_fbu)
    h_response_unf_fbu_norm.SetName("Migration_Matrix_simulation_transpose")
    histograms.append(h_response_unf_fbu_norm)
    M = MakeListResponse(h_response_unf_fbu_norm)
    D = MakeListFromHisto(h_reco_get_input_clone) 
    T = MakeListFromHisto(h_ptcl_or)
    B = MakeListFromHisto(h_reco_get_bkg)

    lower=[]
    upper=[]

    h_det_div_ptcl=h_reco_get_input_clone.Clone("")
    h_det_div_ptcl.Divide(h_ptcl_or)
    h_det_div_ptcl.Divide(h_eff)
    h_det_div_ptcl.SetName("det_div_ptcl")
    histograms.append(h_det_div_ptcl)

    for l in range(len(D)):
        lower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-args.par)*h_det_div_ptcl.GetBinContent(l+1)/const)
        upper.append(h_reco_get_input_clone.GetBinContent(l+1)*args.par*h_det_div_ptcl.GetBinContent(l+1)/const) 

    M_np = np.array(M)
    D_np = np.array(D)
    B_np = np.array(B)
    T_np = np.array(T)

    basic_model = pm.Model()

    with basic_model:

        priormethod = getattr(pm,'Uniform')
        print(priormethod)
        truthprior = []
        for bin,(l,u) in enumerate(zip(lower,upper)):
            name = 'prior_in_bin_%d'%(bin+1)
            my_default_args = dict(name=name,lower=l,upper=u)
            print(my_default_args)
            my_args = dict(list(my_default_args.items()))
            prior = priormethod(**my_args)
            truthprior.append(prior)
        truth = pm.math.stack(truthprior)
        unfolded = pm.Poisson('unfolded', mu=theano.dot(truth, M_np), observed=D_np)
        print(unfolded)
        #map_estimate = pm.find_MAP(model=basic_model, method='L-BFGS-B')
        #print(map_estimate)
        #self.MAP = map_estimate
        #self.trace = []
        #self.nuisancestrace = []
        print("Now")
        trace = pm.sample( 10000, tune = 1000, cores = 1, chains = 2, nuts_kwargs = None, discard_tuned_samples = True, progressbar = True )
        print("End of Now")
        #print("trace",trace)
    print(pm.summary(trace))
    print("----------------")
    mytrace = [trace['prior_in_bin_%d'%(bin+1)][:] for bin in range(len(T_np))]

    posteriors = MakeTH1Ds(mytrace)


    #print("Done.", mytrace)

    #ListOfPosteriors = []
    #ListOfRealPosteriors = []
    #Binning = []
    #RealBinning = []
    #for k in range(len(D)):
    #    Empty2 = []
    #    Binning.append(Empty2)
    #    help_array = np.arange(lower[k],upper[k],(upper[k]-lower[k])/args.sampling)
    #    Empty3 = []
    #    RealBinning.append(Empty3)
    #    Realhelp_array = np.arange(lower[k]*const/(h_eff.GetBinContent(k+1)),upper[k]*const/(h_eff.GetBinContent(k+1)),(upper[k]-lower[k])*const/(args.sampling*h_eff.GetBinContent(k+1)))
    #    for s in range(len(help_array)):
    #        Binning[k].append(help_array[s])
    #    for j in range(len(Realhelp_array)):
    #        RealBinning[k].append(Realhelp_array[j])
    #    x = array("d",Binning[k])
    #    posteriors = TH1D("poster_"+str(k),"poster_"+str(k), len(Binning[k])-1,x)
    #    Realx = array("d",RealBinning[k])
    #    Realposteriors = TH1D("real_poster_"+str(k),"real_poster_"+str(k), len(RealBinning[k])-1,Realx)
    #    ListOfPosteriors.append(posteriors)
    #    ListOfRealPosteriors.append(Realposteriors)
    #SuperArray = []
    #Myarray1 = []
    #Myarray2 = []
    #SuperArray.append(Myarray1)
    #SuperArray.append(Myarray2)
    #for s in range(args.N):
    #    if s % 10 == 0:
    #        print("Processing ",s)
    #    for m in range(len(D)):
    #        #j = 1
    #        for k in range(len(Binning[m])):
    #            random.seed()
    #            T_random = []
    #            for l in range(len(D)):
    #                T_random.append(random.randrange(int(lower[l]),int(upper[l])))
    #            T_random[m] = ListOfPosteriors[m].GetBinCenter(k+1)        
    #            #print(T_random[m])
    #            R =[]
    #            for r in range(len(D)):
    #                suma = 0.0
    #                for t in range(len(D)):
    #                    #if t != m:
    #                    suma = B[t] + T_random[t]*M[t][r] + suma 
    #                R.append(suma)
    #            R_np = np.array(R)
    #            Likelihood = np.exp(-np.power(D_np-R_np,2) / R_np / 2.) / np.sqrt(R_np)
    #            #Likelihood = 1 / np.sqrt(R_np)
    #            #a = 1.0
    #            LogLike = np.log10(Likelihood)
    #            #for p in range(len(Likelihood)):
    #            #    a = a*Likelihood[p]/1e50
    #            #print("Sum Loglike: ",np.sum(LogLike))
    #            #print("Prod. liike: ",np.prod(Likelihood))
    #            #print("Filling: ",math.exp(np.sum(LogLike)))
    #            SuperArray[0].append(T_random)
    #            SuperArray[1].append(math.exp(np.sum(LogLike)))
    #            #print(math.exp(np.prod(LogLike)/1e30))
    #            ##ListOfPosteriors[m].Fill(T_random[m],np.prod(Likelihood))
    #            ListOfPosteriors[m].Fill(T_random[m],math.exp(np.sum(LogLike)))
    #unfolded = h_ptcl_or.Clone("result_petr")
    #unfolded.Reset()
    #unfolded_max_like = h_ptcl_or.Clone("result_petr_max_like")
    #unfolded_max_like.Reset()
    #Index_max_like = SuperArray[1].index(max(SuperArray[1]))
    #scaters = []
    #for j in range(len(D)):
    #    h_like = TH2D("scater_"+str(j),"scater_"+str(j),100,lower[j]*const/(h_eff.GetBinContent(1+j)),upper[j]*const/(h_eff.GetBinContent(1+j)), 100,min(SuperArray[1]),max(SuperArray[1]))
    #    scaters.append(h_like)
    #for l in range(len(SuperArray[1])):
    #    #print(SuperArray[1][l])
    #    for j in range(len(scaters)):
    #        scaters[j].Fill(SuperArray[0][l][j]*const/(h_eff.GetBinContent(j+1)),SuperArray[1][l])
    ##h_like.Fit("gaus")
    #for j in range(len(scaters)):
    #    histograms.append(scaters[j])
    #for m in range(len(SuperArray[0][Index_max_like])):
    #    unfolded_max_like.SetBinContent(m+1,SuperArray[0][Index_max_like][m]*const)
    #for k in range(len(ListOfPosteriors)):
    #    ListOfPosteriors[k].Fit("gaus")
    #    histograms.append(ListOfPosteriors[k])
    #    binmax = ListOfPosteriors[k].GetMaximumBin()
    #    unfolded.SetBinContent(k+1,(ListOfPosteriors[k].GetXaxis().GetBinCenter(binmax))*const)
    #    for j in range(1,ListOfPosteriors[k].GetXaxis().GetNbins()+1):
    #        ListOfRealPosteriors[k].SetBinContent(j,ListOfPosteriors[k].GetBinContent(j))
    #        ListOfRealPosteriors[k].SetBinError(j,ListOfPosteriors[k].GetBinError(j))
    #    #ListOfRealPosteriors[k].Scale(1.0/ListOfRealPosteriors[k].Integral())
    #    #for m in range(len(ListOfRealPosteriors)):
    #    #    for k in range(1,ListOfRealPosteriors[m].GetXaxis().GetNbins()+1):
    #    #        ListOfRealPosteriors[m].SetBinContent(k,math.exp(ListOfRealPosteriors[m].GetBinContent(k)))
    #    ListOfRealPosteriors[k].Fit("gaus")
    #    histograms.append(ListOfRealPosteriors[k])
    #unfolded.Divide(h_eff)
    #unfolded_max_like.Divide(h_eff)
    #h_ptcl_or.Rebin(args.nrebin)
    #h_compare.Rebin(args.nrebin)
    #histograms.append(h_ptcl_or)
    #histograms.append(unfolded)
    #histograms.append(unfolded_max_like)
    #histograms.append(h_compare)
    SaveHistograms("pymc3")
    ##PlotPosteriors(scaters,unfolded_max_like,h_compare,"test")
    #PlotPosteriors(ListOfRealPosteriors,unfolded_max_like,h_compare,h_ptcl_or,"test")
     
histograms = []

MyRooUnfold()




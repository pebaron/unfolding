#!/usr/bin/env python3

import os
os.environ["MKL_THREADING_LAYER"] = "GNU"
import argparse
import fbu
from fbu import Regularization
import ROOT
import math
import random
import numpy as np
from array import array
import scipy.stats as stats
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed, TMath, TF1, TLine, kBird, TGaxis, TGraph2D
import pandas as pn
import time
from progress.bar import Bar
from Tools import PrintCan, MakeListFromHisto, NormalizeResponse, CheckNormalizeResponse, MakeListResponse, TransposeMatrix, SaveHistograms, PlotPosteriors, PlotRatio, PrepareGlobals

parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")
parser.add_argument('--rfile_data','-rfile_data', type=str, default="./production/out_data_nominal_test.root")
parser.add_argument('--rfile_particle','-rfile_particle', type=str, default="./production/out_data_nominal_test.root")
parser.add_argument('--rfile_matrix','-rfile_matrix', type=str, default="./production/out_data_nominal_test.root")
parser.add_argument('--rfile_background','-rfile_background', type=str, default="./production/out_data_nominal_test.root")
parser.add_argument('--h_data','-h_data', type=str, default="Reco/HadTopPt")
parser.add_argument('--h_particle','-h_particle', type=str, default="Particle/MCHadTopPt")
parser.add_argument('--h_matrix','-h_matrix', type=str, default="Matrix/MigraHadTopPt")
parser.add_argument('--h_background','-h_background', type=str, default="h_background")

parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{t,had} [GeV]")
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)
parser.add_argument('--maxiterations', '-maxiterations', type=int, default=30)
parser.add_argument('--batch', '-batch', type=int, default=0)
parser.add_argument('--counts', '-counts', type=int, default=20000)
parser.add_argument('--tau','-tau', type=float, default=0)
parser.add_argument('--norm','-norm', type=int, default=1)
parser.add_argument('--svd','-svd', type=int, default=0)
parser.add_argument('--outname', '-outname', type=str, default="out")

args = parser.parse_args()

if (args.batch == 1):
    gROOT.SetBatch(1)

def CreateRandomGaus(Dim):
    r = []
    for i in range(Dim):
        r.append(np.random.normal())
    return np.array(r)

def FindReasonableEpsilon(Theta,eps = 1):
    Cycle = True
    while Cycle:
        r = CreateRandomGaus(len(Theta))
        ThetaC, rC = Leapfrog(Theta, r, eps)
        if float('nan') not in rC: 
            Nominator = CalculateLikelihood(ThetaC, tag = "nominator1 in find reasonable eps") 
            if Nominator == "Error":
                Theta = RandomTheta()
                return "Error"
            Nominator = Nominator-0.5*rC.dot(rC)
            Denominator = CalculateLikelihood(Theta, tag = "denominator1 in find reasonable eps")
            if Nominator == "Error":
                Theta = RandomTheta()
                return "Error"
            Denominator = Denominator -0.5*r.dot(r)
            Probability = np.exp(Nominator-Denominator) 
            if CheckNumber(Probability) != "Error":
                break
    if (Probability > 0.5):
        a = 1.0
    else:
        a = -1.0
    while (np.power(Probability,a) > np.power(2.0,-a)):
        eps = np.power(2.0,a)*eps
        ThetaC, rC = Leapfrog(Theta, r, eps)
        if float('nan') not in rC:
            Nominator = CalculateLikelihood(ThetaC, tag = "nominator2 in find reasonable eps") 
            if Nominator == "Error":
                #Theta = RandomTheta()
                return "Error"
            Nominator = Nominator-0.5*rC.dot(rC)
            Denominator = CalculateLikelihood(Theta, tag = "denominator2 in find reasonable eps")
            if Nominator == "Error":
                #Theta = RandomTheta()
                return "Error"
            Denominator = Denominator -0.5*r.dot(r)
            Probability = np.exp(Nominator-Denominator) 
            if CheckNumber(Probability) == "Error":
                return "Error"
    return eps

def IsInputNan(Theta, r, u, v, j, eps):
    nan = float('nan')
    if (nan in Theta) or (nan in r) or (u == nan) or (v == nan) or (j == nan) or (eps == nan):
        return True
    else:
        return False

    
def BuildTree(Theta, r, u, v, j, eps):
    #print("Input buildtree",Theta, r, u, v, j, eps)
    if IsInputNan(Theta, r, u, v, j, eps) or math.isnan(Theta[0]):
        ReturnArray = np.empty(len(Theta),)
        ReturnArray[:] = np.nan
        #print("buildtree, retourning nan",Theta, r, u, v, j, eps )
        return ReturnArray, ReturnArray , ReturnArray, ReturnArray, ReturnArray, float('nan'), float('nan')
    if (j == 0):
        ThetaC, rC = Leapfrog(Theta, r, v*eps)
        if float('nan') in rC: 
            ReturnArray = np.empty(len(Theta),)
            ReturnArray[:] = np.nan
            #print("Vracim nan1", ReturnArray, ReturnArray , float('nan'), float('nan'), float('nan'), float('nan'))
            return ReturnArray, ReturnArray , ReturnArray, ReturnArray, ReturnArray, float('nan'), float('nan')
        check = CalculateLikelihood(ThetaC, writeme=1,tag = "check in buildtree")
        if check != "Error":
            check = check-0.5*rC.dot(rC)
        else:
            ReturnArray = np.empty(len(Theta),)
            ReturnArray[:] = np.nan
            #print("Vracim nan1", ReturnArray, ReturnArray , float('nan'), float('nan'), float('nan'), float('nan'))
            return ReturnArray, ReturnArray , ReturnArray, ReturnArray, ReturnArray, float('nan'), float('nan')
        
        if check == "Error":
            ReturnArray = np.empty(len(Theta),)
            ReturnArray[:] = np.nan
            #print("Vracim nan12", ReturnArray, ReturnArray , float('nan'), float('nan'), float('nan'), float('nan'))
            return ReturnArray, ReturnArray , ReturnArray, ReturnArray, ReturnArray, float('nan'), float('nan')
        if (check >= u):
            nC = 1 
        else:
            nC = 0
        #if (check > np.log(u)-1000): # here 1000 denotes DeltaMax, the value is recommanded
        if (check > u-np.exp(250)): # here 1000 denotes DeltaMax, the value is recommanded
            sC = 1
        else:
            sC = 0
        
        return ThetaC, rC, ThetaC, rC, ThetaC, nC, sC
    else:
        ThetaM, rM, ThetaP, rP, ThetaC, nC, sC = BuildTree(Theta, r, u, v, j-1, eps)
        if (sC == 1):
            if (v == -1):
                ThetaM, rM, lin, lin2, ThetaCC, nCC, sCC = BuildTree(ThetaM, rM, u, v, j-1, eps)
            else:
                lin, lin2, ThetaP, rP, ThetaCC, nCC, sCC = BuildTree(ThetaP, rP, u, v, j-1, eps)
            if ((nCC == 0) and (nC == 0)):
                Prob = 0
            else:
                Prob = nCC/(nCC+nC)
            if (Prob > np.random.random_sample()):
                ThetaC = ThetaCC
            DeltaTheta = ThetaP - ThetaM
            if (DeltaTheta.dot(rM) >= 0.0) and (DeltaTheta.dot(rP) >= 0.0):
                sC = sCC
            else:
                sC = 0.0
            nC = nC + nCC
    return ThetaM, rM, ThetaP, rP, ThetaC, nC, sC

def Leapfrog(Theta, r, eps):
    #print("Leapfrog IN theta, r, eps: ", Theta, r, eps)
    if float('nan') not in Gradient(Theta):
        grad = (eps/2)*Gradient(Theta)
        rW = r + grad
        ThetaW = Theta + eps*rW
        rW = rW + grad
        return ThetaW, rW
        print("Leapfrog OUT theta, r, eps: ", Theta, r, eps)
    else:
        ThetaW.append(float('nan'))
        rW.append(float('nan'))
        print("Leapfrog OUT theta, r, eps, apenduju NAN: ", Theta, r, eps)
        return ThetaW, rW

def CheckNumber(Number):
    if float('-inf') < float(Number) < float('inf'):
        return Number
    else:
        return "Error"

def CheckArray(TestArray):
    if ((float('nan') in TestArray) or (float('-inf') in TestArray) or (float('inf') in TestArray)):
        ReturnArray = np.empty(len(TestArray),)
        ReturnArray[:] = np.nan
        return ReturnArray
    else:
        return TestArray

def RandomTheta():
    while True:
        Theta = []    
        for l in range(len(D)):
            Theta.append((upper[l] - lower[l])*np.random.random_sample() + lower[l])
        test = CalculateLikelihood(Theta, tag = "test in random theta")
        if (test != "Error"):
            break
    return Theta

def Epsilon(Theta, eps_or = 1):
    while True:
        eps = FindReasonableEpsilon(Theta, eps_or)
        #print(eps)
        if eps != "Error":
            break
        else:
            Theta = RandomTheta()
    return eps


def Hamilton(histograms, h_ptcl_or , h_eff , Theta, eps = 1, steps = args.counts, Again = 0, iter = 1, title = ""):   
    if Again == 0:
        ListOfPriors = []
        ListOfPosteriors = []
        SamplingGraphs = []
        LikeGraphs = []
        Binning = []
        for k in range(len(D)):
            help_array = np.arange(lower[k],upper[k],(upper[k]-lower[k])/100) 
            Binning.append(help_array)
            x = array("d",Binning[k])
            posteriors = TH1D("poster_"+str(k)+"iter"+str(iter),"poster_"+str(k)+"iter"+str(iter), len(Binning[k])-1,x)
            priors = TH1D("prior_"+str(k)+"iter"+str(iter),"prior_"+str(k)+"iter"+str(iter), len(Binning[k])-1,x)
            graph = TGraph2D()
            graph.SetName("graph_"+ str(k)+"iter"+str(iter))
            SamplingGraphs.append(graph)
            likegraph = TGraph2D()
            likegraph.SetName("likegraph_"+ str(k)+"iter"+str(iter))
            LikeGraphs.append(likegraph)
            ListOfPosteriors.append(posteriors)       
            ListOfPriors.append(priors)

    ListOfEps = []
    #if iter == 1:
    for i in range(20):
        Theta = RandomTheta()
        eps = Epsilon(Theta)
        ListOfEps.append(eps)
    eps = min(ListOfEps)/2.0
    print('eps: ', eps)

    StopNow = False
    bar = Bar('Processing', fill='#', suffix='%(percent)d%%', max=steps)
    m = 1
    OldLike = -999.0
    while True:
        if (m == 1):
            while True:
                TagAgain = False
                r0 = CreateRandomGaus(len(Theta))
                TempNumber = CalculateLikelihood(Theta, tag = "tempnumber1 in hamilton")
                if TempNumber == OldLike:
                    #print("are same 1")
                    TagAgain = True
                    break
                OldLike = TempNumber
                if TempNumber == "Error":
                    Theta = RandomTheta()
                    eps_old = eps
                    eps = Epsilon(Theta, eps_old)
                    continue
                TempNumber2 = TempNumber-0.5*r0.dot(r0)
            
                #print("TempNumber1", type(CheckNumber(TempNumber)))
                if TempNumber2 != "Error": 
                    #print("TempNumber1", CheckNumber(TempNumber))
                    u = np.random.uniform(0.0, TempNumber2)
                    ThetaMinus = Theta
                    #print("Wrong Theta ", Theta, TempNumber)
                    ThetaPlus = Theta
                    #print("Theta, epsilon 1", Theta, eps)
                    #print("Great1")
                    break
                else:
                    Theta = RandomTheta()
                    eps_old = eps
                    eps = Epsilon(Theta, eps_old)
                    continue
                    #print("Theta, epsilon 2", Theta, eps)
        else:
            while True:
                TagAgain = False
                r0 = CreateRandomGaus(len(Theta))
                TempNumber = CalculateLikelihood(Theta, tag = "tempnumber2 in hamilton")
                #print("TEmp, old: ", TempNumber, OldLike)
                if TempNumber == OldLike:
                    #print("are same: ", TempNumber, OldLike)
                    TagAgain = True
                    break
                OldLike = TempNumber
                if TempNumber == "Error":
                    Theta = RandomTheta()
                    eps_old = eps
                    eps = Epsilon(Theta, eps_old)
                    continue
                TempNumber2 = TempNumber - 0.5*r0.dot(r0)
                if TempNumber2 != "Error": 
                    u = np.random.uniform(0.0, TempNumber2)
                    #print("Wrong theta M1 ", ThetaM1, TempNumber)
                    ThetaMinus = ThetaM1
                    ThetaPlus = ThetaM1
                    #print("Theta, epsilon 3: ", ThetaM1, eps)
                    #print("Great2")
                    break
                else:
                    Theta = RandomTheta()
                    eps_old = eps
                    eps = Epsilon(Theta, eps_old)
                    continue
                    #print("Theta, epsilon 4", Theta, eps)
        #print('eps:', eps)
        if TagAgain:
            Theta = RandomTheta()
            #print("this is happening")
            continue
        n = 1
        s = 1
        j = 0
        rMinus = r0
        rPlus = r0
        QuitThisNan = False
        while (s == 1):
            if QuitThisNan:
                break
                #print("Quit This nan")
            v = np.random.uniform(-1,1)
            if (v < 0):
                ThetaMinus, rMinus, lin, lin2, ThetaC, nC, sC = BuildTree(ThetaMinus, rMinus, u, v, j, eps) 
                if math.isnan(ThetaC[0]) or math.isinf(ThetaC[0]):
                    #print("Here1", ThetaC)
                    QuitThisNan = True
                    continue
                Last = False
                # here arror
            else:
                lin, lin2, ThetaPlus, rPlus, ThetaC, nC, sC = BuildTree(ThetaPlus, rPlus, u, v, j, eps)
                if math.isnan(ThetaC[0]) or math.isinf(ThetaC[0]):
                    #print("Here2", ThetaC)
                    QuitThisNan = True
                    continue
                Last = True
            
            #print("ThetaC, Last", ThetaC, Last)
            
            if (sC == 1):
                Prob = []
                Prob.append(1.0)
                Prob.append(nC/n)
                PriorWeight = CalculatePrior(ThetaC)
                bar.next()
                for k in range(len(Theta)):
                    ListOfPosteriors[k].Fill(ThetaC[k], min(Prob))
                    ListOfPriors[k].Fill(ThetaC[k], PriorWeight)
                    if ListOfPosteriors[len(Theta)-1].GetEntries() >= steps:
                        StopNow = True
                        bar.finish()
                        break
                    if (Last):
                        SamplingGraphs[k].SetPoint(m,ThetaC[k], rPlus[k], min(Prob))
                    else:
                        SamplingGraphs[k].SetPoint(m,ThetaC[k], rMinus[k], min(Prob))
            n = n + nC
            #print("ThetaPLus, ThetaMinus", ThetaPlus , ThetaMinus)
            DeltaTheta = ThetaPlus - ThetaMinus
            if (DeltaTheta.dot(rMinus) >= 0) and (DeltaTheta.dot(rPlus) >= 0):
                s = sC
            else:
                s = 0
            j = j + 1
            if StopNow:
                break
        if StopNow:
            break
        ThetaM1 = ThetaC
        Theta = ThetaC
        m = m + 1

    CorrectErrors(ListOfPosteriors)
    CorrectErrors(ListOfPriors)

    for l in range(len(ListOfPosteriors)):
        if (args.norm == 1):
            ListOfPosteriors[l].Scale(1.0/ListOfPosteriors[l].Integral())
            ListOfPriors[l].Scale(1.0/ListOfPriors[l].Integral())
        histograms.append(ListOfPosteriors[l])
        histograms.append(ListOfPriors[l])
        histograms.append(SamplingGraphs[l])
    RepeatCheck = False
    Unfolded_Baron, ListOfPost, RepeatCheck = PlotPosteriors(histograms, h_ptcl_or, h_eff, ListOfPosteriors, SamplingGraphs, ListOfPriors, outputname=args.outname+"_iter"+str(iter), title = title)
    return Unfolded_Baron, ListOfPost, RepeatCheck, eps, ThetaM1

def CorrectErrors(ListOfHistos):
    for j in range(len(ListOfHistos)):
        for i in range(1, ListOfHistos[j].GetXaxis().GetNbins()):
            if ListOfHistos[j].GetBinContent(i) != 0.0:
                ListOfHistos[j].SetBinError(i,1.0/np.sqrt(ListOfHistos[j].GetBinContent(i)))
            #else:
            #    print("No error correction in ", ListOfHistos[j], " and bin: ", i)

def Gradient(T_random, step = 1e-1):
    gradient = []
    #print(T_random)
    for i in range(len(T_random)):
        T_random_plus = T_random.copy()
        T_random_plus[i] = T_random_plus[i] + step
        T_random_minus = T_random.copy()
        T_random_minus[i] = T_random_minus[i] - step
        R1 = np.array(T_random_plus).dot(M)       
        R2 = np.array(T_random_minus).dot(M)       
        R1 = R1.astype(np.int64)
        R2 = R2.astype(np.int64) 
        D_int64 = D.astype(np.int64)
        #delta = np.sum(np.log(R2/R1)+0.5*(R1-R2)*(np.square(D)/(R1*R2)-1))/(2*step)
        #delta = np.sum( np.log(stats.poisson.pmf(D_int64, R1)/stats.poisson.pmf(D_int64, R2)) )/(2*step)
        delta = np.sum(R2-R1 + D*(np.log(R1)-np.log(R2))) / (2.0*step)
        #print(delta)
        gradient.append(delta)
    return np.array(CheckArray(gradient))

def CalculateLikelihood(T_random, writeme = 0, tag = ""):
    R = np.array(T_random).dot(M)  # CREATING RECO
    if args.svd == 1:
        C = np.zeros(M.shape)
        np.fill_diagonal(C, -2.0)
        C[0,0] = -1.0
        C[len(T_random)-1,len(T_random)-1] = -1.0
        OfsetDiag = np.ones(len(T_random)-1)
        np.fill_diagonal(C[1:], OfsetDiag)
        np.fill_diagonal(C[:,1:], OfsetDiag)
        w = C.dot(T_random)
        lh = np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R) + args.tau*w.dot(w)
    else:
        D_int64 = D.astype(np.int64)
        R_int64 = R.astype(np.int64)
        lh = np.log(stats.poisson.pmf(D_int64, R_int64))
    #print("Like: ", CheckNumber(lh.sum()), tag)
    return CheckNumber(lh.sum())

def CalculatePrior(T_random):
    R = np.array(T_random).dot(M)  # CREATING RECO
    if args.svd == 1:
        C = np.zeros(M.shape)
        np.fill_diagonal(C, -2.0)
        C[0,0] = -1.0
        C[len(T_random)-1,len(T_random)-1] = -1.0
        OfsetDiag = np.ones(len(T_random)-1)
        np.fill_diagonal(C[1:], OfsetDiag)
        np.fill_diagonal(C[:,1:], OfsetDiag)
        w = C.dot(T_random)
        lh = args.tau*w.dot(w)
    else:
        lh = - np.power(args.tau,2)*np.sum(np.power(R-D,2.0))/2.0
    return CheckNumber(np.exp(lh))
        
histograms = []
#READ FILES
rfile_data = TFile(args.rfile_data, 'read')
rfile_particle = TFile(args.rfile_particle, 'read')
rfile_matrix = TFile(args.rfile_matrix, 'read')
rfile_background = TFile(args.rfile_background, 'read')
#GET DATA
h_reco_get = rfile_data.Get(args.h_data)
h_reco_get = rfile_particle.Get(args.h_particle) #closure
h_reco_get.Rebin(args.nrebin)
#GET PARTICLE
h_ptcl_get = rfile_particle.Get(args.h_particle)
h_ptcl_get.Rebin(args.nrebin)

h_ptcl_or = rfile_particle.Get(args.h_particle)
#GET MATRIX
h_response_unf = rfile_matrix.Get(args.h_matrix)
h_response_unf.Rebin2D(args.nrebin,args.nrebin)
#GET NO BINNED RECO
h_reco_get_input = rfile_data.Get(args.h_data)
h_reco_get_input = rfile_particle.Get(args.h_particle) # closureS

h_reco_get_bkg = rfile_background.Get(args.h_background)
h_reco_get_bkg.Rebin(args.nrebin)

# PREPARING ARRAYS
M, D , lower, upper, h_ptcl_or, h_acc, h_eff = PrepareGlobals(h_reco_get_bkg = h_reco_get_bkg, h_ptcl_or = h_ptcl_or, h_reco_get_input_no_rebin = h_reco_get_input, h_reco_get=h_reco_get,h_ptcl_get=h_ptcl_get,h_response_unf=h_response_unf, matrix_name=args.h_matrix, h_reco_getG0_name=args.h_data, h_ptcl_getG0_name = args.h_particle,outputname=args.h_data+"_unfolded",nrebin = args.nrebin, sampling=10000,par = args.par) # h_reco_get_bkg=h_reco_get_bkg, h_reco_get_bkg_name=h_reco_get_bkg.GetName(), ..., , matrix_name=h_response_unf.GetName(), h_ptcl_getG0_name=h_ptcl_get.GetName(), h_reco_getG0_name=h_reco_get.GetName()
# MAIN FUNCTION
#lower = [11103.682388924663, 1972.755083179298, 700.0, 100.0]
#upper = [18506.13731487444, 3287.9251386321635, 214.45544554455446, 220.0]
#lower = [35000, 8000, 800, 100]
#upper = [50000, 12000, 1500, 200]


lowerold = lower
upperold = upper
print("lower: ", lower)
print("upper: ", upper)

FirstTheta = [0,0]
Unfolded_Baron, ListOfPost, Repeat, epsin, IterTheta = Hamilton(histograms = histograms, h_ptcl_or = h_ptcl_or, h_eff = h_eff,Theta = FirstTheta, eps = 1, steps = args.counts, Again = 0, iter = 1)
# ITERATIONS
j = 2
while Repeat:
    print("Runnig iteration number: ",j)
    lower = []
    upper = []
    for l in range(len(D)):
        ListOfPost[l].Fit("gaus", "L")
        fit = ListOfPost[l].GetFunction("gaus") 
        #ListOfPost[l].Draw()
        #fit.Draw("same")
        #print("Zadej lower:")
        #lowerVal = int(input())
        #print("Zadej upper:")
        #upperVal = int(input())
        #lower.append(lowerVal)
        #upper.append(upperVal)
        p1 = fit.GetParameter(1)
        if p1 < 0.0:
            lower = lowerold
            upper = upperold
            break
        p2 = fit.GetParameter(2)
        #lower.append(lowerVal)
        #upper.append(upperVal)
        if p1-4*p2 <= 0:
           lower.append(0) 
        else:
            lower.append(p1-4*p2)
        upper.append(p1+4*p2)
        print("lower: ", lower)
        print("upper: ", upper)
    h_reco_unfolded_Baron, ListOfFinePosteriors, Repeat, epsin, IterTheta = Hamilton(histograms = histograms, h_ptcl_or = h_ptcl_or, h_eff = h_eff, Theta = IterTheta, eps = epsin, steps = args.counts, Again = 0, iter = j, title = args.title)
    if j == args.maxiterations:
        break
    if (Repeat == False):
        break
    j = j+1

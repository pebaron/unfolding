#!/usr/bin/env python3

import os
os.environ["MKL_THREADING_LAYER"] = "GNU"
import argparse
from matplotlib import pyplot as plt
import fbu
from fbu import Regularization
import ROOT
import math
from array import array
from ROOT import RooUnfoldResponse
from ROOT import RooUnfold
from ROOT import RooUnfoldBayes
from ROOT import RooUnfoldSvd
from ROOT import RooUnfoldTUnfold
from ROOT import RooUnfoldIds
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TLine, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed
from Hamilton5 import Hamilton
import numpy as np
from array import array

parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")

parser.add_argument('--rfile_data','-rfile_data', type=str, default="input.root")
parser.add_argument('--rfile_particle','-rfile_particle', type=str, default="input.root")
parser.add_argument('--rfile_matrix','-rfile_matrix', type=str, default="input.root")
parser.add_argument('--rfile_background','-rfile_background', type=str, default="input.root")
#parser.add_argument('--h_data','-h_data', type=str, default="h_data")
#parser.add_argument('--h_particle','-h_particle', type=str, default="h_particle")
#parser.add_argument('--h_matrix','-h_matrix', type=str, default="h_matrix")
#parser.add_argument('--h_background','-h_background', type=str, default="h_background")
parser.add_argument('--h_data','-h_data', type=str, default="Reco/TopPairEta") #Reco/HadTopPt
parser.add_argument('--h_particle','-h_particle', type=str, default="Particle/MCTopPairEta")
parser.add_argument('--h_matrix','-h_matrix', type=str, default="Matrix/MigraTopPairEta")
parser.add_argument('--h_background','-h_background', type=str, default="h_background")
parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{t,had} [GeV]")
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)
parser.add_argument('--SplitFromBinLow', '-SplitFromBinLow', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinLow', '-ParameterSplitFromBinLow', type=float, default=1.5)
parser.add_argument('--SplitFromBinHigh', '-SplitFromBinHigh', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinHigh', '-ParameterSplitFromBinHigh', type=float, default=1.5)
parser.add_argument('--maxiterations', '-maxiterations', type=int, default=30)
parser.add_argument('--tau','-tau', type=float, default=1.0)
parser.add_argument('--batch', '-batch', type=int, default=0)
parser.add_argument('--closure', '-closure', type=int, default=0)
parser.add_argument('--outname', '-outname', type=str, default="out")
parser.add_argument('--drawopt', '-drawopt', type=str, default="hist same")
parser.add_argument('--other', '-other', type=int, default=0) # other methods
parser.add_argument('--BinCoef', '-BinCoef', type=float, default=20.0) # other methods

args = parser.parse_args()

print("-----------------", args)
if (args.batch == 1):
    ROOT.gROOT.SetBatch(1)

def DivideBinWidth(h):
    for j in range(1, h.GetNbinsX()+1):
        h.SetBinContent(j,((h.GetBinContent(j))/(h.GetBinWidth(j))))
        h.SetBinError(j,((h.GetBinError(j))/(h.GetBinWidth(j))))

def PrintCan(can, outputname):
    outputname = outputname.split('/')
    can.Print('./png/'+outputname[len(outputname)-1] + '_' + args.outname + '.png')
    #can.Print('./pic/'+outputname[len(outputname)-1] + '.pdf')

def MakeListFromHisto(hist):
    vals = []
    for i in range(1, hist.GetXaxis().GetNbins()+1):
        val = hist.GetBinContent(i)
        vals.append(val)
    return vals

def NormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetXaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetYaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetYaxis().GetNbins()+1):
                migra.SetBinContent(i,j,migra.GetBinContent(i,j) / sum)
    return migra

def CheckNormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(j,i)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetXaxis().GetNbins()+1):
                migra.SetBinContent(j,j,migra.GetBinContent(j,i) / sum)
    return migra

def MakeListResponse(h2):
    vals = []
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        column = []
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            column.append(val)
        vals.append(column)
    return vals

def MakeUnfoldedHisto(reco4bins, h1s, tag = '_unfolded'):
    hname = reco4bins.GetName()+tag
    hist = reco4bins.Clone(hname)
    hist.Reset()
    histMean = reco4bins.Clone(hname+"Mean")
    histMean.Reset()
    i = -1
    for h1 in h1s:
        i = i+1
        h1.Rebin(8)
        h1.Fit("gaus", "q")
        fit = h1.GetFunction("gaus") 
        chi2 = fit.GetChisquare()
        p1 = fit.GetParameter(1)
        p0 = fit.GetParameter(0)
        p2 = fit.GetParameter(2)
        hist.SetBinContent(i+1, p1)
        hist.SetBinError(i+1, p2)
        histMean.SetBinContent(i+1, h1.GetMean())
        histMean.SetBinError(i+1, h1.GetRMS())
    return hist, histMean

def MakeTH1Ds(trace, lower , upper , BinCoef = 1.0, tag = 'trace', nbins = 600):
    h1s = []
    gmax = -999
    gmin = 1e19
    for line in trace:
        xmin = min(line)
        xmax = max(line)
        gmin = min(xmin, gmin)
        gmax = max(xmax, gmax)
    i = -1
    gmax = -999
    gmin = 1e19
    Binning = []    
    for line in trace:
        gxmin = min(line)
        gxmax = max(line)
        i = i+1
        hname = tag + '_{:}'.format(i)
        help_array = np.arange(lower[i],upper[i],(upper[i]-lower[i])/(100*BinCoef)) 
        Binning.append(help_array)
        x = array("d",Binning[i])
        h1 = TH1D(hname, hname, len(Binning[i])-1, x)
        #h1 = TH1D(hname, hname, nbins, gmin, gmax)
        print("-----------------------")
        k=0
        for val in line:
            h1.Fill(val)
        h1s.append(h1)
        print(i)
        print("Appending traces")
        print(h1)
        histograms.append(h1)
    return h1s

def TransposeMatrix(h_response_unf):
    h_responce_transpose = h_response_unf.Clone(h_response_unf.GetName()+"clone")
    h_responce_transpose.Reset()
    for i in range(1,h_response_unf.GetXaxis().GetNbins()+1):
        for j in range(1,h_response_unf.GetXaxis().GetNbins()+1):
            h_responce_transpose.SetBinContent(i,j,h_response_unf.GetBinContent(j,i))
            h_responce_transpose.SetBinError(i,j,h_response_unf.GetBinError(j,i))
    h_responce_transpose.GetXaxis().SetTitle(h_response_unf.GetYaxis().GetTitle())
    h_responce_transpose.GetYaxis().SetTitle(h_response_unf.GetXaxis().GetTitle())
    return h_responce_transpose
    
def SaveHistograms(outputname = "output"):
    outputname = outputname.split('/')
    outfile = TFile('./outputs/'+outputname[len(outputname)-1]+".root", 'recreate')
    outfile.cd()
    for j in range(len(histograms)):
        histograms[j].Write()
    print("End of the unfolding.")
    outfile.Write()
    outfile.Close() 

def PlotPosteriors(h_ptcl,ListOfPosteriors, outputname = ""):
    RepeatIteration = False
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(math.ceil(pow(len(ListOfPosteriors),0.5)),math.ceil(pow(len(ListOfPosteriors),0.5)))
    legends = []
    for i in range(len(ListOfPosteriors)):
        ListOfPosteriors[i].SetMaximum(ListOfPosteriors[i].GetMaximum()*2.0)
        ListOfPosteriors[i].Fit("gaus", "q0")
        fit = ListOfPosteriors[i].GetFunction("gaus") 
        if fit:
            chi2 = fit.GetChisquare()
            p1 = fit.GetParameter(1)
            #p0 = fit.GetParameter(0)
            p2 = fit.GetParameter(2)
            FitIntegral = fit.Integral(p1-10*p2,p1+10*p2) # fit integral , get mean plus minus 10 sigma
            PriorIntegral = ListOfPosteriors[i].Integral("width")
            Percentage = round((100*PriorIntegral/FitIntegral),0)
            c.cd(i+1)
            gPad.SetFrameFillColor(10)
            if (Percentage < 90):                # 1.CONDITION of iteration, integral of histogram is at least 90% on fit integral
                gPad.SetFillColor(kRed-4)
                RepeatIteration = True
            else:
                gPad.SetFillColor(8)
            leg = TLegend(0.1,0.5,0.85,0.9)
            leg.SetFillStyle(0)
            leg.AddEntry(None,"MeanHist = "+str(round(ListOfPosteriors[i].GetMean(),0))+", RMShist = "+str(round(ListOfPosteriors[i].GetRMS(),0))+", MeanFit = "+str(round(p1,0))+", #sigma_{fit} = "+str(round(p2,0)),"")
            leg.AddEntry(ListOfPosteriors[i],"Posterior ","l")
            leg.AddEntry(fit,"Fit ","l")
            leg.AddEntry(None,"#chi^{2}/NDF = "+str(round(chi2/len(ListOfPosteriors),2)),"")
            print("Chi2: ", chi2, ", NDF: ", len(ListOfPosteriors))
            leg.AddEntry(None,"Integral Prior/Fit = "+str(round(Percentage,0))+" %","")

        else:
            leg = TLegend(0.1,0.5,0.85,0.9)
            leg.SetFillStyle(0)
            leg.AddEntry(None,"MeanHist = "+str(round(ListOfPosteriors[i].GetMean(),0))+", RMShist = "+str(round(ListOfPosteriors[i].GetRMS(),0)),"")
            leg.AddEntry(ListOfPosteriors[i],"Posterior ","l")
        leg.SetBorderSize(0)
        legends.append(leg)
        ListOfPosteriors[i].SetTitle("Posterior in bin "+str(i+1))
        gStyle.SetOptStat(0)
        #ListOfPosteriors[i].GetYaxis().SetRangeUser(0.01, 1.0)
        ListOfPosteriors[i].GetXaxis().SetRangeUser(ListOfPosteriors[i].GetMean()-4*ListOfPosteriors[i].GetRMS(), ListOfPosteriors[i].GetMean()+4*ListOfPosteriors[i].GetRMS())
        ListOfPosteriors[i].Draw("hist")
        if fit:
            fit.SetName("fit"+str(i+1))
            fit.Draw("same")
        #line = TLine(h_ptcl.GetBinContent(i+1),0,h_ptcl.GetBinContent(i+1),ListOfPosteriors[i].GetMaximum())
        #line.SetLineColor(3)
        #lines.append(line)
        #lines[i].SetLineColor(3)
        #lines[i].Draw("same")
        legends[i].Draw("same")
        

    input("Zkontroluj posteriory...")

    #c.cd(1)
    gStyle.SetOptStat(0)
    gPad.Modified()
    c.Update()
    PrintCan(c,c.GetName()+"_posteriors")
    return RepeatIteration

def PlotRatio(h_reco_unfolded_Baron_Reg2, h_unfolded2,h_ptcl_get2,h_reco_unfolded_roof2, h_reco_unfolded_svd2, h_reco_unfolded_T2,h_reco_unfolded_Ids2,h_reco_unfolded_Baron2, svd_par, Ids_par ,outputname="test.png"):
    
    drawopt = args.drawopt
    gStyle.SetPadLeftMargin(0.15)
    gStyle.SetPadRightMargin(0.12)
    c = TCanvas("canvas_"+outputname,"canvas_"+outputname,0,0,800, 800)
    c.cd()
    pad1 = TPad("pad1"+outputname,"pad1"+outputname,0,0.40,1,1)
    pad1.SetTopMargin(0.15)
    pad1.SetBottomMargin(0.01)
    pad1.SetFillStyle(0)
    pad1.SetTicks(1,1)
    pad1.SetBorderMode(0)
    pad1.Draw()
    c.cd()
    pad2 = TPad("pad2"+outputname,"pad2"+outputname,0,0.01,1,0.422)
    pad2.SetFillStyle(0)
    pad2.SetTopMargin(0.043)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks(1,1)
    pad2.Draw()
    ##pad2.Modified()
    #c.cd()
    #pad1.cd()
    gStyle.SetOptStat(0)
    ###if args.closure == 0:
    ###    h_reco_unfolded_roof2.SetTitle("")
    ###    h_reco_unfolded_roof2.SetLineColor(1)
    ###    h_reco_unfolded_roof2.SetLineStyle(2)
    ###    h_reco_unfolded_roof2.GetYaxis().SetRangeUser(0,h_reco_unfolded_roof2.GetMaximum()*2)
    ###    h_reco_unfolded_roof2.GetXaxis().SetTitleSize(34)
    ###    h_reco_unfolded_roof2.GetXaxis().SetTitleFont(43)
    ###    h_reco_unfolded_roof2.GetYaxis().SetTitleSize(27)
    ###    h_reco_unfolded_roof2.GetYaxis().SetTitleFont(43)
    ###    h_reco_unfolded_roof2.GetYaxis().SetTitleOffset(1.5)
    ###    h_reco_unfolded_roof2.GetYaxis().SetTitle("Events")
    ###    h_reco_unfolded_roof2.GetYaxis().SetLabelFont(43)
    ###    h_reco_unfolded_roof2.GetYaxis().SetLabelSize(25)
    ###    h_reco_unfolded_roof2.GetYaxis().SetMaxDigits(3)
    ###else:
    h_ptcl_get2.SetTitle("") 
    h_ptcl_get2.SetLineColor(2)
    h_ptcl_get2.GetYaxis().SetRangeUser(0,h_ptcl_get2.GetMaximum()*1.5)
    h_ptcl_get2.GetXaxis().SetTitleSize(34)
    h_ptcl_get2.GetXaxis().SetTitleFont(43)
    h_ptcl_get2.GetYaxis().SetTitleSize(27)
    h_ptcl_get2.GetYaxis().SetTitleFont(43)
    h_ptcl_get2.GetYaxis().SetTitleOffset(1.5)
    h_ptcl_get2.GetYaxis().SetTitle("Events")
    h_ptcl_get2.GetYaxis().SetLabelFont(43)
    h_ptcl_get2.GetYaxis().SetLabelSize(25)
    h_ptcl_get2.GetYaxis().SetMaxDigits(3)
    legend = TLegend(0.55,0.5,0.85,0.8)
    legend.SetFillStyle(0)
    h_ptcl_get2.SetLineColor(1)
    h_ptcl_get2.SetLineStyle(2)
    legend.AddEntry(h_ptcl_get2,"Particle","l")
    legend.AddEntry(h_reco_unfolded_Baron2,"FBU Baron","l")
    legend.AddEntry(h_reco_unfolded_Baron_Reg2,"FBU Baron Reg. #tau = "+str(args.tau),"l")
    legend.AddEntry(h_unfolded2,"FBU Gerbaudo","l")
    ###legend.AddEntry(h_reco_unfolded_roof2,"D'Agostini RooUnfold","p") #, par. 4
    #
    #legend.AddEntry(h_reco_unfolded_svd2,"SVD RooUnfold, par. " + str(svd_par),"p")
    #legend.AddEntry(h_reco_unfolded_T2,"T RooUnfold","p")
    #legend.AddEntry(h_reco_unfolded_Ids2,"Ids RooUnfold, par. " + str(Ids_par),"p")
    legend.SetBorderSize(0)
    h_reco_unfolded_Baron2.SetLineColor(2)
    h_reco_unfolded_Baron2.SetMarkerColor(2)
    h_reco_unfolded_Baron2.SetMarkerStyle(22)
    h_reco_unfolded_Baron_Reg2.SetLineColor(3)
    h_reco_unfolded_Baron_Reg2.SetLineWidth(2)
    h_reco_unfolded_Baron_Reg2.SetMarkerColor(3)
    h_reco_unfolded_Baron_Reg2.SetMarkerStyle(22)
    h_unfolded2.SetMarkerColor(4)
    h_unfolded2.SetLineColor(4)
    h_unfolded2.SetMarkerStyle(20)
    h_reco_unfolded_svd2.SetMarkerColor(4)
    h_reco_unfolded_svd2.SetLineColor(4)
    h_reco_unfolded_svd2.SetMarkerStyle(5)
    h_reco_unfolded_T2.SetMarkerColor(7)
    h_reco_unfolded_T2.SetLineColor(7)
    h_reco_unfolded_T2.SetMarkerStyle(34)
    h_reco_unfolded_Ids2.SetMarkerColor(8)
    h_reco_unfolded_Ids2.SetLineColor(8)
    h_reco_unfolded_Ids2.SetMarkerStyle(3)
    h_reco_unfolded_roof2.SetLineColor(1)
    h_reco_unfolded_roof2.SetMarkerColor(1)
    h_reco_unfolded_roof2.SetMarkerStyle(22)
    pad1.cd()
    ###if args.closure == 0:
    ###    h_reco_unfolded_roof2.Draw("hist same p0")
    ###else:
    ###    h_ptcl_get2.Draw(drawopt)  
    h_ptcl_get2.Draw(drawopt)  
    h_unfolded2.Draw(drawopt)
    if args.other == 1:
        h_reco_unfolded_svd2.Draw(drawopt)
        h_reco_unfolded_T2.Draw(drawopt)
        h_reco_unfolded_Ids2.Draw(drawopt)
    h_reco_unfolded_Baron2.Draw(drawopt)
    h_reco_unfolded_Baron_Reg2.Draw(drawopt)
    legend.Draw("same")
    pad1.RedrawAxis()
    pad2.cd()
    h_reco_unfolded_clone = h_unfolded2.Clone(h_unfolded2.GetName()+"_clone")
    ###h_reco_unfolded_roof2_clone = h_reco_unfolded_roof2.Clone(h_reco_unfolded_roof2.GetName()+"_clone")
    h_ptcl_get2_clone = h_ptcl_get2.Clone(h_ptcl_get2.GetName()+"_clone")
    if args.other == 1:
        h_reco_unfolded_svd2_clone = h_reco_unfolded_svd2.Clone(h_reco_unfolded_svd2.GetName()+"_clone")
        h_reco_unfolded_T2_clone = h_reco_unfolded_T2.Clone(h_reco_unfolded_T2.GetName()+"_clone")
        h_reco_unfolded_Ids2_clone = h_reco_unfolded_Ids2.Clone(h_reco_unfolded_Ids2.GetName()+"_clone")
    h_reco_unfolded_Baron2_clone = h_reco_unfolded_Baron2.Clone(h_reco_unfolded_Baron2.GetName()+"_clone")
    h_reco_unfolded_Baron_Reg2_clone = h_reco_unfolded_Baron_Reg2.Clone(h_reco_unfolded_Baron_Reg2.GetName()+"_clone")
    pad2.cd()
    if args.closure == 0:
        h_ptcl_get2_clone.Divide(h_ptcl_get2)
        h_reco_unfolded_clone.Divide(h_ptcl_get2)
        h_reco_unfolded_Baron2_clone.Divide(h_ptcl_get2)
        h_reco_unfolded_Baron_Reg2_clone.Divide(h_ptcl_get2)
        ##h_reco_unfolded_roof2_clone.Divide(h_ptcl_get2)
        ##if args.other == 1:
        ##    h_reco_unfolded_svd2_clone.Divide(h_ptcl_get2)
        ##    h_reco_unfolded_T2_clone.Divide(h_ptcl_get2)
        ##    h_reco_unfolded_Ids2_clone.Divide(h_ptcl_get2)
        ##h_reco_unfolded_Baron2_clone.Divide(h_ptcl_get2)
        ##h_reco_unfolded_Baron_Reg2_clone.Divide(h_ptcl_get2)
        ##h_reco_unfolded_roof2_clone.GetXaxis().SetTitleSize(27)
        ##h_reco_unfolded_roof2_clone.GetXaxis().SetTitleFont(43)
        ##h_reco_unfolded_roof2_clone.GetYaxis().SetTitleSize(27)
        ##h_reco_unfolded_roof2_clone.GetYaxis().SetTitleFont(43)
        ##h_reco_unfolded_roof2_clone.GetXaxis().SetLabelFont(43)
        ##h_reco_unfolded_roof2_clone.GetXaxis().SetLabelSize(25)
        ##h_reco_unfolded_roof2_clone.GetYaxis().SetLabelFont(43)
        ##h_reco_unfolded_roof2_clone.GetYaxis().SetLabelSize(25)
        ##h_reco_unfolded_roof2_clone.SetMaximum(1.05)
        ##h_reco_unfolded_roof2_clone.SetMinimum(0.95)
        ##h_reco_unfolded_roof2_clone.GetXaxis().SetTitleOffset(2.5)
        ##h_reco_unfolded_roof2_clone.GetYaxis().SetTitleOffset(1.7)
        ##h_reco_unfolded_roof2_clone.GetXaxis().SetTitle(args.title)
        ##h_reco_unfolded_roof2_clone.GetYaxis().SetTitle("#frac{Unfolded}{Particle}      ")
        
        h_ptcl_get2_clone.GetXaxis().SetTitleSize(27)
        h_ptcl_get2_clone.GetXaxis().SetTitleFont(43)
        h_ptcl_get2_clone.GetYaxis().SetTitleSize(27)
        h_ptcl_get2_clone.GetYaxis().SetTitleFont(43)
        h_ptcl_get2_clone.GetXaxis().SetLabelFont(43)
        h_ptcl_get2_clone.GetXaxis().SetLabelSize(25)
        h_ptcl_get2_clone.GetYaxis().SetLabelFont(43)
        h_ptcl_get2_clone.GetYaxis().SetLabelSize(25)
        h_ptcl_get2_clone.SetMaximum(1.20)
        h_ptcl_get2_clone.SetMinimum(0.8)
        h_ptcl_get2_clone.GetXaxis().SetTitleOffset(2.5)
        h_ptcl_get2_clone.GetYaxis().SetTitleOffset(1.7)
        h_ptcl_get2_clone.GetXaxis().SetTitle(args.title)
        h_ptcl_get2_clone.GetYaxis().SetTitle("#frac{Unfolded}{Particle}      ")

        ###h_reco_unfolded_roof2_clone.Draw("hist same p0")
        h_ptcl_get2_clone.Draw(drawopt)
    else:
        h_reco_unfolded_clone.Divide(h_ptcl_get2)
        h_ptcl_get2_clone.Divide(h_ptcl_get2)
        if args.other == 1:
            h_reco_unfolded_svd2_clone.Divide(h_ptcl_get2)
            h_reco_unfolded_T2_clone.Divide(h_ptcl_get2)
            h_reco_unfolded_Ids2_clone.Divide(h_ptcl_get2)
        h_reco_unfolded_Baron2_clone.Divide(h_ptcl_get2)
        h_reco_unfolded_Baron_Reg2_clone.Divide(h_ptcl_get2)
        h_ptcl_get2_clone.GetXaxis().SetTitleSize(27)
        h_ptcl_get2_clone.GetXaxis().SetTitleFont(43)
        h_ptcl_get2_clone.GetYaxis().SetTitleSize(27)
        h_ptcl_get2_clone.GetYaxis().SetTitleFont(43)
        h_ptcl_get2_clone.GetXaxis().SetLabelFont(43)
        h_ptcl_get2_clone.GetXaxis().SetLabelSize(25)
        h_ptcl_get2_clone.GetYaxis().SetLabelFont(43)
        h_ptcl_get2_clone.GetYaxis().SetLabelSize(25)
        h_ptcl_get2_clone.SetMaximum(1.20)
        h_ptcl_get2_clone.SetMinimum(0.8)
        h_ptcl_get2_clone.GetXaxis().SetTitleOffset(2.5)
        h_ptcl_get2_clone.GetYaxis().SetTitleOffset(1.7)
        h_ptcl_get2_clone.GetXaxis().SetTitle(args.title)
        h_ptcl_get2_clone.GetYaxis().SetTitle("#frac{Unfolded}{D'Agostini}      ")
        h_ptcl_get2_clone.Draw(drawopt)
    #h_ptcl_get2_clone.Draw(drawopt)
    h_reco_unfolded_clone.Draw(drawopt)
    if args.other == 1:
        h_reco_unfolded_svd2_clone.Draw(drawopt)
        h_reco_unfolded_T2_clone.Draw(drawopt)
        h_reco_unfolded_Ids2_clone.Draw(drawopt)
    
    h_reco_unfolded_Baron2_clone.Draw(drawopt)
    h_reco_unfolded_Baron_Reg2_clone.Draw(drawopt)
    pad2.RedrawAxis()
    c.Update()
    histograms.append(c)
    PrintCan(c, outputname)
    input("Zkontroluj Ratio...")
    
#def CutInput(matrix = h_response_unf, data = h_reco_get , particle = h_ptcl_get):
#    return 0



def MyRooUnfold(matrix_name=args.h_matrix, h_reco_getG0_name=args.h_data, h_ptcl_getG0_name = args.h_particle,h_reco_get_bkg_name = args.h_background,outputname=args.h_data+"_unfolded",nrebin = args.nrebin):

    rfile_data = TFile(args.rfile_data, 'read')
    rfile_particle = TFile(args.rfile_particle, 'read')
    rfile_matrix = TFile(args.rfile_matrix, 'read')
    rfile_background = TFile(args.rfile_background, 'read')

    myfbu = fbu.PyFBU()
    myfbu.verbose = True 
    #GET DATA
    h_reco_get = rfile_data.Get(h_reco_getG0_name)
    h_reco_get.Rebin(nrebin)
    #GET PARTICLE
    h_ptcl_get = rfile_particle.Get(h_ptcl_getG0_name)
    h_ptcl_get.Rebin(nrebin)
    #GET MATRIX
    h_response_unf = rfile_matrix.Get(matrix_name)
    h_response_unf.ClearUnderflowAndOverflow()

    #h_response_unf = CutInput(matrix = h_response_unf, data = h_reco_get , particle = h_ptcl_get)

    h_response_unf.GetXaxis().SetRange(1, h_response_unf.GetXaxis().GetNbins() )
    h_response_unf.GetYaxis().SetRange(1, h_response_unf.GetYaxis().GetNbins() )
    h_response_unf.Rebin2D(nrebin,nrebin)
    h_response_unf.SetName("Migration_Matrix_simulation")

    ########### ACCEPTANCY
    h_acc = h_response_unf.ProjectionX("reco_recoandparticleX") # Reco M
    h_acc.Divide(h_reco_get)
    ########### AKCEPTANCE saved in h_acc #############
    ########### EFFICIENCY
    h_eff = h_response_unf.ProjectionY("reco_recoandparticleY") # Ptcl M
    h_eff.Divide(h_ptcl_get)
    
    h_reco_get_input = rfile_data.Get(h_reco_getG0_name)
    #h_reco_get_bkg = rfile_background.Get(h_reco_get_bkg_name)
    h_reco_get_bkg = h_reco_get_input.Clone("")
    #h_reco_get_bkg.Rebin(nrebin)
    h_reco_get_bkg.Reset() 
    

    h_reco_get_input_clone=h_reco_get_input.Clone("")

    #h_reco_get_input_clone.Add(h_reco_get_bkg,-1)
    h_reco_get_input_clone.Multiply(h_acc)
    
   
    h_reco_or = rfile_data.Get(h_reco_getG0_name)
    h_ptcl_or = rfile_particle.Get(h_ptcl_getG0_name)
    h_ptcl_or.SetMaximum(h_ptcl_or.GetMaximum()*1.5)
    
    ### ROOUNFOLD METHOD ###
    
    m_RooUnfold = RooUnfoldBayes()
    m_RooUnfold.SetRegParm( 4 )
    m_RooUnfold.SetNToys( 10000 )
    m_RooUnfold.SetVerbose( 0 )
    m_RooUnfold.SetSmoothing( 0 )
  
    response = RooUnfoldResponse(None, None, h_response_unf, "response", "methods")
    
    m_RooUnfold.SetResponse( response )
    m_RooUnfold.SetMeasured( h_reco_get_input_clone )
    
    ### SVD METHOD ###
    
    m_RooUnfold_svd = RooUnfoldSvd (response, h_reco_get_input_clone, int(round(h_reco_get_input_clone.GetNbinsX()/2.0,0))) #8
    svd_par = int(round(h_reco_get_input_clone.GetNbinsX()/2.0,0))
    m_RooUnfold_T = RooUnfoldTUnfold (response, h_reco_get_input_clone)         #  OR
    m_RooUnfold_Ids= RooUnfoldIds (response, h_reco_get_input_clone,int(round(h_reco_get_input_clone.GetNbinsX()/12.0,0))) ## TO DO, SET PARAMETERS TO THE BINNING
    Ids_par = int(round(h_reco_get_input_clone.GetNbinsX()/12.0,0))
    
    ### FBU METHOD ###
    
    h_response_unf_fbu = TransposeMatrix(h_response_unf)
    h_response_unf_fbu_norm = NormalizeResponse(h_response_unf_fbu)
    #h_response_unf_fbu_norm.SetName("Migration_Matrix_simulation_transpose")
    histograms.append(h_response_unf_fbu_norm)


    myfbu.response = MakeListResponse(h_response_unf_fbu_norm)
    myfbu.data = MakeListFromHisto(h_reco_get_input_clone) 

    #print("response", myfbu.response)
    #print("data", myfbu.data)
    #h_response_unf_fbu_norm.Draw("lego2")
    #h_reco_get_input_clone.Draw("hist")
    #gApplication.Run()

    lower = []
    upper = []
    
    h_det_div_ptcl=h_reco_get_input_clone.Clone("")
    h_det_div_ptcl.Divide(h_ptcl_or)
    h_det_div_ptcl.Divide(h_eff)
    h_det_div_ptcl.SetName("det_div_ptcl")
    histograms.append(h_det_div_ptcl)
    histograms.append(h_ptcl_or)

    for l in range(len(myfbu.data)):
        if ( args.SplitFromBinLow != 0) and ( l+1 <= args.SplitFromBinLow ):
            lower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-args.ParameterSplitFromBinLow)*h_det_div_ptcl.GetBinContent(l+1))
            upper.append(h_reco_get_input_clone.GetBinContent(l+1)*args.ParameterSplitFromBinLow*h_det_div_ptcl.GetBinContent(l+1))
        elif ( args.SplitFromBinHigh != 0 ) and ( l+1 >= args.SplitFromBinHigh ):
            lower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-args.ParameterSplitFromBinHigh)*h_det_div_ptcl.GetBinContent(l+1))
            upper.append(h_reco_get_input_clone.GetBinContent(l+1)*args.ParameterSplitFromBinHigh*h_det_div_ptcl.GetBinContent(l+1))
        else:
            lower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-args.par)*h_det_div_ptcl.GetBinContent(l+1))
            upper.append(h_reco_get_input_clone.GetBinContent(l+1)*args.par*h_det_div_ptcl.GetBinContent(l+1))
    #myfbu.regularization = Regularization('Tikhonov',parameters=[{'refcurv':0.1,'alpha':0.2}]) works for old FBU package and python2.7 and old pymc

    myfbu.lower = lower/np.array(5)
    myfbu.upper = upper*np.array(5)
    Repeat = True
    j = 1

    while Repeat:
        print("Runnig iteration number: ",j)
        if j > 1:    
            lower = []
            upper = []
            lowerReg = []
            upperReg = []
            for l in range(len(myfbu.data)):
                posteriors_diag[l].Fit("gaus")
                fit = posteriors_diag[l].GetFunction("gaus") 
                p1 = fit.GetParameter(1)
                p2 = fit.GetParameter(2)
                lower.append(p1-4*p2)
                upper.append(p1+4*p2)
                lowerReg.append(p1-10*p2)
                upperReg.append(p1+10*p2)
            myfbu.lower = lower/np.array(5)
            myfbu.upper = upper*np.array(5)
        h_reco_unfolded_Baron, ListOfFinePosteriors = Hamilton(data = myfbu.data, matrix = myfbu.response, low = myfbu.lower/np.array(5), up = myfbu.upper*np.array(5), particle = h_ptcl_or, eff = h_eff, acc = h_acc , eps = 1, steps = 40000, Again = 0, Tau = 0, BinCoef=args.BinCoef) #steps 40 000
        h_reco_unfolded_Baron.SetName(h_reco_unfolded_Baron.GetName()+'_iteration_'+str(j))
        for m in range(len(ListOfFinePosteriors)):
            ListOfFinePosteriors[m].SetName(ListOfFinePosteriors[m].GetName()+'_iteration_'+str(j))
            histograms.append(ListOfFinePosteriors[m])
            ###h_reco_unfolded_Baron.SetBinError(m,ListOfFinePosteriors[m].GetFunction("gaus").GetParameter(2))
        PlotPosteriors(h_ptcl_get, ListOfFinePosteriors,outputname+'_iteration_baron'+str(j))

        #REGUL
        #if j == 1:
        #    h_reco_unfolded_Baron_Reg, ListOfFinePosteriors_Reg = Hamilton(data = myfbu.data, matrix = myfbu.response, low = myfbu.lower, up = myfbu.upper, particle = h_ptcl_or, eff = h_eff, acc = h_acc , eps = 1, steps = 20000, Again = 0, Tau = args.tau) #steps 40 000
        #else:
        h_reco_unfolded_Baron_Reg, ListOfFinePosteriors_Reg = Hamilton(data = myfbu.data, matrix = myfbu.response, low = myfbu.lower/np.array(5), up = myfbu.upper*np.array(5), particle = h_ptcl_or, eff = h_eff, acc = h_acc , eps = 1, steps = 40000, Again = 0, Tau = args.tau, BinCoef=args.BinCoef) #steps 40 000
        h_reco_unfolded_Baron_Reg.SetName(h_reco_unfolded_Baron_Reg.GetName()+'_Reg_' +str(args.tau) + '_iteration_'+str(j))
        for m in range(len(ListOfFinePosteriors_Reg)):
            ListOfFinePosteriors_Reg[m].SetName(ListOfFinePosteriors_Reg[m].GetName()+'_Reg_' +str(args.tau) +'_iteration_'+str(j))
            histograms.append(ListOfFinePosteriors_Reg[m])
            ###h_reco_unfolded_Baron_Reg.SetBinError(m,ListOfFinePosteriors_Reg[m].GetFunction("gaus").GetParameter(2))
        PlotPosteriors(h_ptcl_get, ListOfFinePosteriors_Reg,outputname+'_Reg_' +str(args.tau)+'_iteration_baron'+str(j))
        ###
        myfbu.run()
        trace = myfbu.trace
        traceName = 'Posterior_'+str(j)+'_iteration'
        posteriors_diag = MakeTH1Ds(trace, lower = myfbu.lower, upper = myfbu.upper, BinCoef = args.BinCoef, tag =  traceName)
        h_reco_unfolded, h_reco_unfolded_Mean = MakeUnfoldedHisto(h_reco_or, posteriors_diag)
        h_reco_unfolded.SetName(h_reco_unfolded.GetName()+'_iteration_'+str(j))
        for i in range(len(posteriors_diag)):
            if posteriors_diag[i].Integral() != 0:
                posteriors_diag[i].Scale(1.0/posteriors_diag[i].Integral())
                posteriors_diag[i].Fit("gaus", "q")
                h_reco_unfolded.SetBinError(i,posteriors_diag[i].GetFunction("gaus").GetParameter(2))
        PlotPosteriors(h_ptcl_get, posteriors_diag,outputname+'_iteration_'+str(j))
        ###
        # EFFICIENCY AND ACCEPTANCY CORRECTIONS
        h_reco_unfolded.Divide(h_eff)
        h_reco_unfolded_Mean.Divide(h_eff)
        #h_reco_unfolded.SetName(h_reco_unfolded.GetName()+'_iteration_'+str(j))
        h_reco_unfolded_Mean.SetName(h_reco_unfolded_Mean.GetName()+'_iteration_'+str(j))
        h_reco_unfolded_roof = m_RooUnfold.Hreco()
        h_reco_unfolded_roof.Divide(h_eff)
        h_reco_unfolded_roof.SetName(h_reco_unfolded_roof.GetName()+'_iteration_'+str(j))
        h_reco_unfolded_svd = m_RooUnfold_svd.Hreco()
        h_reco_unfolded_svd.Divide(h_eff)
        h_reco_unfolded_svd.SetName(h_reco_unfolded_svd.GetName()+'_iteration_'+str(j))
        h_reco_unfolded_T = m_RooUnfold_T.Hreco()
        h_reco_unfolded_T.Divide(h_eff)
        h_reco_unfolded_T.SetName(h_reco_unfolded_T.GetName()+'_iteration_'+str(j))
        h_reco_unfolded_Ids = m_RooUnfold_Ids.Hreco()
        h_reco_unfolded_Ids.Divide(h_eff)
        h_reco_unfolded_Ids.SetName(h_reco_unfolded_Ids.GetName()+'_iteration_'+str(j))
        h_ptcl_or.SetName(h_ptcl_or.GetName()+'_iteration_'+str(j))
        h_reco_unfolded_Baron_Reg.SetLineStyle(1)
        h_reco_unfolded_Baron_Reg.SetLineWidth(2)
        h_reco_unfolded_Baron.SetLineStyle(1)
        h_reco_unfolded.SetLineStyle(1)
        h_reco_unfolded_roof.SetLineStyle(1)
        PlotRatio(h_reco_unfolded_Baron_Reg, h_reco_unfolded, h_ptcl_get, h_reco_unfolded_roof, h_reco_unfolded_svd, h_reco_unfolded_T, h_reco_unfolded_Ids , h_reco_unfolded_Baron, svd_par, Ids_par, outputname=outputname+'_iteration_'+str(j))
        histograms.append(h_reco_unfolded)
        histograms.append(h_reco_unfolded_Mean)
        histograms.append(h_reco_unfolded_svd)
        histograms.append(h_reco_unfolded_T)
        histograms.append(h_reco_unfolded_Ids)
        histograms.append(h_reco_unfolded_Baron)
        histograms.append(h_reco_unfolded_Baron_Reg)
        histograms.append(h_ptcl_or)
        print("Prejete si dalsi iteraci? Ano = 1, Ne = 0")
        Repeat = int(input())
        print("Repeat: ", Repeat)
        if Repeat == 0:
            break
        j = j+1

    h_eff.SetName("efficiency")
    histograms.append(h_eff)
    h_acc.SetName("acceptancy")
    histograms.append(h_acc)
    SaveHistograms(outputname=args.outname)

histograms = []
MyRooUnfold()
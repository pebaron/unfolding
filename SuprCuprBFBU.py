#!/usr/bin/env python3

import os
os.environ["MKL_THREADING_LAYER"] = "GNU"
import argparse
import fbu
from fbu import Regularization
import ROOT
import math
import random
import numpy as np
from array import array
import scipy.stats as stats
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed, TMath, TF1, TLine, kBird
import pandas as pn
import time

parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")
parser.add_argument('--N', '-N', type=int, default=100)
parser.add_argument('--rfile_data','-rfile_data', type=str, default="input.root")
parser.add_argument('--rfile_particle','-rfile_particle', type=str, default="input.root")
parser.add_argument('--rfile_matrix','-rfile_matrix', type=str, default="input.root")
parser.add_argument('--rfile_background','-rfile_background', type=str, default="input.root")
parser.add_argument('--h_data','-h_data', type=str, default="h_data")
parser.add_argument('--h_particle','-h_particle', type=str, default="h_particle")
parser.add_argument('--h_matrix','-h_matrix', type=str, default="h_matrix")
parser.add_argument('--h_background','-h_background', type=str, default="h_background")

parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{t,had} [GeV]")
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)
parser.add_argument('--SplitFromBinLow', '-SplitFromBinLow', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinLow', '-ParameterSplitFromBinLow', type=float, default=1.5)
parser.add_argument('--SplitFromBinHigh', '-SplitFromBinHigh', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinHigh', '-ParameterSplitFromBinHigh', type=float, default=1.5)
parser.add_argument('--maxiterations', '-maxiterations', type=int, default=30)
parser.add_argument('--batch', '-batch', type=int, default=0)
parser.add_argument('--sampling', '-sampling', type=int, default=2000)
parser.add_argument('--plotlike', '-plotlike', type=int, default=0)
parser.add_argument('--nrebinpost', '-nrebinpost', type=int, default=1)
parser.add_argument('--counts', '-counts', type=int, default=10000)


args = parser.parse_args()

if (args.batch == 1):
    ROOT.gROOT.SetBatch(1)

def PrintCan(can, outputname):
    outputname = outputname.split('/')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.png')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.pdf')

def MakeListFromHisto(hist):
    vals = []
    for i in range(1, hist.GetXaxis().GetNbins()+1):
        val = hist.GetBinContent(i)
        vals.append(val)
    return vals

def NormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetXaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetYaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetYaxis().GetNbins()+1):
                migra.SetBinContent(i,j,migra.GetBinContent(i,j) / sum)
    return migra

def CheckNormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(j,i)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetXaxis().GetNbins()+1):
                migra.SetBinContent(j,j,migra.GetBinContent(j,i) / sum)
    return migra

def MakeListResponse(h2):
    vals = []
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        column = []
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            column.append(val)
        vals.append(column)
    return vals

def TransposeMatrix(h_response_unf):
    h_responce_transpose = h_response_unf.Clone(h_response_unf.GetName()+"clone")
    h_responce_transpose.Reset()
    for i in range(1,h_response_unf.GetXaxis().GetNbins()+1):
        for j in range(1,h_response_unf.GetXaxis().GetNbins()+1):
            h_responce_transpose.SetBinContent(i,j,h_response_unf.GetBinContent(j,i))
            h_responce_transpose.SetBinError(i,j,h_response_unf.GetBinError(j,i))
    h_responce_transpose.GetXaxis().SetTitle(h_response_unf.GetYaxis().GetTitle())
    h_responce_transpose.GetYaxis().SetTitle(h_response_unf.GetXaxis().GetTitle())
    return h_responce_transpose
    
def SaveHistograms(outputname = "output"):
    outputname = outputname.split('/')
    outfile = TFile('./outputs/'+outputname[len(outputname)-1]+".root", 'recreate')
    outfile.cd()
    for j in range(len(histograms)):
        histograms[j].Write()
    print("End of the unfolding.")
    outfile.Write()
    outfile.Close() 

def Plot2D(ListOf2DPosteriors):
    c = TCanvas("Posteriors2D"+outputname,"Posteriors2D"+outputname,0,0,1600, 1600)
    c.Divide(math.ceil(pow(len(ListOf2DPosteriors),0.5)),math.ceil(pow(len(ListOf2DPosteriors),0.5)))
    for i in range(len(ListOf2DPosteriors)):
        c.cd(i+1)
        ListOf2DPosteriors[i].GetZaxis().SetRangeUser(0,1.0)
        ListOf2DPosteriors[i].Draw("colz")
    gApplication.Run()

def PlotPosteriors(ListOfPosteriors, unfolded, h_ptcl_or, ListOfDensities, LogLike, outputname = ""):
    RepeatIteration = False
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.5),int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.0))
    index = 1

    for i in range(len(ListOfDensities)):
        c.cd(i+1)
        ListOfPosteriors[i].Draw("hist")
        ListOfDensities[i].Draw("hist same")
        ListOfPosteriors[i].SetLineColor(2)
        ListOfPosteriors[i].Draw("hist same")
        #fit2.Draw("same")
        index = index + 1 

    for i in range(len(ListOfDensities)):
        c.cd(index)
        ListOfDensities[i].Draw("hist same")
        ListOfPosteriors[i].SetLineColor(2)
        ListOfPosteriors[i].Draw("hist same")
        #fit2.Draw("same")
        index = index + 1 

    c.cd(index)
    LogLike.Draw()
    index = index + 1        
        #c.Update()

    c.cd(1)
    gStyle.SetOptStat(0)
    gPad.Modified()
    #c.Update()

    c.cd(index)
    pad1 = TPad("pad1","pad1",0,0.40,1,1)
    pad1.SetTopMargin(0.15)
    pad1.SetBottomMargin(0.01)
    pad1.SetFillStyle(0)
    pad1.SetTicks(1,1)
    pad1.SetBorderMode(0)
    pad1.Draw()
    c.cd(index)
    pad2 = TPad("pad2","pad2",0,0.01,1,0.422)
    pad2.SetFillStyle(0)
    pad2.SetTopMargin(0.043)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks(1,1)
    pad2.Draw()
    pad2.Modified()
    c.cd(index)
    pad1.cd()
    gStyle.SetOptStat(0)
    h_ptcl_or.SetTitle("")
    h_ptcl_or.SetLineColor(2)
    #h_ptcl_or.GetYaxis().SetRangeUser(0,h_ptcl_or.GetMaximum()*1.5)
    #h_ptcl_or.GetXaxis().SetTitleSize(34)
    #h_ptcl_or.GetXaxis().SetTitleFont(43)
    #h_ptcl_or.GetYaxis().SetTitleSize(27)
    #h_ptcl_or.GetYaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleOffset(1.5)
    h_ptcl_or.GetYaxis().SetTitle("Events")
    #h_ptcl_or.GetYaxis().SetLabelFont(43)
    #h_ptcl_or.GetYaxis().SetLabelSize(25)
    legend = TLegend(0.55,0.5,0.85,0.8)
    legend.SetFillStyle(0)
    legend.AddEntry(h_ptcl_or,"Particle")
    legend.AddEntry(unfolded,"FBU Baron imp.","p")
    legend.SetBorderSize(0)
    unfolded.SetLineColor(1)
    unfolded.SetMarkerColor(1)
    unfolded.SetMarkerStyle(22)
    unfolded.Draw("same p x0")
    h_ptcl_or.Draw("hist same")
    legend.Draw("same")
    pad1.RedrawAxis()
    pad2.cd()
    h_ptcl_or_clone = h_ptcl_or.Clone(h_ptcl_or.GetName()+"_clone") 
    unfolded_clone = unfolded.Clone(unfolded.GetName()+"_clone")
    h_ptcl_or_clone.Divide(h_ptcl_or) 
    unfolded_clone.Divide(h_ptcl_or) 
    
    #h_ptcl_or_clone.GetXaxis().SetTitleSize(27)
    #h_ptcl_or_clone.GetXaxis().SetTitleFont(43)
    #h_ptcl_or_clone.GetYaxis().SetTitleSize(27)
    #h_ptcl_or_clone.GetYaxis().SetTitleFont(43)
    #h_ptcl_or_clone.GetXaxis().SetLabelFont(43)
    #h_ptcl_or_clone.GetXaxis().SetLabelSize(25)
    #h_ptcl_or_clone.GetYaxis().SetLabelFont(43)
    #h_ptcl_or_clone.GetYaxis().SetLabelSize(25)
    
    h_ptcl_or_clone.SetMaximum(1.3)
    h_ptcl_or_clone.SetMinimum(0.7)
    
    h_ptcl_or_clone.GetXaxis().SetTitleOffset(2.5)
    h_ptcl_or_clone.GetXaxis().SetTitle(args.title)
    
    #h_ptcl_or_clone.GetYaxis().SetTitle("#frac{Unfolded}{Simulation}      ")
    h_ptcl_or_clone.GetYaxis().SetTitle("Ratio      ")
    h_ptcl_or_clone.Draw("hist")
    unfolded_clone.Draw("same p x0")
    pad2.RedrawAxis()
    #c.cd(index+1)
    #correlation.SetTitle("Bin correlations")
    #correlation.SetMaximum(1.0)
    #correlation.SetMinimum(-1.0)
    ##gStyle.SetPalette(kBird)
    ##correlation.SetContour(4)
    #correlation.Draw("colz")
    #c.Update()
    histograms.append(c) # here is the crash probably
    
    PrintCan(c,c.GetName()+"_posteriors")

    #PlotRatio(unfolded, h_ptcl_or, c.GetName())
    #gApplication.Run()
    return RepeatIteration

def PlotRatio(h_reco_unfolded, h_ptcl_or, Name = ""):
    
    gStyle.SetPadLeftMargin(0.12)
    gStyle.SetPadRightMargin(0.12)
    c = TCanvas("canvas_ratio"+Name,"canvas_ratio"+Name,0,0,800, 800)
    c.cd()
    pad1 = TPad("pad1"+Name,"pad1"+Name,0,0.40,1,1)
    pad1.SetTopMargin(0.15)
    pad1.SetBottomMargin(0.01)
    pad1.SetFillStyle(0)
    pad1.SetTicks(1,1)
    pad1.SetBorderMode(0)
    pad1.Draw()
    c.cd()
    pad2 = TPad("pad2"+Name,"pad2"+Name,0,0.01,1,0.422)
    pad2.SetFillStyle(0)
    pad2.SetTopMargin(0.043)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks(1,1)
    pad2.Draw()
    pad2.Modified()
    c.cd()
    pad1.cd()
    gStyle.SetOptStat(0)
    h_ptcl_or.SetTitle("")
    h_ptcl_or.SetLineColor(2)
    #h_ptcl_or.GetYaxis().SetRangeUser(0,h_ptcl_or.GetMaximum()*1.5)
    h_ptcl_or.GetXaxis().SetTitleSize(34)
    h_ptcl_or.GetXaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleSize(27)
    h_ptcl_or.GetYaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleOffset(1.5)
    h_ptcl_or.GetYaxis().SetTitle("Events")
    h_ptcl_or.GetYaxis().SetLabelFont(43)
    h_ptcl_or.GetYaxis().SetLabelSize(25)
    legend = TLegend(0.55,0.5,0.85,0.8)
    legend.SetFillStyle(0)
    legend.AddEntry(h_ptcl_or,"Particle")
    legend.AddEntry(h_reco_unfolded,"FBU Baron imp.","p")
    legend.SetBorderSize(0)
    h_reco_unfolded.SetLineColor(1)
    h_reco_unfolded.SetMarkerColor(1)
    h_reco_unfolded.SetMarkerStyle(22)
    h_reco_unfolded.Draw("same p x0")
    h_ptcl_or.Draw("hist same")
    legend.Draw("same")
    pad1.RedrawAxis()
    pad2.cd()
    h_ptcl_or_clone = h_ptcl_or.Clone(h_ptcl_or.GetName()+"_clone"+Name) 
    h_reco_unfolded_clone = h_reco_unfolded.Clone(h_reco_unfolded.GetName()+"_clone"+Name)
    h_ptcl_or_clone.Divide(h_ptcl_or) 
    h_reco_unfolded_clone.Divide(h_ptcl_or) 
    
    h_ptcl_or_clone.GetXaxis().SetTitleSize(27)
    h_ptcl_or_clone.GetXaxis().SetTitleFont(43)
    h_ptcl_or_clone.GetYaxis().SetTitleSize(27)
    h_ptcl_or_clone.GetYaxis().SetTitleFont(43)
    
    h_ptcl_or_clone.GetXaxis().SetLabelFont(43)
    h_ptcl_or_clone.GetXaxis().SetLabelSize(25)
    h_ptcl_or_clone.GetYaxis().SetLabelFont(43)
    h_ptcl_or_clone.GetYaxis().SetLabelSize(25)
    
    h_ptcl_or_clone.SetMaximum(1.3)
    h_ptcl_or_clone.SetMinimum(0.7)
    
    h_ptcl_or_clone.GetXaxis().SetTitleOffset(2.5)
    h_ptcl_or_clone.GetXaxis().SetTitle(args.title)
    
    #h_ptcl_or_clone.GetYaxis().SetTitle("#frac{Unfolded}{Simulation}      ")
    h_ptcl_or_clone.GetYaxis().SetTitle("Ratio      ")
    h_ptcl_or_clone.Draw("hist")
    h_reco_unfolded_clone.Draw("same p x0")
    pad2.RedrawAxis()
    c.Update()
    histograms.append(c) # here is the crash probably
    PrintCan(c, "ratio"+Name)

def CalculateLikelihood(T_random,M,D):
    R = np.array(T_random).dot(M)                                       # CREATING RECO
    D_int64 = D.astype(np.int64)
    R_int64 = R.astype(np.int64)
    LikeTest = stats.poisson.pmf(D_int64, R_int64)
    return np.prod(LikeTest)

def FindNewPseudo(Temp_T_random, M, D, ListOfColumns, lower, upper):
    Temp_T_random_orig = Temp_T_random
    Temp_T_random_No_like = Temp_T_random
    #print("Test: ", Temp_T_random_No_like)

    FindBestLike = pn.DataFrame(columns=ListOfColumns)
    NewDictOfPseudos = {}
    for j in range(len(Temp_T_random)):
        Steps = np.arange(Temp_T_random_orig[j]-(upper[j]-lower[j])/10,Temp_T_random_orig[j]+(upper[j]-lower[j])/10,(upper[j]-lower[j])/1000)
        for PercentStep in Steps:
            #Temp_T_random_No_like[j] = Temp_T_random_orig[j] + Temp_T_random_orig[j]*PercentStep
            Temp_T_random_No_like[j] = PercentStep
            NewLikelihood = CalculateLikelihood(Temp_T_random_No_like, M, D)
            for l in range(len(D)):
                    NewDictOfPseudos['True_bin_'+str(l)] = Temp_T_random_No_like[l]
            NewDictOfPseudos['LogLikelihood'] = [NewLikelihood]
            TempPseudoExp = pn.DataFrame.from_dict(NewDictOfPseudos) 
            if (NewLikelihood != 0.0)and(math.isnan(NewLikelihood) == False)and(math.isinf(NewLikelihood) == False) :
                FindBestLike = pn.concat([FindBestLike, TempPseudoExp], ignore_index=True, sort = True)

            Temp_T_random_No_like[j] = Temp_T_random_orig[j] - Temp_T_random_orig[j]*PercentStep
            NewLikelihood = CalculateLikelihood(Temp_T_random_No_like, M, D)
            for l in range(len(D)):
                NewDictOfPseudos['True_bin_'+str(l)] = Temp_T_random_No_like[l]
            NewDictOfPseudos['LogLikelihood'] = [NewLikelihood]
            TempPseudoExp = pn.DataFrame.from_dict(NewDictOfPseudos) 
            if (NewLikelihood != 0.0)and(math.isnan(NewLikelihood) == False)and(math.isinf(NewLikelihood) == False) :
                FindBestLike = pn.concat([FindBestLike, TempPseudoExp], ignore_index=True, sort = True)

            Temp_T_random_No_like = Temp_T_random_orig

    return FindBestLike


def MyBaronRooUnfold(h_reco_get,h_ptcl_get,h_response_unf,h_reco_get_bkg,matrix_name=args.h_matrix, h_reco_getG0_name=args.h_data, h_ptcl_getG0_name = args.h_particle,h_reco_get_bkg_name = args.h_background,outputname=args.h_data+"_unfolded",nrebin = args.nrebin, N = 100, sampling = 1000, maxiteration = 30):

    h_response_unf.ClearUnderflowAndOverflow()
    h_response_unf.GetXaxis().SetRange(1, h_response_unf.GetXaxis().GetNbins() )
    h_response_unf.GetYaxis().SetRange(1, h_response_unf.GetYaxis().GetNbins() )
    h_response_unf.SetName("Migration_Matrix_simulation")

    ########### ACCEPTANCY
    h_acc = h_response_unf.ProjectionX("reco_recoandparticleX") # Reco M
    h_acc.Divide(h_reco_get)
    ########### AKCEPTANCE saved in h_acc #############
    ########### EFFICIENCY
    h_eff = h_response_unf.ProjectionY("reco_recoandparticleY") # Ptcl M
    h_eff.Divide(h_ptcl_get)

    h_reco_get_input_clone=h_reco_get_input.Clone("")
    #h_reco_get_input_clone.Add(h_reco_get_bkg,-1)
    h_reco_get_input_clone.Multiply(h_acc)
   
    h_ptcl_or = rfile_particle.Get(h_ptcl_getG0_name)
    h_ptcl_or.SetMaximum(h_ptcl_or.GetMaximum()*1.5)
    
    h_response_unf_fbu = TransposeMatrix(h_response_unf)
    h_response_unf_fbu_norm = NormalizeResponse(h_response_unf_fbu)
    h_response_unf_fbu_norm.SetName("Migration_Matrix_simulation_transpose")
    histograms.append(h_response_unf_fbu_norm)
    M = np.array(MakeListResponse(h_response_unf_fbu_norm))
    D = np.array(MakeListFromHisto(h_reco_get_input_clone)) 
    B = np.array(MakeListFromHisto(h_reco_get_bkg))

    lower=[]
    upper=[]
    OldLower = []
    OldUpper = []

    h_det_div_ptcl=h_reco_get_input_clone.Clone("")
    h_det_div_ptcl.Divide(h_ptcl_or)
    h_det_div_ptcl.Divide(h_eff)
    h_det_div_ptcl.SetName("det_div_ptcl")
    
    ListOfColumns = []

    for l in range(len(D)):
        lower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-args.par)*h_det_div_ptcl.GetBinContent(l+1))
        upper.append(h_reco_get_input_clone.GetBinContent(l+1)*args.par*h_det_div_ptcl.GetBinContent(l+1)) 
        OldLower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-args.par)*h_det_div_ptcl.GetBinContent(l+1))
        OldUpper.append(h_reco_get_input_clone.GetBinContent(l+1)*args.par*h_det_div_ptcl.GetBinContent(l+1)) 
        ListOfColumns.append('True_bin_'+str(l))
        ListOfColumns.append('Likelihood'+str(l))

    #ListOfColumns.append('LogLikelihood')
    
    IterNumber = 1
    counts = 0

    SuperPseudoExp = pn.DataFrame(columns=ListOfColumns)
    while counts < args.counts:
        print("Number of counts: ", counts)
        PseudoExp = pn.DataFrame(columns=ListOfColumns)

        print("Runnig iteration number: ",IterNumber)
        ListOfPosteriors = []
        ListOfFinePosteriors = []
        for s in range(len(lower)):
            print("Space check: ",lower[s], upper[s])
        Binning = []
        ListOfDensities = []
        for k in range(len(D)):
            Empty = []
            Binning.append(Empty)
            #help_array = np.arange(lower[k],upper[k],(upper[k]-lower[k])/sampling)
            help_array = np.arange(lower[k],upper[k],(upper[k]-lower[k])/10000)
            for s in range(len(help_array)):
                Binning[k].append(help_array[s])
            x = array("d",Binning[k])
            posteriors = TH1D("poster_"+str(k)+"_iter"+str(IterNumber),"poster_"+str(k)+"_iter"+str(IterNumber), len(Binning[k])-1,x)
            fine_posteriors = TH1D("poster_fine"+str(k)+"_iter"+str(IterNumber),"poster_fine"+str(k)+"_iter"+str(IterNumber), len(Binning[k])-1,x)
            density = TH2D("density_"+str(k)+"_iter_"+str(IterNumber),"density_"+str(k)+"_iter_"+str(IterNumber), len(Binning[k])-1,x,1000,0,1000)
            ListOfPosteriors.append(posteriors)
            ListOfFinePosteriors.append(fine_posteriors)
            ListOfDensities.append(density)        

        for m in range(len(D)): # number of POSTERIORS 
            print("Processing Posterior ",m)
            FillingNumber = 0
            Filling = False
            while (FillingNumber < args.N):
                T_random = []
                for l in range(len(D)):
                    RandomValue = (upper[l] - lower[l])*np.random.random_sample() + lower[l]
                    T_random.append(RandomValue)
                Loglike = CalculateLikelihood(T_random,M,D)
                if (Loglike != 0.0)and(math.isnan(Loglike) == False)and(math.isinf(Loglike) == False) :
                    FindBestLike = FindNewPseudo(T_random, M, D, ListOfColumns, lower, upper)
                    PseudoExp = pn.concat([PseudoExp, FindBestLike], ignore_index=True, sort = True)
                    FillingNumber = FillingNumber + 1
                    Filling = True
            if Filling == True:
                print("OK: ", FindBestLike['LogLikelihood'].max())

        PseudoExp = PseudoExp.drop_duplicates()
        PseudoExp = PseudoExp.sort_values(by=['LogLikelihood'],ascending=False)
        PseudoExp = PseudoExp.reset_index(drop=True)

        print("PseudoExp:")
        print(PseudoExp)
        
        for m in range(len(D)): # number of POSTERIORS 
            print("Processing Posterior ",m)
            FillingNumber = 0
            Filling = False
            i=0
            while (FillingNumber < args.N):
                T_random = []
                for l in range(len(D)):
                    RandomValue = PseudoExp[ListOfColumns[l]][i]
                    T_random.append(RandomValue)
                print("TRandon with max like:", T_random)        
                i = i+1
                Loglike = CalculateLikelihood(T_random,M,D)
                if (Loglike != 0.0)and(math.isnan(Loglike) == False)and(math.isinf(Loglike) == False) :
                    FindBestLike = FindNewPseudo(T_random, M, D, ListOfColumns, lower, upper)
                    PseudoExp = pn.concat([PseudoExp, FindBestLike], ignore_index=True, sort = True)

                    FillingNumber = FillingNumber + 1
                    Filling = True
            if Filling == True:
                print("OK: ", FindBestLike['LogLikelihood'].max())

        PseudoExp = PseudoExp.drop_duplicates()
        PseudoExp = PseudoExp.sort_values(by=['LogLikelihood'],ascending=False)
        PseudoExp = PseudoExp.reset_index(drop=True)

        #PseudoExp.set_index(np.arange(len(PseudoExp.index)))

        print("PseudoExp super thin:")
        print(PseudoExp)
        
        for m in range(len(D)): # number of POSTERIORS 
            print("Processing Posterior ",m)
            FillingNumber = 0
            Filling = False
            i=0
            while (FillingNumber < args.N):
                T_random = []
                for l in range(len(D)):
                    RandomValue = PseudoExp[ListOfColumns[l]][i]
                    T_random.append(RandomValue)
                print("TRandon with max like:", T_random)        
                i = i+1
                Loglike = CalculateLikelihood(T_random,M,D)
                if (Loglike != 0.0)and(math.isnan(Loglike) == False)and(math.isinf(Loglike) == False) :
                    FindBestLike = FindNewPseudo(T_random, M, D, ListOfColumns, lower, upper)
                    PseudoExp = pn.concat([PseudoExp, FindBestLike], ignore_index=True, sort = True)
                    
                    FillingNumber = FillingNumber + 1
                    Filling = True
            if Filling == True:
                print("OK: ", FindBestLike['LogLikelihood'].max())
        SuperPseudoExp = pn.concat([SuperPseudoExp, PseudoExp], ignore_index=True, sort = True)

        New_T_random = PseudoExp.loc[PseudoExp['LogLikelihood'].idxmax()]
        New_T_random = New_T_random.tolist()
        New_T_random.pop(0)
        #print(New_T_random)

        LogLike = TH1D("loglike_"+str(IterNumber),"loglike_"+str(IterNumber), 1000,0,1000)

        if IterNumber == 1:
            ListOfColumns.pop()
        for index in range(len(PseudoExp.LogLikelihood)):
            LogLike.Fill(int(np.log(PseudoExp.LogLikelihood[index])+800))
            i = 0
            counts = counts + 1
            for poster in ListOfColumns:
                ListOfDensities[i].Fill(PseudoExp[poster][index],int(np.log(PseudoExp.LogLikelihood[index])+800))
                ListOfPosteriors[i].Fill(PseudoExp[poster][index],PseudoExp.LogLikelihood[index])
                i = i + 1

        unfolded = h_ptcl_or.Clone("result_petr_by_fit")
        unfolded2 = unfolded.Clone("result_petr_by_mean")
        unfolded.Reset()
        unfolded2.Reset()

        DensitiesX = []
        lower = []
        upper = []

        for k in range(len(ListOfPosteriors)):
            if ListOfPosteriors[k].Integral() !=  0.0:
                ListOfPosteriors[k].Scale(1./ListOfPosteriors[k].Integral())
                print("Normalize")
            else:
                print("WARNING1: Integral is zero, I am not normalizing. You need to increase number of iterations.")
            unfolded2.SetBinContent(k+1,ListOfPosteriors[k].GetMean())
            unfolded2.SetBinError(k+1,ListOfPosteriors[k].GetRMS())
            histograms.append(ListOfPosteriors[k])
            lower.append(ListOfPosteriors[k].GetMean()-ListOfPosteriors[k].GetMean()*0.1)
            upper.append(ListOfPosteriors[k].GetMean()+ListOfPosteriors[k].GetMean()*0.1)
            histograms.append(ListOfDensities[k].ProfileX())
            DensitiesX.append(ListOfDensities[k].ProfileX())

        for j in range(len(DensitiesX)):
            if DensitiesX[j].Integral() != 0:
                DensitiesX[j].Scale(1./DensitiesX[j].Integral())

        unfolded.Divide(h_eff)
        unfolded2.Divide(h_eff)
        histograms.append(h_ptcl_or)
        histograms.append(unfolded)
        histograms.append(unfolded2)   
        histograms.append(LogLike)
        Repeat = PlotPosteriors(ListOfPosteriors, unfolded2, h_ptcl_or,DensitiesX, LogLike, str(IterNumber)) 
        IterNumber = IterNumber + 1
        #SaveHistograms("test") 
        
    
    ListOfSuperDensities = []
    ListOfSuperDensitiesX = []

    for s in range(len(ListOfDensities)):
        SuperDensity = ListOfDensities[s].Clone("Super"+ListOfDensities[s].GetName())
        SuperDensity.Reset("")
        ListOfSuperDensities.append(SuperDensity)

    for index in range(len(SuperPseudoExp.LogLikelihood)):
        i = 0
        for poster in ListOfColumns:
            ListOfSuperDensities[i].Fill(SuperPseudoExp[poster][index],int(np.log(SuperPseudoExp.LogLikelihood[index])+800))
            i = i + 1

    FinalPosteriors = []
    FitParams = ['Vertex', 'Sigma', 'Lower', 'Upper']
    FitParamsFrame = pn.DataFrame(columns=FitParams)
    ParamDict = {}

    #print("PseudoExp:")
    #print(PseudoExp)
    #print("SuperPseudoExp:")
    #print(SuperPseudoExp)
    #print("ListOfColumns", ListOfColumns)

    for k in range(len(D)):
        SuperDensityX = ListOfSuperDensities[k].ProfileX()
        SuperDensityX.Fit("pol2")
        minimum = SuperDensityX.GetXaxis().GetBinLowEdge(SuperDensityX.FindFirstBinAbove() )
        maximum = SuperDensityX.GetXaxis().GetBinUpEdge( SuperDensityX.FindLastBinAbove() )
        PolFit = TF1("PolFit"+str(k),"[2]*x*x-[1]*x+[0]" , minimum,maximum)
        #FitFunction.SetParameter(0,ListOfPosteriors[k].GetMean())
        #FitFunction.SetParameter(1,ListOfPosteriors[k].GetRMS())
        SuperDensityX.Fit("PolFit"+str(k))
        PolFit2 =SuperDensityX.GetFunction("PolFit"+str(k))
        a = PolFit2.GetParameter(2)
        b = PolFit2.GetParameter(1)
        ParamDict = {'Vertex':[abs(b/(2*a))],'Sigma':[np.sqrt(abs(1.0/(2*a)))],'Lower':[abs(b/(2*a)) - np.sqrt(abs(1.0/(2*a)))], 'Upper':[abs(b/(2*a)) + np.sqrt(abs(1.0/(2*a)))]}
        TempParam = pn.DataFrame.from_dict(ParamDict) 
        FitParamsFrame = pn.concat([FitParamsFrame, TempParam], ignore_index=True, sort = True)
        #FinalPosterior = TH1D("Final_posterior_"+str(k),"Final_posterior_"+str(k), 1000,int(0.95*ListOfPosteriors[k].GetMean()),int(1.05*ListOfPosteriors[k].GetMean()))
        #FinalPosterior = TH1D("Final_posterior_"+str(k),"Final_posterior_"+str(k), 100,int(FitParamsFrame['Lower'][k]),int(FitParamsFrame['Upper'][k]))
        #FinalPosterior = TH1D("Final_posterior_"+str(k),"Final_posterior_"+str(k), 1000000,OldLower[k],OldUpper[k])
        FinalPosterior = ListOfPosteriors[k].Clone(ListOfPosteriors[k].GetName()+"_final")
        FinalPosterior.Reset()
        ##Filter = SuperPseudoExp[(SuperPseudoExp[ListOfColumns[k]] > (abs(b/(2*a)) - np.sqrt(abs(1.0/(2*a))))) & (SuperPseudoExp[ListOfColumns[k]] < (abs(b/(2*a)) + np.sqrt(abs(1.0/(2*a)))))]
        #print("FitParamsFrame, k:",k)
        #print(FitParamsFrame)
        #print("Filter is here, k:",k)
        #print(Filter)
        for index, rows in SuperPseudoExp.iterrows():
            #print("Filling:", rows[ListOfColumns[k]], rows['LogLikelihood'])
            FinalPosterior.Fill(rows[ListOfColumns[k]], rows["LogLikelihood"])
        if FinalPosterior.Integral() !=  0.0:
            FinalPosterior.Scale(1./FinalPosterior.Integral())
            print("Final Normalize")
        else:
            print("No final nornalize")
        unfolded.SetBinContent(k+1,FinalPosterior.GetMean())
        unfolded.SetBinError(k+1, FinalPosterior.GetRMS())
        FinalPosteriors.append(FinalPosterior)
        ListOfSuperDensitiesX.append(SuperDensityX)
        histograms.append(SuperDensityX)
        histograms.append(ListOfSuperDensities[k])
        histograms.append(FinalPosterior)
    
    unfolded.Divide(h_eff)

    for j in range(len(ListOfSuperDensitiesX)):
        if ListOfSuperDensitiesX[j].Integral() != 0:
            ListOfSuperDensitiesX[j].Scale(1./ListOfSuperDensitiesX[j].Integral())


    #print("SUPER EXP &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&7")
    #print(SuperPseudoExp.to_string())
    PlotPosteriors(FinalPosteriors, unfolded, h_ptcl_or ,ListOfSuperDensitiesX, LogLike, str(IterNumber) )
    SaveHistograms("test") 
    return unfolded, ListOfFinePosteriors
        
histograms = []


matrix_name=args.h_matrix
h_reco_getG0_name=args.h_data
h_ptcl_getG0_name = args.h_particle
h_reco_get_bkg_name = args.h_background
outputname=args.h_data+"_unfolded"
nrebin = args.nrebin

rfile_data = TFile(args.rfile_data, 'read')
rfile_particle = TFile(args.rfile_particle, 'read')
rfile_matrix = TFile(args.rfile_matrix, 'read')
rfile_background = TFile(args.rfile_background, 'read')
#GET DATA
h_reco_get = rfile_data.Get(h_reco_getG0_name)
h_reco_get.Rebin(nrebin)
#GET PARTICLE
h_ptcl_get = rfile_particle.Get(h_ptcl_getG0_name)
h_ptcl_get.Rebin(nrebin)
#GET MATRIX
h_response_unf = rfile_matrix.Get(matrix_name)
h_response_unf.Rebin2D(nrebin,nrebin)
h_reco_get_input = rfile_data.Get(h_reco_getG0_name)
h_reco_get_bkg = rfile_background.Get(h_reco_get_bkg_name)
h_reco_get_bkg.Rebin(nrebin)

MyBaronRooUnfold(h_reco_get=h_reco_get,h_ptcl_get=h_ptcl_get,h_response_unf=h_response_unf,h_reco_get_bkg=h_reco_get_bkg, matrix_name=h_response_unf.GetName(), h_ptcl_getG0_name=h_ptcl_get.GetName(), h_reco_getG0_name=h_reco_get.GetName(), h_reco_get_bkg_name=h_reco_get_bkg.GetName(), N=args.N, sampling=args.sampling)

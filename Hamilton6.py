#!/usr/bin/env python3

import os
os.environ["MKL_THREADING_LAYER"] = "GNU"
import argparse
import fbu
from fbu import Regularization
import ROOT
import math
import random
import numpy as np
from array import array
import scipy.stats as stats
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed, TMath, TF1, TLine, kBird, TGaxis, TGraph2D
import pandas as pn
import time
from progress.bar import Bar
import pymc3 as pm
import theano

parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")
parser.add_argument('--rfile_data','-rfile_data', type=str, default="input.root")
parser.add_argument('--rfile_particle','-rfile_particle', type=str, default="input.root")
parser.add_argument('--rfile_matrix','-rfile_matrix', type=str, default="input.root")
parser.add_argument('--rfile_background','-rfile_background', type=str, default="input.root")
parser.add_argument('--h_data','-h_data', type=str, default="h_data")
parser.add_argument('--h_particle','-h_particle', type=str, default="h_particle")
parser.add_argument('--h_matrix','-h_matrix', type=str, default="h_matrix")
parser.add_argument('--h_background','-h_background', type=str, default="h_background")
parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{t,had} [GeV]")
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)
parser.add_argument('--maxiterations', '-maxiterations', type=int, default=30)
parser.add_argument('--batch', '-batch', type=int, default=0)
parser.add_argument('--sampling', '-sampling', type=int, default=1000)
parser.add_argument('--plotlike', '-plotlike', type=int, default=0)
parser.add_argument('--nrebinpost', '-nrebinpost', type=int, default=1)
parser.add_argument('--counts', '-counts', type=int, default=30)
parser.add_argument('--eps','-e', type=float, default=1.0)
parser.add_argument('--L','-L', type=int, default=100)
parser.add_argument('--drawopt','-drawopt', type=str, default="surf1")
parser.add_argument('--output','-output', type=str, default="output")
parser.add_argument('--tau','-tau', type=float, default=0.01)
parser.add_argument('--norm','-norm', type=int, default=1)
parser.add_argument('--svd','-svd', type=int, default=0)
parser.add_argument('--fit','-fit', type=int, default=0)
parser.add_argument('--outname', '-outname', type=str, default="out")
parser.add_argument('--range', '-range', type=float, default=1.0)
parser.add_argument('--other', '-other', type=int, default=0) # other methods
parser.add_argument('--BinCoef', '-BinCoef', type=float, default=1.0) # other methods
parser.add_argument('--steps', '-steps', type=int, default=20000) # other methods

args = parser.parse_args()

if (args.batch == 1):
    ROOT.gROOT.SetBatch(1)

def PrintCan(can, outputname):
    outputname = outputname.split('/')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.png')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.pdf')

def MakeListFromHisto(hist):
    vals = []
    for i in range(1, hist.GetXaxis().GetNbins()+1):
        val = hist.GetBinContent(i)
        vals.append(val)
    return vals

def NormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetXaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetYaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetYaxis().GetNbins()+1):
                migra.SetBinContent(i,j,migra.GetBinContent(i,j) / sum)
    return migra

def CheckNormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(j,i)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetXaxis().GetNbins()+1):
                migra.SetBinContent(j,j,migra.GetBinContent(j,i) / sum)
    return migra

def MakeListResponse(h2):
    vals = []
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        column = []
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            column.append(val)
        vals.append(column)
    return vals

def TransposeMatrix(h_response_unf):
    h_responce_transpose = h_response_unf.Clone(h_response_unf.GetName()+"clone")
    h_responce_transpose.Reset()
    for i in range(1,h_response_unf.GetXaxis().GetNbins()+1):
        for j in range(1,h_response_unf.GetXaxis().GetNbins()+1):
            h_responce_transpose.SetBinContent(i,j,h_response_unf.GetBinContent(j,i))
            h_responce_transpose.SetBinError(i,j,h_response_unf.GetBinError(j,i))
    h_responce_transpose.GetXaxis().SetTitle(h_response_unf.GetYaxis().GetTitle())
    h_responce_transpose.GetYaxis().SetTitle(h_response_unf.GetXaxis().GetTitle())
    return h_responce_transpose
    
def SaveHistograms(outputname = "output"):
    outputname = outputname.split('/')
    outfile = TFile('./outputs/'+outputname[len(outputname)-1]+".root", 'recreate')
    outfile.cd()
    for j in range(len(histograms)):
        histograms[j].Write()
    print("End of the unfolding.")
    outfile.Write()
    outfile.Close() 

def Plot2D(ListOf2DPosteriors):
    c = TCanvas("Posteriors2D"+outputname,"Posteriors2D"+outputname,0,0,1600, 1600)
    c.Divide(math.ceil(pow(len(ListOf2DPosteriors),0.5)),math.ceil(pow(len(ListOf2DPosteriors),0.5)))
    for i in range(len(ListOf2DPosteriors)):
        c.cd(i+1)
        ListOf2DPosteriors[i].GetZaxis().SetRangeUser(0,1.0)
        ListOf2DPosteriors[i].Draw("colz")
    gApplication.Run()

#def PlotPosteriors(ListOfPosteriors, SamplingGraphs, ListOfPriors, outputname = "", fit = 1):
def PlotPosteriors(ListOfPosteriors, outputname = "", fit = 1):
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/2),int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.0))
    ###c2 = TCanvas("Sampling"+outputname,"Sampling"+outputname,0,0,1600, 1600)
    ###c2.Divide(int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/2),int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.0))
    index = 1
    lines = []
    h_compare_ptcl = h_ptcl_or.Clone("ptcl_mult_eff")
    h_compare_ptcl.Multiply(h_eff)
    unfolded = h_ptcl_or.Clone("unfolded")
    unfolded.Reset()
    unfolded2 = h_ptcl_or.Clone("unfolded2")
    unfolded2.Reset()
    for i in range(len(ListOfPosteriors)):
        c.cd(i+1)
        #ListOfPriors[i].SetLineColor(6)
        if args.fit == 1:
            ListOfPosteriors[i].Fit("gaus", "q")
            #ListOfPosteriors[i].Draw("hist")
            #ListOfPriors[i].Draw("hist same")
            f1 = ListOfPosteriors[i].GetFunction("gaus")
            #f1.Draw("same")
            if f1.GetParameter(1):
                mu = f1.GetParameter(1)
                sigma = f1.GetParameter(2)
                unfolded.SetBinContent(i+1,mu)
                unfolded.SetBinError(i+1,sigma)
                print("Fit introduced. Mu: ", mu, " sigme: ", sigma)
        else:        
            #ListOfPosteriors[i].Draw("hist")
            #ListOfPriors[i].Draw("hist same")
            #ListOfPosteriors[i].Draw("hist same")
            unfolded.SetBinContent(i+1,ListOfPosteriors[i].GetMean())
            unfolded.SetBinError(i+1,ListOfPosteriors[i].GetRMS())
            print("No Fit introduced. Mean: ", ListOfPosteriors[i].GetMean(), " RMS: " ,ListOfPosteriors[i].GetRMS())
        ###line = TLine(h_compare_ptcl.GetBinContent(i+1),0,h_compare_ptcl.GetBinContent(i+1),ListOfPosteriors[i].GetMaximum())
        ###line.SetLineColor(3)
        ###lines.append(line)
        ###lines[i].SetLineColor(3)
        ####lines[i].Draw("same")
        ###c2.cd(i+1) 
        ###SamplingGraphs[i].Draw()
        ###SamplingGraphs[i].GetXaxis().SetRangeUser(lower[i],upper[i])
        ###SamplingGraphs[i].SetLineStyle(2)
        ###SamplingGraphs[i].SetMarkerColor(2)
        ####SamplingGraphs[i].Draw("LINE E0 P0")
        ###c2.Update()
        index = index + 1
    unfolded.Divide(h_eff)
    return unfolded, ListOfPosteriors

def PlotRatio(h_reco_unfolded, h_ptcl_or, Name = ""):
    
    gStyle.SetPadLeftMargin(0.12)
    gStyle.SetPadRightMargin(0.12)
    c = TCanvas("canvas_ratio"+Name,"canvas_ratio"+Name,0,0,800, 800)
    c.cd()
    pad1 = TPad("pad1"+Name,"pad1"+Name,0,0.40,1,1)
    pad1.SetTopMargin(0.15)
    pad1.SetBottomMargin(0.01)
    pad1.SetFillStyle(0)
    pad1.SetTicks(1,1)
    pad1.SetBorderMode(0)
    pad1.Draw()
    c.cd()
    pad2 = TPad("pad2"+Name,"pad2"+Name,0,0.01,1,0.422)
    pad2.SetFillStyle(0)
    pad2.SetTopMargin(0.043)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks(1,1)
    pad2.Draw()
    pad2.Modified()
    c.cd()
    pad1.cd()
    gStyle.SetOptStat(0)
    h_ptcl_or.SetTitle("")
    h_ptcl_or.SetLineColor(2)
    h_ptcl_or.GetXaxis().SetTitleSize(34)
    h_ptcl_or.GetXaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleSize(27)
    h_ptcl_or.GetYaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleOffset(1.5)
    h_ptcl_or.GetYaxis().SetTitle("Events")
    h_ptcl_or.GetYaxis().SetLabelFont(43)
    h_ptcl_or.GetYaxis().SetLabelSize(25)
    legend = TLegend(0.55,0.5,0.85,0.8)
    legend.SetFillStyle(0)
    legend.AddEntry(h_ptcl_or,"Particle")
    legend.AddEntry(h_reco_unfolded,"FBU Baron imp.","p")
    legend.SetBorderSize(0)
    h_reco_unfolded.SetLineColor(1)
    h_reco_unfolded.SetMarkerColor(1)
    h_reco_unfolded.SetMarkerStyle(22)
    h_reco_unfolded.Draw("same p x0")
    h_ptcl_or.Draw("hist same")
    legend.Draw("same")
    pad1.RedrawAxis()
    pad2.cd()
    h_ptcl_or_clone = h_ptcl_or.Clone(h_ptcl_or.GetName()+"_clone"+Name) 
    h_reco_unfolded_clone = h_reco_unfolded.Clone(h_reco_unfolded.GetName()+"_clone"+Name)
    h_ptcl_or_clone.Divide(h_ptcl_or) 
    h_reco_unfolded_clone.Divide(h_ptcl_or) 
    
    h_ptcl_or_clone.GetXaxis().SetTitleSize(27)
    h_ptcl_or_clone.GetXaxis().SetTitleFont(43)
    h_ptcl_or_clone.GetYaxis().SetTitleSize(27)
    h_ptcl_or_clone.GetYaxis().SetTitleFont(43)
    
    h_ptcl_or_clone.GetXaxis().SetLabelFont(43)
    h_ptcl_or_clone.GetXaxis().SetLabelSize(25)
    h_ptcl_or_clone.GetYaxis().SetLabelFont(43)
    h_ptcl_or_clone.GetYaxis().SetLabelSize(25)
    
    h_ptcl_or_clone.SetMaximum(1.3)
    h_ptcl_or_clone.SetMinimum(0.7)
    
    h_ptcl_or_clone.GetXaxis().SetTitleOffset(2.5)
    h_ptcl_or_clone.GetXaxis().SetTitle(args.title)
    
    h_ptcl_or_clone.GetYaxis().SetTitle("Ratio      ")
    h_ptcl_or_clone.Draw("hist")
    h_reco_unfolded_clone.Draw("same p x0")
    pad2.RedrawAxis()
    c.Update()
    PrintCan(c, "ratio"+Name)


def CreateRandomGaus(Dim):
    r = []
    for i in range(Dim):
        r.append(np.random.normal())
    return np.array(r)

def FindReasonableEpsilon(Theta,eps = 1):
    #eps = 1
    Cycle = True
    while Cycle:    
        r = CreateRandomGaus(len(Theta))
        ThetaC, rC = Leapfrog(Theta, r, eps)
        Nominator = CalculateLikelihood(ThetaC)-0.5*rC.dot(rC)
        Denominator = CalculateLikelihood(Theta)-0.5*r.dot(r)
        Probability = np.exp(Nominator-Denominator) 
        if CheckNumber(Probability) != np.nan:
            break
    if (Probability > 0.5):
        a = 1.0
    else:
        a = -1.0
    while (np.power(Probability,a) > np.power(2.0,-a)):
        eps = np.power(2.0,a)*eps
        ThetaC, rC = Leapfrog(Theta, r, eps)
        Nominator = CalculateLikelihood(ThetaC)-0.5*rC.dot(rC)
        Denominator = CalculateLikelihood(Theta)-0.5*r.dot(r)
        Probability = np.exp(Nominator-Denominator) 
        if CheckNumber(Probability) == np.nan:
            return np.nan
    return eps

def IsInputNan(Theta, r, u, v, j, eps):
    #nan = np.nan
    if check(Theta) or check(r) or check(u) or check(v) or check(j) or check(eps):
        return True
    else:
        return False

def IsInputNan2(Theta, r, u, v, j, eps,Theta0, r0):
    #nan = np.nan
    if check(Theta) or check(r) or check(u) or check(v) or check(j) or check(eps) or check(Theta0) or check(r0):
        return True
    else:
        return False

def check(a):
    return np.isnan(np.min(a))

    
def BuildTree(Theta, r, u, v, j, eps, Theta0, r0, layer):
    #print("----------",Theta[0], Theta0[0])
    #time.sleep(1)
    counts = layer
    counts = counts + 1
    if IsInputNan2(Theta, r, u, v, j, eps, Theta0, r0):
        #if math.isnan(Theta[0]):
        ReturnArray = np.empty(Dim,)
        ReturnArray[:] = np.nan
        #print("Here is nan")
        return ReturnArray, ReturnArray , ReturnArray, ReturnArray, ReturnArray ,np.nan, np.nan, np.nan, np.nan, counts
    if (j == 0):
        #print("Theta: hre: ", Theta)
        ThetaC, rC = Leapfrog(Theta, r, v*eps)
        #if GTau !=0:
        #print("returned ThetaC, rC : ", ThetaC, rC)
        check = np.exp(CalculateLikelihood(ThetaC, writeme=1))-0.5*rC.dot(rC)
        check0 = np.exp(CalculateLikelihood(Theta0, writeme=1))-0.5*r0.dot(r0)
        #print("check, u: ", np.exp(check), u)
        if np.isnan(np.min(check)) or np.isnan(np.min(check0)):
            ReturnArray = np.empty(Dim,)
            ReturnArray[:] = np.nan
            #print("Here is nan2")
            return ReturnArray, ReturnArray , ReturnArray, ReturnArray, ReturnArray ,np.nan, np.nan, np.nan, np.nan, counts
        #print("check, u: ", check, u)
        if (np.exp(check) >= u):
            nC = 1 
            #print("******************************************************************************************************************")
        else:
            nC = 0
        #if (check > np.log(u)-1000): # here 1000 denotes DeltaMax, the value is recommanded
        if (np.exp(check) > u-np.exp(250)): # here 1000 denotes DeltaMax, the value is recommanded, delta max = np.exp(250)
            sC = 1
        else:
            sC = 0
        checkC = CalculateLikelihood(ThetaC, writeme=1)-0.5*rC.dot(rC)
        check0C = CalculateLikelihood(Theta0, writeme=1)-0.5*r0.dot(r0)
        aC = min([1.0, np.exp(checkC - check0C)])
        #print("like thC: ", CalculateLikelihood(ThetaC, writeme=1)-0.5*rC.dot(rC), "like th0: ", CalculateLikelihood(Theta0, writeme=1) -0.5*r0.dot(r0))

        return ThetaC, rC, ThetaC, rC, ThetaC, nC, sC, aC, 1.0, counts
    else:
        ThetaM, rM, ThetaP, rP, ThetaC, nC, sC, aC, naC, counts = BuildTree(Theta, r, u, v, j-1, eps, Theta0, r0, counts)
        if (sC == 1):
            if (v == -1):
                ThetaM, rM, lin, lin2, ThetaCC, nCC, sCC, aCC, naCC, counts = BuildTree(ThetaM, rM, u, v, j-1, eps, Theta0, r0, counts)
                #print("ThetaM, rM, lin, lin2, ThetaCC, nCC, sCC", ThetaM, rM, lin, lin2, ThetaCC, nCC, sCC)
            else:
                lin, lin2, ThetaP, rP, ThetaCC, nCC, sCC, aCC, naCC, counts = BuildTree(ThetaP, rP, u, v, j-1, eps, Theta0, r0, counts)
                #print("lin, lin2, ThetaP, rP, ThetaCC, nCC, sCC", lin, lin2, ThetaP, rP, ThetaCC, nCC, sCC)
            #print("nCC/(nCC+nC)",nCC,nC)
            if ((nCC == 0) and (nC == 0)):
                Prob = 0
            else:
                Prob = nCC/(nCC+nC)
            if (Prob > np.random.random_sample()):
                ThetaC = ThetaCC
                #print(Prob)
            DeltaTheta = ThetaP - ThetaM
            if (DeltaTheta.dot(rM) >= 0.0) and (DeltaTheta.dot(rP) >= 0.0):
                sC = sCC
            else:
                sC = 0.0
            nC = nC + nCC
            #print("nC in build tree: ", nC)
            aC = aC + aCC
            naC = naC + naCC
    #print(ThetaM, rM, ThetaP, rP, ThetaC, nC, sC, counts)
    ThetaM = np.array(ThetaM)
    rM = np.array(rM)
    ThetaP = np.array(ThetaP)
    rP = np.array(rP)
    ThetaC = np.array(ThetaC)
    return CheckArray(ThetaM), CheckArray(rM), CheckArray(ThetaP), CheckArray(rP), CheckArray(ThetaC), CheckNumber(nC), CheckNumber(sC), CheckNumber(aC), CheckNumber(naC), counts    

def Leapfrog(Theta, r, eps):
    grad = (eps/2)*Gradient(Theta, eps)
    #if GTau != 0:
    #    print("Grad in leapfrog: ", grad)
    rW = r + grad
    ThetaW = Theta + eps*rW
    rW = rW + grad
    return CheckArray(ThetaW), CheckArray(rW)

def CheckNumber(Number):
    if ((Number == np.nan) or (Number == -np.inf) or (Number == np.inf)):
        return np.nan
    else:
        return Number

def CheckArray(TestArray):
    if ((np.nan in TestArray) or (-np.inf in TestArray) or (np.inf in TestArray)):
        ReturnArray = np.empty(len(TestArray),)
        ReturnArray[:] = np.nan
        return ReturnArray
    else:
        return TestArray

def RandomTheta():
    while True:
        Theta = []    
        for l in range(len(D)):
            Theta.append((upper[l] - lower[l])*np.random.random_sample() + lower[l])
        test = np.exp(CalculateLikelihood(Theta))
        if (CheckNumber(test) != np.nan):
            break
    return Theta

def Epsilon(Theta, eps_or = 1):
    while True:
        eps = FindReasonableEpsilon(Theta, eps_or)
        if eps != np.nan:
            break
    return eps


def Hamilton(data, matrix , low , up , particle , eff , acc  , eps = args.eps, steps = args.counts, Again = 0, Tau = 0, BinCoef = 1.0):   
    global Dim
    global D 
    global M 
    global lower 
    global upper
    global h_ptcl_or
    global h_eff 
    global h_acc 
    global GTau
    
    D = np.array(data)
    Dim = len(data)
    M = np.array(matrix)
    lower = np.array(low)
    upper = np.array(up)
    h_ptcl_or = particle
    h_eff = eff
    h_acc = acc
    GTau = np.array(Tau)

    if Again == 0:
        #ListOfPriors = []
        ListOfPosteriors = []
        ###SamplingGraphs = []
        ###LikeGraphs = []
        Binning = []
        #print("D",D)
        #print("M",M)
        for k in range(len(D)):
            #print("l: ", lower)
            #print("u: ",upper)
            #print("---")
            
            help_array = np.arange(lower[k],upper[k],(upper[k]-lower[k])/(100*BinCoef)) 
            Binning.append(help_array)
            x = array("d",Binning[k])
            posteriors = TH1D("poster_"+str(k),"poster_"+str(k), len(Binning[k])-1,x)
            #priors = TH1D("prior_"+str(k),"prior_"+str(k), len(Binning[k])-1,x)
            ###graph = TGraph2D()
            ###graph.SetName("graph_"+ str(k))
            ###SamplingGraphs.append(graph)
            ###likegraph = TGraph2D()
            ###likegraph.SetName("likegraph_"+ str(k))
            ###LikeGraphs.append(likegraph)
            ListOfPosteriors.append(posteriors)       
            #ListOfPriors.append(priors)

    M_np = np.array(M)
    D_np = np.array(D)
    #B_np = np.array(B)
    T_np = np.array(MakeListFromHisto(h_ptcl_or))

    basic_model = pm.Model()

    

    with basic_model:

        priormethod = getattr(pm,'Uniform')
        print(priormethod)
        truthprior = []
        for bin,(l,u) in enumerate(zip(lower,upper)):
            name = 'prior_in_bin_%d'%(bin+1)
            my_default_args = dict(name=name,lower=l,upper=u)
            print(my_default_args)
            my_args = dict(list(my_default_args.items()))
            prior = priormethod(**my_args)
            print("prior. ",prior)
            truthprior.append(prior)
        truth = pm.math.stack(truthprior)
        #print("TRUTH: ",theano.dot(truth, M_np))
        print("Theano dot, ", theano.dot(truth, M_np))
        unfolded = pm.Poisson('unfolded', mu=theano.dot(truth, M_np), observed=D_np)
        print(unfolded)
        #map_estimate = pm.find_MAP(model=basic_model, method='L-BFGS-B')
        #print(map_estimate)
        #self.MAP = map_estimate
        #self.trace = []
        #self.nuisancestrace = []
        print("Now")
        trace = pm.sample( 10000, tune = 1000, cores = 1, chains = 2, nuts_kwargs = None, discard_tuned_samples = True, progressbar = True )
        print("End of Now")
        #print("trace",trace)
    print(pm.summary(trace))
    print("----------------")
    mytrace = [trace['prior_in_bin_%d'%(bin+1)][:] for bin in range(len(T_np))]

    ListOfPosteriors = MakeTH1Ds(mytrace, lower = lower, upper = upper, BinCoef = BinCoef)
    
    for l in range(len(ListOfPosteriors)):
        if ListOfPosteriors[l].Integral() != 0:
            ListOfPosteriors[l].Scale(1.0/ListOfPosteriors[l].GetMaximum())
        ###for s in range(1,ListOfPosteriors[l].GetNbinsX() - 1):
        ###    ListOfPosteriors[l].SetBinContent(s, np.exp(ListOfPosteriors[l].GetBinContent(s)))
        ###if ListOfPosteriors[l].Integral() != 0:
        ###    ##    ListOfPosteriors[l].Scale(1.0/ListOfPosteriors[l].Integral())
        ###    ListOfPosteriors[l].Scale(1.0/ListOfPosteriors[l].GetMaximum())
        #if ListOfPriors[l].Integral() != 0:
        #    ListOfPriors[l].Scale(1.0/ListOfPriors[l].Integral())
            
    ###Unfolded_Baron, ListOfPost = PlotPosteriors(ListOfPosteriors, SamplingGraphs, ListOfPriors, outputname=args.output)
    Unfolded_Baron, ListOfPost = PlotPosteriors(ListOfPosteriors, outputname=args.output)
    return Unfolded_Baron, ListOfPost

def logp(failure, value):
    return (failure * log(λ) - λ * value).sum()

def MakeTH1Ds(trace, lower , upper , BinCoef = 1.0, tag = 'trace', nbins = 600):
    h1s = []
    gmax = -999
    gmin = 1e19
    for line in trace:
        xmin = min(line)
        xmax = max(line)
        gmin = min(xmin, gmin)
        gmax = max(xmax, gmax)
    i = -1
    gmax = -999
    gmin = 1e19
    Binning = []    
    for line in trace:
        gxmin = min(line)
        gxmax = max(line)
        i = i+1
        hname = tag + '_{:}'.format(i)
        help_array = np.arange(lower[i],upper[i],(upper[i]-lower[i])/(100*BinCoef)) 
        Binning.append(help_array)
        x = array("d",Binning[i])
        #h1 = listBinning[i].Clone(hname)
        #Sh1.Reset()
        h1 = TH1D(hname, hname, len(Binning[i])-1, x)
        #h1 = TH1D(hname, hname, nbins, gmin, gmax)
        print("-----------------------")
        k=0
        for val in line:
            h1.Fill(val)
        h1s.append(h1)
        print(i)
        print("Appending traces")
        print(h1)
        #histograms.append(h1)
    return h1s

def Gradient(T_random, step = 1e-2):
    gradient = []
    #print(T_random)
    for i in range(len(T_random)):
        T_random_plus = T_random.copy()
        if GTau != 0.0:
            step = T_random[i]/100
            T_random_plus[i] = T_random_plus[i] + step
            T_random_minus = T_random.copy()
            T_random_minus[i] = T_random_minus[i] - step
            #print(T_random[i], step)
            R1 = np.array(T_random_plus).dot(M)      
            R2 = np.array(T_random_minus).dot(M)
            #f1 = np.gradient(T_random_plus)
            #f2 = np.gradient(f1)        
            #CURV1 = curvature(f1, f2)   
            #f1 = np.gradient(T_random_minus)
            #f2 = np.gradient(f1)          
            #CURV2 = curvature(f1, f2)   
            delta = np.sum(np.log(R2/R1)+0.5*(R1-R2)*(np.square(D)/(R1*R2)-1) + GTau*(CalculateCurvature(T_random_plus) - CalculateCurvature(T_random_minus)))/(2*step)    
            #print("curv: ", GTau*(CURV1 - CURV2))
        else:
            step = T_random[i]/100
            T_random_plus[i] = T_random_plus[i] + step
            T_random_minus = T_random.copy()
            T_random_minus[i] = T_random_minus[i] - step
            #print(T_random[i], step)
            R1 = np.array(T_random_plus).dot(M)      
            R2 = np.array(T_random_minus).dot(M)
            delta = np.sum(np.log(R2/R1)+0.5*(R1-R2)*(np.square(D)/(R1*R2)-1))/(2*step)
        #plus = CalculateLikelihood(T_random_plus)
        #minus = CalculateLikelihood(T_random_minus)
        #delta = (plus - minus)/(2*step)
        gradient.append(delta)
    return np.array(CheckArray(gradient))

def curvature(f1,f2):
    return abs(f2)*(1 + f1**2)**-1.5

def CalculateCurvature(T_curv):
    CurvSum = 0.0
    for i in range(1,len(T_curv)-1):
        #print(i)
        #print(T_curv[i])
        CurvSum = CurvSum + ((T_curv[i+1] - T_curv[i])-(T_curv[i] - T_curv[i-1]))*((T_curv[i+1] - T_curv[i])-(T_curv[i] - T_curv[i-1]))
    return CurvSum

def CalculateEntropy(T_entro):
    Entropy = 0.0
    for i in range(0,len(T_entro)):
        #print(i)
        #print(T_curv[i])
        Entropy = Entropy + np.log(T_entro[i]/np.sum(T_entro))*T_entro[i]/np.sum(T_entro)
        #print("Entr ", Entropy)
    return np.log(Entropy)


def CalculateLikelihood(T_random, writeme = 0, foreps = 0):
    R = np.array(T_random).dot(M) 
    #lh = np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R) - np.power(GTau,2) #*np.sum(np.power(R-D,2.0))/2.0 works well but slow
    if GTau != 0.0 and foreps == 0:
        #f1 = np.gradient(T_random)
        #f2 = np.gradient(f1)        
        #CURV = curvature(f1, f2)
        #Entro = CalculateEntropy(T_random)
        #print("Entropy: ", Entro)
        CURV2 = CalculateCurvature(T_random)
        #print("Curvature: ", CURV2)
        lh = np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R) - GTau*CURV2#/np.sum(CURV)
        a = np.sum(np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R))
        b = np.sum(lh)
        d = 100*(b - a) / b
        #####R_np = np.array(T_random).dot(np.array(M))  one of the possibilities
        #####D_np = np.array(D)  one of the possibilities
        #####D_np_int64 = D_np.astype(np.int64) one of the possibilities
        #####R_np_int64 = R_np.astype(np.int64) one of the possibilities
        #####lh = np.log(stats.poisson.pmf(D_np_int64, R_np_int64)) one of the possibilities
        #lh = np.log()
        #print("likelihood no curv: ", np.sum(np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R)) , "likelihood curv: ",np.sum(lh) ,"curvature is: ", CURV2*GTau)
        #print("Delta: ", d, " %")
        #print("Curv: ", CURV2)
        #print(np.sum(CURV))
    else:
        lh = np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R)
        #print("here")
    #lh = -0.5*np.log(2*3.1416*R) - 0.5*D*D/R - D + 0.5*R - GTau*GTau
    #lh = np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R) #- np.power(args.tau,2)*np.sum(np.power(R-D,2.0))/2.0
    #print(lh.sum())
    return CheckNumber(lh.sum())

def CalculatePrior(T_random):
    #R = np.array(T_random).dot(M)  # CREATING RECO
    #if args.svd == 1:
    #    C = np.zeros(M.shape)
    #    np.fill_diagonal(C, -2.0)
    #    C[0,0] = -1.0
    #    C[len(T_random)-1,len(T_random)-1] = -1.0
    #    OfsetDiag = np.ones(len(T_random)-1)
    #    np.fill_diagonal(C[1:], OfsetDiag)
    #    np.fill_diagonal(C[:,1:], OfsetDiag)
    #    w = C.dot(T_random)
    #    lh = args.tau*w.dot(w)
    #else:
    #    lh = - np.power(args.tau,2)*np.sum(np.power(R-D,2.0))/2.0
    #print("PRIOR: ", lh.sum())
    f1 = np.gradient(T_random)
    f2 = np.gradient(f1)        
    # print("curv: ", curvature(f1,f2))
    CURV = curvature(f1, f2)
    lh = -GTau*CURV
    return np.exp(lh)

        
//=====================================================================-*-C++-*-
// File and Version Information:
//      $Id: RooUnfoldExample.cxx 354 2017-07-11 17:49:50Z T.J.Adye@rl.ac.uk $
//
// Description:
//      Simple example usage of the RooUnfold package using toy MC.
//
// Authors: Tim Adye <T.J.Adye@rl.ac.uk> and Fergus Wilson <fwilson@slac.stanford.edu>
//
//==============================================================================

#if !(defined(__CINT__) || defined(__CLING__)) || defined(__ACLIC__)
#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
using std::cout;
using std::endl;
using std::vector;

#include "TRandom.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TGraph2D.h"
#include "TString.h"
#include "TTimer.h"
#include "TF2.h"
#include "TColor.h"
#include "TLine.h"

#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldResponse.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldBayes.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldBinByBin.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldBaron.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldSvd.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldTUnfold.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldIds.h"
#endif

//==============================================================================
// Global definitions
//==============================================================================

const Double_t cutdummy= -99999.0;
vector<TH1D*> TH1Ds;
vector<TH2D*> TH2Ds;

vector<TCanvas*> Canvases;
//==============================================================================
// Gaussian smearing, systematic translation, and variable inefficiency
//==============================================================================

Double_t smear (Double_t xt)
{
  Double_t xeff= 0.3 + (1.0-0.3)/20*(xt+10.0);  // efficiency
  Double_t x= gRandom->Rndm();
  if (x>xeff) return cutdummy;
  Double_t xsmear= gRandom->Gaus(-2.5,0.2);     // bias and smear
  return xt+xsmear;
}

//==============================================================================
// Example Unfolding
//==============================================================================

// PLOTING

void PlotListOfPosteriors( vector<TH1D *> ListOfPosteriors, float Tau , TH1D * Truth){
  int Dim = ListOfPosteriors.size();
  TCanvas * can_0 = new TCanvas("Posteriors", "Posteriors", 0, 0, 1600, 1600);
  can_0->Divide((int)(TMath::Power(Dim, 0.5) + 1) , (int)(TMath::Power(Dim, 0.5) + 1));
  //can_0->Divide(2,2);
  Double_t x, q;
  q = 0.5; 
  vector<TLine*> lines;
  vector<TLine*> TruthLines;
  for (int i = 0; i < Dim; i++){
        x=0;
        q=0.5;
        ListOfPosteriors[i]->ComputeIntegral(); // just a precaution
        ListOfPosteriors[i]->GetQuantiles(1, &x, &q);  
        TLine * line = new TLine(x,0,x,ListOfPosteriors[i]->GetMaximum());
        TLine * Truthline = new TLine(Truth->GetBinContent(i),0,Truth->GetBinContent(i),ListOfPosteriors[i]->GetMaximum());
        lines.push_back(line);
        TruthLines.push_back(Truthline);
        can_0->cd(i+1);
        ListOfPosteriors[i]->SetTitle(TString::Format("%s, Reg. par. = %f", ListOfPosteriors[i]->GetTitle() ,Tau));
        ListOfPosteriors[i]->GetXaxis()->SetTitle(TString::Format("Truth Value in bin %i", i + 1));
        ListOfPosteriors[i]->GetYaxis()->SetTitle("");
        ListOfPosteriors[i]->Draw("hist");
        ListOfPosteriors[i]->Fit("gaus", "q");
        TF1 * f = ListOfPosteriors[i]->GetFunction("gaus");
        if(f != nullptr){f->Draw("same");}
        lines[i]->SetLineColor(2);
        lines[i]->SetLineWidth(2);
        lines[i]->Draw("same");
        TruthLines[i]->SetLineColor(kGreen -2);
        TruthLines[i]->SetLineWidth(2);
        TruthLines[i]->Draw("same");
  }
  Canvases.push_back(can_0);
}

void PlotPathsOfLeapFrog(vector<TGraph2D *> PathsOfLeapFrog){
    TCanvas * can_1 = new TCanvas("Paths of LeapFrog Function", "Paths of LeapFrog Function", 0, 0, 1600, 1600);
    int Dim = PathsOfLeapFrog.size();
    can_1->Divide((int)(TMath::Power(Dim, 0.5) + 1 ) , (int)(TMath::Power(Dim, 0.5) + 1));
    for (int i = 0; i < Dim; i++){
        can_1->cd(i+1);
        gStyle->SetPalette(1);
        PathsOfLeapFrog[i]->SetMarkerStyle(20);
        PathsOfLeapFrog[i]->Draw("surf4");
      }
    Canvases.push_back(can_1);
}

void PlotPathsOfLeapFrogWeightedByLikelihood(vector<TGraph2D *> PathsOfLeapFrogWeightedByLikelihood){
    TCanvas * can_2 = new TCanvas("Paths_of_LeapFrog_Function_Weighted_by_Likelihood", "Paths of LeapFrog Function Weighted by Likelihood", 0, 0, 1600, 1600);
    gStyle->SetCanvasPreferGL(true);
    
    int Dim = PathsOfLeapFrogWeightedByLikelihood.size();
    can_2->Divide((int)(TMath::Power(Dim, 0.5) + 1 ) , (int)(TMath::Power(Dim, 0.5) + 1));
    //can_2->Divide(2,2);
    for (int i = 0; i < Dim; i++){
        can_2->cd(i+1);
        //cout << "Margin: " << PathsOfLeapFrogWeightedByLikelihood[i]->GetMargin() << endl;
        gStyle->SetPalette(1);
        PathsOfLeapFrogWeightedByLikelihood[i]->GetXaxis()->SetTitle(TString::Format("Truth value in the bin %i",i+1));
        PathsOfLeapFrogWeightedByLikelihood[i]->GetYaxis()->SetTitle("Velocity");
        PathsOfLeapFrogWeightedByLikelihood[i]->GetZaxis()->SetTitle("Likelihood");
        PathsOfLeapFrogWeightedByLikelihood[i]->GetXaxis()->SetTitleOffset(1.3);
        PathsOfLeapFrogWeightedByLikelihood[i]->GetYaxis()->SetTitleOffset(1.1);
        PathsOfLeapFrogWeightedByLikelihood[i]->GetZaxis()->SetTitleOffset(1.1);
        PathsOfLeapFrogWeightedByLikelihood[i]->SetMarkerStyle(20);
        PathsOfLeapFrogWeightedByLikelihood[i]->Draw("pcol");
      }
    Canvases.push_back(can_2);
}


void PlotNormalizedUnfoldingMatrix(TH2D * NormalizedUnfoldingMatrix, float Tau){
        TCanvas * can_3 = new TCanvas("Normalized_Unfolding_Matrix", "Normalized Unfolding Matrix", 0, 0, 1600, 1600);
        can_3->cd();
        gStyle->SetOptStat(0);
        NormalizedUnfoldingMatrix->SetTitle(TString::Format("%s, Reg. par. = %f", NormalizedUnfoldingMatrix->GetTitle() ,Tau));
        //NormalizedUnfoldingMatrix->GetXaxis()->SetRangeUser(0.0,1.0);
        gStyle->SetPaintTextFormat("4.2f");
        NormalizedUnfoldingMatrix->SetMarkerSize(2); 
        NormalizedUnfoldingMatrix->Draw("colz text");
        Canvases.push_back(can_3);
}

void PlotBinCorrelationMatrix(vector<vector<TH2D *>> BinCorrelationMatrix, vector<TH1D *> ListOfPosteriors ){
    TCanvas * can_4 = new TCanvas("Bin_Correlation_Matrices", "Bin Correlation Matrices", 0, 0, 1600, 1600);
    int Dim = BinCorrelationMatrix.size();
    can_4->Divide(Dim, Dim);
    for (int i = 0 ; i < Dim ; i++){
        for (int j = 0 ; j < Dim ; j++){
        can_4->cd((int)((j + 1) + Dim*(Dim - (i + 1))));      
        BinCorrelationMatrix[i][j]->GetXaxis()->SetTitle(TString::Format("Events in bin %i", i+1));
        BinCorrelationMatrix[i][j]->GetYaxis()->SetTitle(TString::Format("Events in bin %i", j+1));
        //cout  << "Limits x: " << ListOfPosteriors[i]->GetXaxis()->GetBinLowEdge(ListOfPosteriors[i]->FindFirstBinAbove( )) << " " << ListOfPosteriors[i]->GetXaxis()->GetBinUpEdge(ListOfPosteriors[i]->FindLastBinAbove( )) << endl;
        //cout  << "Limits y: " << ListOfPosteriors[j]->GetYaxis()->GetBinLowEdge(ListOfPosteriors[j]->FindFirstBinAbove( )) << " " << ListOfPosteriors[j]->GetYaxis()->GetBinUpEdge(ListOfPosteriors[j]->FindLastBinAbove( )) << endl;
        //BinCorrelationMatrix[i][j]->GetXaxis()->SetRangeUser(ListOfPosteriors[i]->GetXaxis()->GetBinLowEdge(ListOfPosteriors[i]->FindFirstBinAbove( )), ListOfPosteriors[i]->GetXaxis()->GetBinUpEdge(ListOfPosteriors[i]->FindLastBinAbove( )) );
        //BinCorrelationMatrix[i][j]->GetYaxis()->SetRangeUser(ListOfPosteriors[j]->GetYaxis()->GetBinLowEdge(ListOfPosteriors[j]->FindFirstBinAbove( )), ListOfPosteriors[j]->GetYaxis()->GetBinUpEdge(ListOfPosteriors[j]->FindLastBinAbove( )) );
        float Correlation = BinCorrelationMatrix[i][j]->GetCorrelationFactor();
        //TH2CorrelationMatrix->SetBinContent(i+1,j+1, Correlation);
        TLegend * leg = new TLegend(0.1,0.8,0.45,0.9);
        leg->SetHeader(TString::Format("Corr. factor = %.4f", Correlation));
        //gStyle->SetLegendBorderSize(0);
        gStyle->SetLegendTextSize(0.05);
        gStyle->SetOptStat(0);
        //BinCorrelationMatrix[i][j]->GetZaxis()->SetRangeUser(-1.0,1.0);
        BinCorrelationMatrix[i][j]->Draw("colz");
        leg->Draw("same");
        }
    }
    Canvases.push_back(can_4);
}

void PlotBinCorrelationFactorMatrix(TH2D * BinCorrelationFactorMatrix, TString TitleCorr){
        TCanvas * can_5 = new TCanvas("Bin_Correlation_Factor_Matrix", "Bin Correlation Factor Matrix", 0, 0, 1600, 1600);
        can_5->cd();
        gStyle->SetOptStat(0);
        BinCorrelationFactorMatrix->SetLineColor(1);
        BinCorrelationFactorMatrix->SetTitle(TitleCorr.Data());
        BinCorrelationFactorMatrix->Draw("colz text");
        Canvases.push_back(can_5);
        TCanvas * can_5b = new TCanvas("Bin_Correlation_Factor_Matrix_lego2", "Bin Correlation Factor Matrix_lego2", 0, 0, 1600, 1600);
        can_5b->cd();
        gStyle->SetOptStat(0);
        BinCorrelationFactorMatrix->SetLineColor(1);
        BinCorrelationFactorMatrix->SetTitle(TitleCorr.Data());
        BinCorrelationFactorMatrix->Draw("lego2");
        Canvases.push_back(can_5b);
}

void PlotRelativeRecoResolution(TH1D * RelativeRecoResolution, float Tau){
      TCanvas * can_6 = new TCanvas("Relative Reco Resolution", "Relative Reco Resolution", 0, 0, 1600, 1600);
      can_6->cd();
      //gStyle->SetOptStat(0);
      RelativeRecoResolution->SetTitle(TString::Format("%s, Reg. par. = %f", RelativeRecoResolution->GetTitle(),Tau));
      RelativeRecoResolution->SetMinimum(0.0);
      //RelativeRecoResolution->SetMaximum(1.1);
      RelativeRecoResolution->Draw("hist");
      Canvases.push_back(can_6);

}

void PlotReziduals(TH1D* h_observed, TH1D* h_expected, TString ParameterString = ""){
  gStyle->SetOptStat(1111111111);
  TCanvas * can_7 = new TCanvas("Residuals_from_Ratio_Plot", "Residuals_from_Ratio_Plot", 0, 0, 1600, 1600);
  can_7->cd();
  //TPad * pad1 = new TPad("pad1","pad1",0,0,1,1);
  ////pad1->SetTopMargin(0.15);
  ////pad1->SetBottomMargin(0.01);
  //pad1->SetFillStyle(0);
  ////pad1->SetTicks(1,1);
  ////pad1->SetBorderMode(0);
  //pad1->Draw();
  //pad1->cd();
  TH1D * residual = new TH1D("Residuals Derived from Ratio Plot", "Residuals Derived from Ratio Plot", 1000, -10, 10);
  for (int i = 1; i<=h_expected->GetXaxis()->GetNbins(); i++){
    residual->Fill(h_observed->GetBinContent(i)-h_expected->GetBinContent(i));
  }
  //can_7->cd();
  //pad1->cd();
  residual->Draw("hist");
  //gPad->Update();
  can_7->Print(TString::Format("./plots/%s_can_%s.png", ParameterString.Data(), can_7->GetName()));  
  can_7->Print(TString::Format("./plots/%s_can_%s.pdf", ParameterString.Data(), can_7->GetName()));  
  //Canvases.push_back(can_7);
}

double CalculateChi2NDF(TH1D* h_observed, TH1D* h_expected){
  cout << "I am in the function chi2." << endl;
  double chi2 = 0.0;
  cout << "1." << endl;
  for (int i = 1; i<=h_expected->GetXaxis()->GetNbins(); i++){
    cout << i << endl;
    cout << "Deltas squered: " << (h_observed->GetBinContent(i)-h_expected->GetBinContent(i))*(h_observed->GetBinContent(i)-h_expected->GetBinContent(i)) << endl;
    chi2 = chi2 + (h_observed->GetBinContent(i)-h_expected->GetBinContent(i))*(h_observed->GetBinContent(i)-h_expected->GetBinContent(i))/h_observed->GetBinError(i);
    cout << "chi2: " << chi2 << endl;
  }
  chi2 = chi2/h_expected->GetXaxis()->GetNbins();
  cout << "999." << endl;
  return chi2;
}

// END OF PLOTTING FUNCTIONS

//global variables
TTimer *timer = new TTimer(0);
double x,y,z;
double x2,y2,z2;
int t = 1;
float phi = 30;
TGraph2D * GlobalGraph;
TGraph2D * GlobalGraphLog = new TGraph2D();
TH2D * h_GlobalGraph = new TH2D();
TH2D * h_GlobalGraphLog = new TH2D();
////TCanvas * can_anim = new TCanvas("Paths of LeapFrog Function Weighted by Likelihood Animation", "Paths of LeapFrog Function Weighted by Likelihood Animation", 0, 0, 1600, 1600);
////TCanvas * can_anim2 = new TCanvas("Paths of LeapFrog Function Weighted by Likelihood Animation2", "Paths of LeapFrog Function Weighted by Likelihood Animation2", 0, 0, 1600, 1600);
TF2  *f2 = new TF2("f2","[0]*(-1)*x*x+[1]*(-1)*y*y + [2]",0,7000,-40,40);
TF2  *f2gaus = new TF2("f2gaus","gaus",2800,3000,-5,5);
TF2  *_f2 = new TF2();
TF2  *_f2gaus = new TF2();

void DivideEff(TH1D * h,TH1D * eff){
    for (int i=1;i < h->GetNbinsX()+1 ;i++){
      if (eff->GetBinContent(i) != 0.0){
        h->SetBinContent(i,h->GetBinContent(i)/eff->GetBinContent(i));
      } else{
        cout << "WARNING: Nedelim histogram, eff = 0 v binu: " << i << endl;
      }
    }
    return h;
} 

void DivideEffTruth(TH1D * h,TH1D * eff){
    for (int i=1;i < h->GetNbinsX()+1 ;i++){
      if (eff->GetBinContent(i) != 0.0){
        h->SetBinContent(i,h->GetBinContent(i)/eff->GetBinContent(i));
        //cout << "ERROR: " << eff->GetBinError(i) << endl;
        //cout << "CONTENT: " << eff->GetBinContent(i) << endl;
        //cout << "New ERROR: " << eff->GetBinError(i)/eff->GetBinContent(i) << endl;
        //cout << "****************************8" << endl;
        h->SetBinError(i,eff->GetBinError(i)/eff->GetBinContent(i));
      } else{
        cout << "WARNING: Nedelim histogram, eff = 0 v binu: " << i << endl;
      }
    }
    return h;
} 

int RooUnfoldExample(int NRebin = 1, float Tau = 0.0, TString RegMethod = "", int NSteps = 100000, int Seed = 10, TString Spectrum="TopPairEta", int Closure = 0)
{
  
  cout << "==================================== START TO UNFOLD ===================================" << endl;
  cout << "==================================== Parameter= " << NRebin << endl;
  //RooUnfoldResponse response (40, -10.0, 10.0);
  
  TFile *rfile_data = new TFile("/home/petr/GitLab/unfolding/CCode/RooUnfold/input_hMeas.root", "read");
  TFile *rfile_particle = new TFile("/home/petr/GitLab/unfolding/CCode/RooUnfold/input_hMeas.root", "read");
  TFile *rfile_matrix = new TFile("/home/petr/GitLab/unfolding/CCode/RooUnfold/input.root", "read");

  TString PartName = TString::Format("Particle/MC%s", Spectrum.Data());
  TString DataName = TString::Format("Reco/%s", Spectrum.Data());
  TString MatrixName = TString::Format("Matrix/Migra%s",Spectrum.Data());


  TH1D *hTruth = (TH1D*)rfile_particle->Get(PartName.Data());
  //TH1D *hMeas = (TH1D*)rfile_data->Get("Reco/TopPairEta");
  //if (Closure == 1){
  TH1D *hMeas = (TH1D*)rfile_data->Get(PartName.Data());   

  TH1D *hTruthA = (TH1D*)rfile_matrix->Get(PartName.Data());
  TH1D *hMeasA = (TH1D*)rfile_matrix->Get(PartName.Data());   
  //} else{
  //  TH1D *hMeas = (TH1D*)rfile_data->Get(DataName.Data()); 
  //}

  TH2D *hMatrix = (TH2D*)rfile_matrix->Get(MatrixName.Data());
  hMatrix->ClearUnderflowAndOverflow();
  TH2D *hMatrixTranspose = (TH2D*)hMatrix->Clone("hMatrixTrasnpose");
  //TH1F *hnew = (TH1F*)h->Clone("hnew");
  hMatrixTranspose->Reset();
  int nrebin = NRebin;
  hMeas->Rebin(nrebin);
  hTruth->Rebin(nrebin);
  hMeasA->Rebin(nrebin);
  hTruthA->Rebin(nrebin);
  hMatrix->Rebin2D(nrebin,nrebin);
  hMatrixTranspose->Rebin2D(nrebin,nrebin);

  double VMin1, VMin2, VMax1, VMax2, SumRow, SumColumn;;
  VMin1 = -999;
  VMin2 = -999;
  VMax1 = -999;
  VMax2 = -999;
  vector<double> listlow1,listup1, listlow2, listup2;

  SumRow = 0.0;

  for (int i = 1; i <= hMatrix->GetNbinsX(); i++){
    SumColumn = 0.0;
    for (int j = 1; j <= hMatrix->GetNbinsX(); j++){
      SumColumn += hMatrix->GetBinContent(i,j) ;
      //SumRow += hMatrix->GetBinContent(j,i);
  }
   if (SumColumn == 0){
     if (i < hMatrix->GetNbinsX()/2.0){
        listlow1.push_back(i);  
        cout << i << endl;
     } else{
       listup1.push_back(i);
     }
   }
  }

  cout << "-----------" << endl;

  for (int i = 1; i <= hMatrix->GetNbinsX(); i++){
    SumColumn = 0.0;
    for (int j = 1; j <= hMatrix->GetNbinsX(); j++){
      SumColumn += hMatrix->GetBinContent(j,i) ;
      //SumRow += hMatrix->GetBinContent(j,i);
  }
   if (SumColumn == 0){
    if (i < hMatrix->GetNbinsX()/2.0){
        listlow2.push_back(i);  
        cout << i << endl;
     } else{
       listup2.push_back(i);
     }
   }
  }

  if (listlow1.size() > 0.0){
    VMin1 = listlow1[listlow1.size()-1];
  }
  if (listlow2.size() > 0.0){
    VMin2 = listlow2[listlow2.size()-1];
  }
  
  //cout << "listlow1: " << listlow1[0] << endl;
  //cout << "listlow2: " << listlow2[0] << endl;

  if (listup1.size() > 0.0){
    VMax1 = listup1[0] - 1;

  }
  if (listup2.size() > 0.0){
    VMax2 = listup2[0] - 1;
  }

  if (VMin1 < VMin2){
    VMin1 = VMin2;
  }

  if (VMax1 > VMax2){
    VMax1 = VMax2;
  }

  bool CorrectLeftSide = false;
  bool CorrectRightSide = false;
  bool CorrectBothSides = false;
  bool DoNotCorrect = false;

  if ((listlow1.size() > 0.0) || (listlow2.size() > 0.0)){
    CorrectLeftSide = true;
  }

  if ((listup1.size() > 0.0) || (listup2.size() > 0.0)){
    CorrectRightSide = true;
  }

  if ((CorrectLeftSide == true) && (CorrectRightSide == true)){
    CorrectBothSides = true;
  }
   
  if ((CorrectLeftSide == false) && (CorrectRightSide == false)){
    DoNotCorrect = true;
  }


  cout << "-------------" << endl; 
  bool ApplyZoom = false;
  if ((VMin1 != -999) || (VMax1 != -999)){
    ApplyZoom = true;
  }

  if (VMin1 == -999){
    VMin1 = 1;
  } 
  if (VMax1 == -999){
    VMax1 = hMatrix->GetNbinsX();
  }

  cout << CorrectLeftSide  << endl;
  cout << CorrectRightSide << endl;
  cout << CorrectBothSides << endl;
  cout << DoNotCorrect << endl;
  TH2D * hMatrix2;
  TH2D* hMatrixTranspose2;
  TH1D * hMeas2;
  TH1D * hMeasA2;
  TH1D * hTruth2;
  TH1D * hTruthA2;



  cout << hMatrix->GetXaxis()->GetNbins() << endl;

  //cout << hMatrix->GetNbinsX() << endl;
  //const TArrayD * xbins = hMatrix->GetXaxis()->GetXbins();
  //cout << xbins << endl;
  //const Double_t * xbinsArray = xbins->GetArray();
  //cout << xbinsArray << endl;
  //cout << "here" << endl;
  cout << "VMin: " << VMin1 << endl;
  cout << "VMax: " << VMax1 << endl;



  if ((CorrectLeftSide == true) && (CorrectRightSide == false)){ 
    TArrayD * helpArray = new TArrayD(hMatrix->GetNbinsX() - VMin1+1);
    cout << "N elements: " << hMatrix->GetNbinsX() - VMin1 << endl;
    for (int i = VMin1; i<=hMatrix->GetXaxis()->GetNbins(); i++){
      //cout << hMatrix->GetXaxis()->GetBinLowEdge(i) << endl;
      helpArray->AddAt(hMatrix->GetXaxis()->GetBinLowEdge(i+1),i-VMin1);
      cout << "Now helparray: " << i-VMin1 << " " << hMatrix->GetXaxis()->GetBinLowEdge(i+1) << endl;
    }
    //hMatrix2 = new TH2D("hMatrix2", "hMatrix2", (int)(VMax1-1), hMatrix->GetXaxis()->GetBinLowEdge(1), hMatrix->GetXaxis()->GetBinLowEdge(VMax1), (int)(VMax1 -1), hMatrix->GetXaxis()->GetBinLowEdge(1), hMatrix->GetXaxis()->GetBinLowEdge(VMax1) );  
    hMatrix2 = new TH2D("hMatrix2", "hMatrix2", (int)(hMatrix->GetNbinsX() - VMin1), (Double_t *)helpArray->GetArray(), (int)(hMatrix->GetNbinsX() - VMin1),  (Double_t *)helpArray->GetArray());  
    hMatrixTranspose2  = new TH2D(hMatrixTranspose->GetName(), hMatrixTranspose->GetTitle(), (int)(hMatrix->GetNbinsX() - VMin1), (Double_t *)helpArray->GetArray(), (int)(hMatrix->GetNbinsX() - VMin1),  (Double_t *)helpArray->GetArray());  
    hMeas2 =  new TH1D(hMeas->GetName(), hMeas->GetTitle(), (int)(hMatrix->GetNbinsX() - VMin1), (Double_t *)helpArray->GetArray());
    hMeasA2 =  new TH1D(hMeasA->GetName(), hMeasA->GetTitle(), (int)(hMatrix->GetNbinsX() - VMin1), (Double_t *)helpArray->GetArray());
    hTruth2 =  new TH1D(hTruth->GetName(), hTruth->GetTitle(), (int)(hMatrix->GetNbinsX() - VMin1), (Double_t *)helpArray->GetArray());
    hTruthA2 = new TH1D(hTruthA->GetName(), hTruthA->GetTitle(), (int)(hMatrix->GetNbinsX() - VMin1), (Double_t *)helpArray->GetArray());    
    
    //hMatrix2->Draw("colz");
    //gApplication->Run();
  }

  if ((CorrectLeftSide == false) && (CorrectRightSide == true)){
    TArrayD * helpArray = new TArrayD(VMax1);
    for (int i = 0; i<VMax1; i++){
      //cout << hMatrix->GetXaxis()->GetBinLowEdge(i) << endl;
      helpArray->AddAt(hMatrix->GetXaxis()->GetBinLowEdge(i+1),i);
      cout << "Now right: " << i << " " << hMatrix->GetXaxis()->GetBinLowEdge(i+1) << endl;
    }
    //hMatrix2 = new TH2D("hMatrix2", "hMatrix2", (int)(VMax1-1), hMatrix->GetXaxis()->GetBinLowEdge(1), hMatrix->GetXaxis()->GetBinLowEdge(VMax1), (int)(VMax1 -1), hMatrix->GetXaxis()->GetBinLowEdge(1), hMatrix->GetXaxis()->GetBinLowEdge(VMax1) );  
    hMatrix2 = new TH2D("hMatrix2", "hMatrix2", (int)(VMax1-1), (Double_t *)helpArray->GetArray(), (int)(VMax1 -1), (Double_t *)helpArray->GetArray());  
    hMatrixTranspose2  = new TH2D(hMatrixTranspose->GetName(), hMatrixTranspose->GetTitle(), (int)(VMax1-1), (Double_t *)helpArray->GetArray(), (int)(VMax1 -1), (Double_t *)helpArray->GetArray());  
    hMeas2 =  new TH1D(hMeas->GetName(), hMeas->GetTitle(), (int)(VMax1-1), (Double_t *)helpArray->GetArray());
    hMeasA2 =  new TH1D(hMeasA->GetName(), hMeasA->GetTitle(), (int)(VMax1-1), (Double_t *)helpArray->GetArray());
    hTruth2 =  new TH1D(hTruth->GetName(), hTruth->GetTitle(), (int)(VMax1-1), (Double_t *)helpArray->GetArray());
    hTruthA2 = new TH1D(hTruthA->GetName(), hTruthA->GetTitle(), (int)(VMax1-1), (Double_t *)helpArray->GetArray());  
    

  }

  if (CorrectBothSides == true){
    TArrayD * helpArray = new TArrayD(VMax1 - VMin1);
    for (int i = VMin1; i<VMax1; i++){
      //cout << hMatrix->GetXaxis()->GetBinLowEdge(i) << endl;
      helpArray->AddAt(hMatrix->GetXaxis()->GetBinLowEdge(i+1),i - VMin1);
      cout << "Now both: " << i + 1 << " " << hMatrix->GetXaxis()->GetBinLowEdge(i+1) << endl;
    }

    hMatrix2 = new TH2D("hMatrix2", "hMatrix2", (int)(VMax1 - VMin1 - 1), (Double_t *)helpArray->GetArray(), (int)(VMax1 - VMin1 - 1), (Double_t *)helpArray->GetArray() );  
    hMatrixTranspose2  = new TH2D(hMatrixTranspose->GetName(), hMatrixTranspose->GetTitle(), (int)(VMax1 - VMin1 - 1), (Double_t *)helpArray->GetArray(),(int)(VMax1 - VMin1 - 1), (Double_t *)helpArray->GetArray());  
    hMeas2 =  new TH1D(hMeas->GetName(), hMeas->GetTitle(), (int)(VMax1 - VMin1 - 1), (Double_t *)helpArray->GetArray());
    hMeasA2 =  new TH1D(hMeasA->GetName(), hMeasA->GetTitle(), (int)(VMax1 - VMin1 - 1), (Double_t *)helpArray->GetArray());
    hTruth2 =  new TH1D(hTruth->GetName(), hTruth->GetTitle(), (int)(VMax1 - VMin1 - 1), (Double_t *)helpArray->GetArray());
    hTruthA2 = new TH1D(hTruthA->GetName(), hTruthA->GetTitle(), (int)(VMax1 - VMin1 - 1), (Double_t *)helpArray->GetArray());  
    
  }


  //TH2D * hMatrix2 = (TH2D *)hMatrix->Clone("hMatrix2");
  if (DoNotCorrect == false){
  //cout << "VMin: " << VMin1 << endl;
  //cout << "VMax: " << VMax1 << endl;
  //cout << "N bins: " << (int)(VMax1 - VMin1 + 1) << endl;
  //cout << "Xmin: " << hMatrix->GetXaxis()->GetBinLowEdge(VMin1) << endl;
  //cout << "XMax: " << hMatrix->GetXaxis()->GetBinLowEdge(VMax1) << endl;
  //cout << "Ymin: " << hMatrix->GetXaxis()->GetBinLowEdge(VMin1) << endl;
  //cout << "YMax: " << hMatrix->GetXaxis()->GetBinLowEdge(VMax1) << endl;
  if (CorrectRightSide == true){
    for (int i = 1; i <= hMatrix2->GetNbinsX(); i++){
        for (int j = 1; j <= hMatrix2->GetNbinsX(); j++){
              hMatrix2->SetBinContent(i, j ,hMatrix->GetBinContent(i , j ));
              hMatrixTranspose2->SetBinContent(i, j ,hMatrixTranspose->GetBinContent(i , j ));
        }
      hMeas2->SetBinContent(i, hMeas->GetBinContent(i));
      hMeasA2->SetBinContent(i, hMeasA->GetBinContent(i));
      hTruth2->SetBinContent(i, hTruth->GetBinContent(i));
      hTruthA2->SetBinContent(i, hTruthA->GetBinContent(i));
      }
  }

  if (CorrectLeftSide == true){
    for (int i = 1; i <= hMatrix2->GetNbinsX(); i++){
        for (int j = 1; j <= hMatrix2->GetNbinsX(); j++){
              hMatrix2->SetBinContent(i, j ,hMatrix->GetBinContent(i + VMin1, j + VMin1));
              hMatrixTranspose2->SetBinContent(i, j ,hMatrixTranspose->GetBinContent(i + VMin1, j + VMin1));
        }
      hMeas2->SetBinContent(i, hMeas->GetBinContent(i + VMin1));
      hMeasA2->SetBinContent(i, hMeasA->GetBinContent(i + VMin1));
      hTruth2->SetBinContent(i, hTruth->GetBinContent(i + VMin1));
      hTruthA2->SetBinContent(i, hTruthA->GetBinContent(i + VMin1));
      }
  }

  if (CorrectBothSides == true){
    for (int i = 1; i <= VMax1; i++){
        for (int j = 1; j <= VMax1; j++){
              hMatrix2->SetBinContent(i, j ,hMatrix->GetBinContent(i + VMin1, j + VMin1));
              hMatrixTranspose2->SetBinContent(i, j ,hMatrixTranspose->GetBinContent(i + VMin1, j + VMin1));
        }
      hMeas2->SetBinContent(i, hMeas->GetBinContent(i + VMin1));
      hMeasA2->SetBinContent(i, hMeasA->GetBinContent(i + VMin1));
      hTruth2->SetBinContent(i, hTruth->GetBinContent(i + VMin1));
      hTruthA2->SetBinContent(i, hTruthA->GetBinContent(i + VMin1));
      }
  }

  TCanvas * testcan = new TCanvas("newcan", "newcan", 0 , 0, 1600, 1600);
  testcan->Divide(2,2);
  testcan->cd(1);
  gStyle->SetOptStat(0);
  hMatrix->Draw("colz text");

  testcan->cd(2);
  gStyle->SetOptStat(0);
  hMatrix2->Draw("colz text");
  testcan->cd(3);
  gStyle->SetOptStat(0);
  hMatrix2->SetLineColor(2);
  hMatrix2->ProjectionX()->Draw("hist");
  hMatrix->ProjectionX()->Draw("hist same");
  testcan->cd(4);
  TH1D * hMeasClone;
  hMeasClone = (TH1D *)hMeas->Clone("");
  hMeasClone->Draw("hist");
  TH1D * hMeas2Clone;
  hMeas2Clone = (TH1D *)hMeas2->Clone("");
  hMeas2Clone->SetLineColor(2);
  hMeas2Clone->Draw("hist same");



  hMatrix = hMatrix2;
  hMatrixTranspose = hMatrixTranspose2;
  hMeas=hMeas2;
  hMeasA=hMeasA2;
  hTruth=hTruth2;
  hTruthA=hTruthA2;


  
  //" " << VMax2 << endl;

  } else {
    //hMatrix2 = (TH2D *)hMatrix->Clone("hMatrix2");
    cout << "NO CUT CORRECTION" << endl;
  }

  
  for (int i = 1; i <= hMatrix->GetNbinsX(); i++){
    for (int j = 1; j <= hMatrix->GetNbinsX(); j++){
      hMatrixTranspose->SetBinContent(i,j,hMatrix->GetBinContent(j,i));
    }
  }


  hMatrixTranspose->GetXaxis()->SetTitle(hMatrix->GetYaxis()->GetTitle());
  hMatrixTranspose->GetYaxis()->SetTitle(hMatrix->GetXaxis()->GetTitle());

  TH1D *hAcc = (TH1D*)hMatrix->ProjectionX("hAcc");
  hAcc->Divide(hMeasA);
  //hMeas->Multiply(hAcc);
  hMeas->Multiply(hAcc);
  TH1D *hEff = (TH1D*)hMatrix->ProjectionY("hEff");
  hEff->Divide(hTruthA);  

  RooUnfoldResponse response (hMeas, hTruth, hMatrix , "Test", "Test");
  //RooUnfoldResponse response2 (hMeas, hTruth, hMatrix , "Test2", "Test2");

  auto m_RooUnfold = RooUnfoldBayes();
  m_RooUnfold.SetRegParm( 4 );
  m_RooUnfold.SetNToys( 10000 );
  m_RooUnfold.SetVerbose( 3 );
  m_RooUnfold.SetSmoothing( 0 );
  //RooUnfoldResponse response;
  RooUnfoldResponse * response2 = new RooUnfoldResponse(nullptr, nullptr, hMatrix, "response", "methods");
  //RooUnfoldResponse * response2 = new RooUnfoldResponse(nullptr, nullptr, hMatrixTranspose, "response", "methods");
  m_RooUnfold.SetResponse( response2 );
  m_RooUnfold.SetMeasured( hMeas );


  //TH1Ds.push_back(hMeas);
  //TH2Ds.push_back(response);

  //RooUnfoldBayes   unfoldBayes (&response2, hMeas, 4);    // OR
  //RooUnfoldSvd     unfoldBayes (&response, hMeas, 20);   // OR
  ////RooUnfoldBayes unfoldBayes (&response, hMeas, 4);
  ////TH1D* hRecoBayes = (TH1D*) unfoldBayes.Hreco();
  TH1D* hRecoBayes = (TH1D*)m_RooUnfold.Hreco();
  RooUnfoldBaron   unfoldBaron (&response, hMeas, 4, Tau, RegMethod, NSteps, Seed);    // OR
  //RooUnfoldSvd     unfoldBayes (&response, hMeas, 20);   // OR
  //RooUnfoldTUnfold unfoldBayes (&response, hMeas);       // OR
  //RooUnfoldIds     unfold (&response, hMeas, 1);

  TH1D* hRecoBaron = (TH1D*) unfoldBaron.Hreco();
  for (int i = 1; i < hRecoBaron->GetXaxis()->GetNbins()+1; i++){
    cout << "************* BIN: " << i << endl;
    cout << "Baron unfolded: " << hRecoBaron->GetBinContent(i)/hEff->GetBinContent(i) << endl;
    cout << "RooUnfold unfolded: " << hRecoBayes->GetBinContent(i)/hEff->GetBinContent(i) << endl;
    cout << "Delta: " << 100*(hRecoBaron->GetBinContent(i)/hEff->GetBinContent(i) - hRecoBayes->GetBinContent(i)/hEff->GetBinContent(i) )/(hRecoBayes->GetBinContent(i)/hEff->GetBinContent(i)) << " \%"  << endl;
    cout << "Truth: " << hTruth->GetBinContent(i) << endl;
  }
  
  vector<TH1D*> ListOfPosteriors;
  ListOfPosteriors = (vector<TH1D*>) unfoldBaron.Hposteriors();
  hRecoBaron->SetLineColor(2);
  hTruth->SetLineColor(8);
  //hRecoBaron->Divide(hTruth);
  //hRecoBaron->Draw("hist");
  hAcc->SetLineColor(2);
  DivideEff(hRecoBaron, hEff);
  DivideEff(hRecoBayes, hEff);
  //hRecoBaron->Divide(hEff);
  //hRecoBayes->Divide(hEff);
  for (int i = 1; i <= hEff->GetNbinsX(); i++){
    //cout << i-1 << " eff: " << hEff->GetBinContent(i) << endl;
    cout << i << " Mean is: " << hRecoBaron->GetBinContent(i) << " Particle is: " << hTruth->GetBinContent(i) << ", Data is: " << hMeas->GetBinContent(i) << endl;
    
    //cout << i << " HMeas is: " << hMeas->GetBinContent(i) << endl;
    //cout << "Acc: " << hAcc->GetBinContent(i) << endl;
  }
  //cout << "Matrix 4, 4: " <<  hMatrix->GetBinContent(4,4) << endl;
  //cout << "Matrix 4, 3: " <<  hMatrix->GetBinContent(4,3) << endl;
  //cout << "Matrix 3, 4: " <<  hMatrix->GetBinContent(3,4) << endl;
  //cout << "Data0: " << hMeas->GetBinContent(1) << endl;
  
  //hAcc->Draw("hist same");
  //hEff->Draw("hist same");
  //ListOfPosteriors[5]->Draw("hist");

  int iRegMethod = 0;
  if (RegMethod == "e"){
    iRegMethod = 2;
  } else if (RegMethod == "c"){
    iRegMethod = 1;  
  }
  
  //TCanvas * can5 = new TCanvas("Spectrum", "Spectrum", 0, 0, 1600, 1600);
  //gStyle->SetPadLeftMargin(0.12);
  //gStyle->SetPadRightMargin(0.12);
  TCanvas * can5a = new TCanvas("Spectrum", "Spectrum", 0, 0, 1600, 1600);
  //can5->Divide(2);
  can5a->cd();
  TPad * pad1 = new TPad("pad1","pad1",0,0.40,1,1);
  //pad1->SetTopMargin(0.15);
  pad1->SetBottomMargin(0.01);
  pad1->SetFillStyle(0);
  pad1->SetTicks(1,1);
  pad1->SetBorderMode(0);
  pad1->Draw();
  can5a->cd();
  TPad * pad2 = new TPad("pad2","pad2",0,0.01,1,0.422);
  pad2->SetFillStyle(0);
  pad2->SetTopMargin(0.043);
  pad2->SetBottomMargin(0.2);
  pad2->SetBorderMode(0);
  pad2->SetTicks(1,1);
  pad2->Draw();
  pad2->Modified();
  can5a->cd();
  pad1->cd();
  gStyle->SetOptStat(0);



  //can5->cd(1);
  //can5a->cd();
  gStyle->SetOptStat(0);
  //gPad->SetLeftMargin(0.2);
  hRecoBaron->SetLineWidth(2);
  hRecoBayes->SetLineWidth(2);
  hTruth->SetLineWidth(2);
  

  TH1D * hRecoBaron_clone = (TH1D *) hRecoBaron->Clone("hbaron_clone");
  TH1D * hRecoBayes_clone = (TH1D *) hRecoBayes->Clone("hbayes_clone");
  TH1D * hTruth_clone = (TH1D *) hTruth->Clone("htruth_clone");
  //TH1D * Reco = unfoldBaron.Reco();
  TH1D * Reco = hMeas;
  Reco->SetLineWidth(2);
  TH1D * Reco_clone = (TH1D *) Reco->Clone("hreco_clone");
  if (iRegMethod == 0){
  hRecoBaron_clone->SetTitle(TString::Format("%s spectrum, no regularization used ", Spectrum.Data()));
  } else if (iRegMethod == 1){
    hRecoBaron_clone->SetTitle(TString::Format("Unfolded Spectra, Reg. using curvature, #tau = %f ", Tau));
  } else if (iRegMethod == 2){
    hRecoBaron_clone->SetTitle(TString::Format("Unfolded Spectra, Reg. using entropy, #tau = %f ", Tau));
  }

  bool Logy = false;

  hRecoBaron_clone->GetYaxis()->SetTitle("Events");
  if (Logy){
    hRecoBaron_clone->SetMaximum(hRecoBaron_clone->GetMaximum()*100);
    hRecoBaron_clone->SetMinimum(1);
  } else{
    hRecoBaron_clone->SetMaximum(hRecoBaron_clone->GetMaximum()*1.5);
    hRecoBaron_clone->SetMinimum(0);
  }
  
  
  //hRecoBaron_clone->SetMinimum(0);
  hRecoBaron_clone->GetXaxis()->SetTitleOffset(1.2);
  hRecoBaron_clone->GetXaxis()->SetTitleSize(34);
  hRecoBaron_clone->GetXaxis()->SetTitleFont(43);
  hRecoBaron_clone->GetYaxis()->SetTitleSize(27);
  hRecoBaron_clone->GetYaxis()->SetTitleFont(43);
  hRecoBaron_clone->GetYaxis()->SetTitleOffset(1.5);
  hRecoBaron_clone->GetYaxis()->SetTitle("Events");
  hRecoBaron_clone->GetYaxis()->SetLabelFont(43);
  hRecoBaron_clone->GetYaxis()->SetLabelSize(25);
  hRecoBaron_clone->GetYaxis()->SetMaxDigits(3);
  if (Logy){
    pad1->SetLogy(1);
  }
  
  hRecoBaron_clone->Draw("hist e1");
  Reco_clone->SetLineColor(4);
  Reco_clone->Draw("hist e1 same");
  hRecoBayes_clone->SetLineColor(1);
  /////hRecoBayes_clone->Draw("hist e1 same");
  TLegend * lege1 = new TLegend(0.68,0.69,0.99,0.85);
  hTruth_clone->Draw("hist e1 same");
  lege1->AddEntry(hTruth_clone,"Truth value","l");
  lege1->AddEntry(hRecoBaron_clone,"Unfolded data","l");
  //double chiNDF, chiNDF2;
  lege1->AddEntry(Reco_clone,"Data","l");
  //lege1->AddEntry((TObject*)0,TString::Format("#chi2/NDF %e",chiNDF),"");
  //lege->AddEntry((TObject*)0,TString::Format("%s",OverBinned.Data()),"");
  //lege->AddEntry((TObject*)0,TString::Format("Number of steps %i",NSteps),"");
  //lege1->AddEntry((TObject*)0,TString::Format("Method %s, #tau = %f ", RegMethod.Data(), Tau),"");
  lege1->SetFillStyle(0);
  /////lege1->AddEntry(hRecoBayes_clone,"Unfolded data iter. Bayes","l");
  /////lege1->AddEntry(Reco_clone,"Measured Data","l");
  lege1->SetBorderSize(0);
  lege1->Draw("same");

  vector<TPad*> Pads;

  double x1 = hRecoBaron_clone->GetYaxis()->GetLabelOffset() + gPad->GetLeftMargin();
  double x2 = 1.0 - gPad->GetRightMargin();
  double y1 = hRecoBaron_clone->GetXaxis()->GetLabelOffset() + gPad->GetBottomMargin();
  double y2 = 1.0 - gPad->GetTopMargin() ;

  cout << hRecoBaron_clone->GetXaxis()->GetLabelOffset() + gPad->GetBottomMargin() << " odhad y1: " << y1 << endl;
  cout << gPad->GetTopMargin() << " odhad y2: " << y2 << endl;
  cout << hRecoBaron_clone->GetYaxis()->GetLabelOffset() + gPad->GetLeftMargin() << " odhad x1: " << x1 << endl;
  cout << gPad->GetRightMargin() << " odhad x2: " << x2 << endl;
  double X1;
  double X2;
  double Y1;
  double Y2;

  for(int i = 0;i < hTruth_clone->GetXaxis()->GetNbins(); i++){
    //hRecoBaron_clone->GetBinContent(i)/(hRecoBaron_clone->GetMaximum() - hRecoBaron_clone->GetMinimum()) + 0.01
    X1 = x1 + (i)*(x2-x1)/hTruth_clone->GetXaxis()->GetNbins() + - 0.0001;
    if (Logy){
      Y1 = (TMath::Log10(hRecoBaron_clone->GetBinContent(i+1))/TMath::Log10(hRecoBaron_clone->GetMaximum() - hRecoBaron_clone->GetMinimum()))*(y2-y1) + y1 + 0.02;  
    } else{
      Y1 = (hRecoBaron_clone->GetBinContent(i+1)/(hRecoBaron_clone->GetMaximum() - hRecoBaron_clone->GetMinimum()))*(y2-y1) + y1 + 0.02;
    }
    X2 = x1 + (i+1)*(x2-x1)/hTruth_clone->GetXaxis()->GetNbins() - 0.0005;
    if (i == 0){
      X2 = x1 + (i+1)*(x2-x1)/hTruth_clone->GetXaxis()->GetNbins() - 0.0015;
    }
    if (Logy){
      Y2 = (TMath::Log10(hRecoBaron_clone->GetBinContent(i+1))/TMath::Log10(hRecoBaron_clone->GetMaximum() - hRecoBaron_clone->GetMinimum()))*(y2-y1) + y1 + 0.1;
    } else{
      Y2 = (hRecoBaron_clone->GetBinContent(i+1)/(hRecoBaron_clone->GetMaximum() - hRecoBaron_clone->GetMinimum()))*(y2-y1) + y1 + 0.1;
    }
    cout << "------------------" << endl;
    cout << x1 << " " << y1 << " " << x2 << " " << y2 << endl;
    cout << X1 << " " << Y1 << " " << X2 << " " << Y2 << endl;
    TPad * pad1 = new TPad(TString::Format("pad_%i",i+1),TString::Format("pad_%i",i+1),X1,Y1,X2,Y2);
    pad1->SetFillStyle(0);
    pad1->cd();
    //Pads.push_back(pad1);
    //Pads[i]->Draw();
    //Pads[i]->cd();
    ListOfPosteriors[i]->Fit("gaus");
    ListOfPosteriors[i]->Draw("same");
    pad1->DrawClone();
  }
  //for(int i = 0;i < hTruth_clone->GetXaxis()->GetNbins(); i++){
  //  Pads[i]->Draw();
  //  Pads[i]->cd();
  //  ListOfPosteriors[i]->Draw();
  //}
  Canvases.push_back(can5a);

  //gApplication->Run();
  
  can5a->cd();
  //gPad->SetLeftMargin(0.2);
  pad2->cd();
  //hRecoBaron->SetMinimum(0);

  hRecoBaron->Divide(hTruth);
  hRecoBayes->Divide(hTruth);
  Reco->Divide(hTruth);
  TH1D * hTruth_clone2 = (TH1D*)hTruth->Clone("truth_clone2");
  DivideEffTruth(hTruth, hTruth_clone2);
  //hTruth->Divide(hTruth);
  //gStyle->SetPadRightMargin(0.12);
  if (iRegMethod == 0){
  hRecoBaron->SetTitle(TString::Format("%s spectrum, no regularization used ", Spectrum.Data()));
  } else if (iRegMethod == 1){
    hRecoBaron->SetTitle(TString::Format("Unfolded Spectra Ratio, Reg. using curvature, #tau = %f ", Tau));
  } else if (iRegMethod == 2){
    hRecoBaron->SetTitle(TString::Format("Unfolded Spectra Ratio, Reg. using entropy, #tau = %f ", Tau));
  }

  hTruth->GetYaxis()->SetTitle("Unfolded / Particle    ");
  hTruth->SetMaximum(1.5);
  hTruth->SetMinimum(0.5);
  hTruth->SetLineColor(kGreen -2);
  //hRecoBaron->Draw("hist e1");
  hTruth->SetTitle("");
  hTruth->GetXaxis()->SetTitleOffset(2);
  hTruth->GetXaxis()->SetTitleSize(34);
  hTruth->GetXaxis()->SetTitleFont(43);
  hTruth->GetYaxis()->SetTitleSize(27);
  hTruth->GetYaxis()->SetTitleFont(43);
  hTruth->GetYaxis()->SetTitleOffset(1.5);
  hTruth->GetYaxis()->SetLabelFont(43);
  hTruth->GetYaxis()->SetLabelSize(25);
  hTruth->GetXaxis()->SetLabelFont(43);
  hTruth->GetXaxis()->SetLabelSize(25);
  hTruth->SetLineWidth(0);
  hTruth->Draw("hist ]["); 
  hTruth->SetLineWidth(1);
  hTruth->DrawCopy("hist ]["); 

  TString OverBinned = "NotOverBinned";
  TH2D * NormalizedUnfoldingMatrix = unfoldBaron.NormalizedUnfoldingMatrix();
    for (int i = 1; i <= NormalizedUnfoldingMatrix->GetXaxis()->GetNbins(); i++){
      for (int j = 1; j <= NormalizedUnfoldingMatrix->GetXaxis()->GetNbins(); j++){
        if (NormalizedUnfoldingMatrix->GetBinContent(i,j) < 0.5){
          OverBinned = "OverBinned";
        }
        }
    }
  double chiNDF, chiNDF2;
  chiNDF = CalculateChi2NDF(hRecoBaron, hTruth);
  TString ParameterString;
  if (RegMethod == ""){
    RegMethod = "n";
  }
  ParameterString = TString::Format("spectrum_%s_rebin_%i_tau_%f_method_%s_steps_%i_seed_%i_chi2_%f_binning_%s_closure_%i.root",Spectrum.Data(), NRebin, Tau, RegMethod.Data(), NSteps, Seed, chiNDF, OverBinned.Data(), Closure);
  TFile *output = new TFile(ParameterString,"UPDATE");

  hTruth->SetName(TString::Format("%s_%i",ParameterString.Data(), Seed));
  hTruth->SetFillColor(kGreen -7);
  hTruth->SetFillStyle(3018);
  hTruth->Draw("e2same");
  hRecoBaron->SetFillColor(1);
  hRecoBaron->SetFillStyle(1);
  hRecoBaron->SetLineWidth(1);
  hRecoBaron->SetLineColor(2);
  hRecoBaron->SetMarkerStyle(8);
  hRecoBaron->SetMarkerColor(2);
  hRecoBaron->Draw("p0 same");
  gPad->Update();
  Reco->SetLineColor(4);
  //Reco->Draw("hist same");
  hRecoBayes->SetLineColor(1);
  /////hRecoBayes->Draw("hist e1 same");
  //hTruth->SetLineColor(8);
  //hTruth->Draw("hist same ][");
  
  TH1D * hTruth2b;
  hTruth2b = (TH1D*)hTruth->Clone("t2");
  chiNDF2 = CalculateChi2NDF(hRecoBayes, hTruth2b);
  TLegend * lege = new TLegend(0.1,0.847,0.2,0.8965);
  lege->SetFillStyle(0);
  //lege->AddEntry(hTruth,"Truth value (Simulation)","l");
  cout << "******************************" << endl;
  //lege->AddEntry(hRecoBaron,"Unfolded","l");
  lege->SetTextSize(0.08);
  lege->AddEntry((TObject*)0,TString::Format("#chi2/NDF = %e",chiNDF),"");
  //lege->AddEntry((TObject*)0,TString::Format("%s",OverBinned.Data()),"");
  //lege->AddEntry((TObject*)0,TString::Format("Number of steps %i",NSteps),"");
  //lege->AddEntry((TObject*)0,TString::Format("Method %s, #tau = %f ", RegMethod.Data(), Tau),"");
  /////lege->AddEntry(hRecoBayes,"Unf. iter Bayes","l");
  /////lege->AddEntry(hRecoBayes,TString::Format("#chi2/NDF %e",chiNDF2),"l");
  
  lege->SetBorderSize(0);
  lege->Draw("same");
  //gPad->RedrawAxis();
  //gPad->GetFrame()->Draw();
  //gPad->GetFrame()->SetFillStyle(0);

  //can5->cd(3);
  //Canvases.push_back(can5);

  vector<TGraph2D *> PathsOfLeapFrog = unfoldBaron.PathsOfLeapFrog();
  vector<TGraph2D *> PathsOfLeapFrogWeightedByLikelihood = unfoldBaron.PathsOfLeapFrogWeightedByLikelihood();
  //TH2D * NormalizedUnfoldingMatrix = unfoldBaron.NormalizedUnfoldingMatrix();
  vector<vector<TH2D *>> BinCorrelationMatrix = unfoldBaron.BinCorrelationMatrix();
  TH2D * BinCorrelationFactorMatrix = unfoldBaron.BinCorrelationFactorMatrix();
  TH1D * RelativeRecoResolution = unfoldBaron.RelativeRecoResolution();
  
  PlotListOfPosteriors( ListOfPosteriors, Tau, hTruth_clone);
  PlotNormalizedUnfoldingMatrix(NormalizedUnfoldingMatrix, Tau);
  //PlotPathsOfLeapFrog(PathsOfLeapFrog);
  //PlotPathsOfLeapFrogWeightedByLikelihood(PathsOfLeapFrogWeightedByLikelihood);

  PlotBinCorrelationMatrix(BinCorrelationMatrix, ListOfPosteriors);
  TString MatrixCorrTitle = TString::Format("Method: %s, reg. strength #tau = %f", RegMethod.Data(), Tau);
  PlotBinCorrelationFactorMatrix(BinCorrelationFactorMatrix, MatrixCorrTitle);
  //PlotRelativeRecoResolution(RelativeRecoResolution, Tau);
  
  TString PadName;
  vector<TPad *> PadVec;
  TCanvas * OverlayCan = new TCanvas("OverlayCan", "OverlayCan", 0, 0, 1600, 1600);
  for (int i = 0; i < Canvases.size(); i++){
    PadName = TString::Format("Pad%i", i);
    if (i==0){
          OverlayCan->cd();
          TPad * pad = new TPad(PadName.Data(),PadName.Data(),0,0.5,0.5,1);
          PadVec.push_back(pad);
          PadVec[i]->Draw();
          PadVec[i]->cd();
          Canvases[i]->DrawClonePad();
    }
    
    if (i==2){
          OverlayCan->cd();
          TPad * pad = new TPad(PadName.Data(),PadName.Data(),0.5,0.5,1,1);
          PadVec.push_back(pad);
          PadVec[1]->Draw();
          PadVec[1]->cd();
          Canvases[i]->DrawClonePad();
    }

    Canvases[i]->Print(TString::Format("./plots/%s_can_%s.png", ParameterString.Data(), Canvases[i]->GetName()));  
    Canvases[i]->Print(TString::Format("./plots/%s_can_%s.pdf", ParameterString.Data(), Canvases[i]->GetName()));      
  }

  //PlotReziduals(hRecoBaron, hTruth2b, ParameterString);


  hRecoBaron->Write();
  hRecoBaron_clone->SetName("unfolded");
  hRecoBaron_clone->Write();
  hTruth_clone->SetName("truth");
  hTruth_clone->Write();
  NormalizedUnfoldingMatrix->Write();
  Reco->SetName("data");
  Reco->Write();
  BinCorrelationFactorMatrix->Write();
  RelativeRecoResolution->Write();
  for (int i = 0; i < ListOfPosteriors.size(); i++){
    ListOfPosteriors[i]->Write();
    PathsOfLeapFrogWeightedByLikelihood[i]->Write();
  }
  output->Close();

  //gApplication->Run();
  return 0;
}

#ifndef __CINT__
int main (int argc, char *argv[]) { 
cout << "Parameters: " << argc << endl;
cout << "Parameters: " << argv[1] << endl;


  //if (argc > 0){
  //    cout << "Parametr je: " << argv[1] << endl;
  //    RooUnfoldExample(atoi(argv[1])); return 0;  
  //} else{
  //RooUnfoldExample(); return 0;
  //}
  }  // Main program when run stand-alone
#endif

//=====================================================================-*-C++-*-
// File and Version Information:
//      $Id: RooUnfoldExample.cxx 354 2017-07-11 17:49:50Z T.J.Adye@rl.ac.uk $
//
// Description:
//      Simple example usage of the RooUnfold package using toy MC.
//
// Authors: Tim Adye <T.J.Adye@rl.ac.uk> and Fergus Wilson <fwilson@slac.stanford.edu>
//
//==============================================================================

#if !(defined(__CINT__) || defined(__CLING__)) || defined(__ACLIC__)
#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
using std::cout;
using std::endl;
using std::vector;

#include "TRandom.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TGraph2D.h"
#include "TString.h"
#include "TTimer.h"
#include "TF2.h"
#include "TColor.h"
#include "TLine.h"

#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldResponse.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldBayes.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldBinByBin.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldBaron.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldSvd.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldTUnfold.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldIds.h"
#endif

//==============================================================================
// Global definitions
//==============================================================================

const Double_t cutdummy= -99999.0;
vector<TH1D*> TH1Ds;
vector<TH2D*> TH2Ds;

vector<TCanvas*> Canvases;
//==============================================================================
// Gaussian smearing, systematic translation, and variable inefficiency
//==============================================================================

Double_t smear (Double_t xt)
{
  Double_t xeff= 0.3 + (1.0-0.3)/20*(xt+10.0);  // efficiency
  Double_t x= gRandom->Rndm();
  if (x>xeff) return cutdummy;
  Double_t xsmear= gRandom->Gaus(-2.5,0.2);     // bias and smear
  return xt+xsmear;
}

//==============================================================================
// Example Unfolding
//==============================================================================

// PLOTING

void PlotListOfPosteriors( vector<TH1D *> ListOfPosteriors, float Tau , TH1D * Truth){
  int Dim = ListOfPosteriors.size();
  TCanvas * can_0 = new TCanvas("Posteriors", "Posteriors", 0, 0, 1600, 1600);
  can_0->Divide((int)(TMath::Power(Dim, 0.5) + 1) , (int)(TMath::Power(Dim, 0.5) + 1));
  Double_t x, q;
  q = 0.5; 
  vector<TLine*> lines;
  vector<TLine*> TruthLines;
  for (int i = 0; i < Dim; i++){
        x=0;
        q=0.5;
        ListOfPosteriors[i]->ComputeIntegral(); // just a precaution
        ListOfPosteriors[i]->GetQuantiles(1, &x, &q);  
        TLine * line = new TLine(x,0,x,ListOfPosteriors[i]->GetMaximum());
        TLine * Truthline = new TLine(Truth->GetBinContent(i),0,Truth->GetBinContent(i),ListOfPosteriors[i]->GetMaximum());
        lines.push_back(line);
        TruthLines.push_back(Truthline);
        can_0->cd(i+1);
        ListOfPosteriors[i]->SetTitle(TString::Format("%s, Reg. par. = %f", ListOfPosteriors[i]->GetTitle() ,Tau));
        ListOfPosteriors[i]->GetXaxis()->SetTitle(TString::Format("Truth Value in bin %i", i + 1));
        ListOfPosteriors[i]->GetYaxis()->SetTitle("");
        ListOfPosteriors[i]->Draw("hist");
        ListOfPosteriors[i]->Fit("gaus", "q");
        TF1 * f = ListOfPosteriors[i]->GetFunction("gaus");
        if(f != nullptr){f->Draw("same");}
        lines[i]->SetLineColor(2);
        lines[i]->SetLineWidth(2);
        lines[i]->Draw("same");
        TruthLines[i]->SetLineColor(3);
        TruthLines[i]->SetLineWidth(2);
        TruthLines[i]->Draw("same");
  }
  Canvases.push_back(can_0);
}

void PlotPathsOfLeapFrog(vector<TGraph2D *> PathsOfLeapFrog){
    TCanvas * can_1 = new TCanvas("Paths of LeapFrog Function", "Paths of LeapFrog Function", 0, 0, 1600, 1600);
    int Dim = PathsOfLeapFrog.size();
    can_1->Divide((int)(TMath::Power(Dim, 0.5) + 1 ) , (int)(TMath::Power(Dim, 0.5) + 1));
    for (int i = 0; i < Dim; i++){
        can_1->cd(i+1);
        gStyle->SetPalette(1);
        PathsOfLeapFrog[i]->SetMarkerStyle(20);
        PathsOfLeapFrog[i]->Draw("surf4");
      }
    Canvases.push_back(can_1);
}

void PlotPathsOfLeapFrogWeightedByLikelihood(vector<TGraph2D *> PathsOfLeapFrogWeightedByLikelihood){
    TCanvas * can_2 = new TCanvas("Paths of LeapFrog Function Weighted by Likelihood", "Paths of LeapFrog Function Weighted by Likelihood", 0, 0, 1600, 1600);
    gStyle->SetCanvasPreferGL(true);
    
    int Dim = PathsOfLeapFrogWeightedByLikelihood.size();
    can_2->Divide((int)(TMath::Power(Dim, 0.5) + 1 ) , (int)(TMath::Power(Dim, 0.5) + 1));
    for (int i = 0; i < Dim; i++){
        can_2->cd(i+1);
        //cout << "Margin: " << PathsOfLeapFrogWeightedByLikelihood[i]->GetMargin() << endl;
        gStyle->SetPalette(1);
        PathsOfLeapFrogWeightedByLikelihood[i]->SetMarkerStyle(20);
        PathsOfLeapFrogWeightedByLikelihood[i]->Draw("pcol");
      }
    Canvases.push_back(can_2);
}


void PlotNormalizedUnfoldingMatrix(TH2D * NormalizedUnfoldingMatrix, float Tau){
        TCanvas * can_3 = new TCanvas("Normalized Unfolding Matrix", "Normalized Unfolding Matrix", 0, 0, 1600, 1600);
        can_3->cd();
        gStyle->SetOptStat(0);
        NormalizedUnfoldingMatrix->SetTitle(TString::Format("%s, Reg. par. = %f", NormalizedUnfoldingMatrix->GetTitle() ,Tau));
        //NormalizedUnfoldingMatrix->GetXaxis()->SetRangeUser(0.0,1.0);
        NormalizedUnfoldingMatrix->Draw("colz text");
        Canvases.push_back(can_3);
}

void PlotBinCorrelationMatrix(vector<vector<TH2D *>> BinCorrelationMatrix, vector<TH1D *> ListOfPosteriors ){
    TCanvas * can_4 = new TCanvas("Bin Correlation Matrices", "Bin Correlation Matrices", 0, 0, 1600, 1600);
    int Dim = BinCorrelationMatrix.size();
    can_4->Divide(Dim, Dim);
    for (int i = 0 ; i < Dim ; i++){
        for (int j = 0 ; j < Dim ; j++){
        can_4->cd((int)((j + 1) + Dim*(Dim - (i + 1))));      
        BinCorrelationMatrix[i][j]->GetXaxis()->SetTitle(TString::Format("Events in bin %i", i+1));
        BinCorrelationMatrix[i][j]->GetYaxis()->SetTitle(TString::Format("Events in bin %i", j+1));
        //cout  << "Limits x: " << ListOfPosteriors[i]->GetXaxis()->GetBinLowEdge(ListOfPosteriors[i]->FindFirstBinAbove( )) << " " << ListOfPosteriors[i]->GetXaxis()->GetBinUpEdge(ListOfPosteriors[i]->FindLastBinAbove( )) << endl;
        //cout  << "Limits y: " << ListOfPosteriors[j]->GetYaxis()->GetBinLowEdge(ListOfPosteriors[j]->FindFirstBinAbove( )) << " " << ListOfPosteriors[j]->GetYaxis()->GetBinUpEdge(ListOfPosteriors[j]->FindLastBinAbove( )) << endl;
        BinCorrelationMatrix[i][j]->GetXaxis()->SetRangeUser(ListOfPosteriors[i]->GetXaxis()->GetBinLowEdge(ListOfPosteriors[i]->FindFirstBinAbove( )), ListOfPosteriors[i]->GetXaxis()->GetBinUpEdge(ListOfPosteriors[i]->FindLastBinAbove( )) );
        BinCorrelationMatrix[i][j]->GetYaxis()->SetRangeUser(ListOfPosteriors[j]->GetYaxis()->GetBinLowEdge(ListOfPosteriors[j]->FindFirstBinAbove( )), ListOfPosteriors[j]->GetYaxis()->GetBinUpEdge(ListOfPosteriors[j]->FindLastBinAbove( )) );
        float Correlation = BinCorrelationMatrix[i][j]->GetCorrelationFactor();
        //TH2CorrelationMatrix->SetBinContent(i+1,j+1, Correlation);
        TLegend * leg = new TLegend(0.1,0.8,0.45,0.9);
        leg->SetHeader(TString::Format("Corr. factor = %.4f", Correlation));
        //gStyle->SetLegendBorderSize(0);
        gStyle->SetLegendTextSize(0.05);
        gStyle->SetOptStat(0);
        BinCorrelationMatrix[i][j]->GetZaxis()->SetRangeUser(-1.0,1.0);
        BinCorrelationMatrix[i][j]->Draw("colz");
        leg->Draw("same");
        }
    }
    Canvases.push_back(can_4);
}

void PlotBinCorrelationFactorMatrix(TH2D * BinCorrelationFactorMatrix){
        TCanvas * can_5 = new TCanvas("Bin Correlation Factor Matrix", "Bin Correlation Factor Matrix", 0, 0, 1600, 1600);
        can_5->cd();
        gStyle->SetOptStat(0);
        BinCorrelationFactorMatrix->Draw("colz text");
        Canvases.push_back(can_5);
}

void PlotRelativeRecoResolution(TH1D * RelativeRecoResolution, float Tau){
      TCanvas * can_6 = new TCanvas("Relative Reco Resolution", "Relative Reco Resolution", 0, 0, 1600, 1600);
      can_6->cd();
      //gStyle->SetOptStat(0);
      RelativeRecoResolution->SetTitle(TString::Format("%s, Reg. par. = %f", RelativeRecoResolution->GetTitle(),Tau));
      RelativeRecoResolution->SetMinimum(0.0);
      //RelativeRecoResolution->SetMaximum(1.1);
      RelativeRecoResolution->Draw("hist");
      Canvases.push_back(can_6);

}

double CalculateChi2NDF(TH1D* h_observed, TH1D* h_expected){
  cout << "I am in the function chi2." << endl;
  double chi2 = 0.0;
  cout << "1." << endl;
  for (int i = 1; i<=h_expected->GetXaxis()->GetNbins(); i++){
    cout << i << endl;
    cout << "Deltas squered: " << (h_observed->GetBinContent(i)-h_expected->GetBinContent(i))*(h_observed->GetBinContent(i)-h_expected->GetBinContent(i)) << endl;
    chi2 = chi2 + (h_observed->GetBinContent(i)-h_expected->GetBinContent(i))*(h_observed->GetBinContent(i)-h_expected->GetBinContent(i))/h_expected->GetBinContent(i);
    cout << "chi2: " << chi2 << endl;
  }
  chi2 = chi2/h_expected->GetXaxis()->GetNbins();
  cout << "999." << endl;
  return chi2;
}

// END OF PLOTTING FUNCTIONS

void DivideEff(TH1D * h,TH1D * eff){
    for (int i=1;i < h->GetNbinsX()+1 ;i++){
      if (eff->GetBinContent(i) != 0.0){
        h->SetBinContent(i,h->GetBinContent(i)/eff->GetBinContent(i));
        //cout << "ERROR: " << eff->GetBinError(i) << endl;
        //cout << "CONTENT: " << eff->GetBinContent(i) << endl;
        //cout << "New ERROR: " << eff->GetBinError(i)/eff->GetBinContent(i) << endl;
        //cout << "****************************8" << endl;
        h->SetBinError(i,eff->GetBinError(i)/eff->GetBinContent(i));
      } else{
        cout << "WARNING: Nedelim histogram, eff = 0 v binu: " << i << endl;
      }
    }
    return h;
} 

int RooUnfoldExampleSeeds(TString FileNameNoReg = "", TString FileNameRegE = "", TString FileNameRegC = "")
{
  TFile *rfile_data = new TFile("/home/petr/GitLab/unfolding/CCode/RooUnfold/input.root", "read");
  TFile *rfile_particle = new TFile("/home/petr/GitLab/unfolding/CCode/RooUnfold/input.root", "read");
  //TFile *rfile_matrix = new TFile("/home/petr/GitLab/unfolding/CCode/RooUnfold/input.root", "read");
  //if (FileNameNoReg == ""){
  TFile *rfile_seeds_noreg = new TFile("100_0.000000__50000","read"); 
  TFile *rfile_seeds_reg_e = new TFile("100_1.000000_e_50000","read"); 
  TFile *rfile_seeds_reg_c = new TFile("100_1.000000_c_50000","read"); 
  FileNameNoReg = "100_0.000000__50000";
  FileNameRegE = "100_1.000000_e_50000";
  FileNameRegC = "100_1.000000_c_50000";
  //} else{
  //    TFile *rfile_seeds_noreg = new TFile(FileNameNoReg,"read"); 
  //    TFile *rfile_seeds_reg_e = new TFile(FileNameRegE,"read"); 
  //    TFile *rfile_seeds_reg_c = new TFile(FileNameRegC,"read"); 
  //}
  
  TH1D *hTruth = (TH1D*)rfile_particle->Get("Particle/MCTopPairEta");
  
  TCanvas * can5 = new TCanvas("Spectrum_no_reg", "Spectrum", 0, 0, 1600, 1600);
  can5->cd();
  gStyle->SetOptStat(0);
  hTruth->SetLineWidth(2);
  hTruth->SetLineColor(3);  
  hTruth->Rebin(100);
  //hTruth->Divide(hTruth);
  TH1D * hTruth_clone = (TH1D*)hTruth->Clone("clone");
  DivideEff(hTruth, hTruth_clone);
  TH1D * hTruth3 = (TH1D*)hTruth->Clone("clone3");
  TH1D * hTruth2 = (TH1D*)hTruth->Clone("clone2");
  hTruth->SetTitle("Unfolded Spectra Ratio, No regularization used ");

  hTruth->GetYaxis()->SetTitle("Unfolded / Particle");
  hTruth->SetMaximum(1.1);
  hTruth->SetMinimum(0.9);
  hTruth->DrawCopy("hist ]["); 
  hTruth->SetFillColor(kGreen -7);
  hTruth->SetFillStyle(3018);
  hTruth->Draw("e2same");
  vector<TH1D*> TH1Ds;
  //cout << rfile_seeds_noreg << endl;
  //TH1D * hist = (TH1D*)rfile_seeds_noreg->Get("100_0.000000__50000_1");
  for(int i=1;i<11;i++){
      //cout << TString::Format("%s_%i",FileNameNoReg.Data(),i ) << endl;
      TH1D * hist = (TH1D*)rfile_seeds_noreg->Get(TString::Format("%s_%i",FileNameNoReg.Data(),i ));
      TH1Ds.push_back(hist);
      TH1Ds[i-1]->SetFillColor(1);
      TH1Ds[i-1]->SetFillStyle(1);
      TH1Ds[i-1]->SetLineWidth(1);
      TH1Ds[i-1]->SetMarkerStyle(8);
      TH1Ds[i-1]->SetMarkerColor(2);
      TH1Ds[i-1]->Draw("p0 same");
  }

  Canvases.push_back(can5);

  TCanvas * can6 = new TCanvas("Spectrum_reg_c", "Spectrum", 0, 0, 1600, 1600);
  can6->cd();
  gStyle->SetOptStat(0);
  hTruth2->SetLineWidth(2);
  hTruth2->SetLineColor(3);  
  //hTruth->Draw("hist e1");
  //DivideEff(hTruth, hTruth);
  //hTruth->Divide(hTruth_clone);
  hTruth2->SetTitle(TString::Format("Unfolded Spectra, Reg. using curvature, #tau = %f ", 1.0));

  hTruth2->GetYaxis()->SetTitle("Unfolded / Particle");
  hTruth2->SetMaximum(1.1);
  hTruth2->SetMinimum(0.9);
  hTruth2->DrawCopy("hist ]["); 
  hTruth2->SetFillColor(kGreen -7);
  hTruth2->SetFillStyle(3018);
  hTruth2->Draw("e2same");
  vector<TH1D*> TH1Ds2;
  //cout << rfile_seeds_noreg << endl;
  //TH1D * hist = (TH1D*)rfile_seeds_noreg->Get("100_0.000000__50000_1");
  for(int i=1;i<11;i++){
      //cout << TString::Format("%s_%i",FileNameRegC.Data(),i ) << endl;
      TH1D * hist = (TH1D*)rfile_seeds_reg_c->Get(TString::Format("%s_%i",FileNameRegC.Data(),i ));
      TH1Ds2.push_back(hist);
      TH1Ds2[i-1]->SetFillColor(1);
      TH1Ds2[i-1]->SetFillStyle(1);
      TH1Ds2[i-1]->SetLineWidth(1);
      TH1Ds2[i-1]->SetMarkerStyle(8);
      TH1Ds2[i-1]->SetMarkerColor(2);
      TH1Ds2[i-1]->Draw("p0 same");
  }

  Canvases.push_back(can6);

  TCanvas * can7 = new TCanvas("Spectrum_reg_e", "Spectrum", 0, 0, 1600, 1600);
  can7->cd();
  gStyle->SetOptStat(0);
  hTruth3->SetLineWidth(2);
  hTruth3->SetLineColor(3);  
  //hTruth->Divide(hTruth);
  hTruth3->SetTitle(TString::Format("Unfolded Spectra, Reg. using entropy, #tau = %f ", 1.0));

  hTruth3->GetYaxis()->SetTitle("Unfolded / Particle");
  hTruth3->SetMaximum(1.1);
  hTruth3->SetMinimum(0.9);
  hTruth3->DrawCopy("hist ]["); 
  hTruth3->SetFillColor(kGreen -7);
  hTruth3->SetFillStyle(3018);
  hTruth3->Draw("e2same");
  vector<TH1D*> TH1Ds3;
  //cout << rfile_seeds_noreg << endl;
  //TH1D * hist = (TH1D*)rfile_seeds_noreg->Get("100_0.000000__50000_1");
  for(int i=1;i<11;i++){
      //cout << TString::Format("%s_%i",FileNameRegC.Data(),i ) << endl;
      TH1D * hist = (TH1D*)rfile_seeds_reg_e->Get(TString::Format("%s_%i",FileNameRegE.Data(),i ));
      TH1Ds3.push_back(hist);
      TH1Ds3[i-1]->SetFillColor(1);
      TH1Ds3[i-1]->SetFillStyle(1);
      TH1Ds3[i-1]->SetLineWidth(1);
      TH1Ds3[i-1]->SetMarkerStyle(8);
      TH1Ds3[i-1]->SetMarkerColor(2);
      TH1Ds3[i-1]->Draw("p0 same");
  }
  Canvases.push_back(can7);

  for (int i = 0; i < Canvases.size(); i++){
    Canvases[i]->Print(TString::Format("./plots_seeds/seeds_%s.png", Canvases[i]->GetName()));      
  }


  return 0;
}






#ifndef __CINT__
int main (int argc, char *argv[]) { 
cout << "Parameters: " << argc << endl;
cout << "Parameters: " << argv[1] << endl;


  //if (argc > 0){
  //    cout << "Parametr je: " << argv[1] << endl;
  //    RooUnfoldExample(atoi(argv[1])); return 0;  
  //} else{
  //RooUnfoldExample(); return 0;
  //}
  }  // Main program when run stand-alone
#endif

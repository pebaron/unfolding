//=====================================================================-*-C++-*-
// File and Version Information:
//      $Id: RooUnfoldExample.cxx 354 2017-07-11 17:49:50Z T.J.Adye@rl.ac.uk $
//
// Description:
//      Simple example usage of the RooUnfold package using toy MC.
//
// Authors: Tim Adye <T.J.Adye@rl.ac.uk> and Fergus Wilson <fwilson@slac.stanford.edu>
//
//==============================================================================

#if !(defined(__CINT__) || defined(__CLING__)) || defined(__ACLIC__)
#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <ifstream>
using std::cout;
using std::endl;
using std::vector;

#include "TRandom.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TGraph2D.h"
#include "TString.h"
#include "TTimer.h"
#include "TF2.h"
#include "TColor.h"
#include "TLine.h"

#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldResponse.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldBayes.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldBinByBin.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldBaron.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldSvd.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldTUnfold.h"
#include "/home/petr/GitLab/unfolding/CCode/RooUnfold/src/RooUnfoldIds.h"
#endif

//==============================================================================
// Global definitions
//==============================================================================

const Double_t cutdummy= -99999.0;
vector<TH1D*> TH1Ds;
vector<TH2D*> TH2Ds;

vector<TCanvas*> Canvases;
//==============================================================================
// Gaussian smearing, systematic translation, and variable inefficiency
//==============================================================================

Double_t smear (Double_t xt)
{
  Double_t xeff= 0.3 + (1.0-0.3)/20*(xt+10.0);  // efficiency
  Double_t x= gRandom->Rndm();
  if (x>xeff) return cutdummy;
  Double_t xsmear= gRandom->Gaus(-2.5,0.2);     // bias and smear
  return xt+xsmear;
}

//==============================================================================
// Example Unfolding
//==============================================================================

// PLOTING

void PlotListOfPosteriors( vector<TH1D *> ListOfPosteriors, float Tau , TH1D * Truth){
  int Dim = ListOfPosteriors.size();
  TCanvas * can_0 = new TCanvas("Posteriors", "Posteriors", 0, 0, 1600, 1600);
  can_0->Divide((int)(TMath::Power(Dim, 0.5) + 1) , (int)(TMath::Power(Dim, 0.5) + 1));
  Double_t x, q;
  q = 0.5; 
  vector<TLine*> lines;
  vector<TLine*> TruthLines;
  for (int i = 0; i < Dim; i++){
        x=0;
        q=0.5;
        ListOfPosteriors[i]->ComputeIntegral(); // just a precaution
        ListOfPosteriors[i]->GetQuantiles(1, &x, &q);  
        TLine * line = new TLine(x,0,x,ListOfPosteriors[i]->GetMaximum());
        TLine * Truthline = new TLine(Truth->GetBinContent(i),0,Truth->GetBinContent(i),ListOfPosteriors[i]->GetMaximum());
        lines.push_back(line);
        TruthLines.push_back(Truthline);
        can_0->cd(i+1);
        ListOfPosteriors[i]->SetTitle(TString::Format("%s, Reg. par. = %f", ListOfPosteriors[i]->GetTitle() ,Tau));
        ListOfPosteriors[i]->GetXaxis()->SetTitle(TString::Format("Truth Value in bin %i", i + 1));
        ListOfPosteriors[i]->GetYaxis()->SetTitle("");
        ListOfPosteriors[i]->Draw("hist");
        ListOfPosteriors[i]->Fit("gaus", "q");
        TF1 * f = ListOfPosteriors[i]->GetFunction("gaus");
        if(f != nullptr){f->Draw("same");}
        lines[i]->SetLineColor(2);
        lines[i]->SetLineWidth(2);
        lines[i]->Draw("same");
        TruthLines[i]->SetLineColor(3);
        TruthLines[i]->SetLineWidth(2);
        TruthLines[i]->Draw("same");
  }
  Canvases.push_back(can_0);
}

void PlotPathsOfLeapFrog(vector<TGraph2D *> PathsOfLeapFrog){
    TCanvas * can_1 = new TCanvas("Paths of LeapFrog Function", "Paths of LeapFrog Function", 0, 0, 1600, 1600);
    int Dim = PathsOfLeapFrog.size();
    can_1->Divide((int)(TMath::Power(Dim, 0.5) + 1 ) , (int)(TMath::Power(Dim, 0.5) + 1));
    for (int i = 0; i < Dim; i++){
        can_1->cd(i+1);
        gStyle->SetPalette(1);
        PathsOfLeapFrog[i]->SetMarkerStyle(20);
        PathsOfLeapFrog[i]->Draw("surf4");
      }
    Canvases.push_back(can_1);
}

void PlotPathsOfLeapFrogWeightedByLikelihood(vector<TGraph2D *> PathsOfLeapFrogWeightedByLikelihood){
    TCanvas * can_2 = new TCanvas("Paths of LeapFrog Function Weighted by Likelihood", "Paths of LeapFrog Function Weighted by Likelihood", 0, 0, 1600, 1600);
    gStyle->SetCanvasPreferGL(true);
    
    int Dim = PathsOfLeapFrogWeightedByLikelihood.size();
    can_2->Divide((int)(TMath::Power(Dim, 0.5) + 1 ) , (int)(TMath::Power(Dim, 0.5) + 1));
    for (int i = 0; i < Dim; i++){
        can_2->cd(i+1);
        //cout << "Margin: " << PathsOfLeapFrogWeightedByLikelihood[i]->GetMargin() << endl;
        gStyle->SetPalette(1);
        PathsOfLeapFrogWeightedByLikelihood[i]->SetMarkerStyle(20);
        PathsOfLeapFrogWeightedByLikelihood[i]->Draw("pcol");
      }
    Canvases.push_back(can_2);
}


void PlotNormalizedUnfoldingMatrix(TH2D * NormalizedUnfoldingMatrix, float Tau){
        TCanvas * can_3 = new TCanvas("Normalized Unfolding Matrix", "Normalized Unfolding Matrix", 0, 0, 1600, 1600);
        can_3->cd();
        gStyle->SetOptStat(0);
        NormalizedUnfoldingMatrix->SetTitle(TString::Format("%s, Reg. par. = %f", NormalizedUnfoldingMatrix->GetTitle() ,Tau));
        //NormalizedUnfoldingMatrix->GetXaxis()->SetRangeUser(0.0,1.0);
        NormalizedUnfoldingMatrix->Draw("colz text");
        Canvases.push_back(can_3);
}

void PlotBinCorrelationMatrix(vector<vector<TH2D *>> BinCorrelationMatrix, vector<TH1D *> ListOfPosteriors ){
    TCanvas * can_4 = new TCanvas("Bin Correlation Matrices", "Bin Correlation Matrices", 0, 0, 1600, 1600);
    int Dim = BinCorrelationMatrix.size();
    can_4->Divide(Dim, Dim);
    for (int i = 0 ; i < Dim ; i++){
        for (int j = 0 ; j < Dim ; j++){
        can_4->cd((int)((j + 1) + Dim*(Dim - (i + 1))));      
        BinCorrelationMatrix[i][j]->GetXaxis()->SetTitle(TString::Format("Events in bin %i", i+1));
        BinCorrelationMatrix[i][j]->GetYaxis()->SetTitle(TString::Format("Events in bin %i", j+1));
        //cout  << "Limits x: " << ListOfPosteriors[i]->GetXaxis()->GetBinLowEdge(ListOfPosteriors[i]->FindFirstBinAbove( )) << " " << ListOfPosteriors[i]->GetXaxis()->GetBinUpEdge(ListOfPosteriors[i]->FindLastBinAbove( )) << endl;
        //cout  << "Limits y: " << ListOfPosteriors[j]->GetYaxis()->GetBinLowEdge(ListOfPosteriors[j]->FindFirstBinAbove( )) << " " << ListOfPosteriors[j]->GetYaxis()->GetBinUpEdge(ListOfPosteriors[j]->FindLastBinAbove( )) << endl;
        BinCorrelationMatrix[i][j]->GetXaxis()->SetRangeUser(ListOfPosteriors[i]->GetXaxis()->GetBinLowEdge(ListOfPosteriors[i]->FindFirstBinAbove( )), ListOfPosteriors[i]->GetXaxis()->GetBinUpEdge(ListOfPosteriors[i]->FindLastBinAbove( )) );
        BinCorrelationMatrix[i][j]->GetYaxis()->SetRangeUser(ListOfPosteriors[j]->GetYaxis()->GetBinLowEdge(ListOfPosteriors[j]->FindFirstBinAbove( )), ListOfPosteriors[j]->GetYaxis()->GetBinUpEdge(ListOfPosteriors[j]->FindLastBinAbove( )) );
        float Correlation = BinCorrelationMatrix[i][j]->GetCorrelationFactor();
        //TH2CorrelationMatrix->SetPoint(Point,i+1,j+1, Correlation);
        TLegend * leg = new TLegend(0.1,0.8,0.45,0.9);
        leg->SetHeader(TString::Format("Corr. factor = %.4f", Correlation));
        //gStyle->SetLegendBorderSize(0);
        gStyle->SetLegendTextSize(0.05);
        gStyle->SetOptStat(0);
        BinCorrelationMatrix[i][j]->GetZaxis()->SetRangeUser(-1.0,1.0);
        BinCorrelationMatrix[i][j]->Draw("colz");
        leg->Draw("same");
        }
    }
    Canvases.push_back(can_4);
}

void PlotBinCorrelationFactorMatrix(TH2D * BinCorrelationFactorMatrix){
        TCanvas * can_5 = new TCanvas("Bin Correlation Factor Matrix", "Bin Correlation Factor Matrix", 0, 0, 1600, 1600);
        can_5->cd();
        gStyle->SetOptStat(0);
        BinCorrelationFactorMatrix->Draw("colz text");
        Canvases.push_back(can_5);
}

void PlotRelativeRecoResolution(TH1D * RelativeRecoResolution, float Tau){
      TCanvas * can_6 = new TCanvas("Relative Reco Resolution", "Relative Reco Resolution", 0, 0, 1600, 1600);
      can_6->cd();
      //gStyle->SetOptStat(0);
      RelativeRecoResolution->SetTitle(TString::Format("%s, Reg. par. = %f", RelativeRecoResolution->GetTitle(),Tau));
      RelativeRecoResolution->SetMinimum(0.0);
      //RelativeRecoResolution->SetMaximum(1.1);
      RelativeRecoResolution->Draw("hist");
      Canvases.push_back(can_6);

}

double CalculateChi2NDF(TH1D* h_observed, TH1D* h_expected){
  cout << "I am in the function chi2." << endl;
  double chi2 = 0.0;
  cout << "1." << endl;
  for (int i = 1; i<=h_expected->GetXaxis()->GetNbins(); i++){
    cout << i << endl;
    cout << "Deltas squered: " << (h_observed->GetBinContent(i)-h_expected->GetBinContent(i))*(h_observed->GetBinContent(i)-h_expected->GetBinContent(i)) << endl;
    chi2 = chi2 + (h_observed->GetBinContent(i)-h_expected->GetBinContent(i))*(h_observed->GetBinContent(i)-h_expected->GetBinContent(i))/h_expected->GetBinContent(i);
    cout << "           Chi2: " << chi2 << endl;
  }
  chi2 = chi2/h_expected->GetXaxis()->GetNbins();
  cout << "999." << endl;
  return chi2;
}

// END OF PLOTTING FUNCTIONS

//void DivideEff(TH1D * h,TH1D * eff){
//    for (int i=1;i < h->GetNbinsX()+1 ;i++){
//      if (eff->GetBinContent(i) != 0.0){
//        h->SetPoint(Point,i,h->GetBinContent(i)/eff->GetBinContent(i));
//        //cout << "ERROR: " << eff->GetBinError(i) << endl;
//        //cout << "CONTENT: " << eff->GetBinContent(i) << endl;
//        //cout << "New ERROR: " << eff->GetBinError(i)/eff->GetBinContent(i) << endl;
//        //cout << "****************************8" << endl;
//        h->SetBinError(i,eff->GetBinError(i)/eff->GetBinContent(i));
//      } else{
//        cout << "WARNING: Nedelim histogram, eff = 0 v binu: " << i << endl;
//      }
//    }
//    return h;
//} 

struct InputStruct
  {
    TString Spectrum;
    int Rebin;
    double Tau;
    TString Method;
    int Steps;
    int Seed;
    double Chi2;
    TString Binning;
    int Closure;
  };

int RooUnfoldExampleChi2()
{
std::ifstream input("list.txt");
vector<TString> Lines;
TString Spectrum;
double Rebin;
TString sRebin;
double Tau;
TString sTau;
TString Method;
int Steps;
TString sSteps;
int Seed;
TString sSeed;
double Chi2;
TString sChi2;
TString Binning;
int Closure;
TString sClosure;
int Counter = 0;
vector<TString> Spectra;
vector<double> Taus;
vector<double> Chi2s;

vector<InputStruct> VecStruct;
InputStruct TempStruct;

for (std::string line; getline(input,line);){
  //cout << "line: " << line << endl;
  Lines.push_back(line);
}


for (int j = 0;j<Lines.size(); j++){
  for (int i = 0;i<Lines[j].Length(); i++){
    //cout << Lines[j][i];
    if (Lines[j][i] == '_'){
      Counter++;
      continue;
    } 
      if (Counter == 1){
        Spectrum += Lines[j][i];
      }
      if (Counter == 3){
          sRebin += Lines[j][i];
      }
      if (Counter == 5){
          sTau += Lines[j][i];
      }
      if (Counter == 7){
          Method += Lines[j][i];
      }
      if (Counter == 9){
          sSteps += Lines[j][i];
      }
      if (Counter == 11){
          sSeed += Lines[j][i];
      }
      if (Counter == 13){
          sChi2 += Lines[j][i];
      }
      if (Counter == 15){
          Binning += Lines[j][i];
      }
      if (Counter == 17){
          sClosure += Lines[j][i];
      }
    if (i == Lines[j].Length() - 1){
      Spectra.push_back(Spectrum);
      Taus.push_back(sTau.Atof());
      Chi2s.push_back(sChi2.Atof());
      TempStruct.Spectrum = Spectrum;
      TempStruct.Rebin = sRebin.Atof();
      TempStruct.Tau = sTau.Atof();
      TempStruct.Method = Method;
      TempStruct.Steps = sSteps.Atof();
      TempStruct.Seed = sSeed.Atof();
      TempStruct.Chi2 = sChi2.Atof();
      TempStruct.Binning = Binning;
      TempStruct.Closure = sClosure.Atof();

      VecStruct.push_back(TempStruct);
      // do something

      Counter = 0;
      Spectrum = "";
      sRebin = "";
      sTau = "";
      Method = "";
      sSteps = "";
      sSeed = "";
      sChi2 = "";
      Binning = "";
      sClosure = "";
    }
  }
}

sort( Taus.begin(), Taus.end() );
sort( Spectra.begin(), Spectra.end() );

Taus.erase( unique( Taus.begin(), Taus.end() ), Taus.end() );
Spectra.erase( unique( Spectra.begin(), Spectra.end() ), Spectra.end() );

TH2D * Hh_c = new TH2D("Chi2 using Curvature Regularization", "#chi^{2} using Curvature Regularization", Taus.size(), 0, *max_element(Taus.begin(), Taus.end()) + TMath::Abs(Taus[0]-Taus[1]) , 5, 0, 5);
TH1D * Hh_c_HadTopEta = new TH1D("HadTopEta Chi2 using Curvature Regularization", "#chi^{2} using Curvature Regularization", Taus.size(), 0, *max_element(Taus.begin(), Taus.end()) + TMath::Abs(Taus[0]-Taus[1]));
TH1D * Hh_c_TopPairPt = new TH1D("TopPairPt Chi2 using Curvature Regularization", "#chi^{2} using Curvature Regularization", Taus.size(), 0, *max_element(Taus.begin(), Taus.end()) + TMath::Abs(Taus[0]-Taus[1]));
TH1D * Hh_c_TopPairMass = new TH1D("TopPairMass Chi2 using Curvature Regularization", "#chi^{2} using Curvature Regularization", Taus.size(), 0, *max_element(Taus.begin(), Taus.end()) + TMath::Abs(Taus[0]-Taus[1]));
TH1D * Hh_c_TopPairEta = new TH1D("TopPairEta Chi2 using Curvature Regularization", "#chi^{2} using Curvature Regularization", Taus.size(), 0, *max_element(Taus.begin(), Taus.end()) + TMath::Abs(Taus[0]-Taus[1]));
TH1D * Hh_c_HadTopPt = new TH1D("HadTopPt Chi2 using Curvature Regularization", "#chi^{2} using Curvature Regularization", Taus.size(), 0, *max_element(Taus.begin(), Taus.end()) + TMath::Abs(Taus[0]-Taus[1]));
TH2D * Hh_e = new TH2D("Chi2 using Entropy Regularization", "#chi^{2} using Entropy Regularization", Taus.size(), 0, *max_element(Taus.begin(), Taus.end()) + TMath::Abs(Taus[0]-Taus[1]) , 5, 0, 5); //instead 5 Spectra.size()
TH1D * Hh_e_HadTopEta = new TH1D("HadTopEta Chi2 using Entropy Regularization", "#chi^{2} using Entropy Regularization", Taus.size(), 0, *max_element(Taus.begin(), Taus.end()) + TMath::Abs(Taus[0]-Taus[1]));
TH1D * Hh_e_TopPairPt = new TH1D("TopPairPt Chi2 using Entropy Regularization", "#chi^{2} using Entropy Regularization", Taus.size(), 0, *max_element(Taus.begin(), Taus.end()) + TMath::Abs(Taus[0]-Taus[1]));
TH1D * Hh_e_TopPairMass = new TH1D("TopPairMass Chi2 using Entropy Regularization", "#chi^{2} using Entropy Regularization", Taus.size(), 0, *max_element(Taus.begin(), Taus.end()) + TMath::Abs(Taus[0]-Taus[1]));
TH1D * Hh_e_TopPairEta = new TH1D("TopPairEta Chi2 using Entropy Regularization", "#chi^{2} using Entropy Regularization", Taus.size(), 0, *max_element(Taus.begin(), Taus.end()) + TMath::Abs(Taus[0]-Taus[1]));
TH1D * Hh_e_HadTopPt = new TH1D("HadTopPt Chi2 using Entropy Regularization", "#chi^{2} using Entropy Regularization", Taus.size(), 0, *max_element(Taus.begin(), Taus.end()) + TMath::Abs(Taus[0]-Taus[1]));

TGraph2D * h_c = new TGraph2D();
TGraph2D * h_e = new TGraph2D();
cout << "nbins tau: " << Taus.size()-1 << " min: 0 " << "max: "<<*max_element(Taus.begin(), Taus.end())+TMath::Abs(Taus[0]-Taus[1]) << endl;
cout << "nbins spectra: " << Spectra.size() << " min: 0 " << "max: "<< Spectra.size() << endl;
int Point = 0;
int Point2 = 0;
float epsilon = TMath::Abs((Taus[0]-Taus[1])*1e-5);
epsilon =0.025;

double ScaleHadTopEta;
double ScaleTopPairPt;
double ScaleTopPairMass;
double ScaleTopPairEta;
double ScaleHadTopPt;
for(int i=0; i<VecStruct.size();i++){
   if ((VecStruct[i].Spectrum == "HadTopEta") && (VecStruct[i].Method == "n")){
   ScaleHadTopEta = VecStruct[i].Chi2;
   }
   if ((VecStruct[i].Spectrum == "TopPairPt") && (VecStruct[i].Method == "n")){
   ScaleTopPairPt = VecStruct[i].Chi2;
   }
   if ((VecStruct[i].Spectrum == "TopPairMass") && (VecStruct[i].Method == "n")){
   ScaleTopPairMass = VecStruct[i].Chi2;
   }
   if ((VecStruct[i].Spectrum == "TopPairEta") && (VecStruct[i].Method == "n")){
   ScaleTopPairEta = VecStruct[i].Chi2;
   }
   if ((VecStruct[i].Spectrum == "HadTopPt") && (VecStruct[i].Method == "n")){
   ScaleHadTopPt = VecStruct[i].Chi2;
   }
}

//ScaleHadTopEta = 1.0;
//ScaleTopPairPt = 1.0;
//ScaleTopPairMass = 1.0;
//ScaleTopPairEta = 1.0;
//ScaleHadTopPt = 1.0;

for(int i=0; i<VecStruct.size();i++){
   if (VecStruct[i].Spectrum == "HadTopEta"){
    if ((VecStruct[i].Method == "c") && (VecStruct[i].Tau != 0.0)){
      h_c->SetPoint(Point,VecStruct[i].Tau,1,VecStruct[i].Chi2/ScaleHadTopEta);
      Hh_c->Fill(VecStruct[i].Tau+epsilon,1e-10,VecStruct[i].Chi2/ScaleHadTopEta);
      cout << "here i: " << i << " Tau: " << VecStruct[i].Tau << "           Spectrum: " << "HadTopEta" << "           Chi2: " << VecStruct[i].Chi2 << endl;


      Point++;
      //h_c->GetXaxis()->SetBinLabel(h_c->GetXaxis()->FindBin(VecStruct[i].Tau),TString::Format("Tau %f", VecStruct[i].Tau));
      }
    if ((VecStruct[i].Method == "e") && (VecStruct[i].Tau != 0.0)){
      h_e->SetPoint(Point2,VecStruct[i].Tau,1,VecStruct[i].Chi2/ScaleHadTopEta);
      Hh_e->Fill(VecStruct[i].Tau+epsilon,1e-10,VecStruct[i].Chi2/ScaleHadTopEta);
      Point2++;
      //cout << "i: " << i << " Tau: " << VecStruct[i].Tau << "           Spectrum: " << 1e-10 << "           Chi2: " << VecStruct[i].Chi2 << endl;
      //h_e->GetXaxis()->SetBinLabel(VecStruct[i].Tau+epsilon,TString::Format("Tau %f", VecStruct[i].Tau));
      }
    if ((VecStruct[i].Tau == 0.0) && (VecStruct[i].Method == "n")){
      h_c->SetPoint(Point,0,1,VecStruct[i].Chi2/ScaleHadTopEta);
      Hh_c->Fill(1e-10,1e-10,VecStruct[i].Chi2/ScaleHadTopEta);
      h_e->SetPoint(Point2,0,1,VecStruct[i].Chi2/ScaleHadTopEta);
      Hh_e->Fill(1e-10,1e-10,VecStruct[i].Chi2/ScaleHadTopEta);
      cout << "i: " << i << " Tau: " << 0 << "           Spectrum: " << "HadTopEta" << "           Chi2: " << VecStruct[i].Chi2 << endl;
      Point++;
      Point2++;
      }
   }
   if (VecStruct[i].Spectrum == "TopPairPt"){
    if ((VecStruct[i].Method == "c") && (VecStruct[i].Tau != 0.0)){
      h_c->SetPoint(Point,VecStruct[i].Tau,2,VecStruct[i].Chi2/ScaleTopPairPt);
      Hh_c->Fill(VecStruct[i].Tau+epsilon,1.9,VecStruct[i].Chi2/ScaleTopPairPt);
      cout << "i: " << i << " Tau: " << VecStruct[i].Tau << "           Spectrum: " << 1.9 << "           Chi2: " << VecStruct[i].Chi2 << endl;
      Point++;
      }
    if ((VecStruct[i].Method == "e") && (VecStruct[i].Tau != 0.0)){
      h_e->SetPoint(Point2,VecStruct[i].Tau,2,VecStruct[i].Chi2/ScaleTopPairPt);
      Hh_e->Fill(VecStruct[i].Tau+epsilon,1.9,VecStruct[i].Chi2/ScaleTopPairPt);
      Point2++;
      }
    if ((VecStruct[i].Tau == 0.0) &&(VecStruct[i].Method == "n")){
      h_c->SetPoint(Point,0,2,VecStruct[i].Chi2/ScaleTopPairPt);
      Hh_c->Fill(1e-10,1.9,VecStruct[i].Chi2/ScaleTopPairPt);
      h_e->SetPoint(Point2,0,2,VecStruct[i].Chi2/ScaleTopPairPt);
      Hh_e->Fill(1e-10,1.9,VecStruct[i].Chi2/ScaleTopPairPt);
      cout << "i: " << i << " Tau: " << 1e-10 << "           Spectrum: " << 1.9 << "           Chi2: " << VecStruct[i].Chi2 << endl;
      Point++;
      Point2++;
      }
   }
   if (VecStruct[i].Spectrum == "TopPairMass"){
    if ((VecStruct[i].Method == "c") && (VecStruct[i].Tau != 0.0)){
      h_c->SetPoint(Point,VecStruct[i].Tau,3,VecStruct[i].Chi2/ScaleTopPairMass);
      Hh_c->Fill(VecStruct[i].Tau+epsilon,2.9,VecStruct[i].Chi2/ScaleTopPairMass);
      cout << "i: " << i << " Tau: " << VecStruct[i].Tau << "           Spectrum: " << 2.9 << "           Chi2: " << VecStruct[i].Chi2 << endl;
      Point++;
      }
    if ((VecStruct[i].Method == "e") && (VecStruct[i].Tau != 0.0)){
      h_e->SetPoint(Point2,VecStruct[i].Tau,3,VecStruct[i].Chi2/ScaleTopPairMass);
      Hh_e->Fill(VecStruct[i].Tau+epsilon,2.9,VecStruct[i].Chi2/ScaleTopPairMass);
      Point2++;
      }
    if ((VecStruct[i].Tau == 0.0) &&(VecStruct[i].Method == "n")){
      h_c->SetPoint(Point,0,3,VecStruct[i].Chi2/ScaleTopPairMass);
      Hh_c->Fill(1e-10,2.9,VecStruct[i].Chi2/ScaleTopPairMass);
      h_e->SetPoint(Point2,0,3,VecStruct[i].Chi2/ScaleTopPairMass);
      Hh_e->Fill(1e-10,2.9,VecStruct[i].Chi2/ScaleTopPairMass);
      cout << "i: " << i << " Tau: " << 1e-10 << "           Spectrum: " << 2.9 << "           Chi2: " << VecStruct[i].Chi2 << endl;
      Point++;
      Point2++;
      }
   }
   if (VecStruct[i].Spectrum == "TopPairEta"){
    if ((VecStruct[i].Method == "c") && (VecStruct[i].Tau != 0.0)){
      h_c->SetPoint(Point,VecStruct[i].Tau,4,VecStruct[i].Chi2/ScaleTopPairEta);
      Hh_c->Fill(VecStruct[i].Tau+epsilon,3.9,VecStruct[i].Chi2/ScaleTopPairEta);
      cout << "i: " << i << " Tau: " << VecStruct[i].Tau << "           Spectrum: " << "TopPairEta" << "           Chi2: " << VecStruct[i].Chi2 << endl;
      Point++;
      }
    if ((VecStruct[i].Method == "e") && (VecStruct[i].Tau != 0.0)){
      h_e->SetPoint(Point2,VecStruct[i].Tau,4,VecStruct[i].Chi2/ScaleTopPairEta);
      Hh_e->Fill(VecStruct[i].Tau+epsilon,3.9,VecStruct[i].Chi2/ScaleTopPairEta);
      Point2++;
      }
    if ((VecStruct[i].Tau == 0.0) &&(VecStruct[i].Method == "n")){
      h_c->SetPoint(Point,0,4,VecStruct[i].Chi2/ScaleTopPairEta);
      Hh_c->Fill(1e-10,3.9,VecStruct[i].Chi2/ScaleTopPairEta);
      h_e->SetPoint(Point2,0,4,VecStruct[i].Chi2/ScaleTopPairEta);
      Hh_e->Fill(1e-10,3.9,VecStruct[i].Chi2/ScaleTopPairEta);
      cout << "i: " << i << " Tau: " << 0 << "           Spectrum: " << "TopPairEta" << "           Chi2: " << VecStruct[i].Chi2 << endl;
      Point++;
      Point2++;
      }
     
   }

  if (VecStruct[i].Spectrum == "HadTopPt"){
    if ((VecStruct[i].Method == "c") && (VecStruct[i].Tau != 0.0)){
      h_c->SetPoint(Point,VecStruct[i].Tau,5,VecStruct[i].Chi2/ScaleHadTopPt);
      Hh_c->Fill(VecStruct[i].Tau+epsilon,4.9,VecStruct[i].Chi2/ScaleHadTopPt);
      Point++;
      }
    if ((VecStruct[i].Method == "e") && (VecStruct[i].Tau != 0.0)){
      h_e->SetPoint(Point2,VecStruct[i].Tau,5,VecStruct[i].Chi2/ScaleHadTopPt);
      Hh_e->Fill(VecStruct[i].Tau+epsilon,4.9,VecStruct[i].Chi2/ScaleHadTopPt);
      Point2++;
      }
    if ((VecStruct[i].Tau == 0.0) &&(VecStruct[i].Method == "n")){
      h_c->SetPoint(Point,0,5,VecStruct[i].Chi2/ScaleHadTopPt);
      Hh_c->Fill(1e-10,4.9,VecStruct[i].Chi2/ScaleHadTopPt);
      h_e->SetPoint(Point2,0,5,VecStruct[i].Chi2/ScaleHadTopPt);
      Hh_e->Fill(1e-10,4.9,VecStruct[i].Chi2/ScaleHadTopPt);
      Point++;
      Point2++;
      }
   }
}


//HadTopEta_rebin_100_tau_0.000000_method__steps_50000_seed10_chi2_2.286266_binning_OverBinned_closure_1.root
//HadTopEta_rebin_100_tau_1.000000_method_e_steps_50000_seed10_chi2_1.222554_binning_OverBinned_closure_1.root
//HadTopEta_rebin_100_tau_1.000000_method_c_steps_50000_seed10_chi2_1.222554_binning_OverBinned_closure_1.root
//

  //Hh_c->GetBin(1,1)->SetFillColor(2);
  Hh_c->SetMaximum(2.0);
  Hh_c->SetMinimum(0.0);
  Hh_e->SetMaximum(2.0);
  Hh_e->SetMinimum(0.0);
  gStyle->SetPalette(1);
  //Double_t zcontours[2] = {0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0};
  Double_t zcontours[2] = {0,1.0};
  Hh_c->SetContour(2, zcontours);
  Hh_e->SetContour(2, zcontours);
  //Hh_c->SetFillColor(2)
  //h_c->GetXaxis()->SetBinLabel(1,"No Reg.");
  Hh_c->GetYaxis()->SetBinLabel(1,"HadTopEta");
  Hh_c->GetYaxis()->SetBinLabel(2,"TopPairPt");
  Hh_c->GetYaxis()->SetBinLabel(3,"TopPairMass");
  Hh_c->GetYaxis()->SetBinLabel(4,"TopPairEta");
  Hh_c->GetYaxis()->SetBinLabel(5,"HadTopPt");
  Hh_e->GetYaxis()->SetBinLabel(1,"HadTopEta");
  Hh_e->GetYaxis()->SetBinLabel(2,"TopPairPt");
  Hh_e->GetYaxis()->SetBinLabel(3,"TopPairMass");
  Hh_e->GetYaxis()->SetBinLabel(4,"TopPairEta");
  Hh_e->GetYaxis()->SetBinLabel(5,"HadTopPt");
  //Hh->GetYaxis()->SetBinLabel(5,"Different spectra");
  Hh_c->GetZaxis()->SetTitle("Relative #chi^{2}");
  Hh_c->GetXaxis()->SetTitle("Tau");
  //Hh_c->GetYaxis()->SetTitle("Spectra");
  Hh_c->SetMarkerColor(2);
  Hh_c->SetMarkerStyle(20);
  Hh_e->GetXaxis()->SetTitle("Tau");
  
  //Hh_e->GetYaxis()->SetTitle("Spectra");
  TCanvas * can = new TCanvas("can", "can", 1600,1600);
  can->Divide(2);
  can->cd(1);
  gStyle->SetOptStat(0);
  TLegend * leg_c = new TLegend(0.7,0.7,0.9,0.9);
  leg_c->AddEntry((TObject*)0,TString::Format("Rebin %i",VecStruct[0].Rebin),"");
  leg_c->AddEntry((TObject*)0,TString::Format("Steps %i",VecStruct[0].Steps),"");
  leg_c->AddEntry((TObject*)0,TString::Format("Seed %i",VecStruct[0].Seed),"");
  leg_c->AddEntry((TObject*)0,TString::Format("%s", VecStruct[0].Binning.Data()),"");
  if (VecStruct[0].Closure == 1){
    leg_c->AddEntry((TObject*)0,"Closure","");  
  } else{
    leg_c->AddEntry((TObject*)0,"No closure","");  
  }
  
  //h_c->Draw("lego2 FB");
  //TH2D * hh_c = new TH2D();
  //hh_c = h_c->GetHistogram();
  Hh_c->Draw("lego2d FB");
  leg_c->Draw("same");
  //gPad->SetLogz();
  

  //h_e->GetXaxis()->SetBinLabel(1,"No Reg.");
  Hh_e->SetMarkerColor(2);
  Hh_e->SetMarkerStyle(20);
  Hh_e->GetZaxis()->SetTitle("Relative #chi^{2}");
  //h_e->GetYaxis()->SetBinLabel(1,"HadTopEta");
  //h_e->GetYaxis()->SetBinLabel(2,"TopPairPt");
  //h_e->GetYaxis()->SetBinLabel(3,"TopPairMass");
  //h_e->GetYaxis()->SetBinLabel(4,"TopPairEta");
  h_c->GetZaxis()->SetTitle("Relative #chi^{2}");
  h_c->GetXaxis()->SetTitle("Tau");
  h_c->GetYaxis()->SetTitle("Spectra");
  h_c->SetMarkerColor(2);
  h_c->SetMarkerStyle(20);
  h_e->GetXaxis()->SetTitle("Tau");
  h_e->GetYaxis()->SetTitle("Spectra");
  can->cd(2);
  gStyle->SetOptStat(0);
  //h_e->Draw("lego2 FB");
  Hh_e->Draw("lego2d FB");
  leg_c->Draw("same");
  //gPad->SetLogz();
  ////can->cd(3);
  ////h_c->Draw("p FB");
  ////can->cd(4);
  ////
  ////h_e->SetMarkerColor(2);
  ////h_e->SetMarkerStyle(20);
  ////h_e->GetZaxis()->SetTitle("#chi^{2}");
  ////
  ////h_e->Draw("p FB");
  cout << " --- " << Hh_c->GetBinContent(28,5) << endl;

  for (int i = 1; i <= Hh_c->GetYaxis()->GetNbins(); i++){
    for (int j = 1; j <= Hh_c->GetXaxis()->GetNbins(); j++){
      
      if (i == 1){        
        cout << "i: " << i << " j: " << j << " " << Hh_c->GetBinContent(i,j) << endl;
        Hh_c_HadTopEta->SetBinContent(j,Hh_c->GetBinContent(j,i));
        Hh_e_HadTopEta->SetBinContent(j,Hh_e->GetBinContent(j,i));
      }
      if (i == 2){
        //cout << j << " " << Hh_c->GetBinContent(i,j) << endl;
        Hh_c_TopPairPt->SetBinContent(j,Hh_c->GetBinContent(j,i));
        Hh_e_TopPairPt->SetBinContent(j,Hh_e->GetBinContent(j,i));
      }
      if (i == 3){
        Hh_c_TopPairMass->SetBinContent(j,Hh_c->GetBinContent(j,i));
        Hh_e_TopPairMass->SetBinContent(j,Hh_e->GetBinContent(j,i));
      }
      if (i == 4){
        Hh_c_TopPairEta->SetBinContent(j,Hh_c->GetBinContent(j,i));
        Hh_e_TopPairEta->SetBinContent(j,Hh_e->GetBinContent(j,i));
      }
      if (i == 5){
        Hh_c_HadTopPt->SetBinContent(j,Hh_c->GetBinContent(j,i));
        Hh_e_HadTopPt->SetBinContent(j,Hh_e->GetBinContent(j,i));
      }
  }
  }

  Hh_c_HadTopEta->SetLineColor(1);
  Hh_e_HadTopEta->  SetLineColor(1);
  Hh_c_TopPairPt->SetLineColor(2);
  Hh_e_TopPairPt->SetLineColor(2);
  Hh_c_TopPairMass->SetLineColor(kGreen -2);
  Hh_e_TopPairMass->SetLineColor(kGreen -2);
  Hh_c_TopPairEta->SetLineColor(4);
  Hh_e_TopPairEta->SetLineColor(4);
  Hh_c_HadTopPt->SetLineColor(6);
  Hh_e_HadTopPt->SetLineColor(6);

  Hh_c_HadTopEta->SetLineWidth(2);
  Hh_e_HadTopEta->  SetLineWidth(2);
  Hh_c_TopPairPt->SetLineWidth(2);
  Hh_e_TopPairPt->SetLineWidth(2);
  Hh_c_TopPairMass->SetLineWidth(2);
  Hh_e_TopPairMass->SetLineWidth(2);
  Hh_c_TopPairEta->SetLineWidth(2);
  Hh_e_TopPairEta->SetLineWidth(2);
  Hh_c_HadTopPt->SetLineWidth(2);
  Hh_e_HadTopPt->SetLineWidth(2);


  TCanvas * can2 = new TCanvas("can2", "can2", 1600,1600);
  can2->Divide(2);
  can2->cd(1);
  gStyle->SetOptStat(0);
  Hh_c_HadTopEta->GetXaxis()->SetTitle("Regularization strength #tau");
  Hh_c_HadTopEta->GetYaxis()->SetTitle("Relative #chi^{2}");
  Hh_c_HadTopEta->SetMaximum(2.5);
  Hh_c_HadTopEta->SetMinimum(0.0);
  Hh_c_HadTopEta->Draw("hist");
  Hh_c_HadTopPt->Draw("hist same");
  Hh_c_TopPairPt->Draw("hist same");
  Hh_c_TopPairMass->Draw("hist same");
  Hh_c_TopPairEta->Draw("hist same");
  TLegend * leg1 = new TLegend(0.58,0.65,0.86,0.87);
  leg1->SetFillStyle(0);
  leg1->AddEntry(Hh_c_HadTopEta,"HadTopEta");
  leg1->AddEntry(Hh_c_HadTopPt,"HadTopPt");
  leg1->AddEntry(Hh_c_TopPairEta,"TopPairEta");
  leg1->AddEntry(Hh_c_TopPairPt,"TopPairPt");
  leg1->AddEntry(Hh_c_TopPairMass,"TopPairMass");
  leg1->Draw("same");
  can2->cd(2);
  gStyle->SetOptStat(0);
  Hh_e_HadTopEta->GetXaxis()->SetTitle("Regularization strength #tau");
  Hh_e_HadTopEta->GetYaxis()->SetTitle("Relative #chi^{2}");
  Hh_e_HadTopEta->SetMaximum(2.5);
  Hh_e_HadTopEta->SetMinimum(0.0);
  Hh_e_HadTopEta->Draw("hist");
  Hh_e_HadTopPt->Draw("hist same");
  Hh_e_TopPairPt->Draw("hist same");
  Hh_e_TopPairMass->Draw("hist same");
  Hh_e_TopPairEta->Draw("hist same");
  leg1->Draw("same");
  return 0;
}






#ifndef __CINT__
int main (int argc, char *argv[]) { 
cout << "Parameters: " << argc << endl;
cout << "Parameters: " << argv[1] << endl;


  //if (argc > 0){
  //    cout << "Parametr je: " << argv[1] << endl;
  //    RooUnfoldExample(atoi(argv[1])); return 0;  
  //} else{
  //RooUnfoldExample(); return 0;
  //}
  }  // Main program when run stand-alone
#endif

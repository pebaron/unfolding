!/bin/bash

#make clean
#make
#echo "1 - nrebin, 2 - tau1, 3 - method1, 4 - tau2, 5 - method2, 6 - steps, 7 - seed, 8 - closure" 
#echo $1
#echo $2
#echo $3
#echo $4
#echo $5
#echo $6
#echo $7
#echo $8
#first parameter rebin
#second parametr tau, streght of regulariyation
#third parameter is reg method, e=entropy, c=curvature
# HadTopPt
#for i in HadTopPt HadTopEta TopPairPt TopPairMass TopPairEta;do
for i in TopPairMass TopPairEta;do
#for i in HadTopEta;do
#root -b -q -l "./examples/RooUnfoldExample.cxx(${1},0,\"\",${6}, ${7},\"${i}\",${8})";
#for j in $(seq -3 0.1 3);do 
for j in $(seq 0 0.05 3);do 
b=`echo $j | sed "s/,/./g"`;
result=`echo "$b<0.001" | bc`;
if [ $result -eq 1 ]
then
root -l -q -b "./examples/RooUnfoldExample.cxx(${1},0,\"n\",100000,10,\"${i}\",1)";
else
root -l -q -b "./examples/RooUnfoldExample.cxx(${1},${b},\"c\",100000,10,\"${i}\",1)";
root -l -q -b "./examples/RooUnfoldExample.cxx(${1},${b},\"e\",100000,10,\"${i}\",1)"; 
fi
#convert ./plots/*.pdf ./finalpdfs/spectrum_${i}_rebin_$1_tau_${b}_steps_50000_seed_10_closure_1.pdf;
#rm ./plots/*.pdf;
done

#root -b -q -l "./examples/RooUnfoldExample.cxx(${1},${2},\"${3}\",${6},${7},\"${i}\",${8})";
#root -b -q -l "./examples/RooUnfoldExample.cxx(${1},${4},\"${5}\",${6},${7},\"${i}\",${8})";
#root -b -q -l "./examples/RooUnfoldExample.cxx(${1},2,\"${3}\",${6},${7},\"${i}\",${8})";
#root -b -q -l "./examples/RooUnfoldExample.cxx(${1},2,\"${5}\",${6},${7},\"${i}\",${8})";
##convert ./plots/*.png ./pdfs/$1_reg$2.pdf
##rm ./plots/*.png
##convert ./pdfs/*.pdf ./finalpdfs/$1_$2.pdf

done;
##rm ./pdfs/*.pdf
#"standard_analysis.C+(\"../rootfiles/Outputs/Run0535\",\"./out\",\"iif\",12000,${myvar},-0.00015)"

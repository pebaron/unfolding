#!/bin/bash

ls spectrum*.root &> list.txt
input="./list.txt"
while IFS= read -r line
do 
  echo $line | sed "s|.root||g" &>> new_list.txt
done < "$input" 
mv new_list.txt list.txt
//=====================================================================-*-C++-*-
// File and Version Information:
//      $Id: RooUnfoldBaron.cxx xxx 2020-01-01 00:00:00Z Petr Baron $
//
// Description:
//      Fullt Bayesian unfolding with Regularization.
//
// Author: Petr Baron <petrbaron@petrbaron.com>
//
//==============================================================================

//____________________________________________________________
/* BEGIN_HTML
<p>Links to the RooUnfoldBaronImpl class which uses Fully Bayesian unfolding to reconstruct the truth distribution.</p>
<p>Returned errors can be either as a diagonal matrix or as a full matrix of covariances
<p>Regularisation parameter tau sets the strenght of regularization. In Case of tau = 0 no regularization is applied
<p>Returns unfolded spectra and probability distributions for each bin.
END_HTML */

/////////////////////////////////////////////////////////////

#include "RooUnfoldBaron.h"

#include <iostream>
#include <ctime>
#include <string>
#include <iomanip>
#include <math.h>

#include <cmath>
#include <vector>

#include "TClass.h"
#include "TMatrixD.h"
#include "TBuffer.h"
#include "TVectorD.h"
#include "TRandom3.h"
#include "TMath.h"
#include "TNamed.h"
#include "TH1.h"
#include "TH2.h"
#include "TH2D.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TString.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TArrayD.h"
#include "TGraph2D.h"

#include "RooUnfoldResponse.h"

using std::min;
using std::cerr;
using std::endl;
using std::cout;
using std::setw;
using std::left;
using std::right;
using std::vector;

TRandom3 RandomNumber;

ClassImp (RooUnfoldBaron);

RooUnfoldBaron::RooUnfoldBaron (const RooUnfoldResponse* res, const TH1* meas, Int_t niter, float Tau, TString RegMethod, int NSteps, int Seed, Bool_t smoothit,
                                const char* name, const char* title)
  : RooUnfold (res, meas, name, title), _niter(niter), _Tau(Tau), _RegMethod(RegMethod),_NSteps(NSteps), _Seed(Seed), _smoothit(smoothit)
{
  
}

void RooUnfoldBaron::Init()
{
  
}

void RooUnfoldBaron::Unfold()
{
  setup();
  unfold();
  _rec.ResizeTo(_nc);
  _rec = _nbarCi;
  _rec.ResizeTo(_nt);  // drop fakes in final bin
  _unfolded= true;
}

void RooUnfoldBaron::setup()
{
  RandomNumber.SetSeed(_Seed);
  _nc = _nt;
  _ne = _nm;
  _nEstj.ResizeTo(_ne);
  _nEstj= Vmeasured();
  _nCi.ResizeTo(_nt);
  _nCi= _res->Vtruth();
  NumberOfLikes = 0;
  Dim = _nm;
  grad.resize(Dim);
  _acceptancyCi.resize(_ne);
  NormalizedMatrix.ResizeTo(Dim,Dim);
  RPrimeLeap.resize(Dim);
  ThetaPrimeLeap.resize(Dim);
  GradPrimeLeap.resize(Dim);
  low.resize(Dim);
  up.resize(Dim);
  Rmin.resize(Dim);
  Rmax.resize(Dim);
  Paths.resize(Dim);
  ChosenPaths.resize(Dim);
  AllPaths.resize(Dim);
  AllPathsExp.resize(Dim);
  ListOfPosteriors.resize(Dim);
  for (int i = 0; i < Dim ; i++){
    TGraph2D * graph = new TGraph2D();
    graph->SetName(TString::Format("Paths in bin %i", i+1));
    graph->SetTitle(TString::Format("Paths in bin %i", i+1));
    graph->GetXaxis()->SetName(TString::Format("Truth in bin %i", i+1));
    graph->GetYaxis()->SetName(TString::Format("Momentum in bin %i", i+1));
    graph->GetZaxis()->SetName(TString::Format("Likelihood in bin %i", i+1));
    Paths[i] = graph;

    TGraph2D * graph2 = new TGraph2D();
    graph2->SetName(TString::Format("Chosen Paths in bin %i", i+1));
    graph2->SetTitle(TString::Format("Chosen Paths in bin %i", i+1));
    graph2->GetXaxis()->SetName(TString::Format("Truth in bin %i", i+1));
    graph2->GetYaxis()->SetName(TString::Format("Momentum in bin %i", i+1));
    graph2->GetZaxis()->SetName(TString::Format("Likelihood in bin %i", i+1));
    ChosenPaths[i] = graph2;
  }

  _Nji.ResizeTo(_ne,_nt);
  H2M (_res->Hresponse(), _Nji, _overflow);   // don't normalise, which is what _res->Mresponse() would give us

  if (_res->FakeEntries()) {
    TVectorD fakes= _res->Vfakes();
    Double_t nfakes= fakes.Sum();
    if (verbose()>=0) cout << "Add truth bin for " << nfakes << " fakes" << endl;
    _nc++;
    _nCi.ResizeTo(_nc);
    _nCi[_nc-1]= nfakes;
    _Nji.ResizeTo(_ne,_nc);
    for (Int_t i= 0; i<_nm; i++) _Nji(i,_nc-1)= fakes[i];
  }

  NormMatrix = GetNormFactor(_Nji);
  NormalizedMatrix = NormalizeMatrix(_Nji);
  TH2NormalizedMatrix = (TH2D *)_res->Hresponse()->Clone("Normalized Unfolding Matrix");
  TH2NormalizedMatrix->SetTitle("Normalized Unfolding Matrix");
  TH2NormalizedMatrix->ClearUnderflowAndOverflow();
  TH2NormalizedMatrix->Reset();
  for (int i = 0; i < Dim ; i++){
    for (int j = 0; j < Dim ; j++){
      TH2NormalizedMatrix->SetBinContent(i+1,j+1,NormalizedMatrix(i,j));
    }
  }
  TH2CorrelationMatrix = (TH2D *)TH2NormalizedMatrix->Clone("Bin Correlations");
  TH2CorrelationMatrix->SetTitle("Bin Correlations");
  TH2CorrelationMatrix->Reset();

  _nbarCi.ResizeTo(_nc);
  _efficiencyCi.ResizeTo(_nc);
}

//-------------------------------------------------------------------------

TMatrixD& RooUnfoldBaron::H2M (const TH2* h, TMatrixD& m, Bool_t overflow)
{
  //cout << "-----------------------H2M--------------------------\n" << endl;
  // TH2 -> TMatrixD
  if (!h) return m;
  Int_t first= overflow ? 0 : 1;
  Int_t nm= m.GetNrows(), nt= m.GetNcols();
  for (Int_t j= 0; j < nt; j++)
    for (Int_t i= 0; i < nm; i++)
      m(i,j)= h->GetBinContent(i+first,j+first);
  return m;
}

//-------------------------------------------------------------------------

bool RooUnfoldBaron::CheckInput(RooUnfoldBaron::InputStruct a){

    bool b_Theta;
    bool b_R;
    bool b_Grad;
    bool b_LogU;
    bool b_V;
    bool b_J;
    bool b_Epsilon;
    bool b_Joint0;

    b_Theta = b_CheckArray(a.Theta);
    b_R = b_CheckArray(a.R);
    b_Grad = b_CheckArrayL(a.Grad);
    b_LogU = b_CheckNumberL(a.LogU);
    b_V = b_CheckNumber(a.V);
    b_J = b_CheckNumber(a.J);
    b_Epsilon = b_CheckNumber(a.Epsilon);
    b_Joint0 = b_CheckNumberL(a.Joint0);

    return (b_Theta || b_R || b_Grad || b_LogU || b_V || b_J || b_Epsilon || b_Joint0);
}

double RooUnfoldBaron::SquereVec(vector<double> a){
      double b = 0.0;
      for (unsigned int j = 0; j < a.size(); j++){
        b = a[j]*a[j]; 
      }
      return b;
}

//-------------------------------------------------------------------------

double RooUnfoldBaron::MultiplyVec(vector<double> a, vector<double> b){
      double c = 0.0;
      for (unsigned int j = 0; j < a.size(); j++){
        c = a[j]*b[j]; 
      }
      return c;
}

//-------------------------------------------------------------------------

bool RooUnfoldBaron::StopCriterion(vector<double> ThetaMinus, vector<double> ThetaPLus, vector<double> RMinus, vector<double> RPlus){
    vector<double> DTheta(Dim);
    for (int i = 0; i < Dim; i++){
      DTheta[i] = ThetaPLus[i] - ThetaMinus[i];
    }
    double RMinusDTheta;
    RMinusDTheta = MultiplyVec(DTheta, RMinus);
    double RPlusDTheta;
    RPlusDTheta = MultiplyVec(DTheta, RPlus);
    if ((RMinusDTheta >= 0.0) && (RPlusDTheta >= 0.0)){
      return true;
    } else {
      return false;
    }


}

//-------------------------------------------------------------------------

RooUnfoldBaron::BuildTreeStruct RooUnfoldBaron::BuildTree(vector<double> Theta, vector<double> R, vector<long double> Grad, long double LogU, double V, double J, double Epsilon, long double Joint0){
  
  RooUnfoldBaron::InputStruct InputCheckStruct;

  InputCheckStruct.Theta = Theta;
  InputCheckStruct.R = R;
  InputCheckStruct.Grad = Grad;
  InputCheckStruct.LogU = LogU;
  InputCheckStruct.V = V;
  InputCheckStruct.J = J;
  InputCheckStruct.Epsilon = Epsilon;
  InputCheckStruct.Joint0 = Joint0;

  ReturnNans = CheckInput(InputCheckStruct);

  if (ReturnNans == true){
    cout << "WARING: NaN is in pseudoexp truth spectrum." << endl;
    ReturnBuildStruct.NaN = 1;
    return ReturnBuildStruct;
  }

  //END OF CHECING INPUT

  if (J == 0){
        LeapStruct0 = LeapFrog(Theta, R, Grad, Epsilon*V, Epsilon);
        if (LeapStruct0.NaN == 1){
            ReturnBuildStruct.NaN = 1;
            return ReturnBuildStruct;
        }
        LeapStruct0.RPrime.resize(Dim);
        joint = LeapStruct0.LogPrime - 0.5*SquereVec(LeapStruct0.RPrime);
        if (LogU < joint){
          NPrime = 1.0;
        } else{
          NPrime = 0.0;
        }

        if (LogU - 1000.0 < joint){
          SPrime = 1.0;
        } else{
          SPrime = 0.0;
        }
        ThetaMinus = LeapStruct0.ThetaPrime;
        ThetaPlus = LeapStruct0.ThetaPrime;
        RMinus = LeapStruct0.RPrime;
        RPlus = LeapStruct0.RPrime;
        GradMinus = LeapStruct0.GradPrime;
        GradPlus = LeapStruct0.GradPrime;
        GradPrime = LeapStruct0.GradPrime;
        LogPPrime = LeapStruct0.LogPrime;
        ThetaPrime = LeapStruct0.ThetaPrime;
        if (1.0 < TMath::Exp(joint - Joint0)){
            AlphaPrime = 1.0;
        } else{
            AlphaPrime = TMath::Exp(joint - Joint0);
        }
        NAlphaPrime = 1.0;
  } else{
        BuildStruct = BuildTree(Theta, R, Grad, LogU, V, J - 1.0, Epsilon, Joint0);
        if (BuildStruct.NaN == 1){
          ReturnBuildStruct.NaN = 1;
          return ReturnBuildStruct;
        }
        ThetaMinus = BuildStruct.ThetaMinus;
        RMinus = BuildStruct.RMinus;
        GradMinus = BuildStruct.GradMinus;
        ThetaPlus = BuildStruct.ThetaPlus;
        RPlus = BuildStruct.RPlus;
        GradPlus = BuildStruct.GradPlus;
        ThetaPrime = BuildStruct.ThetaPrime;
        GradPrime = BuildStruct.GradPrime;
        LogPPrime = BuildStruct.LogPPrime;
        NPrime = BuildStruct.NPrime;
        SPrime = BuildStruct.SPrime;
        AlphaPrime = BuildStruct.AlphaPrime;
        NAlphaPrime = BuildStruct.NAlphaPrime;
        
        if (SPrime == 1.0){
          if (V == -1){
              BuildStruct2 = BuildTree(ThetaMinus, RMinus, GradMinus, LogU, V, J - 1.0, Epsilon, Joint0);
              if (BuildStruct2.NaN == 1){
                ReturnBuildStruct.NaN = 1;
                return ReturnBuildStruct;
              }
              ThetaMinus = BuildStruct2.ThetaMinus;
              RMinus = BuildStruct2.RMinus;
              GradMinus = BuildStruct2.GradMinus;
              ThetaPrime2 = BuildStruct2.ThetaPrime;
              GradPrime2 = BuildStruct2.GradPrime;
              LogPPrime2 = BuildStruct2.LogPPrime;
              NPrime2 = BuildStruct2.NPrime;
              SPrime2 = BuildStruct2.SPrime;
              AlphaPrime2 = BuildStruct2.AlphaPrime;
              NAlphaPrime2 = BuildStruct2.NAlphaPrime;
          } else{
              BuildStruct2 = BuildTree(ThetaPlus, RPlus, GradPlus, LogU, V, J - 1.0, Epsilon, Joint0);
              if (BuildStruct2.NaN == 1){
                ReturnBuildStruct.NaN = 1;
                return ReturnBuildStruct;
              }
              ThetaPlus = BuildStruct2.ThetaPlus;
              RPlus = BuildStruct2.RPlus;
              GradPlus = BuildStruct2.GradPlus;
              ThetaPrime2 = BuildStruct2.ThetaPrime;
              GradPrime2 = BuildStruct2.GradPrime;
              LogPPrime2 = BuildStruct2.LogPPrime;
              NPrime2 = BuildStruct2.NPrime;
              SPrime2 = BuildStruct2.SPrime;
              AlphaPrime2 = BuildStruct2.AlphaPrime;
              NAlphaPrime2 = BuildStruct2.NAlphaPrime;
          }
          RandN = ::RandomNumber.Rndm();
          if ((NPrime + NPrime2) > 1.0){
            CompareNumber = NPrime2 / (NPrime + NPrime2);
          } else{
            CompareNumber = NPrime2;
          }
          if (RandN < CompareNumber){
            ThetaPrime = ThetaPrime2;
            GradPrime = GradPrime2;
            LogPPrime = LogPPrime2;
          } 

          NPrime = NPrime + NPrime2;
          SPrime = (int)((bool)(SPrime) && (bool)(SPrime2) && StopCriterion(ThetaMinus, ThetaPlus, RMinus, RPlus));
          AlphaPrime = AlphaPrime + AlphaPrime2;
          NAlphaPrime = NAlphaPrime + NAlphaPrime2;
        }
        
  }

    if ((b_CheckNumberL(LogPPrime) == true) || (b_CheckNumber(NPrime) == true) || (b_CheckNumber(SPrime) == true) || (b_CheckNumber(AlphaPrime) == true) || (b_CheckNumber(NAlphaPrime) == true)){
        cout << "LogPrime: " << LogPPrime << endl;    
        ReturnBuildStruct.NaN = 1;
        return ReturnBuildStruct;
    }

    ReturnBuildStruct.ThetaMinus = ThetaMinus;
    ReturnBuildStruct.RMinus = RMinus;
    ReturnBuildStruct.GradMinus = GradMinus;
    ReturnBuildStruct.ThetaPlus = ThetaPlus;
    ReturnBuildStruct.RPlus = RPlus;
    ReturnBuildStruct.GradPlus = GradPlus;
    ReturnBuildStruct.ThetaPrime = ThetaPrime;
    ReturnBuildStruct.GradPrime = GradPrime;
    ReturnBuildStruct.LogPPrime = LogPPrime;
    ReturnBuildStruct.NPrime = NPrime;
    ReturnBuildStruct.SPrime = SPrime;
    ReturnBuildStruct.AlphaPrime = AlphaPrime;
    ReturnBuildStruct.NAlphaPrime = NAlphaPrime;
    
  return ReturnBuildStruct;
}

//-------------------------------------------------------------------------
RooUnfoldBaron::LeapFrogStruct RooUnfoldBaron::LeapFrog(vector<double> Theta, vector<double> R, vector<long double> Grad, double Epsilon, double GradEpsilon){
    Grad = Gradient(Theta, GradEpsilon);
    for (int i = 0; i < Dim ; i++){
      if (Epsilon > 0.0){
      Rmin[i] = (low[i] - Theta[i] - 0.5*Epsilon*Epsilon*Grad[i])/Epsilon;
      Rmax[i] = (up[i] - Theta[i] - 0.5*Epsilon*Epsilon*Grad[i])/Epsilon;
      } else{
        Rmax[i] = (low[i] - Theta[i] - 0.5*Epsilon*Epsilon*Grad[i])/Epsilon;
        Rmin[i] = (up[i] - Theta[i] - 0.5*Epsilon*Epsilon*Grad[i])/Epsilon;
      }
      RPrimeLeap[i] = R[i] + 0.5*Epsilon*Grad[i];
      ThetaPrimeLeap[i] = Theta[i] + Epsilon*RPrimeLeap[i];
      if (ThetaPrimeLeap[i] < 0.0){
        RPrimeLeap[i] = R[i] - 0.5*Epsilon*Grad[i];
        ThetaPrimeLeap[i] = Theta[i] - Epsilon*RPrimeLeap[i];
      }
      if (ThetaPrimeLeap[i] < 0.0){
        R[i] = ::RandomNumber.Uniform(Rmin[i], Rmax[i]);
        RPrimeLeap[i] = R[i] + 0.5*Epsilon*Grad[i];
        ThetaPrimeLeap[i] = Theta[i] + Epsilon*RPrimeLeap[i];
        if (ThetaPrimeLeap[i] < 0.0){
        RPrimeLeap[i] = R[i] - 0.5*Epsilon*Grad[i];
        ThetaPrimeLeap[i] = Theta[i] - Epsilon*RPrimeLeap[i];
        }
      }
    }
    llh = LogLikelihood(ThetaPrimeLeap);
    GradPrimeLeap = Gradient(ThetaPrimeLeap, GradEpsilon);
    if (b_CheckArrayL(GradPrimeLeap)){
        LeapStruct.NaN = 1;  
        return LeapStruct;
    }
    for (int i = 0; i < Dim ; i++){
      RPrimeLeap[i] = RPrimeLeap[i] + 0.5*Epsilon*GradPrimeLeap[i];
      ChosenPaths[i]->SetPoint(Chosenpoints, ThetaPrimeLeap[i], RPrimeLeap[i], llh );
      if (TMath::Exp(llh) > 0.0){
        Paths[i]->SetPoint(points, ThetaPrimeLeap[i], RPrimeLeap[i], TMath::Exp(llh) );
        }
      }

    if (Chosenpoints >= 1e5){
      Chosenpoints = -1;
    }

    if (points >= 1e5){
      points = -1;
    }
    Chosenpoints++;
    points++;

    LeapStruct.ThetaPrime = ThetaPrimeLeap;
    LeapStruct.RPrime = RPrimeLeap;
    LeapStruct.GradPrime = GradPrimeLeap;
    LeapStruct.LogPrime = llh;
    LeapStruct.NaN = 0;
    return LeapStruct;
}

//-------------------------------------------------------------------------

bool RooUnfoldBaron::b_CheckNumber(double a){
if ((std::isnan(std::abs(a))) || (TMath::Infinity() == std::abs(a)) || (a != a)){
  return true;
} else{
  return false;
}
}

//-------------------------------------------------------------------------

bool RooUnfoldBaron::b_CheckArray(vector<double> a){

for (unsigned int i = 0; i < a.size(); i++){
  if ((std::isnan(std::abs(a[i]))) || (TMath::Infinity() == std::abs(a[i])) || (a[i] != a[i])){
      return true;
  }


}
return false;
}

//-------------------------------------------------------------------------

bool RooUnfoldBaron::b_CheckNumberL(long double a){
if ((std::isnan(std::abs(a))) || (TMath::Infinity() == std::abs(a)) || (a != a)){
  return true;
} else{
  return false;
}
}

//-------------------------------------------------------------------------

bool RooUnfoldBaron::b_CheckArrayL(vector<long double> a){

for (unsigned int i = 0; i < a.size(); i++){
  if ((std::isnan(std::abs(a[i]))) || (TMath::Infinity() == std::abs(a[i])) || (a[i] != a[i])){
      return true;
  }


}
return false;
}


//************************************************************************

//------------------------------------------------------------------------

double RooUnfoldBaron::CheckNumber(double a){
if ((std::isnan(std::abs(a))) || (TMath::Infinity() == std::abs(a)) || (a != a)){
  return NAN;
} else{
  return a;
}
}

//-------------------------------------------------------------------------

vector<double> RooUnfoldBaron::CheckArray(vector<double> a){

for (unsigned int i = 0; i < a.size(); i++){
  if ((std::isnan(std::abs(a[i]))) || (TMath::Infinity() == std::abs(a[i])) || (a[i] != a[i])){
      vector<double> ArrayOfNans;
      for (unsigned int j = 0; j < a.size(); j++){
        ArrayOfNans[j] = NAN;  
      }
      return ArrayOfNans;
  }


}
return a;
}

//-------------------------------------------------------------------------

long double RooUnfoldBaron::CheckNumberL(long double a){
if ((std::isnan(std::abs(a))) || (TMath::Infinity() == std::abs(a)) || (a != a)){
  return NAN;
} else{
  return a;
}
}

//-------------------------------------------------------------------------

vector<long double> RooUnfoldBaron::CheckArrayL(vector<long double> a){

for (unsigned int i = 0; i < a.size(); i++){
  if ((std::isnan(std::abs(a[i]))) || (TMath::Infinity() == std::abs(a[i])) || (a[i] != a[i])){
      vector<long double> ArrayOfNans;
      for (unsigned int j = 0; j < a.size(); j++){
        ArrayOfNans[j] = NAN;  
      }
      return ArrayOfNans;
  }


}
return a;
}

//-------------------------------------------------------------------------

double RooUnfoldBaron::GetNormFactor(TMatrixD NMatrix){
  double NormFactor = 0.0;
  for (int i = 0; i < Dim ; i++){
    for (int j = 0 ; j < Dim ; j++) {
    NormFactor = NormFactor + NMatrix(i,j);
    }
  }
  return NormFactor;
}

TMatrixD RooUnfoldBaron::NormalizeMatrix(TMatrixD NMatrix){
  TMatrixD Matrix(Dim,Dim);
  double Sum;
  for (int i = 0; i < Dim ; i++){
    Sum = 0.0;
    for (int j = 0 ; j < Dim ; j++) {
    Sum = Sum + NMatrix(j,i);
    }
    if (Sum > 0.){
        for (int j = 0 ; j < Dim ; j++) {
          Matrix(j,i) = NMatrix(j,i)/Sum;
        }
    }
  }
  return Matrix;
}

//-------------------------------------------------------------------------

vector<long double> RooUnfoldBaron::Gradient(vector<double> Theta, long double step){
    vector<double> ThetaPlus(Dim);
    vector<double>  ThetaMinus(Dim); 
    vector<double>  RecoPlus(Dim);
    vector<double>  RecoMinus(Dim);
    long double delta = 0.0;
    ThetaPlus = Theta;
    ThetaMinus = Theta;
    step = 1.0;
    for (int i = 0; i < Dim ; i++){
        if (step >= Theta[i]){
          step = Theta[i]*0.25;
        }
    }


    for (int i = 0; i < Dim ; i++){
        ThetaPlus[i] = Theta[i] + step;
        ThetaMinus[i] = Theta[i] - step;
        RecoPlus = CalculateReco(ThetaPlus);
        RecoMinus = CalculateReco(ThetaMinus);
        if ((_Tau != 0.0) && (_RegMethod = "e")){
            for (int j = 0; j < Dim ; j++){
              delta = delta + (TMath::Log(RecoMinus[j]/RecoPlus[j]) + 0.5*(RecoPlus[j] - RecoMinus[j])*(_nEstj[j]*_nEstj[j]/(RecoPlus[j]*RecoMinus[j]) - 1.0))/(2*step);
              }
            grad[i] = delta + _Tau*(CalculateEntropy(ThetaPlus) - CalculateEntropy(ThetaMinus))/(2.0*step);
            
            } else  if ((_Tau != 0.0) && (_RegMethod = "c")){
                for (int j = 0; j < Dim ; j++){
                  delta = delta + (TMath::Log(RecoMinus[j]/RecoPlus[j]) + 0.5*(RecoPlus[j] - RecoMinus[j])*(_nEstj[j]*_nEstj[j]/(RecoPlus[j]*RecoMinus[j]) - 1.0))/(2*step);
                  }
                grad[i] = delta + _Tau*(CalculateCurvature(ThetaPlus) - CalculateCurvature(ThetaMinus))/(2.0*step);
            }
            else{
                for (int j = 0; j < Dim ; j++){
                  delta = delta + (TMath::Log(RecoMinus[j]/RecoPlus[j]) + 0.5*(RecoPlus[j] - RecoMinus[j])*(_nEstj[j]*_nEstj[j]/(RecoPlus[j]*RecoMinus[j]) - 1.0))/(2*step);
                  }
                grad[i] = delta;
            }
        ThetaPlus[i] = Theta[i];
        ThetaMinus[i] = Theta[i];
    }

    grad = CheckArrayL(grad);
    return grad;
}

//-------------------------------------------------------------------------

vector<double> RooUnfoldBaron::CalculateReco(vector<double> Theta){

  vector<double> Reco(Dim);
  for (int i = 0; i < Dim ; i++){
    Reco[i] = 0.0;
    for (int j = 0 ; j < Dim ; j++) {
      Reco[i] = Reco[i] + NormalizedMatrix(i,j)*Theta[j];   
    }
  }
  return Reco;
}
//-------------------------------------------------------------------------

long double RooUnfoldBaron::LogFactorial(double a){
//    Γ(n+1) = n!.
//    Stirling’s approximation says that ... https://www.johndcook.com/blog/2010/08/16/how-to-compute-log-factorial/
//    
//    log Γ(x) ≈ (x – 1/2) log(x) – x  + (1/2) log(2 π)
//    log Γ(x + 1) ≈ (x + 1/2) log(x + 1) – x - 1  + (1/2) log(2 π)
  return ((a + 0.5)*TMath::Log(a + 1) - a - 1  + 0.5*TMath::Log(2*TMath::Pi()));
}

long double RooUnfoldBaron::CalculateEntropy(vector<double> Theta){
  double SumOfTheta = 0.0;
  for (int i = 0; i < Dim ; i++){
      SumOfTheta = SumOfTheta + Theta[i];
  }
  double Entropy = 0.0;
  for (int i = 0; i < Dim ; i++){
      Entropy = Entropy + TMath::Log((TMath::Abs(Theta[i])/SumOfTheta))*Theta[i]/SumOfTheta;
  }
  return Entropy;
}

long double RooUnfoldBaron::CalculateCurvature(vector<double> Theta){
  double CurvSum = 0.0;
  for (int i = 1; i < Dim - 1 ; i++){
      CurvSum = CurvSum + ((Theta[i+1] - Theta[i])-(Theta[i] - Theta[i-1]))*((Theta[i+1] - Theta[i])-(Theta[i] - Theta[i-1]));
  }
  return CurvSum;
}

long double RooUnfoldBaron::LogLikelihood(vector<double> Theta){

vector<double> Reco(Dim);
Reco = CalculateReco(Theta);
long double lhi2;
long double lh2 = 0.0;
for (int i = 0; i < Dim ; i++){
    lhi2 = _nEstj[i]*TMath::Log(Reco[i]) - LogFactorial(_nEstj[i]) - Reco[i];
    //cout << i << ",i , lhi2: "<< lhi2 << endl;
    lh2 = lh2 + lhi2;
}

if ((_Tau != 0.0) && (_RegMethod = "e")){
  long double Entropy; 
  Entropy = CalculateEntropy(Theta);
  if (NumberOfLikes < 10){
  cout << "Difference: " << 100*(lh2 - (lh2 + _Tau*Entropy))/lh2 << " \%" << endl;
  }
  lh2 = lh2 + _Tau*Entropy;
} else if ((_Tau != 0.0) && (_RegMethod = "c")){
      long double Curvature; 
      Curvature = CalculateEntropy(Theta);
      if (NumberOfLikes < 10){
      cout << "Difference: " << 100*(lh2 - (lh2 + _Tau*Curvature))/lh2 << " \%" << endl;
      }
      lh2 = lh2 + _Tau*Curvature;
    }

NumberOfLikes++;
lh2 = CheckNumberL(lh2/2);
return lh2;
}

bool RooUnfoldBaron::IsFinite(long double a){
  if ((std::isnan(abs(a))) || (TMath::Infinity() == TMath::Abs(a)) || (a != a)){
    return false;
  } else
  {
    return true;
  }
  
}

vector<double> RooUnfoldBaron::RandomTheta(vector<double> lower, vector<double> upper){
  vector<double> Theta(Dim);
  bool ReasonableTheta = false;
  long double TestLogLikelihood;
  while (ReasonableTheta == false)
  {
    for (int i = 0; i < Dim ; i++){
      Theta[i]= ::RandomNumber.Uniform(lower[i], upper[i]);
    }
    TestLogLikelihood = LogLikelihood(Theta);
    if (b_CheckNumberL(TestLogLikelihood)){
      Theta.clear();
      Theta.resize(Dim);
    } else{
      ReasonableTheta = true;
    } 
  }
  return Theta;
}

void RooUnfoldBaron::PrintVec(vector<double> vec){
    for(unsigned int i = 0; i < vec.size(); i++){
        cout << vec[i] << " ";
    }
    cout << endl;
}

void RooUnfoldBaron::PrintVecL(vector<long double> vec){
    for(unsigned int i = 0; i < vec.size(); i++){
        cout << vec[i] << " ";
    }
    cout << endl;
}

//------------------------------------------------------------------
long double RooUnfoldBaron::FindReasonableEpsilon(vector<double> Theta0, vector<long double> GradTheta0, long double logp){

    vector<double> R0(Dim);
    for ( int i = 0; i < Dim; i++){
      R0[i] = ::RandomNumber.Rndm();
    }
    long double Epsilon = 1.0;
    LeapFrogStruct LeapStruct0;
    LeapStruct0 = LeapFrog(Theta0, R0, GradTheta0, Epsilon, Epsilon);
    long double k = 1.0;
    LeapFrogStruct LeapStruct1;
    while (b_CheckNumberL(LeapStruct0.LogPrime) && (b_CheckArrayL(LeapStruct0.GradPrime))){
      k = k*0.5;
      LeapStruct1 = LeapFrog(Theta0, R0, GradTheta0, Epsilon*k, Epsilon);
      LeapStruct0.LogPrime = LeapStruct1.LogPrime;
    }

    Epsilon = 0.5* k * Epsilon;

    long double LogAccProb;
    LeapStruct1.RPrime.resize(Dim);
    LogAccProb = LeapStruct1.LogPrime - logp - 0.5*(MultiplyVec(LeapStruct1.RPrime, LeapStruct1.RPrime) + MultiplyVec(R0, R0));
    double a;
    if (LogAccProb > TMath::Log(0.5)){
      a = 1.0;
    } else{
      a = -1;
    }
    LeapFrogStruct LeapStruct3;
    while (a * LogAccProb > -1.0 * a * TMath::Log(2)){
        Epsilon = Epsilon * TMath::Power(2.0, a);
        LeapStruct3 = LeapFrog(Theta0, R0, GradTheta0, Epsilon, Epsilon);
        LogAccProb = LeapStruct3.LogPrime - logp - 0.5*(MultiplyVec(LeapStruct3.RPrime, LeapStruct3.RPrime) + MultiplyVec(R0, R0));
      }
    return Epsilon;

}
//------------------------------------------------------------------
void RooUnfoldBaron::unfold()
{
  TMatrixD PEjCi(Dim,Dim), PEjCiEff(Dim,Dim);
  TMatrixD Matrix(Dim, Dim);
  TVectorD Data(Dim);
  for (Int_t i = 0 ; i < Dim ; i++) {
    if (_nCi[i] <= 0.0) { _efficiencyCi[i] = 0.0; continue; }
    Double_t eff = 0.0;
    for (Int_t j = 0 ; j < Dim ; j++) {
      Data(j) = _nCi[j];
      Double_t response = _Nji(j,i) / _nCi[i];
      Matrix(i,j) = _Nji(j,i); 
      PEjCi(j,i) = PEjCiEff(j,i) = response; 
      eff += response;
    }
    _efficiencyCi[i] = eff;
  }

  for (Int_t i = 0 ; i < Dim ; i++) {
    if (_nCi[i] <= 0.0) { _acceptancyCi[i] = 0.0; continue; }
    Double_t acc = 0.0;
    for (Int_t j = 0 ; j < Dim ; j++) {
      Double_t response2 = _Nji(i,j) / _nEstj[i];
      acc += response2;
    }
    _acceptancyCi[i] = acc;
  }

  vector<double> lower(Dim), upper(Dim);
  double ColumnProjection, RowProjection;
  for (int i = 0 ; i < Dim  ; i++) {
      ColumnProjection = 0.0;
      RowProjection = 0.0;
      for (int j = 0 ; j < Dim ; j++) {
      ColumnProjection = ColumnProjection + Matrix(i,j);
      RowProjection = RowProjection + Matrix(j,i);
      }
      if (RowProjection != 0.0){
        upper[i] = Data(i)*ColumnProjection*3/RowProjection + 1000;
        lower[i] = Data(i)*ColumnProjection*0.000001/RowProjection;
      } else {
        upper[i] = Data(i)*ColumnProjection*3 + 1000;
        lower[i] = Data(i)*ColumnProjection*0.000001;
      }
      lower[i] = 0.0;
  }
  
  low = lower;
  up = upper;
  cout << "Lower Range Limits for each bin: " << lower.size() << endl;
  PrintVec(lower);
  cout << "Upper Range Limits for each bin: " << upper.size() << endl;
  PrintVec(upper);
  vector<double> Theta0(Dim);
  Theta0 = RandomTheta(lower, upper);
  long double logp = LogLikelihood(Theta0);
  cout << "Found non zero LogLikelihood: " << logp << endl;
  PrintVec(Theta0);
  vector<long double> GradTheta0(Dim);
  
  //****************************************************************************
  int OK = 0;
  int NOOK = 0;
  int Mn = _NSteps;
  int Madapt = (int)Mn/2.0;
  Madapt = 200;
  double Delta = 0.25;
  long double Epsilon;
  Epsilon = FindReasonableEpsilon(Theta0, GradTheta0, logp);
  //cout << "Epsilon found: " << Epsilon << endl;
  GradTheta0 = Gradient(Theta0, Epsilon);
  //cout << "Gradient: " << GradTheta0.size() << endl;
  //PrintVecL(GradTheta0);
  
  long double Gamma = 0.05;
  long double T0 = 10.0;
  long double Kappa = 0.75;
  long double Mu = TMath::Log( 10.0 * Epsilon);
  long double EpsilonBar = 1.0;
  long double Eta;
  long double HBar = 0.0;
  bool StopNow = false;
  double Joint;
  double Logu;
  vector<double> R0(Dim);
  vector<vector<double>> Samples(Mn + Madapt);
  vector< long double> LnProb(Mn + Madapt);
  vector< long double> Epsilons(Mn + Madapt);
  LnProb[0] = logp;
  Samples[0] = Theta0;
  vector<double> ThetaPlus(Dim);
  vector<double> ThetaMinus(Dim);
  vector<double> RPlus(Dim);
  vector<double> RMinus(Dim);
  vector<double> RPrimeHelp(Dim);
  vector<long double> GradPlus(Dim);
  vector<long double> GradMinus(Dim);
  double J, N, S, V;
  double RandN;
  BuildTreeStruct BuildStruct1, BuildStruct2;
  vector<double> ThetaPrime;
  vector<long double> GradPrime;
  long double LogPPrime;
  double NPrime;
  double SPrime;
  double Alpha;
  double NAlpha;
  double _tmp;
  bool MCycleAgain;
  int Counter;
  
  vector<vector<TH2D * >> ListOfCorrelations(Dim);
  for (int i = 0 ; i < Dim; i++){
    TH1D * post = new TH1D(TString::Format("Posterior in bin %i", i+1), TString::Format("Posterior in bin %i", i+1), 1000, lower[i], upper[i] );
    ListOfPosteriors[i] = post;
    for (int j = 0 ; j < Dim; j++){
        ListOfCorrelations[i].resize(Dim);
        TH2D * cor = new TH2D(TString::Format("Correlation between bins %i and %i", i+1, j+1),TString::Format("Correlation between bins %i and %i", i+1, j+1), 200, lower[i], upper[i], 200, lower[j], upper[j]);
        ListOfCorrelations[i][j] = cor;
      }
  }

  Epsilons[0] = Epsilon;
  LnProb[0] = logp;

  for (int m = 1; m < Mn + Madapt; m++){
    if (m==1){
      cout << "Processing: " << endl;
    }
    if (m % 1000 == 0){
      cout << "#" << m << endl;
    }
    MCycleAgain = false;
    Joint = LnProb[m-1] - 0.5 * MultiplyVec(R0,R0);
    Logu = Joint - ::RandomNumber.Exp(1.0);
    ThetaPlus = Samples[m - 1];
    ThetaMinus = Samples[m - 1];
    Samples[m] = Samples[m - 1];
    LnProb[m] = LnProb[m - 1];
    Epsilon = Epsilons[m-1];
    GradMinus = Gradient(ThetaMinus, Epsilon);
    GradPlus = Gradient(ThetaPlus, Epsilon);
    //if ((m==1) || (b_CheckArray(RPrimeHelp))){
    for (int i = 0; i < Dim; i++){
      R0[i] = ::RandomNumber.Uniform(0.0, 1.0);
    }
    //} else{
    //  R0 = RPrimeHelp;
    //}

    //PrintVec(R0);
    
    RMinus = R0;
    RPlus = R0;
    J = 0.0;
    N = 1.0;
    S = 1.0;
    while (S == 1){
        RandN = ::RandomNumber.Rndm();
        if (RandN < 0.5){
          V = 1.0;
        } else{
          V = -1.0;
        }

        if (V == -1.0){
            BuildStruct1 = BuildTree(ThetaMinus, RMinus, GradMinus, Logu, V, J, Epsilon, Joint);
            if (BuildStruct1.NaN == 1){
              cout << "NAN1" << endl; 
              PrintVec(ThetaMinus);
              PrintVec(RMinus);
              PrintVecL(GradMinus);
              cout << "Eps: " << Epsilon << endl;
              cout << "Logu: " << Logu << endl;
              cout << "Joint: " << Joint << endl;
              cout << "V: " << V << endl;
              cout << "J: " << J << endl;

              MCycleAgain = true;
              break;
            }
            ThetaMinus = BuildStruct1.ThetaMinus;
            RMinus = BuildStruct1.RMinus;
            GradMinus = BuildStruct1.GradMinus;
            ThetaPrime = BuildStruct1.ThetaPrime;
            GradPrime = BuildStruct1.GradPrime;
            LogPPrime = BuildStruct1.LogPPrime;
            NPrime = BuildStruct1.NPrime;
            SPrime = BuildStruct1.SPrime;
            Alpha = BuildStruct1.AlphaPrime;
            NAlpha = BuildStruct1.NAlphaPrime;
            RPrimeHelp = BuildStruct1.RMinus;
        } else{
            BuildStruct2 = BuildTree(ThetaPlus, RPlus, GradPlus, Logu, V, J, Epsilon, Joint);
            if (BuildStruct2.NaN == 1){
              cout << "NAN2" << endl;
              PrintVec(ThetaMinus);
              PrintVec(RMinus);
              PrintVecL(GradMinus);
              cout << "Eps: " << Epsilon << endl;
              cout << "Logu: " << Logu << endl;
              cout << "Joint: " << Joint << endl;
              cout << "V: " << V << endl;
              cout << "J: " << J << endl;
              MCycleAgain = true;
              break;
            }
            ThetaPlus = BuildStruct2.ThetaPlus;
            RPlus = BuildStruct2.RPlus;
            GradPlus = BuildStruct2.GradPlus;
            ThetaPrime = BuildStruct2.ThetaPrime;
            GradPrime = BuildStruct2.GradPrime;
            LogPPrime = BuildStruct2.LogPPrime;
            NPrime = BuildStruct2.NPrime;
            SPrime = BuildStruct2.SPrime;
            Alpha = BuildStruct2.AlphaPrime;
            NAlpha = BuildStruct2.NAlphaPrime;
            RPrimeHelp = BuildStruct1.RMinus;
        }

        if ( 1.0 >= NPrime/N ){
          _tmp = NPrime / N;
        } else{
          _tmp = 1.0;
        }
        if (SPrime == 1.0){
          Samples[m] = ThetaPrime;
          LnProb[m] = LogPPrime;
          OK = OK + 1;
          if ((m > Madapt) && (TMath::Exp(LogPPrime) > 0.0)){
            Counter = 0;
            for (int i = 0; i < Dim; i++){
              ListOfPosteriors[i]->Fill(ThetaPrime[i], _tmp);
              for (int j = 0; j < Dim; j++){
              ListOfCorrelations[i][j]->Fill(ThetaPrime[i], ThetaPrime[j], TMath::Exp(LogPPrime));
              }
              ////if (ListOfPosteriors[i]->Integral() >= 100000){
              ////    Counter = Counter + 1;
              ////    if (Counter == Dim){
              ////        StopNow = true;
              ////    }
              ////}
            }
          }
        } else{
          NOOK = NOOK + 1;
        }


        N = N + NPrime;
        S = SPrime * (int)StopCriterion(ThetaMinus, ThetaPlus, RMinus, RPlus);
        J = J + 1;
        if (StopNow == true){
          break;
        }
    } //END OF S CYCLE
        if (MCycleAgain == true){
          Epsilons[m] = Epsilons[m-1];
          Samples[m] = Samples[m-1];
          LnProb[m] = LnProb[m-1];
          continue;
        }
            if (m <= Madapt){
            Eta = 1.0 / ((long double)(m) + T0);
            HBar = (1.0 - Eta) * HBar + Eta * (Delta - Alpha / NAlpha);
            Epsilon = TMath::Exp(Mu - TMath::Sqrt(m) * HBar / Gamma);
            Eta = TMath::Power(m,-1.0*Kappa);
            EpsilonBar = TMath::Exp((1.0 - Eta) * TMath::Log(EpsilonBar) + Eta * TMath::Log(Epsilon));
            } 
        else{
            Epsilon = EpsilonBar;
        }
        Epsilons[m] = Epsilon;

        //if (m == Madapt + 1){
        //  //cout << "------------------" << endl;  
        //  cout << "Final step in LeapFrog function:" << Epsilon << endl;  
        //  //cout << "------------------" << endl;  
        //  //cout << "------------------" << endl;  
        //} //else if (m < Madapt){
          //cout << m << " New Epsilon:" << Epsilon << endl;
        //}
        
        if (StopNow == true){
          break;
        }
  }
  cout << endl;
  cout << "Step in LeapFrog function:" << Epsilon << endl;  

  vector<TH1D * > ListOfFinePosteriors(Dim);
  for (int i = 0 ; i < Dim; i++){
    Paths[i]->SetNpx(500);
    //Paths[i]->Draw();
    //TH1D * CheckPointer;
    //CheckPointer = (TH1D*)Paths[i]->Project("x");
    //if (CheckPointer == NULL){
    //    cout << "WARNING: X-Projection of paths " << i << " is zero pointer" << endl;     
    //} else{
    //    ListOfPosteriors[i] = (TH1D*)Paths[i]->Project("x");
    //}
    ListOfPosteriors[i] = (TH1D*)Paths[i]->Project("x");
    ListOfPosteriors[i]->SetName(TString::Format("Posteroir in bin %i", i+1));
    ListOfPosteriors[i]->SetTitle(TString::Format("Posteroir in bin %i", i+1));
    if (ListOfPosteriors[i]->Integral() != 0){
      ListOfPosteriors[i]->Scale(1.0/ListOfPosteriors[i]->Integral());
      ListOfPosteriors[i]->Scale(1.0/ListOfPosteriors[i]->GetMaximum());
    } else{
      cout << "Not normalizing, might cause the problem" << endl;
    }
    }

  //TCanvas * can = new TCanvas("Posteriors", "Posteriors", 0, 0, 1600, 1600);
  //can->Divide((int)(TMath::Power(Dim, 0.5) + 2) , (int)(TMath::Power(Dim, 0.5))+ 1 );
  //TCanvas * can2 = new TCanvas("#sigma_{Reco} / BinWidth", "#sigma_{Reco} / BinWidth", 0, 0, 1600, 1600);
  //TCanvas * can3 = new TCanvas("Slices", "Slices", 0, 0, 1600, 1600);
  //can3->Divide((int)(TMath::Power(Dim, 0.5) + 1) , (int)(TMath::Power(Dim, 0.5))+ 1 );
  float Correlation;

  //TCanvas * can2 = new TCanvas("All Paths Weighted by Likelihood", "All Paths Weighted by Likelihood", 0, 0, 1600, 1600);
  //can2->Divide((int)(TMath::Power(Dim, 0.5) + 1) , (int)(TMath::Power(Dim, 0.5))+ 1 );

  vector<double> FinalTheta(Dim);
  vector<double> FinalReco(Dim);

  
  
  Double_t x, q;
  q = 0.5; // 0.5 for "median"

  for (int i = 0 ; i < Dim ; i++){
    for (int j = 0 ; j < Dim ; j++){
      //can2->cd((int)((j + 1) + Dim*(Dim - (i + 1)))); 
      ListOfCorrelations[i][j]->GetXaxis()->SetTitle(TString::Format("Events in bin %i", i+1));
      ListOfCorrelations[i][j]->GetYaxis()->SetTitle(TString::Format("Events in bin %i", j+1));
      ListOfCorrelations[i][j]->GetXaxis()->SetRange(ListOfPosteriors[i]->FindFirstBinAbove( ),ListOfPosteriors[i]->FindLastBinAbove( ));
      ListOfCorrelations[i][j]->GetYaxis()->SetRange(ListOfPosteriors[j]->FindFirstBinAbove( ),ListOfPosteriors[j]->FindLastBinAbove( ));
      Correlation = ListOfCorrelations[i][j]->GetCorrelationFactor();
      TH2CorrelationMatrix->SetBinContent(i+1,j+1, Correlation);
      TLegend * leg = new TLegend(0.1,0.8,0.45,0.9);
      leg->SetHeader(TString::Format("Corr. factor = %.4f", Correlation));
      gStyle->SetLegendTextSize(0.05);
      }

    _BinCorrelationMatrix = ListOfCorrelations; // here before changing range of ListOfPosteriors

    //cout << i << " Mean is :" << ListOfPosteriors[i]->GetMean() << endl;
    //can->cd(i+1);
    ListOfPosteriors[i]->GetXaxis()->SetRange(ListOfPosteriors[i]->FindFirstBinAbove( ListOfPosteriors[i]->GetMaximum()*0.01 ),ListOfPosteriors[i]->FindLastBinAbove( ListOfPosteriors[i]->GetMaximum()*0.01 ));
    //ListOfPosteriors[i]->Draw("hist e1");
    ListOfPosteriors[i]->Fit("gaus", "q0WL", "", ListOfPosteriors[i]->GetXaxis()->GetBinLowEdge(ListOfPosteriors[i]->FindFirstBinAbove(ListOfPosteriors[i]->GetMaximum()*0.01)),ListOfPosteriors[i]->GetXaxis()->GetBinUpEdge(ListOfPosteriors[i]->FindLastBinAbove(ListOfPosteriors[i]->GetMaximum()*0.01)));
    TF1 * fit = ListOfPosteriors[i]->GetFunction("gaus");
          // CALCULATING MEDIAN
    ////x=0;
    ////q=0.5;
    ////ListOfPosteriors[i]->ComputeIntegral(); // just a precaution
    ////ListOfPosteriors[i]->GetQuantiles(1, &x, &q);          
    
    ////if (fit == NULL){
    _nbarCi[i] = ListOfPosteriors[i]->GetMean();
    ////} else{
      ////_nbarCi[i] = ListOfPosteriors[i]->GetMean();
    ////  _nbarCi[i] = x;
      //_nbarCi[i] = fit->GetParameter(1);
      //fit->Draw("same");
    ////}
    FinalTheta[i] = _nbarCi[i];
  }

  FinalReco = CalculateReco(FinalTheta);
  TH1D * TH1Reco = TH2NormalizedMatrix->ProjectionX();
  TH1Reco->Reset();
  
  for (int i = 0 ; i < Dim ; i++){
      TH1Reco->SetBinContent(i+1,FinalReco[i]);
      ////TF1 * fit = ListOfPosteriors[i]->GetFunction("gaus");
    ////if (fit == NULL){
      ////cout << "Sigma in bin " << i+1 << " is taken from RMS: " << ListOfPosteriors[i]->GetRMS() << endl;
      TH1Reco->SetBinError(i+1, ListOfPosteriors[i]->GetRMS());
    ////} else{
      ////cout << "Sigma in bin " << i+1 << " is taken from fit: " << fit->GetParameter(2) << endl;
      //_nbarCi[i] = ListOfPosteriors[i]->GetMean();
      ////TH1Reco->SetBinError(i+1, ListOfPosteriors[i]->GetRMS());
      //TH1Reco->SetBinError(i+1, fit->GetParameter(2));
      //fit->Draw("same");
    ////}
      
  }

  _Reco = TH1Reco;

  cout << "INFO: Algorithm effectivity: " << 100 * OK /(OK + NOOK) << " \% " << endl;

  vector <TH1D *> ListOfSlices(Dim);
  TH1D * SigmaSlices = TH2NormalizedMatrix->ProjectionY();
  SigmaSlices->Reset();
  SigmaSlices->SetName("Relative resololution #sigma_{Reco} / Bin Center");
  SigmaSlices->SetTitle("Relative resololution #sigma_{Reco} / Bin Center");
  for (int i = 0; i < Dim; i++){
    ListOfSlices[i] = TH2NormalizedMatrix->ProjectionY();
    ListOfSlices[i]->SetName(TString::Format("Slice in bin %i", i+1));
    ListOfSlices[i]->SetTitle(TString::Format("Slice in bin %i", i+1));
    ListOfSlices[i]->Reset();
    for (int j = 0; j < Dim; j++){
        ListOfSlices[i]->SetBinContent(j+1,TH2NormalizedMatrix->GetBinContent(j+1,i+1));      
    }
    //can3->cd(i+1);
    ListOfSlices[i]->Fit("gaus", "Q0");
    TF1 * fit2 = ListOfSlices[i]->GetFunction("gaus");
    //cout << fit2->GetParameter(2) << endl;
    //ListOfSlices[i]->Draw("hist");

    SigmaSlices->SetBinContent(i+1,fit2->GetParameter(2)/TMath::Abs(SigmaSlices->GetBinCenter(i+1)));

    //fit2->Draw("same");
  }

  //can2->cd();
  SigmaSlices->SetLineColor(2);
  SigmaSlices->SetLineStyle(1);
  //SigmaSlices->Draw("hist");


  // saving outputs
  _ListOfPosteriors = ListOfPosteriors;
  _PathsOfLeapFrog = ChosenPaths;
  _PathsOfLeapFrogWeightedByLikelihood = Paths;
  _NormalizedUnfoldingMatrix = TH2NormalizedMatrix;
  _BinCorrelationMatrix = ListOfCorrelations;
  _BinCorrelationFactorMatrix = TH2CorrelationMatrix;
  _RelativeRecoResolution = SigmaSlices;
  
}

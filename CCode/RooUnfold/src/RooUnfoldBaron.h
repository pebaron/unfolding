//=====================================================================-*-C++-*-
// File and Version Information:
//      $Id: RooUnfoldBaron.h 316 2011-10-17 19:48:27Z T.J.Adye $
//
// Description:
//      Bayesian unfolding. Just an interface to RooUnfoldBaronImpl.
//
// Author: Tim Adye <T.J.Adye@rl.ac.uk>
//
//==============================================================================

#ifndef ROOUNFOLDBARON_HH
#define ROOUNFOLDBARON_HH

#include "RooUnfold.h"
#include <vector>
#include "TVectorD.h"
#include "TMatrixD.h"
#include "TH2D.h"
#include "TGraph2D.h"
#include "TCanvas.h"


using std::vector;

class TH1;
class TH2;
class RooUnfoldResponse;

class RooUnfoldBaron : public RooUnfold {

public:

  // Standard methods

  RooUnfoldBaron(); // default constructor
  RooUnfoldBaron (const char*    name, const char*    title); // named constructor
  RooUnfoldBaron (const TString& name, const TString& title); // named constructor
  RooUnfoldBaron& operator= (const RooUnfoldBaron& rhs); // assignment operator

  // Special constructors

  RooUnfoldBaron (const RooUnfoldResponse* res, const TH1* meas, Int_t niter= 4, float Tau = 0.0, TString RegMethod = "", int NSteps = 100000, int Seed = 10, Bool_t smoothit= false,
                  const char* name= 0, const char* title= 0);

  void SetIterations (Int_t niter= 4);
  void SetSmoothing  (Bool_t smoothit= false);
  Int_t GetIterations() const;
  Int_t GetSmoothing()  const;
  const TMatrixD& UnfoldingMatrix() const;

  virtual void  SetRegParm (Double_t parm);
  virtual Double_t GetRegParm() const;

  static TMatrixD& H2M (const TH2* h, TMatrixD& m, Bool_t overflow);

protected:
  virtual void Unfold();
  void setup();
  void unfold();

  vector<double> RandomTheta(vector<double> lower, vector<double> upper);
  long double LogLikelihood(vector<double> Theta );
  void PrintVec(vector<double> vec);
  void PrintVecL(vector<long double> vec);
  bool IsFinite(long double a);
  double GetNormFactor(TMatrixD NMatrix);
  TMatrixD NormalizeMatrix(TMatrixD NMatrix);
  vector<double> CalculateReco(vector<double> Theta);
  long double CalculateEntropy(vector<double> Theta);
  long double CalculateCurvature(vector<double> Theta);
  vector<long double> Gradient(vector<double> Theta, long double step = 1e-2);
  long double CheckNumberL(long double a);
  vector<long double> CheckArrayL(vector<long double> a);
  double CheckNumber(double a);
  vector<double> CheckArray(vector<double> a);

  bool b_CheckNumberL(long double a);
  bool b_CheckArrayL(vector<long double> a);
  bool b_CheckNumber(double a);
  bool b_CheckArray(vector<double> a);

  struct LeapFrogStruct
  {
    vector<double> ThetaPrime;
    vector<double> RPrime;
    vector<long double> GradPrime;
    long double LogPrime;
    int NaN = 0;
  };

  struct BuildTreeStruct
  {
    vector<double> ThetaMinus;
    vector<double> RMinus;
    vector<long double> GradMinus;
    vector<double> ThetaPlus;
    vector<double> RPlus;
    vector<long double> GradPlus;
    vector<double> ThetaPrime;
    vector<long double> GradPrime;
    long double LogPPrime;
    double NPrime;
    double SPrime;
    double AlphaPrime;
    double NAlphaPrime;
    int NaN = 0;
  };

  struct InputStruct
  {
    vector<double> Theta;
    vector<double> R;
    vector<long double> Grad;
    long double LogU;
    double V;
    double J;
    double Epsilon;
    long double Joint0;
  };
  

  LeapFrogStruct LeapFrog(vector<double> Theta, vector<double> R, vector<long double> Grad, double Epsilon, double GradEpsilon);
  BuildTreeStruct BuildTree(vector<double> Theta, vector<double> R, vector<long double> Grad, long double LogU, double V, double J, double Epsilon, long double Joint0);
  bool CheckInput(InputStruct a);
  double SquereVec(vector<double> a);
  double MultiplyVec(vector<double> a, vector<double> b);
  bool StopCriterion(vector<double> ThetaMinus, vector<double> ThetaPLus, vector<double> RMinus, vector<double> RPlus);
  long double FindReasonableEpsilon(vector<double> Theta0, vector<long double> GradTheta0, long double logp);
  long double LogFactorial(double a);

private:
  void Init();

protected:
  // instance variables
  Int_t _niter;
  float _Tau;
  TString _RegMethod;
  int _NSteps;
  int _Seed;
  Int_t _smoothit;

  Int_t _nc;              // number of causes  (same as _nt)
  Int_t _ne;              // number of effects (same as _nm)
  Int_t Dim;
  vector<long double> grad;
  double NormMatrix;
  TMatrixD NormalizedMatrix;
  TH2D * TH2NormalizedMatrix;
  TH2D * TH2CorrelationMatrix;
  vector<double> low;
  vector<double> up;

  RooUnfoldBaron::BuildTreeStruct ReturnBuildStruct;
  long double joint;
  bool ReturnNans;
  long double LogPPrime;
  double NPrime;
  double SPrime;
  double AlphaPrime;
  double NAlphaPrime;
  vector<double> ThetaMinus;
  vector<double> RMinus;
  vector<long double> GradMinus;
  vector<double> ThetaPlus;
  vector<double> RPlus;
  vector<long double> GradPlus;
  vector<double> ThetaPrime;
  vector<long double> GradPrime;
  long double LogPPrime2;
  double NPrime2;
  double SPrime2;
  double AlphaPrime2;
  double NAlphaPrime2;
  vector<double> ThetaPrime2;
  vector<long double> GradPrime2;
  double RandN;
  double CompareNumber;
  LeapFrogStruct LeapStruct0;
  RooUnfoldBaron::BuildTreeStruct BuildStruct;
  RooUnfoldBaron::BuildTreeStruct BuildStruct2;

  vector<double> RPrimeLeap;
  vector<double> ThetaPrimeLeap;
  vector<long double> GradPrimeLeap;
  long double llh;
  LeapFrogStruct LeapStruct;

  vector<double> Rmin;
  vector<double> Rmax;

  int NumberOfLikes;

  Double_t _N0C;          // number of events in prior
  Double_t _nbartrue;     // best estimate of number of true events

  TVectorD _nEstj;        // Number of measured events from Effect E_j
  TVectorD _nCi;          // Number of true events from cause C_i
  TVectorD _nbarCi;       // Estimated number of true events from cause C_i
  TVectorD _efficiencyCi; // efficiency for detecting cause C_i
  vector<double> _acceptancyCi; //acceptancy
  vector<TH1D * > ListOfPosteriors;
  vector<TGraph2D *> Paths;
  vector<vector<TGraph2D *>> AllPaths;
  vector<vector<TGraph2D *>> AllPathsExp;
  int OverFlowNumber = 0;
  int OverFlowNumberExp = 0;
  int points = 0;
  vector<TGraph2D *> ChosenPaths;
  int Chosenpoints = 0;

  TVectorD _P0C;          // prior before last iteration
  TVectorD _UjInv;        // 1 / (folded prior) from last iteration

  TMatrixD _Nji;          // mapping of causes to effects
  TMatrixD _Mij;          // unfolding matrix
  TMatrixD _Vij;          // covariance matrix
  TMatrixD _VnEstij;      // covariance matrix of effects
  TMatrixD _dnCidnEj;     // measurement error propagation matrix
  TMatrixD _dnCidPjk;     // response error propagation matrix (stack j,k into each column)

public:
  ClassDef (RooUnfoldBaron, 1) // Bayesian Unfolding
};

// Inline method definitions

inline
RooUnfoldBaron::RooUnfoldBaron()
  : RooUnfold()
{
  // Default constructor. Use Setup() to prepare for unfolding.
  Init();
}

inline
RooUnfoldBaron::RooUnfoldBaron (const char* name, const char* title)
  : RooUnfold(name,title)
{
  // Basic named constructor. Use Setup() to prepare for unfolding.
  Init();
}

inline
RooUnfoldBaron::RooUnfoldBaron (const TString& name, const TString& title)
  : RooUnfold(name,title)
{
  // Basic named constructor. Use Setup() to prepare for unfolding.
  Init();
}

inline
RooUnfoldBaron& RooUnfoldBaron::operator= (const RooUnfoldBaron& rhs)
{
  // Assignment operator for copying RooUnfoldBaron settings.
  Assign(rhs);
  return *this;
}


inline
void RooUnfoldBaron::SetIterations (Int_t niter)
{
  // Set regularisation parameter (number of iterations)
  _niter= niter;
}

inline
void RooUnfoldBaron::SetSmoothing (Bool_t smoothit)
{
  // Enable smoothing
  _smoothit= smoothit;
}

inline
Int_t RooUnfoldBaron::GetIterations() const
{
  // Return regularisation parameter (number of iterations)
  return _niter;
}

inline
Int_t RooUnfoldBaron::GetSmoothing()  const
{
  // Return smoothing setting
  return _smoothit;
}

inline
const TMatrixD& RooUnfoldBaron::UnfoldingMatrix() const
{
  // Access unfolding matrix (Mij)
  return _Mij;
}

inline
void  RooUnfoldBaron::SetRegParm (Double_t parm)
{
  // Set regularisation parameter (number of iterations)
  SetIterations(Int_t(parm+0.5));
}

inline
Double_t RooUnfoldBaron::GetRegParm() const
{
  // Return regularisation parameter (number of iterations)
  return GetIterations();
}

#endif

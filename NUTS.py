#!/usr/bin/env python3

import os
os.environ["MKL_THREADING_LAYER"] = "GNU"
import argparse
import fbu
from fbu import Regularization
import ROOT
import math
import random
import numpy as np
from numpy import log, exp, sqrt
from array import array
import scipy.stats as stats
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed, TMath, TF1, TLine, kBird, TGaxis, TGraph2D
import pandas as pn
import time
from progress.bar import Bar


parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")
parser.add_argument('--rfile_data','-rfile_data', type=str, default="input.root")
parser.add_argument('--rfile_particle','-rfile_particle', type=str, default="input.root")
parser.add_argument('--rfile_matrix','-rfile_matrix', type=str, default="input.root")
parser.add_argument('--rfile_background','-rfile_background', type=str, default="input.root")
parser.add_argument('--h_data','-h_data', type=str, default="h_data")
parser.add_argument('--h_particle','-h_particle', type=str, default="h_particle")
parser.add_argument('--h_matrix','-h_matrix', type=str, default="h_matrix")
parser.add_argument('--h_background','-h_background', type=str, default="h_background")
parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{t,had} [GeV]")
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)
parser.add_argument('--maxiterations', '-maxiterations', type=int, default=30)
parser.add_argument('--batch', '-batch', type=int, default=0)
parser.add_argument('--sampling', '-sampling', type=int, default=1000)
parser.add_argument('--plotlike', '-plotlike', type=int, default=0)
parser.add_argument('--nrebinpost', '-nrebinpost', type=int, default=1)
parser.add_argument('--counts', '-counts', type=int, default=30)
parser.add_argument('--eps','-e', type=float, default=1.0)
parser.add_argument('--L','-L', type=int, default=100)
parser.add_argument('--drawopt','-drawopt', type=str, default="surf1")
parser.add_argument('--output','-output', type=str, default="output")
parser.add_argument('--tau','-tau', type=float, default=0.01)
parser.add_argument('--norm','-norm', type=int, default=1)
parser.add_argument('--svd','-svd', type=int, default=0)
parser.add_argument('--fit','-fit', type=int, default=1)
parser.add_argument('--outname', '-outname', type=str, default="out")
parser.add_argument('--range', '-range', type=float, default=1.0)
parser.add_argument('--other', '-other', type=int, default=0) # other methods
parser.add_argument('--BinCoef', '-BinCoef', type=float, default=1.0) # other methods
parser.add_argument('--steps', '-steps', type=int, default=20000) # other methods

args = parser.parse_args()

if (args.batch == 1):
    ROOT.gROOT.SetBatch(1)

def PrintCan(can, outputname):
    outputname = outputname.split('/')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.png')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.pdf')

def PlotPosteriors(ListOfPosteriors, outputname = "", fit = 1):
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/2),int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.0))
    index = 1
    lines = []
    h_compare_ptcl = h_ptcl_or.Clone("ptcl_mult_eff")
    h_compare_ptcl.Multiply(h_eff)
    unfolded = h_ptcl_or.Clone("unfolded")
    unfolded.Reset()
    unfolded2 = h_ptcl_or.Clone("unfolded2")
    unfolded2.Reset()
    for i in range(len(ListOfPosteriors)):
        c.cd(i+1)
        if args.fit == 1:
            ListOfPosteriors[i].Fit("gaus", "q0WL", "", ListOfPosteriors[i].GetXaxis().GetBinLowEdge(ListOfPosteriors[i].FindFirstBinAbove(0.1)),ListOfPosteriors[i].GetXaxis().GetBinUpEdge(ListOfPosteriors[i].FindLastBinAbove(0.1)))
            f1 = ListOfPosteriors[i].GetFunction("gaus")
            if f1 is None:
                unfolded.SetBinContent(i+1, ListOfPosteriors[i].GetMean())
                unfolded.SetBinError(i+1, ListOfPosteriors[i].GetRMS())
            else:
                mu = f1.GetParameter(1)
                sigma = f1.GetParameter(2)
                unfolded.SetBinContent(i+1,mu)
                unfolded.SetBinError(i+1,sigma)
                
        else:        
            unfolded.SetBinContent(i+1,ListOfPosteriors[i].GetMean())
            unfolded.SetBinError(i+1,ListOfPosteriors[i].GetRMS())
        index = index + 1
    return unfolded, ListOfPosteriors

def CreateRandomGaus(Dim):
    r = []
    for i in range(Dim):
        r.append(np.random.normal())
    return np.array(r)

def FindReasonableEpsilon(Theta,eps = 1):
    Cycle = True
    while Cycle:    
        r = CreateRandomGaus(len(Theta))
        ThetaC, rC = Leapfrog(Theta, r, eps)
        Nominator = CalculateLikelihood(ThetaC)-0.5*rC.dot(rC)
        Denominator = CalculateLikelihood(Theta)-0.5*r.dot(r)
        Probability = np.exp(Nominator-Denominator) 
        if CheckNumber(Probability) != np.nan:
            break
    if (Probability > 0.5):
        a = 1.0
    else:
        a = -1.0
    while (np.power(Probability,a) > np.power(2.0,-a)):
        eps = np.power(2.0,a)*eps
        ThetaC, rC = Leapfrog(Theta, r, eps)
        Nominator = CalculateLikelihood(ThetaC)-0.5*rC.dot(rC)
        Denominator = CalculateLikelihood(Theta)-0.5*r.dot(r)
        Probability = np.exp(Nominator-Denominator) 
        if CheckNumber(Probability) == np.nan:
            return np.nan
    return eps

def IsInputNan2(Theta, r, u, v, j, eps,Theta0, r0):
    if check(Theta) or check(r) or check(u) or check(v) or check(j) or check(eps) or check(Theta0) or check(r0):
        return True
    else:
        return False

def check(a):
    return np.isnan(np.min(a))
    
def BuildTree(Theta, r, u, v, j, eps, Theta0, r0, layer):
    counts = layer
    counts = counts + 1
    if IsInputNan2(Theta, r, u, v, j, eps, Theta0, r0):
        ReturnArray = np.empty(Dim,)
        ReturnArray[:] = np.nan
        return ReturnArray, ReturnArray , ReturnArray, ReturnArray, ReturnArray ,np.nan, np.nan, np.nan, np.nan, counts
    if (j == 0):
        ThetaC, rC = Leapfrog(Theta, r, v*eps)
        check = np.exp(CalculateLikelihood(ThetaC, writeme=1))-0.5*rC.dot(rC)
        check0 = np.exp(CalculateLikelihood(Theta0, writeme=1))-0.5*r0.dot(r0)
        if np.isnan(np.min(check)) or np.isnan(np.min(check0)):
            ReturnArray = np.empty(Dim,)
            ReturnArray[:] = np.nan
            return ReturnArray, ReturnArray , ReturnArray, ReturnArray, ReturnArray ,np.nan, np.nan, np.nan, np.nan, counts
        if (np.exp(check) >= u):
            nC = 1 
        else:
            nC = 0
        if (np.exp(check) > u-np.exp(250)): 
            sC = 1
        else:
            sC = 0
        checkC = CalculateLikelihood(ThetaC, writeme=1)-0.5*rC.dot(rC)
        check0C = CalculateLikelihood(Theta0, writeme=1)-0.5*r0.dot(r0)
        aC = min([1.0, np.exp(checkC - check0C)])
        return ThetaC, rC, ThetaC, rC, ThetaC, nC, sC, aC, 1.0, counts
    else:
        ThetaM, rM, ThetaP, rP, ThetaC, nC, sC, aC, naC, counts = BuildTree(Theta, r, u, v, j-1, eps, Theta0, r0, counts)
        if (sC == 1):
            if (v == -1):
                ThetaM, rM, lin, lin2, ThetaCC, nCC, sCC, aCC, naCC, counts = BuildTree(ThetaM, rM, u, v, j-1, eps, Theta0, r0, counts)
            else:
                lin, lin2, ThetaP, rP, ThetaCC, nCC, sCC, aCC, naCC, counts = BuildTree(ThetaP, rP, u, v, j-1, eps, Theta0, r0, counts)
            if ((nCC == 0) and (nC == 0)):
                Prob = 0
            else:
                Prob = nCC/(nCC+nC)
            if (Prob > np.random.random_sample()):
                ThetaC = ThetaCC
            DeltaTheta = ThetaP - ThetaM
            if (DeltaTheta.dot(rM) >= 0.0) and (DeltaTheta.dot(rP) >= 0.0):
                sC = sCC
            else:
                sC = 0.0
            nC = nC + nCC
            aC = aC + aCC
            naC = naC + naCC
    ThetaM = np.array(ThetaM)
    rM = np.array(rM)
    ThetaP = np.array(ThetaP)
    rP = np.array(rP)
    ThetaC = np.array(ThetaC)
    return CheckArray(ThetaM), CheckArray(rM), CheckArray(ThetaP), CheckArray(rP), CheckArray(ThetaC), CheckNumber(nC), CheckNumber(sC), CheckNumber(aC), CheckNumber(naC), counts    

def Leapfrog(Theta, r, eps):
    grad = (eps/2)*Gradient(Theta, eps)
    rW = r + grad
    ThetaW = Theta + eps*rW
    rW = rW + grad
    return CheckArray(ThetaW), CheckArray(rW)

def CheckNumber(Number):
    if ((Number == np.nan) or (Number == -np.inf) or (Number == np.inf)):
        return np.nan
    else:
        return Number

def CheckArray(TestArray):
    if ((np.nan in TestArray) or (-np.inf in TestArray) or (np.inf in TestArray)):
        ReturnArray = np.empty(len(TestArray),)
        ReturnArray[:] = np.nan
        return ReturnArray
    else:
        return TestArray

def RandomTheta():
    while True:
        Theta = []    
        for l in range(len(D)):
            Theta.append((upper[l] - lower[l])*np.random.random_sample() + lower[l])
        test = CalculateLikelihood(Theta)
        test = np.exp(test)
        if (CheckNumber(test) != np.nan):
            break
    return Theta

def Epsilon(Theta, eps_or = 1):
    while True:
        eps = FindReasonableEpsilon(Theta, eps_or)
        if eps != np.nan:
            break
    return eps

def Gradient(T_random, step = 1e-8):
    gradient = []
    #step = 1e-2
    for i in range(len(T_random)):
        T_random_plus = T_random.copy()
        if GTau != 0:
            #step = T_random[i]/100
            T_random_plus[i] = T_random_plus[i] + step
            T_random_minus = T_random.copy()
            T_random_minus[i] = T_random_minus[i] - step
            R1 = np.array(T_random_plus).dot(M)      
            R2 = np.array(T_random_minus).dot(M)
            delta = np.sum(np.log(R2/R1)+0.5*(R1-R2)*(np.square(D)/(R1*R2)-1) + GTau*(CalculateEntropy(T_random_plus) - CalculateEntropy(T_random_minus)))/(2*step)    
        else:
            #step = T_random[i]/100
            T_random_plus[i] = T_random_plus[i] + step
            T_random_minus = T_random.copy()
            T_random_minus[i] = T_random_minus[i] - step
            R1 = np.array(T_random_plus).dot(M)      
            R2 = np.array(T_random_minus).dot(M)
            delta = np.sum(np.log(R2/R1)+0.5*(R1-R2)*(np.square(D)/(R1*R2)-1))/(2*step)
        gradient.append(delta)
    return np.array(CheckArray(gradient))

def curvature(f1,f2):
    return abs(f2)*(1 + f1**2)**-1.5

def CalculateCurvature(T_curv):
    CurvSum = 0.0
    for i in range(1,len(T_curv)-1):
        CurvSum = CurvSum + ((T_curv[i+1] - T_curv[i])-(T_curv[i] - T_curv[i-1]))*((T_curv[i+1] - T_curv[i])-(T_curv[i] - T_curv[i-1]))
    return CurvSum

def CalculateEntropy(T_entro):
    Entropy = 0.0
    for i in range(0,len(T_entro)):
        Entropy = Entropy + np.log(T_entro[i]/np.sum(T_entro))*T_entro[i]/np.sum(T_entro)
    return -1.0*Entropy


def CalculateLikelihood(T_random, writeme = 0, foreps = 0):
    R = np.array(T_random).dot(M) 
    if GTau != 0.0 and foreps == 0:
        Entro = CalculateEntropy(T_random)
        lh = np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R) - GTau*Entro#/np.sum(CURV)
        a = np.sum(np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R))
        b = np.sum(lh)
        d = 100*(1 - a/b)
        #printme = ("Loglike: " + str(a) + ", LogLikeReg: " + str(b) + ", diff: " + str(d) + " %", ", Entropy: ", Entro)
    else:
        lh = np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R)
        d = 0.0
        #printme = ("Loglike: " + str(lh))
    return CheckNumber(lh.sum())


def Hamilton(data, matrix , low , up , particle , eff , acc  , eps = args.eps, steps = args.counts, Again = 0, Tau = 0, BinCoef = 1.0):   
    global Dim
    global D 
    global M 
    global lower 
    global upper
    global h_ptcl_or
    global h_eff 
    global h_acc 
    global GTau
    global Gsteps
    global BuildTreeList

    BuildTreeList = []
    D = data
    Dim = len(data)
    M = matrix
    lower = low
    upper = up
    h_ptcl_or = particle
    h_eff = eff
    h_acc = acc
    GTau = Tau
    Gsteps = steps
    
    global ListOfPosteriors
    ListOfPosteriors = []
    Binning = []
    for k in range(len(D)):
        help_array = np.arange(lower[k],upper[k],(upper[k]-lower[k])/(100*BinCoef)) 
        Binning.append(help_array)
        x = array("d",Binning[k])
        posteriors = TH1D("poster_"+str(k),"poster_"+str(k), len(Binning[k])-1,x)
        ListOfPosteriors.append(posteriors)       

    Theta = RandomTheta()
    test_nuts6(Theta)
    
    
    
    
    
    for l in range(len(ListOfPosteriors)):
        if ListOfPosteriors[l].GetMaximum() != 0:
            ListOfPosteriors[l].Scale(1.0/ListOfPosteriors[l].GetMaximum())
    Unfolded_Baron, ListOfPost = PlotPosteriors(ListOfPosteriors, outputname=args.output)
    return Unfolded_Baron, ListOfPost

__all__ = ['nuts6']


def leapfrog(theta, r, grad, epsilon, f):
    """ Perfom a leapfrog jump in the Hamiltonian space
    INPUTS
    ------
    theta: ndarray[float, ndim=1]
        initial parameter position
    r: ndarray[float, ndim=1]
        initial momentum
    grad: float
        initial gradient value
    epsilon: float
        step size
    f: callable
        it should return the log probability and gradient evaluated at theta
        logp, grad = f(theta)
    OUTPUTS
    -------
    thetaprime: ndarray[float, ndim=1]
        new parameter position
    rprime: ndarray[float, ndim=1]
        new momentum
    gradprime: float
        new gradient
    logpprime: float
        new lnp
    """
    # make half step in r
    #rprime = r + 0.5 * epsilon * grad
    #print("eps, theta: ", epsilon, theta)
    _, grad = correlated_normal(theta, epsilon)
    rprime = r + 0.5 * epsilon * grad
    # make new step in theta
    thetaprime = theta + epsilon * rprime
    #compute new gradient
    logpprime, gradprime = correlated_normal(thetaprime, epsilon)
    #print("gradient prime: ", gradprime)
    # make half step in r again
    rprime = rprime + 0.5 * epsilon * gradprime
    return thetaprime, rprime, gradprime, logpprime


def find_reasonable_epsilon(theta0, grad0, logp0, f):
    """ Heuristic for choosing an initial value of epsilon """
    epsilon = 1.
    r0 = np.random.normal(0., 1., len(theta0))

    # Figure out what direction we should be moving epsilon.
    _, rprime, gradprime, logpprime = leapfrog(theta0, r0, grad0, epsilon, correlated_normal)
    # brutal! This trick make sure the step is not huge leading to infinite
    # values of the likelihood. This could also help to make sure theta stays
    # within the prior domain (if any)
    k = 1.
    while np.isinf(logpprime) or np.isinf(gradprime).any():
        k *= 0.5
        _, rprime, _, logpprime = leapfrog(theta0, r0, grad0, epsilon * k, correlated_normal)

    epsilon = 0.5 * k * epsilon

    # acceptprob = np.exp(logpprime - logp0 - 0.5 * (np.dot(rprime, rprime.T) - np.dot(r0, r0.T)))
    # a = 2. * float((acceptprob > 0.5)) - 1.
    logacceptprob = logpprime-logp0-0.5*(np.dot(rprime, rprime)-np.dot(r0,r0))
    a = 1. if logacceptprob > np.log(0.5) else -1.
    # Keep moving epsilon in that direction until acceptprob crosses 0.5.
    # while ( (acceptprob ** a) > (2. ** (-a))):
    while a * logacceptprob > -a * np.log(2):
        epsilon = epsilon * (2. ** a)
        _, rprime, _, logpprime = leapfrog(theta0, r0, grad0, epsilon, correlated_normal)
        # acceptprob = np.exp(logpprime - logp0 - 0.5 * ( np.dot(rprime, rprime.T) - np.dot(r0, r0.T)))
        logacceptprob = logpprime-logp0-0.5*(np.dot(rprime, rprime)-np.dot(r0,r0))

    print("find_reasonable_epsilon=", epsilon)

    return epsilon


def stop_criterion(thetaminus, thetaplus, rminus, rplus):
    """ Compute the stop condition in the main loop
    dot(dtheta, rminus) >= 0 & dot(dtheta, rplus >= 0)
    INPUTS
    ------
    thetaminus, thetaplus: ndarray[float, ndim=1]
        under and above position
    rminus, rplus: ndarray[float, ndim=1]
        under and above momentum
    OUTPUTS
    -------
    criterion: bool
        return if the condition is valid
    """
    dtheta = thetaplus - thetaminus
    return (np.dot(dtheta, rminus.T) >= 0) & (np.dot(dtheta, rplus.T) >= 0)


def build_tree(theta, r, grad, logu, v, j, epsilon, f, joint0, depth = 0):
    """The main recursion."""
    #print("theta, r, grad, logu, v, j, epsilon, f, joint0",theta, r, grad, logu, v, j, epsilon, f, joint0, depth)
    #time.sleep(1)
    #print("Depth in build tree: ", depth, j)
    depth += 1
    Inputs = []
    Inputs.append(np.array(theta))
    Inputs.append(np.array(r))
    Inputs.append(np.array(grad))
    Inputs.append(np.array(logu))
    Inputs.append(np.array(v))
    Inputs.append(np.array(j))
    Inputs.append(np.array(epsilon))
    #Inputs.append(np.array(nprime))
    for i in range(len(Inputs)):
        if np.isnan(Inputs[i]).any() or np.isinf(Inputs[i]).any():
            return np.empty(len(theta),), np.empty(len(r),), np.empty(len(grad),), np.empty(len(grad),),np.empty(len(theta),), np.empty(len(r),), np.empty(len(grad),), np.empty(len(grad),), np.nan, np.nan, np.nan, np.nan, np.nan, np.nan


    if (j == 0):
        # Base case: Take a single leapfrog step in the direction v.
        thetaprime, rprime, gradprime, logpprime = leapfrog(theta, r, grad, v * epsilon, correlated_normal)
        joint = logpprime - 0.5 * np.dot(rprime, rprime.T)
        # Is the new point in the slice?
        nprime = int(logu < joint)
        # Is the simulation wildly inaccurate?
        sprime = int((logu - 1000.) < joint)
        # Set the return values---minus=plus for all things here, since the
        # "tree" is of depth 0.
        thetaminus = thetaprime[:]
        thetaplus = thetaprime[:]
        rminus = rprime[:]
        rplus = rprime[:]
        gradminus = gradprime[:]
        gradplus = gradprime[:]
        # Compute the acceptance probability.
        alphaprime = min(1., np.exp(joint - joint0))
        #alphaprime = min(1., np.exp(logpprime - 0.5 * np.dot(rprime, rprime.T) - joint0))
        nalphaprime = 1
    else:
        # Recursion: Implicitly build the height j-1 left and right subtrees.
        thetaminus, rminus, gradminus, thetaplus, rplus, gradplus, thetaprime, gradprime, logpprime, nprime, sprime, alphaprime, nalphaprime, depth = build_tree(theta, r, grad, logu, v, j - 1, epsilon, correlated_normal, joint0, depth)
        # No need to keep going if the stopping criteria were met in the first subtree.
        if (sprime == 1):
            if (v == -1):
                thetaminus, rminus, gradminus, _, _, _, thetaprime2, gradprime2, logpprime2, nprime2, sprime2, alphaprime2, nalphaprime2, depth = build_tree(thetaminus, rminus, gradminus, logu, v, j - 1, epsilon, correlated_normal, joint0, depth)
            else:
                _, _, _, thetaplus, rplus, gradplus, thetaprime2, gradprime2, logpprime2, nprime2, sprime2, alphaprime2, nalphaprime2, depth = build_tree(thetaplus, rplus, gradplus, logu, v, j - 1, epsilon, f, joint0, depth)
            # Choose which subtree to propagate a sample up from.
            if (np.random.uniform() < (float(nprime2) / max(float(int(nprime) + int(nprime2)), 1.))):
                thetaprime = thetaprime2[:]
                gradprime = gradprime2[:]
                logpprime = logpprime2
            # Update the number of valid points.
            nprime = int(nprime) + int(nprime2)
            # Update the stopping criterion.
            sprime = int(sprime and sprime2 and stop_criterion(thetaminus, thetaplus, rminus, rplus)) 
            # Update the acceptance probability statistics.
            alphaprime = alphaprime + alphaprime2
            nalphaprime = nalphaprime + nalphaprime2
    return thetaminus, rminus, gradminus, thetaplus, rplus, gradplus, thetaprime, gradprime, logpprime, nprime, sprime, alphaprime, nalphaprime, depth


def nuts6(f, Mn, Madapt, theta0, delta=0.6):
    """
    Implements the No-U-Turn Sampler (NUTS) algorithm 6 from from the NUTS
    paper (Hoffman & Gelman, 2011).
    Runs Madapt steps of burn-in, during which it adapts the step size
    parameter epsilon, then starts generating samples to return.
    Note the initial step size is tricky and not exactly the one from the
    initial paper.  In fact the initial step size could be given by the user in
    order to avoid potential problems
    INPUTS
    ------
    epsilon: float
        step size
        see nuts8 if you want to avoid tuning this parameter
    f: callable
        it should return the log probability and gradient evaluated at theta
        logp, grad = f(theta)
    M: int
        number of samples to generate.
    Madapt: int
        the number of steps of burn-in/how long to run the dual averaging
        algorithm to fit the step size epsilon.
    theta0: ndarray[float, ndim=1]
        initial guess of the parameters.
    KEYWORDS
    --------
    delta: float
        targeted acceptance fraction
    OUTPUTS
    -------
    samples: ndarray[float, ndim=2]
    M x D matrix of samples generated by NUTS.
    note: samples[0, :] = theta0
    """

    if len(np.shape(theta0)) > 1:
        raise ValueError('theta0 is expected to be a 1-D array')

    #D = len(theta0)
    samples = np.empty(((Mn + Madapt)*1000, Dim), dtype=float)
    lnprob = np.empty((Mn + Madapt)*1000, dtype=float)

    logp, grad = correlated_normal(theta0)
    #print("logp, grad: ",logp, grad)
    samples[0, :] = theta0
    lnprob[0] = logp

    # Choose a reasonable first epsilon by a simple heuristic.
    epsilon = find_reasonable_epsilon(theta0, grad, logp, correlated_normal)

    # Parameters to the dual averaging algorithm.
    gamma = 0.05
    t0 = 10
    kappa = 0.75
    mu = log(10. * epsilon)

    # Initialize dual averaging algorithm.
    epsilonbar = 1
    Hbar = 0
    bar = Bar('Processing Baron FBU', fill='#', suffix='%(percent)d%%', max=Gsteps)
    StopNow = False
    m = 1
    for m in range(1, (Mn + Madapt)*1000):
        # Resample momenta.
        r0 = np.random.normal(0, 1, Dim)

        #joint lnp of theta and momentum r
        joint = logp - 0.5 * np.dot(r0, r0.T)

        # Resample u ~ uniform([0, exp(joint)]).
        # Equivalent to (log(u) - joint) ~ exponential(1).
        logu = float(joint - np.random.exponential(1, size=1))

        # if all fails, the next sample will be the previous one
        samples[m, :] = samples[m - 1, :]
        lnprob[m] = lnprob[m - 1]

        # initialize the tree
        thetaminus = samples[m - 1, :]
        thetaplus = samples[m - 1, :]
        rminus = r0[:]
        rplus = r0[:]
        gradminus = grad[:]
        gradplus = grad[:]

        j = 0  # initial heigth j = 0
        n = 1  # Initially the only valid point is the initial point.
        s = 1  # Main loop: will keep going until s == 0.

        while (s == 1):
            # Choose a direction. -1 = backwards, 1 = forwards.
            v = int(2 * (np.random.uniform() < 0.5) - 1)
            # Double the size of the tree.
            if (v == -1):
                thetaminus, rminus, gradminus, _, _, _, thetaprime, gradprime, logpprime, nprime, sprime, alpha, nalpha, depth = build_tree(thetaminus, rminus, gradminus, logu, v, j, epsilon, correlated_normal, joint, depth=0)
            else:
                _, _, _, thetaplus, rplus, gradplus, thetaprime, gradprime, logpprime, nprime, sprime, alpha, nalpha, depth = build_tree(thetaplus, rplus, gradplus, logu, v, j, epsilon, correlated_normal, joint, depth=0)
            # Use Metropolis-Hastings to decide whether or not to move to a
            # point from the half-tree we just generated.
            _tmp = min(1, float(nprime) / float(n))
            if (sprime == 1) and (np.random.uniform() < _tmp):
                samples[m, :] = thetaprime[:]
                lnprob[m] = logpprime
                logp = logpprime
                grad = gradprime[:]
                if m > Madapt:
                    bar.next()  
                    for q in range(len(ListOfPosteriors)):
                        ListOfPosteriors[q].Fill(thetaprime[q],_tmp)
                        #delta = delta/m**0.25
                        #print("new delta: ", delta)
                    if ListOfPosteriors[Dim-1].GetEntries() >= Gsteps:
                        StopNow = True
                        bar.finish()
            # Update number of valid points we've seen.
            n += nprime
            # Decide if it's time to stop.
            s = sprime and stop_criterion(thetaminus, thetaplus, rminus, rplus)
            # Increment depth.
            j += 1
            if StopNow == True:
                break
            if j > 8 and m <= 200:
                break
            if j > 10 and m > 200:
                break
        # Do adaptation of epsilon if we're still doing burn-in.
        eta = 1. / float(m + t0)
        Hbar = (1. - eta) * Hbar + eta * (delta - alpha / float(nalpha))
        if (m <= Madapt):
            epsilon = exp(mu - sqrt(m) / gamma * Hbar)
            eta = m ** -kappa
            epsilonbar = exp((1. - eta) * log(epsilonbar) + eta * log(epsilon))
        else:
            epsilon = epsilonbar
        epsilon = np.log(epsilon)
        if StopNow:
            break
    samples = samples[Madapt:, :]
    lnprob = lnprob[Madapt:]
    return samples, lnprob, epsilon

def correlated_normal(theta, eps = 1e-2):
    """
    Example of a target distribution that could be sampled from using NUTS.
    (Although of course you could sample from it more efficiently)
    Doesn't include the normalizing constant.
    """
    R = np.array(theta).dot(M) 
    if GTau != 0.0:
        Entro = CalculateEntropy(theta)
        lh = np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R) - GTau*Entro#/np.sum(CURV)
        a = np.sum(np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R))
        b = np.sum(lh)
        d = 100*(1 - a/b)
        #print("d", d)
    else:
        lh = np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R)
    
    gradient = []
    for i in range(len(theta)):
        theta_plus = theta.copy()
        if GTau != 0:
            #step = theta[i]/100
            theta_plus[i] = theta_plus[i] + eps
            theta_minus = theta.copy()
            theta_minus[i] = theta_minus[i] - eps
            R1 = np.array(theta_plus).dot(M)      
            R2 = np.array(theta_minus).dot(M)
            delta = np.sum(np.log(R2/R1)+0.5*(R1-R2)*(np.square(D)/(R1*R2)-1) + GTau*(CalculateEntropy(theta_plus) - CalculateEntropy(theta_minus)))/(2*eps)    
        else:
            #step = theta[i]/100
            theta_plus[i] = theta_plus[i] + eps
            theta_minus = theta.copy()
            theta_minus[i] = theta_minus[i] - eps
            R1 = np.array(theta_plus).dot(M)      
            R2 = np.array(theta_minus).dot(M)
            delta = np.sum(np.log(R2/R1)+0.5*(R1-R2)*(np.square(D)/(R1*R2)-1))/(2*eps)
        gradient.append(delta)
    return  CheckNumber(np.sum(lh)), np.array(CheckArray(gradient))

def test_nuts6(Theta):
    """ Example usage of nuts6: sampling a 2d highly correlated Gaussian distribution """

    Mn = Gsteps
    Madapt = 200
    theta0 = Theta
    delta = 0.00001

    print('Running HMC with dual averaging and trajectory length %f...' % delta)
    samples, lnprob, epsilon = nuts6(correlated_normal, Mn, Madapt, theta0, delta)

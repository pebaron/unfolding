#!/usr/bin/env python3

import os
os.environ["MKL_THREADING_LAYER"] = "GNU"
import argparse
import fbu
from fbu import Regularization
import ROOT
import math
import random
import numpy as np
from numpy import log, exp, sqrt
from array import array
import scipy.stats as stats
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed, TMath, TF1, TLine, kBird, TGaxis, TGraph2D
import pandas as pn
import time
from progress.bar import Bar


parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")
parser.add_argument('--rfile_data','-rfile_data', type=str, default="input.root")
parser.add_argument('--rfile_particle','-rfile_particle', type=str, default="input.root")
parser.add_argument('--rfile_matrix','-rfile_matrix', type=str, default="input.root")
parser.add_argument('--rfile_background','-rfile_background', type=str, default="input.root")
parser.add_argument('--h_data','-h_data', type=str, default="h_data")
parser.add_argument('--h_particle','-h_particle', type=str, default="h_particle")
parser.add_argument('--h_matrix','-h_matrix', type=str, default="h_matrix")
parser.add_argument('--h_background','-h_background', type=str, default="h_background")
parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{t,had} [GeV]")
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)
parser.add_argument('--maxiterations', '-maxiterations', type=int, default=30)
parser.add_argument('--batch', '-batch', type=int, default=0)
parser.add_argument('--sampling', '-sampling', type=int, default=1000)
parser.add_argument('--plotlike', '-plotlike', type=int, default=0)
parser.add_argument('--nrebinpost', '-nrebinpost', type=int, default=1)
parser.add_argument('--counts', '-counts', type=int, default=30)
parser.add_argument('--eps','-e', type=float, default=1.0)
parser.add_argument('--L','-L', type=int, default=100)
parser.add_argument('--drawopt','-drawopt', type=str, default="surf1")
parser.add_argument('--output','-output', type=str, default="output")
parser.add_argument('--tau','-tau', type=float, default=0.01)
parser.add_argument('--norm','-norm', type=int, default=1)
parser.add_argument('--svd','-svd', type=int, default=0)
parser.add_argument('--fit','-fit', type=int, default=1)
parser.add_argument('--outname', '-outname', type=str, default="out")
parser.add_argument('--range', '-range', type=float, default=1.0)
parser.add_argument('--other', '-other', type=int, default=0) # other methods
parser.add_argument('--BinCoef', '-BinCoef', type=float, default=1.0) # other methods
parser.add_argument('--steps', '-steps', type=int, default=20000) # other methods

args = parser.parse_args()

if (args.batch == 1):
    ROOT.gROOT.SetBatch(1)

def PrintCan(can, outputname):
    outputname = outputname.split('/')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.png')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.pdf')

def PlotPosteriors(ListOfPosteriors, outputname = "", fit = 1):
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/2),int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.0))
    index = 1
    lines = []
    h_compare_ptcl = h_ptcl_or.Clone("ptcl_mult_eff")
    h_compare_ptcl.Multiply(h_eff)
    unfolded = h_ptcl_or.Clone("unfolded")
    unfolded.Reset()
    unfolded2 = h_ptcl_or.Clone("unfolded2")
    unfolded2.Reset()
    for i in range(len(ListOfPosteriors)):
        c.cd(i+1)
        if args.fit == 1:
            ListOfPosteriors[i].Fit("gaus", "q0W", "", ListOfPosteriors[i].GetXaxis().GetBinLowEdge(ListOfPosteriors[i].FindFirstBinAbove(0.1)),ListOfPosteriors[i].GetXaxis().GetBinUpEdge(ListOfPosteriors[i].FindLastBinAbove(0.1)))
            f1 = ListOfPosteriors[i].GetFunction("gaus")
            if f1.GetParameter(1):
                mu = f1.GetParameter(1)
                sigma = f1.GetParameter(2)
                unfolded.SetBinContent(i+1,mu)
                unfolded.SetBinError(i+1,sigma)
        else:        
            unfolded.SetBinContent(i+1,ListOfPosteriors[i].GetMean())
            unfolded.SetBinError(i+1,ListOfPosteriors[i].GetRMS())
        index = index + 1
    return unfolded, ListOfPosteriors

def CreateRandomGaus(Dim):
    r = []
    for i in range(Dim):
        r.append(np.random.normal())
    return np.array(r)

def FindReasonableEpsilon(Theta,eps = 1):
    Cycle = True
    while Cycle:    
        r = CreateRandomGaus(len(Theta))
        ThetaC, rC = Leapfrog(Theta, r, eps)
        Nominator = CalculateLikelihood(ThetaC)-0.5*rC.dot(rC)
        Denominator = CalculateLikelihood(Theta)-0.5*r.dot(r)
        Probability = np.exp(Nominator-Denominator) 
        if CheckNumber(Probability) != np.nan:
            break
    if (Probability > 0.5):
        a = 1.0
    else:
        a = -1.0
    while (np.power(Probability,a) > np.power(2.0,-a)):
        eps = np.power(2.0,a)*eps
        ThetaC, rC = Leapfrog(Theta, r, eps)
        Nominator = CalculateLikelihood(ThetaC)-0.5*rC.dot(rC)
        Denominator = CalculateLikelihood(Theta)-0.5*r.dot(r)
        Probability = np.exp(Nominator-Denominator) 
        if CheckNumber(Probability) == np.nan:
            return np.nan
    return eps

def IsInputNan2(Theta, r, u, v, j, eps,Theta0, r0):
    if check(Theta) or check(r) or check(u) or check(v) or check(j) or check(eps) or check(Theta0) or check(r0):
        return True
    else:
        return False

def check(a):
    return np.isnan(np.min(a))
    
def BuildTree(Theta, r, u, v, j, eps, Theta0, r0, layer):
    counts = layer
    counts = counts + 1
    if IsInputNan2(Theta, r, u, v, j, eps, Theta0, r0):
        ReturnArray = np.empty(Dim,)
        ReturnArray[:] = np.nan
        return ReturnArray, ReturnArray , ReturnArray, ReturnArray, ReturnArray ,np.nan, np.nan, np.nan, np.nan, counts
    if (j == 0):
        ThetaC, rC = Leapfrog(Theta, r, v*eps)
        check = np.exp(CalculateLikelihood(ThetaC, writeme=1))-0.5*rC.dot(rC)
        check0 = np.exp(CalculateLikelihood(Theta0, writeme=1))-0.5*r0.dot(r0)
        if np.isnan(np.min(check)) or np.isnan(np.min(check0)):
            ReturnArray = np.empty(Dim,)
            ReturnArray[:] = np.nan
            return ReturnArray, ReturnArray , ReturnArray, ReturnArray, ReturnArray ,np.nan, np.nan, np.nan, np.nan, counts
        if (np.exp(check) >= u):
            nC = 1 
        else:
            nC = 0
        if (np.exp(check) > u-np.exp(250)): 
            sC = 1
        else:
            sC = 0
        checkC = CalculateLikelihood(ThetaC, writeme=1)-0.5*rC.dot(rC)
        check0C = CalculateLikelihood(Theta0, writeme=1)-0.5*r0.dot(r0)
        aC = min([1.0, np.exp(checkC - check0C)])
        return ThetaC, rC, ThetaC, rC, ThetaC, nC, sC, aC, 1.0, counts
    else:
        ThetaM, rM, ThetaP, rP, ThetaC, nC, sC, aC, naC, counts = BuildTree(Theta, r, u, v, j-1, eps, Theta0, r0, counts)
        if (sC == 1):
            if (v == -1):
                ThetaM, rM, lin, lin2, ThetaCC, nCC, sCC, aCC, naCC, counts = BuildTree(ThetaM, rM, u, v, j-1, eps, Theta0, r0, counts)
            else:
                lin, lin2, ThetaP, rP, ThetaCC, nCC, sCC, aCC, naCC, counts = BuildTree(ThetaP, rP, u, v, j-1, eps, Theta0, r0, counts)
            if ((nCC == 0) and (nC == 0)):
                Prob = 0
            else:
                Prob = nCC/(nCC+nC)
            if (Prob > np.random.random_sample()):
                ThetaC = ThetaCC
            DeltaTheta = ThetaP - ThetaM
            if (DeltaTheta.dot(rM) >= 0.0) and (DeltaTheta.dot(rP) >= 0.0):
                sC = sCC
            else:
                sC = 0.0
            nC = nC + nCC
            aC = aC + aCC
            naC = naC + naCC
    ThetaM = np.array(ThetaM)
    rM = np.array(rM)
    ThetaP = np.array(ThetaP)
    rP = np.array(rP)
    ThetaC = np.array(ThetaC)
    return CheckArray(ThetaM), CheckArray(rM), CheckArray(ThetaP), CheckArray(rP), CheckArray(ThetaC), CheckNumber(nC), CheckNumber(sC), CheckNumber(aC), CheckNumber(naC), counts    

def Leapfrog(Theta, r, eps):
    grad = (eps/2)*Gradient(Theta, eps)
    rW = r + grad
    ThetaW = Theta + eps*rW
    rW = rW + grad
    return CheckArray(ThetaW), CheckArray(rW)

def CheckNumber(Number):
    if ((Number == np.nan) or (Number == -np.inf) or (Number == np.inf)):
        return np.nan
    else:
        return Number

def CheckArray(TestArray):
    if ((np.nan in TestArray) or (-np.inf in TestArray) or (np.inf in TestArray)):
        ReturnArray = np.empty(len(TestArray),)
        ReturnArray[:] = np.nan
        return ReturnArray
    else:
        return TestArray

def RandomTheta():
    while True:
        Theta = []    
        for l in range(len(D)):
            Theta.append((upper[l] - lower[l])*np.random.random_sample() + lower[l])
        test = np.exp(CalculateLikelihood(Theta))
        if (CheckNumber(test) != np.nan):
            break
    return Theta

def Epsilon(Theta, eps_or = 1):
    while True:
        eps = FindReasonableEpsilon(Theta, eps_or)
        if eps != np.nan:
            break
    return eps

def Gradient(T_random, step = 1e-8):
    gradient = []
    for i in range(len(T_random)):
        T_random_plus = T_random.copy()
        if GTau != 0.0:
            #step = T_random[i]/100
            T_random_plus[i] = T_random_plus[i] + step
            T_random_minus = T_random.copy()
            T_random_minus[i] = T_random_minus[i] - step
            R1 = np.array(T_random_plus).dot(M)      
            R2 = np.array(T_random_minus).dot(M)
            delta = np.sum(np.log(R2/R1)+0.5*(R1-R2)*(np.square(D)/(R1*R2)-1) + GTau*(CalculateEntropy(T_random_plus) - CalculateEntropy(T_random_minus)))/(2*step)    
        else:
            #step = T_random[i]/100
            T_random_plus[i] = T_random_plus[i] + step
            T_random_minus = T_random.copy()
            T_random_minus[i] = T_random_minus[i] - step
            R1 = np.array(T_random_plus).dot(M)      
            R2 = np.array(T_random_minus).dot(M)
            delta = np.sum(np.log(R2/R1)+0.5*(R1-R2)*(np.square(D)/(R1*R2)-1))/(2*step)
        gradient.append(delta)
    return np.array(CheckArray(gradient))

def curvature(f1,f2):
    return abs(f2)*(1 + f1**2)**-1.5

def CalculateCurvature(T_curv):
    CurvSum = 0.0
    for i in range(1,len(T_curv)-1):
        CurvSum = CurvSum + ((T_curv[i+1] - T_curv[i])-(T_curv[i] - T_curv[i-1]))*((T_curv[i+1] - T_curv[i])-(T_curv[i] - T_curv[i-1]))
    return CurvSum

def CalculateEntropy(T_entro):
    Entropy = 0.0
    for i in range(0,len(T_entro)):
        Entropy = Entropy + np.log(T_entro[i]/np.sum(T_entro))*T_entro[i]/np.sum(T_entro)
    return -1.0*Entropy


def CalculateLikelihood(T_random, writeme = 0, foreps = 0):
    R = np.array(T_random).dot(M) 
    if GTau != 0.0 and foreps == 0:
        Entro = CalculateEntropy(T_random)
        lh = np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R) - GTau*Entro#/np.sum(CURV)
        a = np.sum(np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R))
        b = np.sum(lh)
        d = 100*(1 - a/b)
    else:
        lh = np.log(1/np.sqrt(2*np.pi*R)) - np.square(D-R)/(2*R)
    return CheckNumber(lh.sum())



def Hamilton(data, matrix , low , up , particle , eff , acc  , eps = args.eps, steps = args.counts, Again = 0, Tau = 0, BinCoef = 1.0):   
    global Dim
    global D 
    global M 
    global lower 
    global upper
    global h_ptcl_or
    global h_eff 
    global h_acc 
    global GTau
    
    D = data
    Dim = len(data)
    M = matrix
    lower = low
    upper = up
    h_ptcl_or = particle
    h_eff = eff
    h_acc = acc
    GTau = Tau
    
    ListOfPosteriors = []
    Binning = []
    for k in range(len(D)):
        help_array = np.arange(lower[k],upper[k],(upper[k]-lower[k])/(100*BinCoef)) 
        Binning.append(help_array)
        x = array("d",Binning[k])
        posteriors = TH1D("poster_"+str(k),"poster_"+str(k), len(Binning[k])-1,x)
        ListOfPosteriors.append(posteriors)       

    ListOfEps = []
    N = 0
    while True:
        Theta = RandomTheta()
        eps = Epsilon(Theta)
        ListOfEps.append(eps)
        eps = min(ListOfEps)
        if (N > 20) and (eps > 1e-3):
            break
        elif (N > 20) and (eps < 1e-3):
            break
            print("Male eps zkousim to znovu: ",eps)
            ListOfEps = []
            N = 0
        N = N + 1
        if N > 100:
            print("Eps nemusi byt vhodne")
            break
    print('Eps: ', eps, ListOfEps)
    print('Chces zmenit eps? Ano = 1, Ne = 0 ')
    a = int(input())
    if a == 1:
        print('Zadej nove eps: ')
        eps = float(input())


    StopNow = False
    bar = Bar('****************************************************************************Processing', fill='#', suffix='%(percent)d%%', max=steps)

    mu = np.log(10*eps)
    eps0Bar = 1.0
    H0Bar = 0.0
    gamma = 0.05
    t0 = 10.0
    kappa = 0.75
    m = 1
    Madapt = steps
    ThetaArray = []
    k = 0
    while True:
        if (m == 1):
            ThetaArray.append(RandomTheta())
            while True:
                r0 = CreateRandomGaus(len(Theta))
                TempNumber = np.exp(np.exp(CalculateLikelihood(Theta))-0.5*r0.dot(r0))
                if (check(TempNumber) == False): 
                    u = np.random.uniform(0.0, TempNumber)
                    ThetaMinus = Theta
                    ThetaPlus = Theta
                    ThetaM1 = Theta
                    ThetaArray.append(Theta)
                    break
                else:
                    Theta = RandomTheta()
                    eps_old = eps
                    eps = Epsilon(Theta, eps_old)
        else:
            while True:
                r0 = CreateRandomGaus(len(Theta))
                TempNumber = np.exp(np.exp(CalculateLikelihood(ThetaM1))-0.5*r0.dot(r0))
                if check(TempNumber) == False: 
                    u = np.random.uniform(0.0, TempNumber)
                    ThetaMinus = ThetaM1
                    ThetaPlus = ThetaM1
                    ThetaArray.append(ThetaM1)
                    break
                else:
                    Theta = RandomTheta()
                    eps_old = eps
                    eps = Epsilon(Theta, eps_old)
        if check(eps):
            eps = Epsilon(Theta)
        n = 1
        s = 1
        j = 0
        rMinus = r0
        rPlus = r0
        sDelta = 1e-10
        while (s == 1):
            v = np.random.uniform(-1,1)
            if (v < 0):
                ThetaMinus, rMinus, lin, lin2, ThetaC, nC, sC, a, na, counts = BuildTree(ThetaMinus, rMinus, u, v, j, eps, ThetaArray[m-1], r0, 0) 
                if check(ThetaMinus[0]):
                    break
                Last = False
            else:
                lin, lin2, ThetaPlus, rPlus, ThetaC, nC, sC, a, na, counts = BuildTree(ThetaPlus, rPlus, u, v, j, eps, ThetaArray[m-1], r0, 0)
                if check(lin[0]):
                    break
                Last = True
            if (sC == 1) and (nC/n > 0.0):
                Prob = []
                Prob.append(1.0)
                Prob.append(nC/n)
                bar.next()
                for k in range(len(Theta)):
                    ListOfPosteriors[k].Fill(ThetaC[k], min(Prob))
                    if ListOfPosteriors[len(Theta)-1].GetEntries() >= steps:
                        StopNow = True
                        bar.finish()
                        break
            n = n + nC
            DeltaTheta = ThetaPlus - ThetaMinus
            if (DeltaTheta.dot(rMinus) >= 0) and (DeltaTheta.dot(rPlus) >= 0):
                s = sC
            else:
                s = 0
            j = j + 1
            if StopNow:
                break
            if (m <= Madapt) and (check(a) == False):
                H0Bar = (1-1/(m+t0))*H0Bar + (sDelta - a/na)*1/(m+t0)
                if m == 1:
                    logeps1 = np.log(eps0Bar)
                else:
                    logeps1 = logeps
                logeps = mu - H0Bar*np.sqrt(m)/gamma
                logepsBar = pow(m,-kappa)*logeps + (1 - pow(m,-kappa))*logeps1
                eps = logepsBar
                

        if StopNow:
            break
        if check(lin[0]) or check(ThetaMinus[0]):
            ThetaM1 = RandomTheta()
            Theta = ThetaM1
            m=0
            ThetaArray = []
        else:
            ThetaM1 = ThetaC
        m = m + 1

    for l in range(len(ListOfPosteriors)):
        if ListOfPosteriors[l].Integral() != 0:
            ListOfPosteriors[l].Scale(1.0/ListOfPosteriors[l].GetMaximum())
    Unfolded_Baron, ListOfPost = PlotPosteriors(ListOfPosteriors, outputname=args.output)
    return Unfolded_Baron, ListOfPost

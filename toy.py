#!/usr/bin/env python

# jk 25.1.2019
# based on the FBU algorith described by G. Choudalakis in his arXiv paper
# the Poisson terms are approximated by a Gaussian as we work in the limit of large
# number of observed events

from __future__ import print_function
#from __future__ import division

import numpy as np

import ROOT
#from array import array
#from math import pow,sqrt,exp


###################################
# some globals
lines = []
cans = []
grs = []
migras = []
stuff = []
legs = []

###################################
# fold T using M:
def Fold(T, M):
    R = np.zeros(len(T))
    for i in range(0, len(R)):
        j = 0
        R[i] = 0
        for m in M[i]:
            R[i] = R[i] + T[j]*M[j][i]
            j = j+1
    return R

###################################
def GetLhoodTerm(D, R, verbose = 0):
    if verbose:
        print('D-R:')
        print(D-R)
        print('(D-R)^2')
        print(np.power(D-R,2))
        print('-(D-R)^2 / (2R)')
        print(-np.power(D-R,2) / R  /2.)
    DminusR2overR = - np.power(D-R,2) / R / 2.
    ee = np.exp(DminusR2overR) / np.sqrt(R)
    return ee 

###################################

def MakeGraph(x, y, name, title = '', mstyle = 20, col = ROOT.kBlack, msize = 1):
    gr = ROOT.TGraph()
    gr.SetName(name)
    if title == '':
        title = name
    gr.SetTitle(title)
    ip = 0
    for i in range(len(y)):
        gr.SetPoint(ip, x[ip], y[ip])
        ip = ip+1
    gr.SetMarkerSize(msize)
    gr.SetMarkerStyle(mstyle)
    gr.SetMarkerColor(col)
    gr.SetLineColor(col)
    return gr

###################################
def MakeMigra(hh,name, title = ''):
    n = len(hh)
    n1 = 1
    n2 = n+1
    h2 = ROOT.TH2D(name, title, n, n1, n2, n, n1, n2)
    i = 0
    for line in hh:
        j = 0
        for val in line:
            h2.SetBinContent(i+1,j+1,val)
            j = j+1
        i = i+1
    h2.GetXaxis().SetTitle('truth bins')
    h2.GetYaxis().SetTitle('reco bins')
    return h2

###################################

def GetModus(posts):
    U = np.zeros(len(posts))
    i = 0
    for post in posts:
        #U[i] = post.GetMean()
        mval = -1.
        mx = -1.
        x = ROOT.Double(0.)
        y = ROOT.Double(0.)
        for ip in range(0, post.GetN()):
            post.GetPoint(ip, x, y)
            #print('ip={} x={} y={} maxval={} maxx={}'.format(ip, x, y, mval, mx) )
            if y > mval:
                print('...ok, got higher stuff!;)')
                mval = 1.*y ### beware, just mval = y only changes the binding and not value assignment and the algo would FAIL!
                mx = 1.*x
                print('    NEW: x={} y={} maxval={} maxx={}'.format(x, y, mval, mx) )
        U[i] = mx
        i = i+1
    return U


###################################
def SmallPrint(M,T,D):    
    print('T integral: {}'.format(np.sum(T)))
    print('D integral: {}'.format(np.sum(D)))
    print('T:')
    print(T)
    print('D:')
    print(D)
    print('M:')
    print(M)

###################################
def GrandPrint(M,T,D,R,U):    
    SmallPrint(M,T,D)
    print('U integral: {}'.format(np.sum(U)))
    print('U:')
    print(U)
    print('R:')
    print(R)


###################################
def MakePosteriors(rand, Truth, M, D, debug = 0):

    nbins = len(D)
    posteriors = []
    
    R = np.zeros(nbins)
    # truth used only to define the min and max of the
    # hyperbox to sample...
    # TODO: to be changed for data w/o the truth knowledge!!!
    Tmin = Truth / 1.5
    Tmax = Truth * 1.5

    nsteps = 20
    ntoys = 200
    # small, for testing:
    #nsteps = 20
    #ntoys = 200
    Ones = np.ones(nbins)
    verbose = nsteps/10

    T = np.zeros(nbins)
    
    for it in range(0, len(T)):
        print('*** Working on truth bin {:}'.format(it))
        # run over a grid of T_i
        timin = Tmin[it]
        timax = Tmax[it]
        step = (timax - timin) / nsteps
        Lhs = []
        print('Will run {} steps from {} to {}.'.format(nsteps,timin,timax))
        Tis = []
        for j in range(0,nsteps):
            if j % verbose == 0: print('step {}'.format(j))
            Ti = timin + j*step
            Tis.append(Ti)
            # throw randomly all the other T's:
            lhsum = 0.
            for itoy in range(0, ntoys):
                #if itoy % verbose == 0: print('   pseudoexp. {}'.format(itoy))
                prior = 1.
                for kt in range(0, len(T)):
                    if kt == it:
                        T[kt] = Ti
                        continue
                    tkmin = Tmin[kt]
                    tkmax = Tmax[kt]
                    T[kt] = rand.Uniform(tkmin, tkmax)
                R = Fold(T, M)
                exps = GetLhoodTerm(D,R)
                termsprod = np.dot(exps,Ones) # silly maybem but works;)
                #print("Printim: ",exps, Ones, termsprod)
                if debug : print('    exps={}'.format(exps,))
                toyterm = prior*termsprod
                lhsum = lhsum + toyterm
            if debug : print('        lhsum: {}'.format(lhsum,))
            Lhs.append(lhsum)
        posterior = MakeGraph(Tis, Lhs, 'posterior{}'.format(it), '', 20, ROOT.kGray)
        posterior.GetXaxis().SetTitle('Bin {}'.format(it+1))
        posteriors.append(posterior)

    return posteriors

###################################

def DrawResults(posteriors, D,Truth, M, U):
    ROOT.gStyle.SetOptTitle(0)
    nbins = len(Truth)
    opt = 'AP'
    canname = 'Posteriors'
    can = ROOT.TCanvas(canname, canname, 0, 0, 1450, 1000)
    can.Divide(3,2)
    cans.append(can)
    ican = 1
    ip = 0
  
    print(U)
    
    for post in posteriors:
        can.cd(ican)
        post.Draw(opt)

        line = ROOT.TLine(Truth[ip], post.GetYaxis().GetXmin(), Truth[ip], post.GetYaxis().GetXmax())
        line.SetLineColor(ROOT.kRed)
        line.Draw()
        lines.append(line)

        modus = U[ip]
        linem = ROOT.TLine(modus, post.GetYaxis().GetXmin(), modus, post.GetYaxis().GetXmax())
        linem.SetLineColor(ROOT.kBlue)
        linem.SetLineStyle(2)
        linem.Draw()
        lines.append(linem)

        lined = ROOT.TLine(D[ip], post.GetYaxis().GetXmin(), D[ip], post.GetYaxis().GetXmax())
        lined.SetLineColor(ROOT.kBlack)
        lined.SetLineStyle(3)
        lined.Draw()
        lines.append(lined)
        
        ican = ican+1
        ip = ip+1

    Bins = range(1,nbins+1)
    gU = MakeGraph(Bins, U, 'Unfolded', '', 22, ROOT.kBlue)
    gD = MakeGraph(Bins, D, 'Data'    , '', 20, ROOT.kBlack)
    gT = MakeGraph(Bins, Truth, 'Truth'   , '', 21, ROOT.kRed)

    can.cd(ican)
    migra = MakeMigra(M, 'Migration')
    migra.SetStats(0)
    migra.SetMinimum(0)
    migra.SetMaximum(1)
    migra.Draw('colz')
    migras.append(migra)
    
    ican = ican+1
    can.cd(ican)
    tmp = ROOT.TH2D('tmp', 'tmp;bins', 100, 0, nbins+1, 100, 0, 1.5*max(Truth))
    tmp.SetStats(0)
    tmp.Draw("")
    stuff.append(tmp)

    gT.Draw('PL')
    gD.Draw('PL')
    gU.Draw('PL')

    leg = ROOT.TLegend(0.60, 0.70, 0.87, 0.87)
    leg.SetBorderSize(0)
    leg.AddEntry(gD, 'Data', 'P')
    leg.AddEntry(gT, 'Truth', 'P')
    leg.AddEntry(gU, 'Unfolded', 'P')
    legs.append(leg)
    leg.Draw()

    grs.append([gU, gD, gT])
    
    return can

###################################
###################################
###################################
def main():
    rand = ROOT.TRandom3()

    #M = np.array( [ [0.8, 0.1, 0.1],
    #                [0.05, 0.9, 0.05],
    #                [0.05, 0.1, 0.85]
    #            ] )
    M = np.array( [ [0.75, 0.15, 0.10],
                    [0.08, 0.80, 0.12],
                    [0.10, 0.15, 0.75]
                  ] )
    Truth = np.array([1000,6000,3000])
    # prepare pseudodata by folding:
    D = Fold(Truth,M)
    SmallPrint(M, Truth, D)

    # run the FBU!
    posteriors = MakePosteriors(rand, Truth, M, D)

    # Make the unfolded array by taking some mean/modus/...
    U = GetModus(posteriors)
    # make TGraphs and plot!
    can = DrawResults(posteriors, D,Truth, M, U)

    GrandPrint(M, Truth, D, Fold(U,M), U)
    can.Print(can.GetName() + '.png')
    ROOT.gApplication.Run()

###################################
###################################
###################################

if __name__ == "__main__":
    # execute only if run as a script
    main()
    
###################################
###################################
###################################

#!/usr/bin/env python3

import os
os.environ["MKL_THREADING_LAYER"] = "GNU"
import argparse
import fbu
from fbu import Regularization
import ROOT
import math
import random
import numpy as np
from array import array
import scipy.stats as stats
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed, TMath, TF1, TLine, kBird, TGaxis, TGraph2D
import pandas as pn

def PrintCan(can, outputname):
    outputname = outputname.split('/')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.png')

def MakeListFromHisto(hist):
    vals = []
    for i in range(1, hist.GetXaxis().GetNbins()+1):
        val = hist.GetBinContent(i)
        vals.append(val)
    return vals

def NormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetXaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetYaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetYaxis().GetNbins()+1):
                migra.SetBinContent(i,j,migra.GetBinContent(i,j) / sum)
    return migra

def CheckNormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(j,i)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetXaxis().GetNbins()+1):
                migra.SetBinContent(j,j,migra.GetBinContent(j,i) / sum)
    return migra

def MakeListResponse(h2):
    vals = []
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        column = []
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            column.append(val)
        vals.append(column)
    return vals

def TransposeMatrix(h_response_unf):
    h_responce_transpose = h_response_unf.Clone(h_response_unf.GetName()+"clone")
    h_responce_transpose.Reset()
    for i in range(1,h_response_unf.GetXaxis().GetNbins()+1):
        for j in range(1,h_response_unf.GetXaxis().GetNbins()+1):
            h_responce_transpose.SetBinContent(i,j,h_response_unf.GetBinContent(j,i))
            h_responce_transpose.SetBinError(i,j,h_response_unf.GetBinError(j,i))
    h_responce_transpose.GetXaxis().SetTitle(h_response_unf.GetYaxis().GetTitle())
    h_responce_transpose.GetYaxis().SetTitle(h_response_unf.GetXaxis().GetTitle())
    return h_responce_transpose
    
def SaveHistograms(histograms, outputname = "output"):
    outputname = outputname.split('/')
    outfile = TFile('./outputs/'+outputname[len(outputname)-1]+".root", 'recreate')
    outfile.cd()
    for j in range(len(histograms)):
        histograms[j].Write()
    print("Histograms saved.")
    outfile.Write()
    outfile.Close() 

def PlotPosteriors(histograms, h_ptcl_or, h_eff, ListOfPosteriors, SamplingGraphs, ListOfPriors, outputname = "", fit = 1, title = ""):
    RepeatIteration = False
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/2),int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.0))
    c2 = TCanvas("Sampling"+outputname,"Sampling"+outputname,0,0,1600, 1600)
    c2.Divide(int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/2),int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.0))
    lines = []
    h_compare_ptcl = h_ptcl_or.Clone("ptcl_mult_eff")
    h_compare_ptcl.Multiply(h_eff)
    unfolded = h_ptcl_or.Clone("unfolded")
    unfolded.Reset()
    unfolded2 = h_ptcl_or.Clone("unfolded2")
    unfolded2.Reset()
    legends = []
    for i in range(len(ListOfPosteriors)):
        ListOfPosteriors[i].SetMaximum(ListOfPosteriors[i].GetMaximum()*2.0)
        ListOfPosteriors[i].Fit("gaus", "L")
        #input("Press Enter to continue fit...")
        fit = ListOfPosteriors[i].GetFunction("gaus") 
        chi2 = fit.GetChisquare()
        p1 = fit.GetParameter(1)
        p2 = fit.GetParameter(2)
        unfolded.SetBinContent(i, p1)
        unfolded.SetBinError(i, p2)
        FitIntegral = fit.Integral(p1-10*p2,p1+10*p2) # fit integral , get mean plus minus 10 sigma
        PriorIntegral = ListOfPosteriors[i].Integral("width")
        Percentage = round((100*PriorIntegral/FitIntegral),0)
        c.cd(i+1)
        gPad.SetFrameFillColor(10)
        if (Percentage < 90):                # 1.CONDITION of iteration, integral of histogram is at least 90% on fit integral
            gPad.SetFillColor(kRed-4)
            RepeatIteration = True
        else:
            gPad.SetFillColor(8)
        leg = TLegend(0.1,0.5,0.85,0.9)
        leg.SetFillStyle(0)
        leg.AddEntry(None,"MeanHist = "+str(round(ListOfPosteriors[i].GetMean(),0))+", RMShist = "+str(round(ListOfPosteriors[i].GetRMS(),0))+", MeanFit = "+str(round(p1,0))+", #sigma_{fit} = "+str(round(p2,0)),"")
        leg.AddEntry(ListOfPosteriors[i],"Posterior ","l")
        leg.AddEntry(fit,"Fit ","l")
        leg.AddEntry(None,"#chi^{2}/NDF = "+str(round(chi2/len(ListOfPosteriors),2)),"")
        leg.AddEntry(None,"Integral Prior/Fit = "+str(round(Percentage,0))+" %","")
        leg.SetBorderSize(0)
        legends.append(leg)
        ListOfPosteriors[i].SetTitle("Posterior in bin "+str(i+1)+"_iter_"+outputname)
        gStyle.SetOptStat(0)
        ListOfPosteriors[i].Draw()
        line = TLine(h_compare_ptcl.GetBinContent(i+1),0,h_compare_ptcl.GetBinContent(i+1),ListOfPosteriors[i].GetMaximum())
        lines.append(line)
        lines[i].SetLineColor(3)
        lines[i].Draw("same")
        legends[i].Draw("same")
    
    c.cd(1)
    gStyle.SetOptStat(0)
    unfolded.Divide(h_eff)
    unfolded2.Divide(h_eff)
    histograms.append(unfolded)
    histograms.append(h_ptcl_or)
    SaveHistograms(histograms = histograms)
    PrintCan(c, "PP_"+"_"+outputname)
    PrintCan(c2, "Frog_"+"_"+outputname)

    PlotRatio(unfolded, h_ptcl_or, Name = outputname, title = title)
    input("Press Enter to continue...")
    return unfolded, ListOfPosteriors, RepeatIteration

def PlotRatio(h_reco_unfolded, h_ptcl_or, Name = "", title = ""):
    
    gStyle.SetPadLeftMargin(0.12)
    gStyle.SetPadRightMargin(0.12)
    cr = TCanvas("canvas_ratio"+Name,"canvas_ratio"+Name,0,0,800, 800)
    cr.cd()
    pad1 = TPad("pad1"+Name,"pad1"+Name,0,0.40,1,1)
    pad1.SetTopMargin(0.15)
    pad1.SetBottomMargin(0.01)
    pad1.SetFillStyle(0)
    pad1.SetTicks(1,1)
    pad1.SetBorderMode(0)
    pad1.Draw()
    cr.cd()
    pad2 = TPad("pad2"+Name,"pad2"+Name,0,0.01,1,0.422)
    pad2.SetFillStyle(0)
    pad2.SetTopMargin(0.043)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks(1,1)
    pad2.Draw()
    pad2.Modified()
    cr.cd()
    pad1.cd()
    gStyle.SetOptStat(0)
    h_ptcl_or.SetTitle("")
    h_ptcl_or.SetLineColor(2)
    h_ptcl_or.GetXaxis().SetTitleSize(34)
    h_ptcl_or.GetXaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleSize(27)
    h_ptcl_or.GetYaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleOffset(1.5)
    h_ptcl_or.GetYaxis().SetTitle("Events")
    h_ptcl_or.GetYaxis().SetLabelFont(43)
    h_ptcl_or.GetYaxis().SetLabelSize(25)
    legend = TLegend(0.55,0.5,0.85,0.8)
    legend.SetFillStyle(0)
    legend.AddEntry(h_ptcl_or,"Particle")
    legend.AddEntry(h_reco_unfolded,"FBU Baron imp.","p")
    legend.SetBorderSize(0)
    h_reco_unfolded.SetLineColor(1)
    h_reco_unfolded.SetMarkerColor(1)
    h_reco_unfolded.SetMarkerStyle(22)
    h_reco_unfolded.Draw("same p x0")
    h_ptcl_or.Draw("hist same")
    legend.Draw("same")
    pad1.RedrawAxis()
    pad2.cd()
    h_ptcl_or_clone = h_ptcl_or.Clone(h_ptcl_or.GetName()+"_clone"+Name) 
    h_reco_unfolded_clone = h_reco_unfolded.Clone(h_reco_unfolded.GetName()+"_clone"+Name)
    h_ptcl_or_clone.Divide(h_ptcl_or) 
    h_reco_unfolded_clone.Divide(h_ptcl_or) 
    
    h_ptcl_or_clone.GetXaxis().SetTitleSize(27)
    h_ptcl_or_clone.GetXaxis().SetTitleFont(43)
    h_ptcl_or_clone.GetYaxis().SetTitleSize(27)
    h_ptcl_or_clone.GetYaxis().SetTitleFont(43)
    
    h_ptcl_or_clone.GetXaxis().SetLabelFont(43)
    h_ptcl_or_clone.GetXaxis().SetLabelSize(25)
    h_ptcl_or_clone.GetYaxis().SetLabelFont(43)
    h_ptcl_or_clone.GetYaxis().SetLabelSize(25)
    
    h_ptcl_or_clone.SetMaximum(1.3)
    h_ptcl_or_clone.SetMinimum(0.7)
    
    h_ptcl_or_clone.GetXaxis().SetTitleOffset(2.5)
    h_ptcl_or_clone.GetXaxis().SetTitle(title)
    
    h_ptcl_or_clone.GetYaxis().SetTitle("Ratio      ")
    h_ptcl_or_clone.Draw("hist")
    h_reco_unfolded_clone.Draw("same p x0")
    pad2.RedrawAxis()
    cr.Update()
    PrintCan(cr, "ratio"+Name)

def PrepareGlobals(h_reco_get_bkg,h_ptcl_or,h_reco_get_input_no_rebin, h_reco_get,h_ptcl_get,h_response_unf,matrix_name="matrix_name", h_reco_getG0_name="data_name", h_ptcl_getG0_name = "particle_name",outputname="_unfolded",nrebin = 0, sampling = 1000, maxiteration = 30,par = 1.25): 

    h_response_unf.ClearUnderflowAndOverflow()
    h_response_unf.GetXaxis().SetRange(1, h_response_unf.GetXaxis().GetNbins() )
    h_response_unf.GetYaxis().SetRange(1, h_response_unf.GetYaxis().GetNbins() )
    h_response_unf.SetName("Migration_Matrix_simulation")

    ########### ACCEPTANCY
    h_acc = h_response_unf.ProjectionX("reco_recoandparticleX") # Reco M
    h_acc.Divide(h_reco_get)
    ########### AKCEPTANCE saved in h_acc #############
    ########### EFFICIENCY
    h_eff = h_response_unf.ProjectionY("reco_recoandparticleY") # Ptcl M
    h_eff.Divide(h_ptcl_get)

    h_reco_get_input_clone=h_reco_get_input_no_rebin.Clone("")
    h_reco_get_input_clone.Add(h_reco_get_bkg,-1)
    h_reco_get_input_clone.Multiply(h_acc)
   
    h_ptcl_or.SetMaximum(h_ptcl_or.GetMaximum()*1.5)
    
    h_response_unf_fbu = TransposeMatrix(h_response_unf)
    h_response_unf_fbu_norm = NormalizeResponse(h_response_unf_fbu)
    h_response_unf_fbu_norm.SetName("Migration_Matrix_simulation_transpose")

    M = np.array(MakeListResponse(h_response_unf_fbu_norm))
    D = np.array(MakeListFromHisto(h_reco_get_input_clone)) 

    lower=[]
    upper=[]
    
    h_det_div_ptcl=h_reco_get_input_clone.Clone("")
    h_det_div_ptcl.Divide(h_ptcl_or)
    h_det_div_ptcl.Divide(h_eff)
    h_det_div_ptcl.SetName("det_div_ptcl")

    for l in range(len(D)):
        lower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-par)*h_det_div_ptcl.GetBinContent(l+1))
        upper.append(h_reco_get_input_clone.GetBinContent(l+1)*par*h_det_div_ptcl.GetBinContent(l+1)) 
        
    return M, D, lower, upper, h_ptcl_or, h_acc, h_eff


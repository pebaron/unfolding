#!/usr/bin/env python

import os
os.environ["MKL_THREADING_LAYER"] = "GNU"
import argparse
#import fbu
#from fbu import Regularization
import ROOT
import math
from array import array
from ROOT import RooUnfoldResponse
from ROOT import RooUnfold
from ROOT import RooUnfoldBayes
from ROOT import RooUnfoldSvd
from ROOT import RooUnfoldTUnfold
from ROOT import RooUnfoldIds
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TLine, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed
from NUTS2 import Hamilton
import numpy as np
from array import array
#from root_numpy import _librootnumpy
#from root_numpy import array

parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")

parser.add_argument('--rfile_data','-rfile_data', type=str, default="input.root")
parser.add_argument('--rfile_particle','-rfile_particle', type=str, default="input.root")
parser.add_argument('--rfile_matrix','-rfile_matrix', type=str, default="input.root")
parser.add_argument('--rfile_background','-rfile_background', type=str, default="input.root")
parser.add_argument('--h_data','-h_data', type=str, default="Reco/TopPairPt") 
parser.add_argument('--h_particle','-h_particle', type=str, default="Particle/MCTopPairPt")
parser.add_argument('--h_matrix','-h_matrix', type=str, default="Matrix/MigraTopPairPt")
parser.add_argument('--h_background','-h_background', type=str, default="h_background")
parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{ttbar} [GeV]") #p_{T}^{ttbar} [GeV], #eta^{ttbar} [-]
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)
parser.add_argument('--maxiterations', '-maxiterations', type=int, default=30)
parser.add_argument('--tauE','-tauE', type=float, default=0.0)
parser.add_argument('--tauC','-tauC', type=float, default=0.0)
parser.add_argument('--batch', '-batch', type=int, default=0)
parser.add_argument('--closure', '-closure', type=int, default=0)
parser.add_argument('--outname', '-outname', type=str, default="out")
parser.add_argument('--drawopt', '-drawopt', type=str, default="hist e1 same")
parser.add_argument('--BinCoef', '-BinCoef', type=float, default=2.0) # other methods
parser.add_argument('--range', '-range', type=float, default=2.0) # other methods
parser.add_argument('--other', '-other', type=int, default=0) # other methods
parser.add_argument('--steps', '-steps', type=int, default=20000) # other methods
parser.add_argument('--reg', '-reg', type=str, default="")

args = parser.parse_args()

if (args.batch == 1):
    ROOT.gROOT.SetBatch(1)

def PrintCan(can, outputname):
    outputname = outputname.split('/')
    can.Print('./png/'+outputname[len(outputname)-1] + '_' + args.outname + '.png')
    #can.Print('./pic/'+outputname[len(outputname)-1] + '.pdf')

def MakeListFromHisto(hist):
    vals = []
    for i in range(1, hist.GetXaxis().GetNbins()+1):
        val = hist.GetBinContent(i)
        vals.append(val)
    return vals

def NormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetXaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetYaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetYaxis().GetNbins()+1):
                migra.SetBinContent(i,j,migra.GetBinContent(i,j) / sum)
    return migra

def CheckNormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(j,i)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetXaxis().GetNbins()+1):
                migra.SetBinContent(j,j,migra.GetBinContent(j,i) / sum)
    return migra

def MakeListResponse(h2):
    vals = []
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        column = []
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            column.append(val)
        vals.append(column)
    return vals

def MakeUnfoldedHisto(reco4bins, h1s, tag = '_unfolded'):
    hname = reco4bins.GetName()+tag
    hist = reco4bins.Clone(hname)
    hist.Reset()
    histMean = reco4bins.Clone(hname+"Mean")
    histMean.Reset()
    i = -1
    for h1 in h1s:
        i = i+1
        #h1.Rebin(8)
        h1.Fit("gaus", "q")
        h1.Fit("gaus", "q0WL", "", h1.GetXaxis().GetBinLowEdge(h1.FindFirstBinAbove(0.5)),h1.GetXaxis().GetBinUpEdge(h1.FindLastBinAbove(0.5)))
        fit = h1.GetFunction("gaus") 
        chi2 = fit.GetChisquare()
        p1 = fit.GetParameter(1)
        p0 = fit.GetParameter(0)
        p2 = fit.GetParameter(2)
        hist.SetBinContent(i+1, p1)
        hist.SetBinError(i+1, p2)
        #hist.SetBinContent(i+1, h1.GetMean())
        #hist.SetBinError(i+1,  h1.GetRMS())
        histMean.SetBinContent(i+1, h1.GetMean())
        histMean.SetBinError(i+1, h1.GetRMS())
    return hist, histMean

def MakeTH1Ds(trace, listBinning, lower , upper , BinCoef = 1.0, tag = 'trace', nbins = 600):
    h1s = []
    gmax = -999
    gmin = 1e19
    for line in trace:
        xmin = min(line)
        xmax = max(line)
        gmin = min(xmin, gmin)
        gmax = max(xmax, gmax)
    i = -1
    gmax = -999
    gmin = 1e19
    Binning = []    
    for line in trace:
        gxmin = min(line)
        gxmax = max(line)
        i = i+1
        hname = tag + '_{:}'.format(i)
        help_array = np.arange(lower[i],upper[i],(upper[i]-lower[i])/(100*BinCoef)) 
        Binning.append(help_array)
        x = array("d",Binning[i])
        #h1 = listBinning[i].Clone(hname)
        #Sh1.Reset()
        h1 = TH1D(hname, hname, len(Binning[i])-1, x)
        #h1 = TH1D(hname, hname, nbins, gmin, gmax)
        print("-----------------------")
        k=0
        for val in line:
            h1.Fill(val)
        h1s.append(h1)
        print(i)
        print("Appending traces")
        print(h1)
        histograms.append(h1)
    return h1s

def TransposeMatrix(h_response_unf):
    h_responce_transpose = h_response_unf.Clone(h_response_unf.GetName()+"clone")
    h_responce_transpose.Reset()
    for i in range(1,h_response_unf.GetXaxis().GetNbins()+1):
        for j in range(1,h_response_unf.GetXaxis().GetNbins()+1):
            h_responce_transpose.SetBinContent(i,j,h_response_unf.GetBinContent(j,i))
            h_responce_transpose.SetBinError(i,j,h_response_unf.GetBinError(j,i))
    h_responce_transpose.GetXaxis().SetTitle(h_response_unf.GetYaxis().GetTitle())
    h_responce_transpose.GetYaxis().SetTitle(h_response_unf.GetXaxis().GetTitle())
    return h_responce_transpose
    
def SaveHistograms(outputname = "output"):
    outputname = outputname.split('/')
    outfile = TFile('./outputs/'+outputname[len(outputname)-1]+".root", 'recreate')
    outfile.cd()
    for j in range(len(histograms)):
        histograms[j].Write()
    print("End of the unfolding.")
    outfile.Write()
    outfile.Close() 

def PlotPosteriors(h_ptcl, ListOfPosteriors, ListOfPosteriors2, ListOfPosteriors3, compare1 = 0.0, compare2 = 0.0, outputname = ""):
    drawopt = args.drawopt
    RepeatIteration = False
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(math.ceil(pow(len(ListOfPosteriors),0.5)),math.ceil(pow(len(ListOfPosteriors),0.5)))
    legends = []
    for i in range(len(ListOfPosteriors)):
        ListOfPosteriors[i].SetMaximum(ListOfPosteriors[i].GetMaximum()*2.0)
        ListOfPosteriors[i].Fit("gaus", "q0WL", "", ListOfPosteriors[i].GetXaxis().GetBinLowEdge(ListOfPosteriors[i].FindFirstBinAbove(0.1)),ListOfPosteriors[i].GetXaxis().GetBinUpEdge(ListOfPosteriors[i].FindLastBinAbove(0.1)))
        fit = ListOfPosteriors[i].GetFunction("gaus") 
        if fit:
            chi2 = fit.GetChisquare()
            p1 = fit.GetParameter(1)
            #p0 = fit.GetParameter(0)
            p2 = fit.GetParameter(2)
            FitIntegral = fit.Integral(p1-10*p2,p1+10*p2) # fit integral , get mean plus minus 10 sigma
            PriorIntegral = ListOfPosteriors[i].Integral("width")
            Percentage = round((100*PriorIntegral/FitIntegral),0)
            c.cd(i+1)
            gPad.SetFrameFillColor(10)
            if (Percentage < 90):                # 1.CONDITION of iteration, integral of histogram is at least 90% on fit integral
                gPad.SetFillColor(kRed-4)
                RepeatIteration = True
            else:
                gPad.SetFillColor(8)
            leg = TLegend(0.1,0.5,0.85,0.9)
            leg.SetFillStyle(0)
            leg.AddEntry(None,"MeanHist = "+str(round(ListOfPosteriors[i].GetMean(),0))+", RMShist = "+str(round(ListOfPosteriors[i].GetRMS(),0))+", MeanFit = "+str(round(p1,0))+", #sigma_{fit} = "+str(round(p2,0)),"")
            leg.AddEntry(ListOfPosteriors[i],"Posterior ","l")
            leg.AddEntry(fit,"Fit ","l")
            leg.AddEntry(None,"#chi^{2}/NDF = "+str(round(chi2/len(ListOfPosteriors),2)),"")
            print("Chi2: ", chi2, ", NDF: ", len(ListOfPosteriors))
            leg.AddEntry(None,"Integral Prior/Fit = "+str(round(Percentage,0))+" %","")

        else:
            leg = TLegend(0.1,0.5,0.85,0.9)
            leg.SetFillStyle(0)
            leg.AddEntry(None,"MeanHist = "+str(round(ListOfPosteriors[i].GetMean(),0))+", RMShist = "+str(round(ListOfPosteriors[i].GetRMS(),0)),"")
            leg.AddEntry(ListOfPosteriors[i],"Posterior ","l")
        leg.SetBorderSize(0)
        legends.append(leg)
        ListOfPosteriors[i].SetTitle("Posterior in bin "+str(i+1))
        gStyle.SetOptStat(0)
        Xmin = []
        Xmax = []
        if compare1 == 1.0 and compare2 == 0.0:
            Xmin.append(ListOfPosteriors[i].FindFirstBinAbove(0.1))
            Xmin.append(ListOfPosteriors2[i].FindFirstBinAbove(0.1))
            Xmax.append(ListOfPosteriors[i].FindLastBinAbove(0.1))
            Xmax.append(ListOfPosteriors2[i].FindLastBinAbove(0.1))
            #ListOfPosteriors[i].GetXaxis().SetRange(min(Xmin), max(Xmax))
            ListOfPosteriors[i].Draw(drawopt)
            ListOfPosteriors2[i].SetLineColor(1)
            ListOfPosteriors2[i].Draw(drawopt)
        elif compare1 == 1.0 and compare2 == 1.0:
            Xmin.append(ListOfPosteriors[i].FindFirstBinAbove(0.1))
            Xmin.append(ListOfPosteriors2[i].FindFirstBinAbove(0.1))
            Xmin.append(ListOfPosteriors3[i].FindFirstBinAbove(0.1))
            Xmax.append(ListOfPosteriors[i].FindLastBinAbove(0.1))
            Xmax.append(ListOfPosteriors2[i].FindLastBinAbove(0.1))
            Xmax.append(ListOfPosteriors3[i].FindLastBinAbove(0.1))
            #ListOfPosteriors[i].GetXaxis().SetRange(min(Xmin), max(Xmax))
            ListOfPosteriors[i].Draw(drawopt)
            ListOfPosteriors2[i].SetLineColor(1)
            ListOfPosteriors2[i].Draw(drawopt)
            ListOfPosteriors3[i].SetLineColor(2)
            ListOfPosteriors3[i].Draw(drawopt)
        else:
            #ListOfPosteriors[i].GetXaxis().SetRange(ListOfPosteriors[i].FindFirstBinAbove(0.5), ListOfPosteriors[i].FindLastBinAbove(0.5))
            ListOfPosteriors[i].Draw(drawopt)
        if fit:
            fit.SetName("fit"+str(i+1))
            fit.Draw("same")
        legends[i].Draw("same")
        

    input("Zkontroluj posteriory...")

    gStyle.SetOptStat(0)
    gPad.Modified()
    c.Update()
    PrintCan(c,c.GetName()+"_posteriors")
    return RepeatIteration

def PlotRatio(h_ptcl_get2, h_unfolded2, h_reco_unfolded_Baron2, h_reco_unfolded_Baron_RegE2, svd_par = 0.0, Ids_par = 0.0  ,outputname="test.png", iter = -1):
    
    drawopt = args.drawopt
    gStyle.SetPadLeftMargin(0.15)
    gStyle.SetPadRightMargin(0.12)
    c = TCanvas("canvas_"+outputname,"canvas_"+outputname,0,0,800, 800)
    c.cd()
    pad1 = TPad("pad1"+outputname,"pad1"+outputname,0,0.40,1,1)
    pad1.SetTopMargin(0.15)
    pad1.SetBottomMargin(0.01)
    pad1.SetFillStyle(0)
    pad1.SetTicks(1,1)
    pad1.SetBorderMode(0)
    pad1.Draw()
    c.cd()
    pad2 = TPad("pad2"+outputname,"pad2"+outputname,0,0.01,1,0.422)
    pad2.SetFillStyle(0)
    pad2.SetTopMargin(0.043)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks(1,1)
    pad2.Draw()
    gStyle.SetOptStat(0)
    h_ptcl_get2.SetTitle("") 
    h_ptcl_get2.SetLineColor(2)
    if iter == 1:
        h_ptcl_get2.GetYaxis().SetRangeUser(0,h_ptcl_get2.GetMaximum()*1.5)
    h_ptcl_get2.GetXaxis().SetTitleSize(34)
    h_ptcl_get2.GetXaxis().SetTitleFont(43)
    h_ptcl_get2.GetYaxis().SetTitleSize(27)
    h_ptcl_get2.GetYaxis().SetTitleFont(43)
    h_ptcl_get2.GetYaxis().SetTitleOffset(1.5)
    h_ptcl_get2.GetYaxis().SetTitle("Events")
    h_ptcl_get2.GetYaxis().SetLabelFont(43)
    h_ptcl_get2.GetYaxis().SetLabelSize(25)
    h_ptcl_get2.GetYaxis().SetMaxDigits(3)
    legend = TLegend(0.55,0.5,0.85,0.8)
    legend.SetFillStyle(0)
    h_ptcl_get2.SetLineColor(2)
    h_ptcl_get2.SetLineStyle(2)
    legend.AddEntry(h_ptcl_get2,"Particle","l")
    legend.AddEntry(h_reco_unfolded_Baron_RegE2,"FBU Baron Entropy Reg. #tau = "+str(args.tauE),"lp")
    ###legend.AddEntry(h_reco_unfolded_Baron_RegC2,"FBU Baron Curvature Reg. #tau = "+str(args.tauC),"l")
    legend.AddEntry(h_reco_unfolded_Baron2,"FBU Baron No Reg.","lp")
    ##legend.AddEntry(h_unfolded2,"FBU Gerbaudo","l")
    legend.AddEntry(h_unfolded2,"D'Agostini RooUnfold, par. 4","lp")
    legend.SetBorderSize(0)
    ###h_reco_unfolded_Baron_RegC2.SetLineColor(2)
    ###h_reco_unfolded_Baron_RegC2.SetMarkerColor(2)
    ###h_reco_unfolded_Baron_RegC2.SetMarkerStyle(22)
    h_reco_unfolded_Baron2.SetLineColor(4)
    h_reco_unfolded_Baron2.SetMarkerColor(4)
    h_reco_unfolded_Baron2.SetMarkerStyle(22)
    h_reco_unfolded_Baron_RegE2.SetLineColor(1)
    h_reco_unfolded_Baron_RegE2.SetLineWidth(2)
    h_reco_unfolded_Baron_RegE2.SetMarkerColor(1)
    h_reco_unfolded_Baron_RegE2.SetMarkerStyle(22)
    h_unfolded2.SetMarkerColor(6)
    h_unfolded2.SetLineColor(6)
    h_unfolded2.SetMarkerStyle(24)
    #h_reco_unfolded_roof2.SetLineColor(1)
    #h_reco_unfolded_roof2.SetMarkerColor(1)
    #h_reco_unfolded_roof2.SetMarkerStyle(22)
    pad1.cd()
    #h_ptcl_get2.Draw(drawopt)  
    h_unfolded2.Draw(drawopt)
    ###h_reco_unfolded_Baron_RegC2.Draw(drawopt)
    h_reco_unfolded_Baron_RegE2.Draw(drawopt)
    h_reco_unfolded_Baron2.Draw(drawopt)
    legend.Draw("same")
    l1 = ROOT.TLatex()
    l1.SetTextAlign(9)
    l1.SetTextSize(0.1)
    l1.SetNDC()
    if iter == 1:
        text = str(iter)+"^{st} Iteration "
    elif iter == 2:
        text = str(iter)+"^{nd} Iteration "
    elif iter == 3:
        text = str(iter)+"^{rd} Iteration "
    else:
        text = str(iter)+"^{th} Iteration "
    l1.DrawLatex(0.21, 0.65, text)
    pad1.RedrawAxis()
    pad2.cd()
    h_reco_unfolded_clone = h_unfolded2.Clone(h_unfolded2.GetName()+"_clone")
    h_ptcl_get2_clone = h_ptcl_get2.Clone(h_ptcl_get2.GetName()+"_clone")
    ###h_reco_unfolded_Baron_RegC2_clone = h_reco_unfolded_Baron_RegC2.Clone(h_reco_unfolded_Baron_RegC2.GetName()+"_clone")
    h_reco_unfolded_Baron_clone2 = h_reco_unfolded_Baron2.Clone(h_reco_unfolded_Baron2.GetName()+"_clone")
    h_reco_unfolded_Baron_RegE2_clone = h_reco_unfolded_Baron_RegE2.Clone(h_reco_unfolded_Baron_RegE2.GetName()+"_clone")
    pad2.cd()
    h_ptcl_get2_clone.Divide(h_ptcl_get2)
    h_reco_unfolded_clone.Divide(h_ptcl_get2)
    ###h_reco_unfolded_Baron_RegC2_clone.Divide(h_ptcl_get2)
    h_reco_unfolded_Baron_clone2.Divide(h_ptcl_get2)
    h_reco_unfolded_Baron_RegE2_clone.Divide(h_ptcl_get2)
    h_ptcl_get2_clone.GetXaxis().SetTitleSize(27)
    h_ptcl_get2_clone.GetXaxis().SetTitleFont(43)
    h_ptcl_get2_clone.GetYaxis().SetTitleSize(27)
    h_ptcl_get2_clone.GetYaxis().SetTitleFont(43)
    h_ptcl_get2_clone.GetXaxis().SetLabelFont(43)
    h_ptcl_get2_clone.GetXaxis().SetLabelSize(25)
    h_ptcl_get2_clone.GetYaxis().SetLabelFont(43)
    h_ptcl_get2_clone.GetYaxis().SetLabelSize(25)
    h_ptcl_get2_clone.SetMaximum(2.0)
    h_ptcl_get2_clone.SetMinimum(0.0)
    h_ptcl_get2_clone.GetXaxis().SetTitleOffset(2.5)
    h_ptcl_get2_clone.GetYaxis().SetTitleOffset(1.7)
    h_ptcl_get2_clone.GetXaxis().SetTitle(args.title)
    h_ptcl_get2_clone.GetYaxis().SetTitle("#frac{Unfolded}{Particle}      ")
    h_ptcl_get2_clone.Draw("hist same")

    h_reco_unfolded_clone.Draw(drawopt)
    
    ###h_reco_unfolded_Baron_RegC2_clone.Draw(drawopt)
    h_reco_unfolded_Baron_clone2.Draw(drawopt)
    h_reco_unfolded_Baron_RegE2_clone.Draw(drawopt)
    pad2.RedrawAxis()
    c.Update()
    histograms.append(c)
    PrintCan(c, outputname)
    input("Zkontroluj Ratio...")

def DivideEff(h, eff):
    for i in range(1,h.GetNbinsX()+1):
        if eff.GetBinContent(i) != 0.0:
            h.SetBinContent(i,h.GetBinContent(i)/eff.GetBinContent(i))
        else: 
            print("WARNING: Nedelim histogram, eff = 0 v binu: ",i)
    return h


def MyRooUnfold(matrix_name=args.h_matrix, h_reco_getG0_name=args.h_data, h_ptcl_getG0_name = args.h_particle,h_reco_get_bkg_name = args.h_background,outputname=args.h_data+"_unfolded",nrebin = args.nrebin):

    rfile_data = TFile(args.rfile_data, 'read')
    rfile_particle = TFile(args.rfile_particle, 'read')
    rfile_matrix = TFile(args.rfile_matrix, 'read')
    rfile_background = TFile(args.rfile_background, 'read')

    #myfbu = fbu.PyFBU()
    #myfbu.verbose = True 
    #GET DATA
    h_reco_get = rfile_data.Get(h_reco_getG0_name)
    h_reco_get.Rebin(nrebin)
    #GET PARTICLE
    h_ptcl_get = rfile_particle.Get(h_ptcl_getG0_name)
    h_ptcl_get.Rebin(nrebin)
    #GET MATRIX
    h_response_unf = rfile_matrix.Get(matrix_name)
    h_response_unf.ClearUnderflowAndOverflow()
    h_response_unf.GetXaxis().SetRange(1, h_response_unf.GetXaxis().GetNbins())
    h_response_unf.GetYaxis().SetRange(1, h_response_unf.GetYaxis().GetNbins())
    h_response_unf.Rebin2D(nrebin,nrebin)
    h_response_unf.SetName("Migration_Matrix_simulation")
    ########### ACCEPTANCY
    h_acc = h_response_unf.ProjectionX("reco_recoandparticleX") # Reco M
    h_acc.Divide(h_reco_get)
    ########### AKCEPTANCE saved in h_acc #############
    ########### EFFICIENCY
    h_eff = h_response_unf.ProjectionY("reco_recoandparticleY") # Ptcl M
    h_eff.Divide(h_ptcl_get)
    h_reco_get_input = rfile_data.Get(h_reco_getG0_name)
    #h_reco_get_bkg = rfile_background.Get(h_reco_get_bkg_name)
    h_reco_get_bkg = h_reco_get_input.Clone("")
    #h_reco_get_bkg.Rebin(nrebin)
    h_reco_get_bkg.Reset() 
    

    h_reco_get_input_clone=h_reco_get_input.Clone("")

    #h_reco_get_input_clone.Add(h_reco_get_bkg,-1)

    h_reco_get_input_clone.Draw("hist")
    h_acc.Draw("same")

    gApplication.Run()

    h_reco_get_input_clone.Multiply(h_acc)
    
   
    h_reco_or = rfile_data.Get(h_reco_getG0_name)
    h_ptcl_or = rfile_particle.Get(h_ptcl_getG0_name)
    h_ptcl_or.SetMaximum(h_ptcl_or.GetMaximum()*1.5)

    ### ROOUNFOLD METHOD ###

    m_RooUnfold = RooUnfoldBayes()
    m_RooUnfold.SetRegParm( 4 )
    m_RooUnfold.SetNToys( 10000 )
    m_RooUnfold.SetVerbose( 3 )
    m_RooUnfold.SetSmoothing( 0 )
    h_response_unf = TransposeMatrix(h_response_unf)
    response = RooUnfoldResponse(None, None, h_response_unf, "response", "methods")
    
    m_RooUnfold.SetResponse( response )
    m_RooUnfold.SetMeasured( h_reco_get_input_clone )
    
    h_reco_unfolded_roof = m_RooUnfold.Hreco()
    h_reco_unfolded_roof = DivideEff(h_reco_unfolded_roof,h_eff)
    h_reco_unfolded_roof.SetLineColor(2)

    h_reco_unfolded_roof.Draw()
    h_ptcl_or.Draw("same")
    h_reco_unfolded_roof.Draw("same")
    gApplication.Run()
    ### SVD METHOD ###
    
    m_RooUnfold_svd = RooUnfoldSvd (response, h_reco_get_input_clone, int(round(h_reco_get_input_clone.GetNbinsX()/2.0,0))) #8
    svd_par = int(round(h_reco_get_input_clone.GetNbinsX()/2.0,0))
    m_RooUnfold_T = RooUnfoldTUnfold (response, h_reco_get_input_clone)         #  OR
    m_RooUnfold_Ids= RooUnfoldIds (response, h_reco_get_input_clone,int(round(h_reco_get_input_clone.GetNbinsX()/12.0,0))) ## TO DO, SET PARAMETERS TO THE BINNING
    Ids_par = int(round(h_reco_get_input_clone.GetNbinsX()/12.0,0))
    
    ### FBU METHOD ###
    
    h_response_unf_fbu = TransposeMatrix(h_response_unf)
    h_response_unf_fbu_norm = NormalizeResponse(h_response_unf_fbu)
    histograms.append(h_response_unf_fbu_norm)


    myfbu.response = MakeListResponse(h_response_unf_fbu_norm)
    myfbu.data = MakeListFromHisto(h_reco_get_input_clone) 

    lower = []
    upper = []
    
    h_det_div_ptcl=h_reco_get_input_clone.Clone("")
    h_det_div_ptcl.Divide(h_ptcl_or)
    h_det_div_ptcl.Divide(h_eff)
    h_det_div_ptcl.SetName("det_div_ptcl")
    histograms.append(h_det_div_ptcl)
    h_ptcl_or.SetName("particle")
    histograms.append(h_ptcl_or)

    for l in range(len(myfbu.data)):
        lower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-args.par)*h_det_div_ptcl.GetBinContent(l+1))
        upper.append(h_reco_get_input_clone.GetBinContent(l+1)*args.par*h_det_div_ptcl.GetBinContent(l+1))

    myfbu.lower = lower/np.array(args.range)
    myfbu.upper = upper*np.array(args.range)
    Repeat = True
    j = 1
    h_eff.SetName("efficiency")
    histograms.append(h_eff)
    h_acc.SetName("acceptancy")
    histograms.append(h_acc)

    while Repeat:
        print("Runnig iteration number: ",j)
        if j > 1:    
            lower = []
            upper = []
            lowerReg = []
            upperReg = []
            for l in range(len(myfbu.data)):
                #posteriors_diag[l].Fit("gaus")
                #fit = posteriors_diag[l].GetFunction("gaus") 
                ListOfFinePosteriors[l].Fit("gaus")
                fit = ListOfFinePosteriors[l].GetFunction("gaus") 
                p1 = fit.GetParameter(1)
                p2 = fit.GetParameter(2)
                lower.append(ListOfFinePosteriors[l].GetMean()*0.01) #p1
                upper.append(ListOfFinePosteriors[l].GetMean()*1.99) # p1
                lowerReg.append(p1-10*p2)
                upperReg.append(p1+10*p2)
            myfbu.lower = lower/np.array(args.range)
            myfbu.upper = upper*np.array(args.range)
        print("lower: ", myfbu.lower)
        print("upper: ", myfbu.upper)
        h_reco_unfolded_Baron_RegE, ListOfFinePosteriors_RegE = Hamilton(data = myfbu.data, matrix = myfbu.response, low = myfbu.lower, up = myfbu.upper, particle = h_ptcl_or, eff = h_eff, acc = h_acc , eps = 1, steps = args.steps, Again = 0, Tau = args.tauE, BinCoef=args.BinCoef, Reg = "e") #steps 40 000
        h_reco_unfolded_Baron_RegE.SetName(h_reco_unfolded_Baron_RegE.GetName()+'_RegE_' +str(args.tauE) + '_iteration_'+str(j))
        for m in range(len(ListOfFinePosteriors_RegE)):
            ListOfFinePosteriors_RegE[m].SetName(ListOfFinePosteriors_RegE[m].GetName()+'_RegE_' +str(args.tauE) +'_iteration_'+str(j))
            histograms.append(ListOfFinePosteriors_RegE[m])
            #ListOfFinePosteriors_RegE[m].Fit("gaus", "q")
            #f0 = ListOfFinePosteriors_RegE[m].GetFunction("gaus")
        NoList1 = []
        NoList2 = []
        PlotPosteriors(h_ptcl_get, ListOfFinePosteriors_RegE, NoList1, NoList2 , 0.0, 0.0 , outputname+'_RegE_' +str(args.tauE)+'_iteration_baron'+str(j))
        print("Stare tauE:", args.tauE)
        print("Prejete si jine tauE? Ano = piste, Ne = -1")
        change = float(input())
        if change != -1:
            args.tauE = change
            continue
        ####
        ###while True:
        ###    h_reco_unfolded_Baron_RegC, ListOfFinePosteriors_RegC = Hamilton(data = myfbu.data, matrix = myfbu.response, low = myfbu.lower, up = myfbu.upper, particle = h_ptcl_or, eff = h_eff, acc = h_acc , eps = 1, steps = args.steps, Again = 0, Tau = args.tauC, BinCoef=args.BinCoef, Reg="c") #steps 40 000
        ###    h_reco_unfolded_Baron_RegC.SetName(h_reco_unfolded_Baron_RegC.GetName()+'_RegC_' +str(args.tauC) +'_iteration_'+str(j))
        ###    for m in range(len(ListOfFinePosteriors_RegC)):
        ###        ListOfFinePosteriors_RegC[m].SetName(ListOfFinePosteriors_RegC[m].GetName()+'_RegC_' +str(args.tauC) +'_iteration_'+str(j))
        ###        histograms.append(ListOfFinePosteriors_RegC[m])
        ###    PlotPosteriors(h_ptcl_get, ListOfFinePosteriors_RegC,ListOfFinePosteriors_RegE,NoList2,  1.0, 0.0 ,outputname+'_RegC_' +str(args.tauC) +'_iteration_baron'+str(j))
        ###    print("Stare tauC:", args.tauC)
        ###    print("Prejete si jine tauC? Ano = piste, Ne = -1")
        ###    change = float(input())
        ###    if change != -1:
        ###        args.tauC = change
        ###        continue
        ###    else:
        ###        break
        
        ####
        h_reco_unfolded_Baron, ListOfFinePosteriors = Hamilton(data = myfbu.data, matrix = myfbu.response, low = myfbu.lower, up = myfbu.upper, particle = h_ptcl_or, eff = h_eff, acc = h_acc , eps = 1, steps = args.steps, Again = 0, Tau = 0.0, BinCoef=args.BinCoef, Reg="") #steps 40 000
        h_reco_unfolded_Baron.SetName(h_reco_unfolded_Baron.GetName()+'_iteration_'+str(j))
        for m in range(len(ListOfFinePosteriors)):
            ListOfFinePosteriors[m].SetName(ListOfFinePosteriors[m].GetName()+'_iteration_'+str(j))
            histograms.append(ListOfFinePosteriors[m])
        PlotPosteriors(h_ptcl_get, ListOfFinePosteriors,ListOfFinePosteriors_RegE,ListOfFinePosteriors_RegE,  1.0, 0.0 ,outputname+'_iteration_baron'+str(j))
        

        ####
        ##myfbu.nMCMC = int(args.steps)
        ##myfbu.nChains = 1
        ##myfbu.run()
        ##trace = myfbu.trace
        ##print(trace)
        ##traceName = 'Posterior_'+str(j)+'_iteration'
        ##posteriors_diag = MakeTH1Ds(trace, ListOfFinePosteriors, lower = myfbu.lower, upper = myfbu.upper, BinCoef = args.BinCoef, tag =  traceName)
        ##h_reco_unfolded, h_reco_unfolded_Mean = MakeUnfoldedHisto(h_reco_or, posteriors_diag)
        ##h_reco_unfolded = DivideEff(h_reco_unfolded, h_eff)
        ###h_reco_unfolded_Baron_RegC = DivideEff(h_reco_unfolded_Baron_RegC, h_eff)
        h_reco_unfolded_Baron_RegE = DivideEff(h_reco_unfolded_Baron_RegE, h_eff)
        h_reco_unfolded_Baron = DivideEff(h_reco_unfolded_Baron, h_eff)
        ##h_reco_unfolded_Mean.Divide(h_eff)
        ##h_reco_unfolded.SetName(h_reco_unfolded.GetName()+'_iteration_'+str(j))
        ##for i in range(len(posteriors_diag)):
        ##    if posteriors_diag[i].Integral() != 0:
        ##        posteriors_diag[i].Scale(1.0/posteriors_diag[i].GetMaximum())
        ##PlotPosteriors(h_ptcl_get, posteriors_diag,ListOfFinePosteriors, ListOfFinePosteriors_RegE,1.0, 1.0 ,outputname+'_iteration_'+str(j))
        ###
        # EFFICIENCY AND ACCEPTANCY CORRECTIONS
        ##h_reco_unfolded.SetName(h_reco_unfolded.GetName()+'_iteration_'+str(j))
        ##h_reco_unfolded_Mean.SetName(h_reco_unfolded_Mean.GetName()+'_iteration_'+str(j))
        h_reco_unfolded_roof = m_RooUnfold.Hreco()
        h_reco_unfolded_roof = DivideEff(h_reco_unfolded_roof, h_eff) 
        #h_reco_unfolded_roof.Divide(h_eff)
        h_reco_unfolded_roof.SetName(h_reco_unfolded_roof.GetName()+'_iteration_'+str(j))
        h_reco_unfolded_svd = m_RooUnfold_svd.Hreco()
        h_reco_unfolded_svd.Divide(h_eff)
        h_reco_unfolded_svd.SetName(h_reco_unfolded_svd.GetName()+'_iteration_'+str(j))
        h_reco_unfolded_T = m_RooUnfold_T.Hreco()
        h_reco_unfolded_T.Divide(h_eff)
        h_reco_unfolded_T.SetName(h_reco_unfolded_T.GetName()+'_iteration_'+str(j))
        h_reco_unfolded_Ids = m_RooUnfold_Ids.Hreco()
        h_reco_unfolded_Ids = DivideEff(h_reco_unfolded_Ids, h_eff)
        h_reco_unfolded_Ids.SetName(h_reco_unfolded_Ids.GetName()+'_iteration_'+str(j))
        h_ptcl_or.SetName(h_ptcl_or.GetName()+'_iteration_'+str(j))
        h_reco_unfolded_Baron_RegE.SetLineStyle(1)
        h_reco_unfolded_Baron_RegE.SetLineWidth(2)
        ###h_reco_unfolded_Baron_RegC.SetLineStyle(1)
        ####h_reco_unfolded_Baron.SetLineStyle(3)
        ##h_reco_unfolded.SetLineStyle(1)
        h_reco_unfolded_roof.SetLineStyle(1)
        h_reco_unfolded_roof.SetLineColor(6)
        h_reco_unfolded = h_reco_unfolded_Baron.Clone("NowUslessHistogram")
        h_reco_unfolded_Baron_RegC = h_reco_unfolded_Baron.Clone("NowUslessHistogram2")
        PlotRatio(h_ptcl_get2 = h_ptcl_or , h_unfolded2 = h_reco_unfolded_roof, h_reco_unfolded_Baron2 = h_reco_unfolded_Baron, h_reco_unfolded_Baron_RegE2 = h_reco_unfolded_Baron_RegE , svd_par = 0.0, Ids_par = 0.0, outputname=outputname+'_iteration_'+str(j), iter=j)
        ###PlotRatio(h_reco_unfolded_Baron_RegE, h_reco_unfolded, h_ptcl_get, h_reco_unfolded_roof, h_reco_unfolded_svd, h_reco_unfolded_T, h_reco_unfolded_Ids , h_reco_unfolded_Baron_RegC,h_reco_unfolded_Baron , svd_par, Ids_par, outputname=outputname+'_iteration_'+str(j), iter=j)
        ##histograms.append(h_reco_unfolded)
        ##histograms.append(h_reco_unfolded_Mean)
        h_reco_unfolded_svd.SetName("svd")
        h_reco_unfolded_roof.SetName("Dagostini")
        h_reco_unfolded_T.SetName("T")
        h_reco_unfolded_Ids.SetName("Ids")
        h_reco_unfolded_Baron.SetName("Baron")
        h_reco_unfolded_Baron_RegE.SetName("Baron_Entropy")
        h_ptcl_or.SetName("ptcl")
        h_reco_get_input.SetName("input")
        histograms.append(h_reco_unfolded_svd)
        histograms.append(h_reco_unfolded_roof)
        histograms.append(h_reco_unfolded_T)
        histograms.append(h_reco_unfolded_Ids)
        ###histograms.append(h_reco_unfolded_Baron_RegC)
        histograms.append(h_reco_unfolded_Baron)
        histograms.append(h_reco_unfolded_Baron_RegE)
        histograms.append(h_ptcl_or)
        histograms.append(h_reco_get_input)
        #histograms.append(ListOfPriors)
        SaveHistograms(outputname=args.outname+str(j))
        print("Prejete si dalsi iteraci? Ano = 1, Ne = 0")
        Repeat = int(input())      
        if Repeat == 0:
            break
        print("Prejete si novy range? Ano = zadej, Ne = 0")
        NewRange = float(input())  
        if NewRange != 0.0:
            args.range = NewRange
        print("Prejete si jine tauE? Ano = piste, Ne = -1")
        print("Stare tauE:", args.tauE)
        change = float(input())
        if change != -1:
            args.tauE = change
            #continue
        print("Repeat: ", Repeat)
        j = j+1

    #SaveHistograms(outputname=args.outname+"final")

histograms = []
MyRooUnfold()
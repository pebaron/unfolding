#!/usr/bin/env python3

import os
os.environ["MKL_THREADING_LAYER"] = "GNU"
import argparse
import fbu
from fbu import Regularization
import ROOT
import math
import random
import numpy as np
from array import array
import scipy.stats as stats
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed, TMath, TF1, TLine, kBird, TGaxis, TGraph2D
import pandas as pn
import time

parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")
parser.add_argument('--rfile_data','-rfile_data', type=str, default="input.root")
parser.add_argument('--rfile_particle','-rfile_particle', type=str, default="input.root")
parser.add_argument('--rfile_matrix','-rfile_matrix', type=str, default="input.root")
parser.add_argument('--rfile_background','-rfile_background', type=str, default="input.root")
parser.add_argument('--h_data','-h_data', type=str, default="h_data")
parser.add_argument('--h_particle','-h_particle', type=str, default="h_particle")
parser.add_argument('--h_matrix','-h_matrix', type=str, default="h_matrix")
parser.add_argument('--h_background','-h_background', type=str, default="h_background")

parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{t,had} [GeV]")
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)
parser.add_argument('--SplitFromBinLow', '-SplitFromBinLow', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinLow', '-ParameterSplitFromBinLow', type=float, default=1.5)
parser.add_argument('--SplitFromBinHigh', '-SplitFromBinHigh', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinHigh', '-ParameterSplitFromBinHigh', type=float, default=1.5)
parser.add_argument('--maxiterations', '-maxiterations', type=int, default=30)
parser.add_argument('--batch', '-batch', type=int, default=0)
parser.add_argument('--sampling', '-sampling', type=int, default=1000)
parser.add_argument('--plotlike', '-plotlike', type=int, default=0)
parser.add_argument('--nrebinpost', '-nrebinpost', type=int, default=1)
parser.add_argument('--counts', '-counts', type=int, default=30)
parser.add_argument('--eps','-e', type=float, default=1.0)
parser.add_argument('--L','-L', type=int, default=100)
parser.add_argument('--drawopt','-drawopt', type=str, default="surf1")
parser.add_argument('--output','-output', type=str, default="output")


args = parser.parse_args()

if (args.batch == 1):
    ROOT.gROOT.SetBatch(1)

def PrintCan(can, outputname):
    outputname = outputname.split('/')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.png')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.pdf')

def MakeListFromHisto(hist):
    vals = []
    for i in range(1, hist.GetXaxis().GetNbins()+1):
        val = hist.GetBinContent(i)
        vals.append(val)
    return vals

def NormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetXaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetYaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetYaxis().GetNbins()+1):
                migra.SetBinContent(i,j,migra.GetBinContent(i,j) / sum)
    return migra

def CheckNormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(j,i)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetXaxis().GetNbins()+1):
                migra.SetBinContent(j,j,migra.GetBinContent(j,i) / sum)
    return migra

def MakeListResponse(h2):
    vals = []
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        column = []
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            column.append(val)
        vals.append(column)
    return vals

def TransposeMatrix(h_response_unf):
    h_responce_transpose = h_response_unf.Clone(h_response_unf.GetName()+"clone")
    h_responce_transpose.Reset()
    for i in range(1,h_response_unf.GetXaxis().GetNbins()+1):
        for j in range(1,h_response_unf.GetXaxis().GetNbins()+1):
            h_responce_transpose.SetBinContent(i,j,h_response_unf.GetBinContent(j,i))
            h_responce_transpose.SetBinError(i,j,h_response_unf.GetBinError(j,i))
    h_responce_transpose.GetXaxis().SetTitle(h_response_unf.GetYaxis().GetTitle())
    h_responce_transpose.GetYaxis().SetTitle(h_response_unf.GetXaxis().GetTitle())
    return h_responce_transpose
    
def SaveHistograms(outputname = "output"):
    outputname = outputname.split('/')
    outfile = TFile('./outputs/'+outputname[len(outputname)-1]+".root", 'recreate')
    outfile.cd()
    for j in range(len(histograms)):
        histograms[j].Write()
    print("End of the unfolding.")
    outfile.Write()
    outfile.Close() 

def Plot2D(ListOf2DPosteriors):
    c = TCanvas("Posteriors2D"+outputname,"Posteriors2D"+outputname,0,0,1600, 1600)
    c.Divide(math.ceil(pow(len(ListOf2DPosteriors),0.5)),math.ceil(pow(len(ListOf2DPosteriors),0.5)))
    for i in range(len(ListOf2DPosteriors)):
        c.cd(i+1)
        ListOf2DPosteriors[i].GetZaxis().SetRangeUser(0,1.0)
        ListOf2DPosteriors[i].Draw("colz")
    gApplication.Run()

def PlotPosteriors(ListOfPosteriors, SamplingGraphs, outputname = "", fit = 1):
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/2),int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.0))
    c2 = TCanvas("Sampling"+outputname,"Sampling"+outputname,0,0,1600, 1600)
    c2.Divide(int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/2),int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.0))
    #c3 = TCanvas("Likelyhood"+outputname,"Likelyhood"+outputname,0,0,1600, 1600)
    #c3.Divide(int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/2),int((2+math.ceil(pow(2*len(ListOfPosteriors),0.5)))/1.0))
    index = 1
    lines = []
    drawopt = args.drawopt
    h_compare_ptcl = h_ptcl_or.Clone("ptcl_mult_eff")
    h_compare_ptcl.Multiply(h_eff)
    for i in range(len(ListOfPosteriors)):
        c.cd(i+1) 
        ListOfPosteriors[i].Fit("gaus")
        ListOfPosteriors[i].Draw()
        line = TLine(h_ptcl_or.GetBinContent(i+1),0,h_ptcl_or.GetBinContent(i+1),1.0)
        line.Draw()
        print("now ",h_ptcl_or.GetBinContent(i+1))
        lines.append(line)
        lines[i].SetLineColor(2)
        lines[i].Draw("same")
        c2.cd(i+1) 
        SamplingGraphs[i].Draw()
        SamplingGraphs[i].GetXaxis().SetRangeUser(lower[i],upper[i])
        SamplingGraphs[i].Draw()
        c2.Update()

        #c3.cd(i+1) 
        #LikeGraphs[i].Draw()
        #LikeGraphs[i].GetXaxis().SetRangeUser(lower[i],upper[i])
        #LikeGraphs[i].Draw()
        #c3.Update()
        index = index + 1 
    SaveHistograms()
    gApplication.Run()

def PlotRatio(h_reco_unfolded, h_ptcl_or, Name = ""):
    
    gStyle.SetPadLeftMargin(0.12)
    gStyle.SetPadRightMargin(0.12)
    c = TCanvas("canvas_ratio"+Name,"canvas_ratio"+Name,0,0,800, 800)
    c.cd()
    pad1 = TPad("pad1"+Name,"pad1"+Name,0,0.40,1,1)
    pad1.SetTopMargin(0.15)
    pad1.SetBottomMargin(0.01)
    pad1.SetFillStyle(0)
    pad1.SetTicks(1,1)
    pad1.SetBorderMode(0)
    pad1.Draw()
    c.cd()
    pad2 = TPad("pad2"+Name,"pad2"+Name,0,0.01,1,0.422)
    pad2.SetFillStyle(0)
    pad2.SetTopMargin(0.043)
    pad2.SetBottomMargin(0.2)
    pad2.SetBorderMode(0)
    pad2.SetTicks(1,1)
    pad2.Draw()
    pad2.Modified()
    c.cd()
    pad1.cd()
    gStyle.SetOptStat(0)
    h_ptcl_or.SetTitle("")
    h_ptcl_or.SetLineColor(2)
    h_ptcl_or.GetXaxis().SetTitleSize(34)
    h_ptcl_or.GetXaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleSize(27)
    h_ptcl_or.GetYaxis().SetTitleFont(43)
    h_ptcl_or.GetYaxis().SetTitleOffset(1.5)
    h_ptcl_or.GetYaxis().SetTitle("Events")
    h_ptcl_or.GetYaxis().SetLabelFont(43)
    h_ptcl_or.GetYaxis().SetLabelSize(25)
    legend = TLegend(0.55,0.5,0.85,0.8)
    legend.SetFillStyle(0)
    legend.AddEntry(h_ptcl_or,"Particle")
    legend.AddEntry(h_reco_unfolded,"FBU Baron imp.","p")
    legend.SetBorderSize(0)
    h_reco_unfolded.SetLineColor(1)
    h_reco_unfolded.SetMarkerColor(1)
    h_reco_unfolded.SetMarkerStyle(22)
    h_reco_unfolded.Draw("same p x0")
    h_ptcl_or.Draw("hist same")
    legend.Draw("same")
    pad1.RedrawAxis()
    pad2.cd()
    h_ptcl_or_clone = h_ptcl_or.Clone(h_ptcl_or.GetName()+"_clone"+Name) 
    h_reco_unfolded_clone = h_reco_unfolded.Clone(h_reco_unfolded.GetName()+"_clone"+Name)
    h_ptcl_or_clone.Divide(h_ptcl_or) 
    h_reco_unfolded_clone.Divide(h_ptcl_or) 
    
    h_ptcl_or_clone.GetXaxis().SetTitleSize(27)
    h_ptcl_or_clone.GetXaxis().SetTitleFont(43)
    h_ptcl_or_clone.GetYaxis().SetTitleSize(27)
    h_ptcl_or_clone.GetYaxis().SetTitleFont(43)
    
    h_ptcl_or_clone.GetXaxis().SetLabelFont(43)
    h_ptcl_or_clone.GetXaxis().SetLabelSize(25)
    h_ptcl_or_clone.GetYaxis().SetLabelFont(43)
    h_ptcl_or_clone.GetYaxis().SetLabelSize(25)
    
    h_ptcl_or_clone.SetMaximum(1.3)
    h_ptcl_or_clone.SetMinimum(0.7)
    
    h_ptcl_or_clone.GetXaxis().SetTitleOffset(2.5)
    h_ptcl_or_clone.GetXaxis().SetTitle(args.title)
    
    h_ptcl_or_clone.GetYaxis().SetTitle("Ratio      ")
    h_ptcl_or_clone.Draw("hist")
    h_reco_unfolded_clone.Draw("same p x0")
    pad2.RedrawAxis()
    c.Update()
    histograms.append(c) # here is the crash probably
    PrintCan(c, "ratio"+Name)

#def CalculateLikelihood(T_random,M,D):
#    #T_random = np.array(D).dot(M)                                       
#    R = np.array(T_random).dot(M)                                       # CREATING RECO
#    D_int64 = D.astype(np.int64)
#    R_int64 = R.astype(np.int64)
#    LikeTest = stats.poisson.pmf(D_int64, R_int64)
#    return np.prod(LikeTest)
def CreateRandomGaus(Dim):
    r = []
    for i in range(Dim):
        r.append(np.random.normal())
    return np.array(r)

def Leapfrog(Theta, r, eps):
    grad = (eps/2)*Gradient(Theta)
    rW = r + grad
    ThetaW = Theta + eps*rW
    rW = rW + grad
    return ThetaW, rW

def Hamilton(eps = args.eps, L = args.L):   
    ListOfPosteriors = []
    SamplingGraphs = []
    LikeGraphs = []
    Binning = []
    for k in range(len(D)):
        help_array = np.arange(lower[k],upper[k],(upper[k]-lower[k])/1000) # here improvement
        Binning.append(help_array)
        x = array("d",Binning[k])
        #posteriors = TH2D("poster_"+str(k),"poster_"+str(k), len(Binning[k])-1,x, 100,-20,20)
        posteriors = TH1D("poster_"+str(k),"poster_"+str(k), len(Binning[k])-1,x)
        graph = TGraph2D()
        graph.SetName("graph_"+ str(k))
        SamplingGraphs.append(graph)
        likegraph = TGraph2D()
        likegraph.SetName("likegraph_"+ str(k))
        LikeGraphs.append(likegraph)
        ListOfPosteriors.append(posteriors)        
    #for s in range(int(args.counts)):
        #if (s % 10 == 0):
        #            print("Processing ",s)

    while True:
        Theta = []    
        for l in range(len(D)):
            Theta.append((upper[l] - lower[l])*np.random.random_sample() + lower[l])
        test = CalculateLikelihood(Theta)
        if (test != False):
            #print("OK: I found non zero Likelyhood.")
            break
    for m in range(1,args.counts):
        Alpha = []
        #Alpha.append(1)
        r0 = CreateRandomGaus(len(Theta))
        if (m == 1):
            ThetaM = Theta
            ThetaW = Theta
        else:
            ThetaM = ThetaM1
            ThetaW = ThetaM1
        rW = r0
        print(m)
        for i in range(1,L+1):
            ThetaW, rW = Leapfrog(ThetaW, rW, eps)
        NomLogLike = CalculateLikelihood(ThetaW)
        DeNomLogLike = CalculateLikelihood(ThetaM)
        if ((NomLogLike != False)and(DeNomLogLike != False)):
            Nominator = CalculateLikelihood(ThetaW)-0.5*rW.dot(rW)
            Denominator = CalculateLikelihood(ThetaM)-0.5*r0.dot(r0)
            Probability = np.exp(Nominator-Denominator) 
            if ((Probability != float('nan')) and (Probability != float('-inf')) and (Probability != float('inf'))):
                Alpha.append(Probability)
            else:
                Probability = 1.1
            ThetaM = ThetaW
            ThetaM1 = ThetaW
            rM = -1*rW
            for j in range(len(ThetaM)):
                ListOfPosteriors[j].Fill(ThetaM[j], min(Alpha))
                SamplingGraphs[j].SetPoint(m,ThetaM[j], rM[j], min(Alpha))
                #LikeGraphs[j].SetPoint(m,ThetaM[j], rM[j], CalculateLikelihood(ThetaM))
    for l in range(len(ListOfPosteriors)):
        #ListOfPosteriors[l].Scale(1.0/ListOfPosteriors[l].Integral())
        histograms.append(ListOfPosteriors[l])
        histograms.append(SamplingGraphs[l])
        #histograms.append(LikeGraphs[l])
    PlotPosteriors(ListOfPosteriors, SamplingGraphs, outputname=args.output)
        
def Gradient(T_random, step = 1e-1):
    gradient = []
    for i in range(len(T_random)):
        T_random_plus = T_random.copy()
        T_random_plus[i] = T_random_plus[i] + step
        T_random_minus = T_random.copy()
        T_random_minus[i] = T_random_minus[i] - step
        R1 = np.array(T_random_plus).dot(M)       
        R2 = np.array(T_random_minus).dot(M)       
        #delta = np.sum(np.log(R2/R1)+np.square(D-R2)/(2*R2)-np.square(D-R1)/(2*R1))/(2*step)
        delta = np.sum(np.log(R2/R1)+0.5*(R1-R2)*(np.square(D)/(R1*R2)-1))/(2*step) # simplified
        #print("delta, delta2", np.square(D-R2)/(2*R2)-np.square(D-R1)/(2*R1), 0.5*(R1-R2)*(np.square(D)/(R1*R2)-1))
        if ((delta != float('nan')) and (delta != float('-inf')) and (delta != float('inf'))):
            gradient.append(delta)
        else:
            gradient.append(float('nan'))
    return np.array(gradient)

def CalculateLikelihood(T_random):
    R = np.array(T_random).dot(M)                                       # CREATING RECO
    #lh = np.log(stats.norm(R, np.sqrt(R)).pdf(D))
    lh = np.log(1/np.sqrt(2*np.pi*R))-np.square(D-R)/(2*R)
    myBool = True
    for i in range(len(lh)):
        if ((lh[i] == float('nan')) or (lh[i] == float('-inf')) or (lh[i] == float('inf'))):
            myBool = False
    if (myBool == True):
        return lh.sum()
    else:
        return False

def PrepareGlobals(h_reco_get,h_ptcl_get,h_response_unf,h_reco_get_bkg,matrix_name=args.h_matrix, h_reco_getG0_name=args.h_data, h_ptcl_getG0_name = args.h_particle,h_reco_get_bkg_name = args.h_background,outputname=args.h_data+"_unfolded",nrebin = args.nrebin, sampling = 1000, maxiteration = 30):

    h_response_unf.ClearUnderflowAndOverflow()
    h_response_unf.GetXaxis().SetRange(1, h_response_unf.GetXaxis().GetNbins() )
    h_response_unf.GetYaxis().SetRange(1, h_response_unf.GetYaxis().GetNbins() )
    h_response_unf.SetName("Migration_Matrix_simulation")

    ########### ACCEPTANCY
    h_acc = h_response_unf.ProjectionX("reco_recoandparticleX") # Reco M
    h_acc.Divide(h_reco_get)
    ########### AKCEPTANCE saved in h_acc #############
    ########### EFFICIENCY
    h_eff = h_response_unf.ProjectionY("reco_recoandparticleY") # Ptcl M
    h_eff.Divide(h_ptcl_get)

    h_reco_get_input_clone=h_reco_get_input.Clone("")
    #h_reco_get_input_clone.Add(h_reco_get_bkg,-1)
    h_reco_get_input_clone.Multiply(h_acc)
   
    h_ptcl_or = rfile_particle.Get(h_ptcl_getG0_name)
    h_ptcl_or.SetMaximum(h_ptcl_or.GetMaximum()*1.5)
    
    h_response_unf_fbu = TransposeMatrix(h_response_unf)
    h_response_unf_fbu_norm = NormalizeResponse(h_response_unf_fbu)
    h_response_unf_fbu_norm.SetName("Migration_Matrix_simulation_transpose")
    histograms.append(h_response_unf_fbu_norm)
    M = np.array(MakeListResponse(h_response_unf_fbu_norm))
    D = np.array(MakeListFromHisto(h_reco_get_input_clone)) 
    B = np.array(MakeListFromHisto(h_reco_get_bkg))

    lower=[]
    upper=[]
    
    h_det_div_ptcl=h_reco_get_input_clone.Clone("")
    h_det_div_ptcl.Divide(h_ptcl_or)
    h_det_div_ptcl.Divide(h_eff)
    h_det_div_ptcl.SetName("det_div_ptcl")

    for l in range(len(D)):
        lower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-args.par)*h_det_div_ptcl.GetBinContent(l+1))
        upper.append(h_reco_get_input_clone.GetBinContent(l+1)*args.par*h_det_div_ptcl.GetBinContent(l+1)) 

    return M, D, lower, upper, h_ptcl_or, h_acc, h_eff
        
histograms = []


matrix_name=args.h_matrix
h_reco_getG0_name=args.h_data
h_ptcl_getG0_name = args.h_particle
h_reco_get_bkg_name = args.h_background
outputname=args.h_data+"_unfolded"
nrebin = args.nrebin

rfile_data = TFile(args.rfile_data, 'read')
rfile_particle = TFile(args.rfile_particle, 'read')
rfile_matrix = TFile(args.rfile_matrix, 'read')
rfile_background = TFile(args.rfile_background, 'read')
#GET DATA
h_reco_get = rfile_data.Get(h_reco_getG0_name)
h_reco_get.Rebin(nrebin)
#GET PARTICLE
h_ptcl_get = rfile_particle.Get(h_ptcl_getG0_name)
h_ptcl_get.Rebin(nrebin)
#GET MATRIX
h_response_unf = rfile_matrix.Get(matrix_name)
h_response_unf.Rebin2D(nrebin,nrebin)
h_reco_get_input = rfile_data.Get(h_reco_getG0_name)
h_reco_get_bkg = rfile_background.Get(h_reco_get_bkg_name)
h_reco_get_bkg.Rebin(nrebin)

M, D , lower, upper, h_ptcl_or, h_acc, h_eff = PrepareGlobals(h_reco_get=h_reco_get,h_ptcl_get=h_ptcl_get,h_response_unf=h_response_unf,h_reco_get_bkg=h_reco_get_bkg, matrix_name=h_response_unf.GetName(), h_ptcl_getG0_name=h_ptcl_get.GetName(), h_reco_getG0_name=h_reco_get.GetName(), h_reco_get_bkg_name=h_reco_get_bkg.GetName(), sampling=args.sampling)
Hamilton()

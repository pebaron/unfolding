#!/usr/bin/env python

import argparse
from matplotlib import pyplot as plt
import fbu
from fbu import Regularization
import ROOT
import math
import random
import numpy
from array import array
from ROOT import RooUnfoldResponse
from ROOT import RooUnfold
from ROOT import RooUnfoldBayes
from ROOT import RooUnfoldSvd
from ROOT import RooUnfoldTUnfold
from ROOT import RooUnfoldIds
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed, TMath, TF1

parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")
parser.add_argument('--N', '-N', type=int, default=100)
parser.add_argument('--rfile_data','-rfile_data', type=str, default="input.root")
parser.add_argument('--rfile_particle','-rfile_particle', type=str, default="input.root")
parser.add_argument('--rfile_matrix','-rfile_matrix', type=str, default="input.root")
parser.add_argument('--rfile_background','-rfile_background', type=str, default="input.root")
parser.add_argument('--h_data','-h_data', type=str, default="h_data")
parser.add_argument('--h_particle','-h_particle', type=str, default="h_particle")
parser.add_argument('--h_matrix','-h_matrix', type=str, default="h_matrix")
parser.add_argument('--h_background','-h_background', type=str, default="h_background")

parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{t,had} [GeV]")
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)
parser.add_argument('--SplitFromBinLow', '-SplitFromBinLow', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinLow', '-ParameterSplitFromBinLow', type=float, default=1.5)
parser.add_argument('--SplitFromBinHigh', '-SplitFromBinHigh', type=int, default=0)
parser.add_argument('--ParameterSplitFromBinHigh', '-ParameterSplitFromBinHigh', type=float, default=1.5)
parser.add_argument('--maxiterations', '-maxiterations', type=int, default=30)
parser.add_argument('--batch', '-batch', type=int, default=0)

args = parser.parse_args()

if (args.batch == 1):
    ROOT.gROOT.SetBatch(1)

def DivideBinWidth(h):
    for j in range(1, h.GetNbinsX()+1):
        h.SetBinContent(j,((h.GetBinContent(j))/(h.GetBinWidth(j))))
        h.SetBinError(j,((h.GetBinError(j))/(h.GetBinWidth(j))))

def PrintCan(can, outputname):
    outputname = outputname.split('/')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.png')
    can.Print('./pic/'+outputname[len(outputname)-1] + '.pdf')

def MakeListFromHisto(hist):
    vals = []
    for i in range(1, hist.GetXaxis().GetNbins()+1):
        val = hist.GetBinContent(i)
        vals.append(val)
    return vals

def NormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetXaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetYaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetYaxis().GetNbins()+1):
                migra.SetBinContent(i,j,migra.GetBinContent(i,j) / sum)
    return migra

def CheckNormalizeResponse(h2, tag = '_migra'):
    migra = h2.Clone(h2.GetName() + tag)
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        sum = 0.
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(j,i)
            sum = sum + val
        if sum > 0.:
            for j in range(1, h2.GetXaxis().GetNbins()+1):
                migra.SetBinContent(j,j,migra.GetBinContent(j,i) / sum)
    return migra

def MakeListResponse(h2):
    vals = []
    for i in range(1, h2.GetYaxis().GetNbins()+1):
        column = []
        for j in range(1, h2.GetXaxis().GetNbins()+1):
            val = h2.GetBinContent(i,j)
            column.append(val)
        vals.append(column)
    return vals

def MakeUnfoldedHisto(reco4bins, h1s, tag = '_unfolded'):
    hname = reco4bins.GetName()+tag
    hist = reco4bins.Clone(hname)
    hist.Reset()
    histMean = reco4bins.Clone(hname+"Mean")
    histMean.Reset()
    i = -1
    for h1 in h1s:
        i = i+1
        h1.Rebin(8)
        h1.Fit("gaus")
        fit = h1.GetFunction("gaus") 
        chi2 = fit.GetChisquare()
        p1 = fit.GetParameter(1)
        p0 = fit.GetParameter(0)
        p2 = fit.GetParameter(2)
        hist.SetBinContent(i+1, p1)
        hist.SetBinError(i+1, p2)
        histMean.SetBinContent(i+1, h1.GetMean())
        histMean.SetBinError(i+1, h1.GetRMS())
    return hist, histMean

def MakeTH1Ds(trace, tag = 'trace', nbins = 600):
    h1s = []
    gmax = -999
    gmin = 1e19
    for line in trace:
        xmin = min(line)
        xmax = max(line)
        gmin = min(xmin, gmin)
        gmax = max(xmax, gmax)
    i = -1
    gmax = -999
    gmin = 1e19
    
    for line in trace:
        gxmin = min(line)
        gxmax = max(line)
        i = i+1
        hname = tag + '_{:}'.format(i)
        h1 = TH1D(hname, hname, nbins, gmin, gmax)
        print("-----------------------")
        k=0
        for val in line:
            h1.Fill(val)
        h1s.append(h1)
        print(i)
        print("Appending traces")
        print(h1)
        histograms.append(h1)
    return h1s

def TransposeMatrix(h_response_unf):
    h_responce_transpose = h_response_unf.Clone(h_response_unf.GetName()+"clone")
    h_responce_transpose.Reset()
    for i in range(1,h_response_unf.GetXaxis().GetNbins()+1):
        for j in range(1,h_response_unf.GetXaxis().GetNbins()+1):
            h_responce_transpose.SetBinContent(i,j,h_response_unf.GetBinContent(j,i))
            h_responce_transpose.SetBinError(i,j,h_response_unf.GetBinError(j,i))
    h_responce_transpose.GetXaxis().SetTitle(h_response_unf.GetYaxis().GetTitle())
    h_responce_transpose.GetYaxis().SetTitle(h_response_unf.GetXaxis().GetTitle())
    return h_responce_transpose
    
def SaveHistograms(outputname = "output"):
    outputname = outputname.split('/')
    outfile = TFile('./outputs/'+outputname[len(outputname)-1]+".root", 'recreate')
    outfile.cd()
    for j in range(len(histograms)):
        histograms[j].Write()
    print("End of the unfolding.")
    outfile.Write()
    outfile.Close() 

def PlotPosteriors(ListOfPosteriors, h, outputname = ""):
    RepeatIteration = False
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(math.ceil(pow(len(ListOfPosteriors),0.5)),math.ceil(pow(len(ListOfPosteriors),0.5)))
    legends = []
    for i in range(len(ListOfPosteriors)):
        ListOfPosteriors[i].SetMaximum(ListOfPosteriors[i].GetMaximum()*2.0)
        #ListOfPosteriors[i].Fit("gaus")
        #ListOfPosteriors[i].Draw()
        fit = ListOfPosteriors[i].GetFunction("gaus") 
        chi2 = fit.GetChisquare()
        p1 = fit.GetParameter(1)
        #p0 = fit.GetParameter(0)
        p2 = fit.GetParameter(2)
        FitIntegral = fit.Integral(p1-10*p2,p1+10*p2) # fit integral , get mean plus minus 10 sigma
        PriorIntegral = ListOfPosteriors[i].Integral("width")
        if FitIntegral != 0 :
            Percentage = round((100*PriorIntegral/FitIntegral),0)
        else: 
            Percentage = 0.0
        c.cd(i+1)
        gPad.SetFrameFillColor(10)
        if (Percentage < 90):                # 1.CONDITION of iteration, integral of histogram is at least 90% on fit integral
            gPad.SetFillColor(kRed-4)
            RepeatIteration = True
        else:
            gPad.SetFillColor(8)
        leg = TLegend(0.1,0.5,0.85,0.9)
        leg.SetFillStyle(0)
        leg.AddEntry(None,"MeanHist = "+str(round(ListOfPosteriors[i].GetMean(),0))+", RMShist = "+str(round(ListOfPosteriors[i].GetRMS(),0))+", MeanFit = "+str(round(p1,0))+", #sigma_{fit} = "+str(round(p2,0)),"")
        leg.AddEntry(ListOfPosteriors[i],"Posterior ","l")
        leg.AddEntry(fit,"Fit ","l")
        leg.AddEntry(None,"#chi^{2}/NDF = "+str(round(chi2/len(ListOfPosteriors),2)),"")
        leg.AddEntry(None,"Integral Prior/Fit = "+str(round(Percentage,0))+" %","")
        leg.SetBorderSize(0)
        legends.append(leg)
        ListOfPosteriors[i].SetTitle("Posterior in bin "+str(i+1))
        gStyle.SetOptStat(0)
        ListOfPosteriors[i].Draw()
        legends[i].Draw("same")
        if (round(chi2/len(ListOfPosteriors),2)) > 20.0: # 2.CONDITION of iteration, chi2/NDF < 20.0 
            RepeatIteration = True
        c.Update()

    c.cd(1)
    gStyle.SetOptStat(0)
    gPad.Modified()
    c.Update()
    #c2 = TCanvas("s"+outputname,"s"+outputname,0,0,1600, 1600)
    #c2.cd()
    #h.Draw()

    gApplication.Run()
    #PrintCan(c,c.GetName()+"_posteriors")
    return RepeatIteration

def MyRooUnfold(matrix_name=args.h_matrix, h_reco_getG0_name=args.h_data, h_ptcl_getG0_name = args.h_particle,h_reco_get_bkg_name = args.h_background,outputname=args.h_data+"_unfolded",nrebin = args.nrebin):

    rfile_data = TFile(args.rfile_data, 'read')
    rfile_particle = TFile(args.rfile_particle, 'read')
    rfile_matrix = TFile(args.rfile_matrix, 'read')
    rfile_background = TFile(args.rfile_background, 'read')

    #GET DATA
    h_reco_get = rfile_data.Get(h_reco_getG0_name)
    h_reco_get.Rebin(nrebin)
    #GET PARTICLE
    h_ptcl_get = rfile_particle.Get(h_ptcl_getG0_name)
    h_ptcl_get.Rebin(nrebin)
    #GET MATRIX
    h_response_unf = rfile_matrix.Get(matrix_name)
    h_response_unf.ClearUnderflowAndOverflow()
    h_response_unf.GetXaxis().SetRange(1, h_response_unf.GetXaxis().GetNbins() )
    h_response_unf.GetYaxis().SetRange(1, h_response_unf.GetYaxis().GetNbins() )
    h_response_unf.Rebin2D(nrebin,nrebin)
    h_response_unf.SetName("Migration_Matrix_simulation")

    ########### ACCEPTANCY
    h_acc = h_response_unf.ProjectionX("reco_recoandparticleX") # Reco M
    h_acc.Divide(h_reco_get)
    ########### AKCEPTANCE saved in h_acc #############
    ########### EFFICIENCY
    h_eff = h_response_unf.ProjectionY("reco_recoandparticleY") # Ptcl M
    h_eff.Divide(h_ptcl_get)
    
    h_reco_get_input = rfile_data.Get(h_reco_getG0_name)
    h_reco_get_bkg = rfile_background.Get(h_reco_get_bkg_name)
    h_reco_get_bkg.Rebin(nrebin)

    h_reco_get_input_clone=h_reco_get_input.Clone("")
    #h_reco_get_input_clone.Add(h_reco_get_bkg,-1)
    h_reco_get_input_clone.Multiply(h_acc)
    
   
    h_reco_or = rfile_data.Get(h_reco_getG0_name)
    h_ptcl_or = rfile_particle.Get(h_ptcl_getG0_name)
    h_ptcl_or.SetMaximum(h_ptcl_or.GetMaximum()*1.5)
    
    h_response_unf_fbu = TransposeMatrix(h_response_unf)
    h_response_unf_fbu_norm = NormalizeResponse(h_response_unf_fbu)
    h_response_unf_fbu_norm.SetName("Migration_Matrix_simulation_transpose")
    histograms.append(h_response_unf_fbu_norm)
    M = MakeListResponse(h_response_unf_fbu_norm)
    D = MakeListFromHisto(h_reco_get_input_clone) 
    T = MakeListFromHisto(h_ptcl_or)
    B = MakeListFromHisto(h_reco_get_bkg)
    print("Data: ",D)
    print("Truth: ",T)
    print("Background: ",B)
    print("Matrix: ",M)
    lower=[]
    upper=[]

    h_det_div_ptcl=h_reco_get_input_clone.Clone("")
    h_det_div_ptcl.Divide(h_ptcl_or)
    h_det_div_ptcl.Divide(h_eff)
    h_det_div_ptcl.SetName("det_div_ptcl")

    ListOfLikelihoods = []

    for l in range(len(D)):
        lower.append(h_reco_get_input_clone.GetBinContent(l+1)*(2-args.par)*h_det_div_ptcl.GetBinContent(l+1))
        upper.append(h_reco_get_input_clone.GetBinContent(l+1)*args.par*h_det_div_ptcl.GetBinContent(l+1)) ##need to think of

    ListOfPosteriors = []
    ListOfScaters = []
    ListOfProjections = []

    ListOfTrueValues = []
    ListofLists = []
    ListOfListsLikelihoods = []

    ListOfGraphs = []

    for k in range(len(D)):
        LikelihoodInBin = []
        ListOfListsLikelihoods.append(LikelihoodInBin)
        Empty = []
        ListOfTrueValues.append(Empty)
        posterior = TH1D("post_"+str(k),"post_"+str(k), 100,lower[k],upper[k])
        #ListOfPosteriors.append(posterior)
        #graph = TH2D("likelihood_"+str(k),"likelihood_"+str(k), int((upper[k]-lower[k])/100), int(lower[k]), int(upper[k]),)
        graph = TGraph()
        graph.SetName("graph_"+str(k))
        ListOfGraphs.append(graph)

    Binning = []

    for k in range(len(D)):
        Empty2 = []
        Binning.append(Empty2)
        for s in range(int(lower[k]),int(upper[k]),100):
            Binning[k].append(s)
        x = array("d",Binning[k])
        posteriors = TH1D("poster_"+str(k),"poster_"+str(k), len(Binning[k])-1,x)   
        ListOfPosteriors.append(posteriors)
        #print(x)

    for i in range(args.N):
        random.seed()
        if i % 100 == 0:
            print("Processing ",i)
        unfolded = h_ptcl_or.Clone("unfolded_"+str(i))
        unfolded.Reset()
        T_random = []
        for l in range(len(D)):
            T_random.append(random.randrange(int(lower[l]),int(upper[l])))

        for k in range(len(D)):
            j = 1
            for s in range(int(lower[k]),int(upper[k]),10):
                T_random[k] = s
                #print(T_random[k])
                LogLikelihood = 0.0
                for r in range(len(D)):
                    suma = 0.0
                    for t in range(len(D)):
                        suma = B[t] + T_random[t]*M[t][r] + suma 
                    #LogLikelihood = D[r]*math.log(suma) - ROOT.Math.lgamma(D[r]+1) - suma + LogLikelihood
                    LogLikelihood = -0.5*math.log(2*3.14*suma) - (D[r]-suma)*(D[r]-suma)/(2*suma)+ LogLikelihood
                    #print("L=D[s]*log(suma)-logGamma(D[s]+1)-suma: ",LogLikelihood, " = ", D[s], " * ", math.log(suma)," - ",ROOT.Math.lgamma(D[s]+1), " - ",suma)
                    #print("Loglikelihood :", numpy.exp(LogLikelihood))
                ListOfListsLikelihoods[k].append(LogLikelihood)
                ListOfTrueValues[k].append(T_random[k])
                ListOfPosteriors[k].SetBinContent(j,ListOfPosteriors[k].GetBinContent(j)+float(numpy.exp(10+LogLikelihood/1e5)))
                ListOfPosteriors[k].SetBinContent(j,ListOfPosteriors[k].GetBinContent(j)+numpy.exp(LogLikelihood))
                #print(j, T_random, numpy.exp(LogLikelihood))
                j = j + 1
                #ListOfGraphs[k].SetPoint(HelpMatrix[i][k][s],T_random[k],LogLikelihood)            

    for k in range(len(ListOfPosteriors)):
        ListOfPosteriors[k].Fit("gaus")
        histograms.append(ListOfPosteriors[k])
    histograms.append(h_ptcl_or)

    SaveHistograms("test")
    PlotPosteriors(ListOfPosteriors,"test")
    #ListOfPosteriors[0].Draw()
    #gApplication.Run()
histograms = []

MyRooUnfold()
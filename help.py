#!/usr/bin/env python

import argparse
from matplotlib import pyplot as plt
import fbu
from fbu import Regularization
import ROOT
import math
from array import array
from ROOT import RooUnfoldResponse
from ROOT import RooUnfold
from ROOT import RooUnfoldBayes
from ROOT import RooUnfoldSvd
from ROOT import RooUnfoldTUnfold
from ROOT import RooUnfoldIds
from ROOT import gRandom, TFile, TH1, TH1D, TH2, TH2D, cout
from ROOT import TGraph, TPad, gPad, TCanvas, TLegend, gStyle, gApplication, gStyle, kRed

parser = argparse.ArgumentParser(description="Unfolding code using RooUnfold and FBU package")
parser.add_argument('--par','-p', type=float, default=1.25)
parser.add_argument('--title', '-title', type=str, default="p_{T}^{t,had} [GeV]")
parser.add_argument('--nrebin', '-nrebin', type=int, default=1)

args = parser.parse_args()

def PrintCan(can, outputname):
    can.Print(outputname + '.png')

def PlotPosteriors(ListOfPosteriors, outputname = ""):
    if (outputname != "" ):
        outputname = "_"+outputname
    c = TCanvas("Posteriors"+outputname,"Posteriors"+outputname,0,0,1600, 1600)
    c.Divide(math.ceil(pow(len(ListOfPosteriors),0.5)),math.ceil(pow(len(ListOfPosteriors),0.5)))
    legends = []
    for i in range(len(ListOfPosteriors)):
        ListOfPosteriors[i].SetMaximum(ListOfPosteriors[i].GetMaximum()*2.0)
        fit = ListOfPosteriors[i].GetFunction("gaus") 
        chi2 = fit.GetChisquare()
        p1 = fit.GetParameter(1)
        #p0 = fit.GetParameter(0)
        p2 = fit.GetParameter(2)
        FitIntegral = fit.Integral(p1-10*p2,p1+10*p2) # fit integral , get mean plus minus 10 sigma
        PriorIntegral = ListOfPosteriors[i].Integral("width")
        Percentage = round((100*PriorIntegral/FitIntegral),0)
        c.cd(i+1)
        gPad.SetFrameFillColor(10)
        if (Percentage < 90):
            gPad.SetFillColor(kRed-4)
        else:
            gPad.SetFillColor(8)

        leg = TLegend(0.1,0.5,0.85,0.9)
        leg.SetFillStyle(0)
        leg.AddEntry(None,"MeanHist = "+str(round(ListOfPosteriors[i].GetMean(),0))+", RMShist = "+str(round(ListOfPosteriors[i].GetRMS(),0))+", MeanFit = "+str(round(p1,0))+", #sigma_{fit} = "+str(round(p2,0)),"")
        leg.AddEntry(ListOfPosteriors[i],"Posterior ","l")
        leg.AddEntry(fit,"Fit ","l")
        leg.AddEntry(None,"#chi^{2}/NDF = "+str(round(chi2/len(ListOfPosteriors),2)),"")
        leg.AddEntry(None,"Integral Prior/Fit = "+str(round(Percentage,0))+" %","")
        leg.SetBorderSize(0)
        legends.append(leg)
        ListOfPosteriors[i].SetTitle("Posterior in bin "+str(i+1))
        gStyle.SetOptStat(0)
        ListOfPosteriors[i].Draw()
        legends[i].Draw("same")
        c.Update()

    c.cd(1)
    gStyle.SetOptStat(0)
    gPad.Modified()
    c.Update()
    PrintCan(c,c.GetName())
    gApplication.Run()


histograms = []
rfile_input = TFile('out.root', 'read')
h1=rfile_input.Get("traceMigration_Matrix_simulation_transpose_0")
h2=rfile_input.Get("traceMigration_Matrix_simulation_transpose_1")
h3=rfile_input.Get("traceMigration_Matrix_simulation_transpose_2")
h4=rfile_input.Get("traceMigration_Matrix_simulation_transpose_3")
h5=rfile_input.Get("traceMigration_Matrix_simulation_transpose_4")
h6=rfile_input.Get("traceMigration_Matrix_simulation_transpose_5")
h7=rfile_input.Get("traceMigration_Matrix_simulation_transpose_6")
h8=rfile_input.Get("traceMigration_Matrix_simulation_transpose_7")
h9=rfile_input.Get("traceMigration_Matrix_simulation_transpose_8")
histograms.append(h1)
histograms.append(h2)
histograms.append(h3)
histograms.append(h4)
histograms.append(h5)
histograms.append(h6)
histograms.append(h7)
histograms.append(h8)
histograms.append(h9)
PlotPosteriors(histograms)

